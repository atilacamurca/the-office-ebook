# A Benihana Christmas

![](Abenihanachristmas.jpg "Abenihanachristmas.jpg"){width="150"}

**\"A Benihana Christmas\"** is the tenth/eleventh episode of the [third
season](Season_3 "wikilink") of \'\'[The
Office](The_Office_(US) "wikilink") \'\'and is the 38th/39th overall. It
was written by Jennifer Celotta and directed by [Harold
Ramis](Harold_Ramis "wikilink"), noted film comedy writer and director.
The episode first aired on December 14, 2006. It was viewed by 8.4
million people. It is a special hour-length episode.

Synopsis
--------

The Day of the Annual Christmas Party at [Dunder Mifflin
Scranton](Dunder_Mifflin_Scranton "wikilink").
[Dwight](Dwight_Schrute "wikilink") enters the office with a goose that
he accidentally struck with his car---which he proclaims to be a
Christmas miracle---and plans to prepare it for the party. After initial
objections to having a dead animal present,
[Toby](Toby_Flenderson "wikilink") allows Dwight to keep it as long as
he cleans it in his car.

When Michael arrives, we see [Pam](Pam_Beesly "wikilink") give him a
gift bag. He mistakenly thinks it\'s from Pam but she explains it\'s
from corporate headquarters.

[Carol](Carol_Stills "wikilink") receives her Christmas card from
[Michael](Michael_Scott "wikilink") and visits him to voice her
disapproval of his Photoshopping himself into a two-year-old ski weekend
photo with her children and former husband (Michael replaces her
husband\'s face with his own). This, along with his marriage proposal
(in the episode \"[Diwali](Diwali "wikilink")\"), pushes her over the
edge, and she breaks off the relationship, leaving Michael (who has
already booked a trip for the two of them to Sandals Resorts)
heartbroken.

Pam gives Jim her gift: she has been sending Dwight messages from the
CIA for several months (including a request of Dwight to admit all the
secrets he\'s sworn never to tell anyone) and she\'s going to let Jim
decide the top secret mission that Dwight will go on. At first, Jim is
clearly happy, but then turns down the gift, saying that he doesn\'t
want to keep doing the same things he did before now that he is \"Number
\#2\" man in the branch and he has a chance to start over. Pam is
obviously embarrassed and saddened that Jim turned down her gift to him.

Dwight realizes that the office is one bathrobe-from-corporate short,
and a depressed Michael \"solves\" the problem by telling Dwight to take
Toby\'s.

[Angela](Angela_Martin "wikilink") and the rest of the office\'s party
planning committee meet to discuss specifics about the day\'s
festivities. [Karen](Karen_Filippelli "wikilink") attempts to offer
several party suggestions, and although they get support from other
members, Angela immediately nixes all of her ideas. Karen is then asked
to leave by Angela for having stupid ideas and does so after receiving
no support from the other committee members.

Stating to the camera that she does not know why she has been so cold
with Karen, [Pam](Pam_Beesly "wikilink") apologizes for the events that
transpired during the party planning meeting and the two decide to
organize their own Margarita-Karaoke party. When they present this idea
to Angela, she declares them to be invalid and asks Dwight get involved
since he \"outranks\" Karen and Pam. Jim, being \"Number \#2\", creates
[The Committee To Determine The Validity Of The Two
Committees](The_Committee_To_Determine_The_Validity_Of_The_Two_Committees "wikilink")
on the spot (consisting only of himself) and declares Pam and Karen\'s
committee to be valid. While this goes on,
[Andy](Andy_Bernard "wikilink") decides to take Michael out to help him
forget about his troubles; Michael summons his \"entourage\"
[Jim](Jim_Halpert "wikilink"), Ryan and Dwight to come along, though
Ryan opts out with a long list of excuses he\'s prepared for such an
invitation from Michael.

Pam and Karen begin their party in the breakroom and Angela (after
calling Dwight for permission) is quick to start her own in the
conference room. After a few tense moments,
[Stanley](Stanley_Hudson "wikilink") stands up and goes to Pam and
Karen\'s party; several other people follow him except for
[Kevin](Kevin_Malone "wikilink"), [Phyllis](Phyllis_Lapin "wikilink"),
and [Hannah](Hannah_Smoterich-Barr "wikilink"). Kevin tells the camera
that he is mainly convinced to go because he has heard that Angela\'s
party will have double-fudge brownies, and that outweighs the fact that
Angela herself will be there. Michael, Andy, Jim, and Dwight arrive at a
Benihana restaurant (which Michael refers to as \"Asian Hooters\"). Andy
slides into a seat next to Michael and arranges the seating so Dwight is
seated on the opposite end of the table and out of earshot of the
conversation of the rest of the entourage. Andy convinces Michael to ask
out his Japanese waitress after getting him slightly drunk. So drunk, in
fact, that he actually steals meat from the plate of [Justin
Spitzer](Stephen_Saux "wikilink"), which causes a quick
chopstick-swordfight between them. Unable to hear these conversations,
Dwight asks Jim to fill him in, but instead Jim tricks him into telling
the waitress how to properly butcher a goose, which disgusts both the
waitress and the others at the table - while initially attempting to
explain himself, Jim finally smiles and admits that it was just the same
as all the other pranks he\'d pulled on Dwight. Meanwhile, the office is
divided into two separate camps as Phyllis, Hannah and Kevin attend
Angela\'s party while the rest of the staff are at Pam and Karen\'s.
Ryan makes the discovery at the party that they do not have a power cord
for their karaoke machine, but [Darryl](Darryl_Philbin "wikilink")
offers to get his synthesizer as an alternative. As he passes Phyllis to
get the music out of his car, she asks him how they are doing over there
and Darryl replies that they are having fun, encouraging Phyllis to join
their party in the break room \"when your meeting is over\". Through
Karen and Pam\'s party, Meredith is seen with a bottle of vodka in her
hand, turning down the margaritas because they are \"too sweet\".

Meanwhile, Angela\'s party proceeds to get more uncomfortable as Angela
makes a dig at Kevin\'s weight, irritating him enough to drive him to
the other party. When Michael, Andy, Jim, and Dwight return with two of
the waitresses from Benihana, Michael finds Angela\'s party to be
\"lame.\" Kevin uses this moment to escape. Angela is comforted by
Dwight\'s attendance at her party (even though he went to the other
party first) until Karen and Pam come in and inform Dwight that he\'s
won the raffle from their party. Dwight wins walkie-talkies, which he
uses later in the episode to communicate with Angela, using their pet
names for one another, \"monkey\" and \"possum\".

Pam notices how upset Angela is, and she and Karen make an offer to
Angela to merge the two parties. Angela agrees and reveals that she
stole the power cord for the karaoke machine. As everyone has fun
singing, Michael---who is very drunk---confuses his date with Andy\'s
since, as he states, \"all waitresses look alike.\" He goes into the
kitchen where both the waitresses are and he discreetly tries to ask
aloud where his one was and finally she acknowledges herself and he
discreetly marks her arm with a marker.

The two waitresses leave because they say the party \"blows\", and
Michael offers his girl a trip to Sandals Jamaica, where he was going to
take Carol. She says no since she has classes, although she does take a
bike that Michael had intended to give to the charity toy drive.

A depressed Michael sulks on the couch and Jim comes up and joins him.
Jim gets Michael to laugh at the whole situation and lets him realize
that what he had with the waitress was nothing more than a rebound. Jim
then tells Michael that a rebound is a fun distraction, but when it is
over, \"you\'re still thinking about the girl you\'re really after---the
one who broke your heart\" (echoing Jim\'s relationship with Karen after
his being rejected by Pam). The two sit in silence as they think.

As the Christmas party ends, Angela sings karaoke, Pam gives Toby her
robe from corporate that Dwight had taken, and Jim and Karen give each
other the same Christmas gift (a DVD copy of *Bridget Jones\'s: Edge of
Reason*), seeing which makes Pam sad. Later Pam receives a gift from
Roy, which Jim sees. Michael makes a phone call to an unknown person
(Who is later revealed to be Jan) to ask her to accompany him to
Sandals; to Michael\'s surprise, the person says yes.
[Oscar](Oscar_Martinez "wikilink") and Gil return from their three-month
vacation in Europe. Looking around, Oscar says \"Too soon\" and leaves.

While Jim is leaving, he decides to go ahead and take Pam up on her
\"gift\" offer from earlier. He tells Pam that Dwight will be summoned
by the CIA to meet with them. Pam quickly looks up how much a bus ticket
will cost, and when she mentions it will cost \$75, Jim tells her that
the CIA will be sending a helicopter instead. The next scene shows
Dwight on the roof waiting for a helicopter. He then receives a text
message reading, \"You have been compromised. Abort mission. Destroy
phone.\" Dwight throws his phone off the roof and walks away.

Deleted scenes
--------------

-   In a talking head interview, Michael incorrectly quotes \"[William
    Randolph](Wikipedia:William_Randolph_Hearst "wikilink")
    [Shakespeare](Wikipedia:William_Shakespeare "wikilink")\".
-   In a talking head interview, Angela angrily strings popcorn and
    expresses her outrage on behalf of baby Jesus. Baby Jesus wouldn\'t
    cancel Christmas because he got dumped or because he\'s two weeks
    behind on party planning. \"Baby Jesus would suck it up.\"
-   In a talking head interview, Karen expresses her surprise that
    Scranton requires an entire committee to plan parties.
-   Angela complains to Toby that \"that trollop\" is throwing her own
    party and demands that he put a stop to it. When Toby refuses,
    Angela slaps him. Toby decides to write up Angela for the
    infraction, and she sneers back, \"Michael was so right about you.
    You are pathetic.\"
-   In separate talking head interviews, Creed, Kevin, and Stanley
    discuss which party they will choose.
-   Angela scolds Phyllis for using the wrong term for her party treats.
-   Angela intimidates Phyllis into coming to her party.
-   At Benihana, Michael calls Carol. He asks her what he did wrong. We
    cannot hear Carol\'s response, but it is long and detailed. \"All
    right, well could you tell me something that I did right?\" Carol
    hangs up.
-   Ryan gives Kelly a Christmas gift. She coldly asks, \"Do you always
    get presents for your ex-girlfriend?\" Kelly explains that she
    won\'t tolerate Ryan\'s inconsiderate behavior any more, and Ryan
    accepts her decision. Kelly panics and apologizes tearfully.
    \"You\'re not mean; you\'re adorable!\" She bought Ryan a present
    but threw it out when she didn\'t think he was getting a present
    from her.
-   Kelly is in the dumpster outside looking for the gift she bought
    Ryan. Ryan, frustrated, shouts, \"Kelly, I am so cold!\" Kelly
    breaks down crying. Ryan tries to break up with her, but Kelly
    shouts back, \"Don\'t dump me while I\'m in the dumpster!\"
-   Ryan has joined Kelly in the dumpster, and Kelly begins wondering
    about the food scraps she finds. Kelly gasps. \"I\'m scared! Hold
    me.\"
-   Angela prevents Phyllis from leaving her party.
-   Michael introduces his date to Stanley.

Goofs and errors
----------------

-   Michael\'s \"new girlfriend\" has make up on when she walks into the
    office, but in the very next shot she is suddenly shown with no make
    up.
-   Cindy, Michael's "new girlfriend", and her friend are different
    actresses from the restaurant than shown in the office the very next
    scene.

Trivia
------

-   In the episode \"[The Alliance](The_Alliance "wikilink")\", Angela
    said that the color green was whorish. In this episode, she says
    that green is acceptable, but orange is whorish. In both cases,
    Angela\'s choice of whorish color matches Phyllis\' outfit.
-   In what some viewers believed to be an inside joke among the
    writers, the two Asian waitresses at Benihana are not the same two
    waitresses Michael and Andy bring back to the office. This \"gag\"
    was thought to play upon the racial stereotype that Asian people all
    look the same.
    -   According to the blog of the propmaster of the show, the reason
        for the different actresses was because Andy and Michael failed
        to pick up the original waitresses, and all they could get were
        \"less attractive ones\".[^1]
    -   This is supported by the visuals; The waitress Cindy talks to
        clearly has blonde highlights, but neither of the waitresses who
        accompany Michael and Andy back to the Office have blonde
        highlights.
    -   At Paley Fest 2007, Greg Daniels admitted that the joke failed
        due to poor casting. The waitresses that come to the office were
        too attractive.[^2]

<!-- -->

-   -   Closed Captioning on the episode contradicts the prettier/uglier
        waitresses story about switching actresses. As Michael\'s party
        date wheels the bicycle out of the party, the CC displays
        \"\[Cindy\] Merry Christmas.\" Cindy was the name of the
        original \"pretty\" waitress.

<!-- -->

-   According to the Diploma on Toby\'s wall he attended California
    Coastal College

<!-- -->

-   Staff writer Mindy Kaling gave fellow writer Michael Schur a poster
    for the movie *[Bridget Jones\'s Diary
    2](Wikipedia:Bridget_Jones:_The_Edge_of_Reason_(film) "wikilink")*
    which hung in his office for an entire year and which may have
    served as inspiration for Jim and Karen\'s DVD exchange.[^3]

<!-- -->

-   The presence of egg yolks in the dumpster is explained by Ryan as
    the result of Michael\'s egg yolk diet (first introduced in a
    deleted scene from \"[The Client](The_Client "wikilink")\").
    However, in that diet, Michael eats the yolks and discards the
    whites.

<!-- -->

-   The Photoshopped Christmas card prop was sold by NBC at auction as a
    charity fundraiser.
    [1](http://www.officetally.com/nbcs-the-office-auction-starts-jan-21)
-   In the talking head interview with Kevin in which he debates whether
    he wants to tolerate Angela so he can get double-fudge brownies,
    both Stanley and Karen can be seen at their desks through the window
    behind him. This is odd, as both Stanley and Karen went to Pam and
    Karen\'s party before the talking head interview with Kevin
    happened.

<!-- -->

-   The song [Creed](Creed_Bratton "wikilink") sings during karoake was
    a song he wrote after leaving his old band, the Grass Roots.
-   This is the second time that a group has chanted Darryl\'s name.
    (The first time was in \"Booze Cruise\".)

Cultural references
-------------------

-   *[Christmas](Wikipedia:Christmas "wikilink")* is a Christian holiday
    which has become largely secularized. Wrapped gifts are typically
    exchanged.
-   *And circle gets the square* is a catch phrase from the game show
    *[Hollywood Squares](Wikipedia:Hollywood_Squares "wikilink")*. It is
    said when the player playing \"circle\" answers a question
    correctly.
-   Telling someone to *take a chill pill* is a somewhat insulting way
    of telling them to relax because they are overreacting.
-   Michael makes a series of puns on Pam\'s name based on traditional
    Christmas foods: Christmas Pam (ham), candied Pams (yams), and Pam
    (lamb) chops.
-   In a *toy drive*, people donate toys for underprivileged children.
-   Andy greets Michael with a (ridiculous) exploding version of the
    \"fist bump\".
-   Michael sings \"[Two Tickets to
    Paradise](Wikipedia:Two_Tickets_to_Paradise "wikilink")\", by [Eddie
    Money](Wikipedia:Eddie_Money "wikilink"), with altered lyrics.
-   *Sandals* is a chain of Caribbean resorts which caters to couples.
-   Michael says to Carol, *You\'re such a blonde.* People with blonde
    hair (particularly women) are portrayed in jokes as stupid.
-   The message on the card *Ski-sons Greetings* is a pun on the
    traditional message \"Seasons Greetings\".
-   *[Photoshop](Wikipedia:Adobe_Photoshop "wikilink")* is a computer
    program used for manipulating digital images. Its use is so
    prevalent it has become a verb. To \"Photoshop\" an image is to
    alter it with the help of Photoshop.
-   The Central Intelligence Agency ([*CIA*](Wikipedia:CIA "wikilink"))
    is a United States government agency dedicated to foreign spying.
-   *Rudolph the Red-Nosed Reindeer* (or *Rudolph* for short) is an
    animated Christmas television program that has aired annually
    since 1972.
-   *[Hanukkah](Wikipedia:Hanukkah "wikilink")* is a Jewish holiday that
    typically falls near mid-December.
-   Dwight acknowledges Michael\'s instructions by saying *Copy*, radio
    jargon meaning that the message was received.
-   *[The Nutcracker](Wikipedia:The_Nutcracker "wikilink")* is a
    Christmas ballet by Tchaikovsky.
-   Michael repeatedly plays a sample from the song *Goodbye My Lover*,
    a 2004 song about a break-up.
-   Andy asks, \"What\'s the haps?\", extremely casual slang for
    \"What\'s happening?\"
-   *[Benihana](Wikipedia:Benihana "wikilink")* is chain of Japanese
    restaurants. *[Hooters](Wikipedia:Hooters "wikilink")* is a chain of
    American restaurants featuring buxom waitresses. In reality, there
    is no Hooters within convenient lunchtime driving distance of
    Scranton - despite Michael and Jim eating there in \"[The
    Secret](The_Secret "wikilink")\" and their catering the festivities
    of \"[Casino Night](Casino_Night "wikilink")\".
-   *[MSG](Wikipedia:monosodium_glutamate "wikilink")* (monosodium
    glutamate) is a flavor enhancer associated with Asian food.
-   *Bros before hos* is rude slang which means \"Men should prioritize
    their male friends ahead of their girlfriends.\" The term \"ho\" is
    rude slang for \"hooker\" (prostitute), applied more generally to
    women.
-   *Got latte?* is a pun on \"Got milk?\" the motto of the milk
    marketing board campaign to get people to drink more milk. The ads
    feature people with a film of milk on their upper lip (a \"milk
    mustache\").
-   *S.O.S.* and *Mayday* are radio distress calls. *Man down* is a
    military term indicate that a soldier is injured, lying on the
    ground. Andy uses it figuratively to indicate that Michael is in
    distress.
-   The *Nog-a-sake* is an invented drink, a pun on the Japanese city of
    [Nagasaki](Wikipedia:Nagasaki "wikilink").
-   Phyllis suggests putting out \"salt for the rims.\" Salted rims are
    a traditional element of the margarita drink.
-   *Nakiri* and *usuba* are Japanese knives. The nakiri is intended for
    home use; the usuba is more difficult to use and is intended for
    professionals. (Dwight is wrong to suggest that the chef should use
    a nakiri.)
-   In *family style* dining, all dishes are shared. This contrasts with
    \"service à la russe\" (common in most American restaurants) wherein
    each diner orders their own dish and is not obliged to share it.
-   *[Hello Kitty](Wikipedia:Hello_Kitty "wikilink")* is a Japanese
    cartoon character whose image has been used on almost every
    imaginable product.
-   Michael says, *You know how all waitresses look alike*, catching
    himself from saying that all Oriental people look alike, a
    stereotypical remark.
-   *[Stevie Wonder](Wikipedia:Stevie_Wonder "wikilink")* is a singer
    who is blind.
-   Dwight asks Angela, *Do you copy?*, radio jargon for \"Did you
    receive my message?\" (Dwight uses the same jargon earlier in the
    episode when talking to Michael.) Angela asks, *What\'s your
    twenty?* [CB slang](Wikipedia:CB_slang "wikilink") for \"What is
    your location?\"
-   Dwight sings the *Styx* song,
    [Lady](https://en.wikipedia.org/wiki/Lady_(Styx_song)).
-   Jim tells Michael that he *just had a rebound*. A \"rebound
    relationship\" is one formed too quickly after a long-term
    relationship has broken up. Michael reinterprets the term literally,
    comparing it to a rebound in the sport of basketball.
-   *Domo arigato, Mr. Scott-o* is a pun on the chorus of the song
    \"[Mr. Roboto](Wikipedia:Mr._Roboto "wikilink").\" The actual lyrics
    are \"Domo arigato, Mr. Roboto.\"
-   [*Langley*, Virginia](Wikipedia:Langley,_Virginia "wikilink"), just
    outside of Washington, D.C., is the location of CIA headquarters.
-   The karaoke song Kevin sings is \"[You Oughta
    Know](Wikipedia:You_Oughta_Know "wikilink")\", a popular song from
    the 90s by musician [Alanis
    Morissette](Wikipedia:Alanis_Morissette "wikilink").

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting cast

-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring cast

-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")
-   [Rashida Jones](Rashida_Jones "wikilink") as [Karen
    Filippelli](Karen_Filippelli "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Nancy Walls](Nancy_Walls "wikilink") as [Carol
    Stills](Carol_Stills "wikilink")
-   [Ursula Burton](Ursula_Burton "wikilink") as [Hannah
    Smoterich-Barr](Hannah_Smoterich-Barr "wikilink")
-   [Tom W. Chick](Tom_W._Chick "wikilink") as [Gil](Gil "wikilink")

### Guest cast

-   [Brittany Ishibashi](Brittany_Ishibashi "wikilink") as
    [Cindy](The_Benihana_Waitresses "wikilink")
-   [Kathrien Ahn](Kathrien_Ahn "wikilink") as
    [Amy](The_Benihana_Waitresses "wikilink")
-   [Kulap Vilaysack](Kulap_Vilaysack "wikilink") as
    [Nikki](The_Benihana_Waitresses "wikilink")
-   [Stephen Saux](Stephen_Saux "wikilink") as [Justin
    Spitzer](Justin_Spitzer_(Character) "wikilink")
-   Chatree \"Chad\" Yodvisitsak as Benihana Chef
-   Annie Sertich as woman at Hibachi Table

References
----------

<references/>
\

External links
--------------

[Eddie
Money](http://favoritealbums.wikia.com/wiki/The_Covers_EP:_Volume_One_(Eddie_Money_album))
on [wikia](http://favoritealbums.wikia.com)

[^1]: See
    blog.myspace.com/index.cfm?fuseaction=blog.view&friendID=102409815&blogID=205783475
    (copy and paste URL)

[^2]: [Paley Fest: The
    Office](http://tv.ign.com/articles/770/770400p3.html)

[^3]: Kaling, Mindy. [Le French Sex
    Shoppe](http://mindyephron.blogspot.com/2007/01/le-french-sex-shoppe.html)
    blog entry.
