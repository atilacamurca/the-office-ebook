{{Office episode
|Title      =Diwali
|Image      =[[Image:Diwali.jpg|250px]]
|Season     =[[Season 3|3]]
|Episode    =6
|Code       =3004
|Original   =November 2, 2006
|Writer(s)  =[[Mindy Kaling]]
|Director   =[[Miguel Arteta]]
|Prev       =[[Initiation]]
|Next       =[[Branch Closing]]
}}
'''"Diwali"''' is the sixth episode of the third season of ''[[The Office (US)|The Office]] ''and the 34th overall. It was written by [[Mindy Kaling]] and directed by [[Miguel Arteta]]. It first aired on November 2, 2006. It was viewed by 8.8 million people.

==Synopsis==
[[Kelly Kapoor|Kelly]] invites the entire staff to a celebration of [[Wikipedia:Diwali|Diwali]], the Hindu Festival of Lights. [[Michael Scott|Michael]] serves as the vocal supporter of such an event and believes it to be an Indian version of Halloween.

A staff meeting is scheduled to enlighten staff members about Hindu culture. This is met with disgusted replies by [[Angela Martin|Angela]], excitement by [[Kevin Malone|Kevin]] (particularly after seeing illustrations in a [[Wikipedia:kama sutra|kama sutra]] booklet distributed by Michael) and an uncomfortable [[Toby Flenderson|Toby]], who decides to end the meeting and takes away the booklets.

Back in Stamford, [[Jim Halpert|Jim]] has decided to follow [[Josh Porter|Josh]]'s example and ride his bike to work. Meanwhile, [[Andy Bernard|Andy]] pulls out a bottle of [[Wikipedia:Jagermeister|Jägermeister]] and shot glasses to turn a late night of work into a drinking game, which [[Karen Filippelli|Karen]] wins by pouring her shots into her [[Wikipedia:Waste container|wastebasket]] while the two men continue to down shots.

Michael and [[Carol Stills|Carol]] arrive at the Diwali festival in costume, since Michael thought the celebration would be a "costume party." Carol is a cheerleader and Michael is wearing his [[Wikipedia:papier-mâché|papier-mâché]] twin from [[Halloween]] 2005, which he quickly removes.

Ryan, dressed in a [[Wikipedia:Kurta|kurta]] (traditional men's Indian attire), tries to acquaint himself with Kelly's family, which is met with giggling by Kelly's younger sisters, whom Ryan believes compared his looks to [[Wikipedia:Zach Braff|Zach Braff]] in their language (they say "Kelly likes Zach Braff... " in [[Wikipedia:Hindi|Hindi]]). and disapproval by Kelly's parents, who want to set her up with an Indian doctor. This only gets worse when Ryan speaks with her parents about his promotion at Dunder Mifflin and his plans to save money for travel and an [[Wikipedia:Xbox|Xbox]]. Dwight gets into the spirit of the festival and dons a red Kurta. Even Michael is shown dancing happily to the music.

Initially reluctant to attend the festival, [[Pam Beesly|Pam]] decides to go and actually enjoys herself as she starts dancing along with the staff and the rest of the festivalgoers. A cleaned up [[Roy Anderson|Roy]] shows up in time to find Pam in the middle of the dancing crowd, with another man too, making him uncomfortable, and he silently exits the party.

Michael has an enlightening conversation with Kelly's parents about Hindu marriage customs and suddenly interrupts the celebration to publicly propose to Carol. Uncomfortable, Carol declines his offer and leaves the room. Michael follows her out to the car, where they briefly talk; Carol citing that this is only their ninth date, while Michael states how much he likes her. Carol drives home, leaving Michael to find a ride.

Meanwhile, Pam is surprised to find herself inspired by Michael's outgoing, romantic nature and she sends a [[Wikipedia:Short message service|text message]] to Jim. However, Jim is passed out on his desk and is unaware of the incoming message on his [[Wikipedia:cell phone|cell phone]]. As the Diwali festival winds down, a dejected Michael makes an attempt to kiss a disappointed Pam, who stops him, and reluctantly agrees to drive him home, as long as he sits in the back seat. On the drive, he mentions the shoes he's wearing aren't his own.

Back in Stamford, Karen leaves for the night, as Jim continues to lie face down on his desk and Andy is laid out on the floor, both completely inebriated. Jim asks Andy if he can get a ride but Andy says "No way, dude," and begins to unfurl an inflatable mattress. He offers to share the "roomy twin" with Jim. Jim decides to ride his bike home but barely exits the front door when he crashes sideways into the bushes. An amused Karen laughs and offers Jim a ride home. He accepts immediately and crawls into her backseat leaving her to deal with his bicycle.

Sometime during the party Michael took the stage with Dwight behind him on guitar. Beginning by saying "this is going out to Indians everywhere," he began a song as a tribute to "one of the greats" Mr. Adam Sandler. The lyrics are: "Diwali is a festival of lights, let me tell you something, tonight has been.. one crazy night, so put on your saris, it's time to celebrate Diwali, everybody looks so jolly, but it's not Christmas, it's Diwali, the goddess of destruction Kali, stopped by to celebrate Diwali, don't invite any zombies, to our celebration of Diwali, along came Polly, to have some fun at Diwali, if your Indian and you love to party, have a happy, happy, happy, happy Diwali." He then wished everybody a Happy Diwali and the crowd cheered.

==Deleted scenes==
*In a talking head interview, Pam explains that she understands that she should get out more, but is not convinced that a Diwali party is the right venue.
*Angela doesn't understand why the office is going to a party for a holiday that doesn't even exist.
*In a talking head interview, Angela explains that some people in the office are too obsessed with things Indian. The documentary filmmaker underscores this point with the moment from Christmas Party where Kelly kisses Dwight.
*In a talking head interview, Michael explains that as the boss, he must support all cultures, and that certain people have been offended, and Diwali provides an opportunity to make it up to them. The documentary filmmaker underscores this point with the moment from Diversity Day where Kelly slaps Michael.
*Michael tires of Kelly's description of Diwali and demands an explanation in three words or less. "An Indian [[Wikipedia:Halloween|Halloween]]." Jan is upset that Michael is letting everyone off work early to go to a party, and Michael acts shocked that Jan doesn't know what Diwali is - after Michael explains its cultural significance (surprising Jan, who had assumed Kelly was Muslim - to which Kelly, listening on the speakerphone, takes great offense), Jan has no problem allowing it, and castigates Michael for not mentioning it earlier, so the company could've contributed to the event in some form.
*In a talking head interview, Michael answers the question "Are you excited about the Diwali party?" by telling a rambling story about his original plans with Carol for a romantic evening.
*Dwight asks Ryan how much his kurta cost.
*Michael asks Pam to notify the office about his Diwali meeting and gets upset when Pam suggests that she isn't going to the party.
*At Michael's conference, he identifies Sir [[Wikipedia:Ben Kingsley|Ben Kingsley]] as responsible for the departure of the British from India. "And then he became an actor, like [[Wikipedia:Ronald Reagan|Ronald Reagan]]." Pam points out to Michael that his shirt is buttoned wrong. In a talking head interview Pam is frustrated that she can't share the moment with Jim.
*Dwight asks Pam if she's afraid Roy will attend the Diwali party with a new girlfriend, or maybe Roy will find a hot Indian girl, or maybe Dwight will. Angela overhears this remark.
*Andy tells the documentary camera that his fraternity name was "[[Wikipedia:Hubble Space Telscope|Hubble]]" for his ability to spot a party. It was better than his freshman nickname, when he had a skin problem, when his nickname was "[[Wikipedia:El Guapo|El Guapo]]".
*Phyllis's curiosity about the food at the Diwali party annoys Stanley.
*In a talking head interview, Angela explains that as the head of the Party Planning Committee, she is obligated to attend all office parties, even one held in Hell.
*When the Stamford staff order sushi for dinner, Andy offers "Big Tuna" Jim the tuna, but Jim asks for the eel. Andy is puzzled.
*Kelly is overjoyed when Pam arrives and takes her to meet some cute guys.
*Carol introduces herself to Ryan, who is uncomfortable that she has learned so much about him from Michael.
*Pam talks with the young Indian doctor, who invites her to dance. Meanwhile, Dwight engages in a lightsaber battle with a young boy.
*Kevin mocks Michael after the failed proposal.
*In a talking head interview, Ryan says that he was at the Diwali party just for the free food. He takes off his kurta and can't find his shoes.
*Dwight stops Angela as she leaves the Diwali party. Angela is quietly upset at Dwight, who reassures his "monkey" that his interest in India is restricted to its culture. He then loudly announces that it is late and asks Angela if he can stop by Angela's house for some Pepto-Bismol. Angela smiles to herself, then spots Kelly and Ryan and scolds them for making out in public.
*Kevin appeals to Kelly's parents to hire his band for Diwali 2007.
*Michael loses at cards to an old Indian woman.

==Trivia==
*[[Kelly's Family|Kelly's parents]] were played by Mindy Kaling's real parents.
*The dances in the episode were choreographed by Bollywood dance instructor Nakul Dev Mahajan and performed by his dance troupe, who struggled to dance more amateurishly in order to be more realistic to the story.[http://boards.nbc.com/nbc/index.php?showtopic=674315&st=60]
*In the deleted scene on the DVD where Angela talks about Diwali not existing, the subtitles are wrong. Pam said "The Hallmarks in India might" whereas the subtitles say "The Hallmarks in India mate".
*When Michael was in Pam's car, he says, "these are not my shoes". In a deleted scene Ryan asks, "where are my shoes"? This would imply that Michael took Ryan's shoes.
*According to dialogue between Michael and Carol, Diwali is their ninth date , but in the episode [http://theoffice.wikia.com/wiki/Sex_Ed Sex Ed], Carol claims Michael proposed to her on their fourth date.
*The building Jim comes out of looks nothing like the building that is shown when the scene switches to Stamford.

==Cultural references==
* ''[[Wikipedia:Diwali|Diwali]]'' is the Hindu Festival of Lights. Dwight's description is accurate. (Michael's is not.) A ''[[Wikipedia:kurta|kurta]]'' is a traditional garment worn in India.
* A ''[[Wikipedia:GMC Yukon|Yukon]]'' is a model of light truck.
* An ''[[Wikipedia:appletini|appletini]]'' is a [[Wikipedia:martini|martini]] that substitutes apple juice (or similar) for vermouth.
* ''[[Wikipedia:Sex and the City|Sex and the City]]'' is a television program about four women living in New York City and their struggles with sex and romance.
* The movie ''[[Wikipedia:Indiana Jones and the Temple of Doom|Indiana Jones and the Temple of Doom]]'' (inaccurately) depicts monkey brains served at an Indian meal.
* ''[[Wikipedia:Kwanzaa|Kwanzaa]]'' is a holiday celebrating African-American heritage.
* ''[[Wikipedia:The Lord of the Rings|The Lord of the Rings]]'' is an epic fantasy novel made by J.R.R. Tolkien.
* ''[[Wikipedia:Subrahmanyan Chandrasekhar|Subrahmanyan Chandrasekhar]]'' is a Nobel prize-winning physicist. ''[[Wikipedia:Apu Nahasapeemapetilon|Apu]]'' is a caricature Indian character from the cartoon ''[[Wikipedia:The Simpsons|The Simpsons]]''. ''[[Wikipedia:M. Night Shyamalan|M. Night Shyamalan]]'' is an award-winning film writer and director; he was born in India but grew up in the United States. ''[[Wikipedia:The Village (film)|The Village]]'', ''[[Wikipedia:Unbreakable (film)|Unbreakable]]'', ''[[Wikipedia:The Sixth Sense|The Sixth Sense]]'', and ''[[Wikipedia:Signs (film)|Signs]]'' are all Shyamalan movies. ''I see dead people'' is a line from the movie ''The Sixth Sense'', and Dwight reveals the twist ending to the movie.
* An important element of the television comedy ''[[Wikipedia:Cheers|Cheers]]'' is the sexual tension between the two lead characters, ''[[Wikipedia:Sam Malone|Sam]]'' and ''[[Wikipedia:Diane Chambers|Diane]]''.
* Michael describes his visual aids as ''culturally explicit'', a reworking of the euphemism ''[[Wikipedia:Sexually explicit material|sexually explicit]]''.
* Michael confuses ''[[Wikipedia:Samosa|samosas]]'' (a traditional Indian food similar to a fried dumpling) with ''[[Wikipedia:S'more|s'mores]]'' (a traditional American campfire food consisting of graham crackers, chocolate, and marshmallow).
* Michael asks Kelly's mother if "she has to throw herself on a fire" when her husband dies, referring to the ancient custom of [[Wikipedia:Sati (practice)|sati]].
* ''[[Wikipedia:Gringo|Gringo]]'' is a Latin American (not Indian) term for foreigners. Within the Hispanic community in the United States, it is often applied pejoratively to Caucasian Americans.
* An ''[[Wikipedia:Xbox|Xbox]]'' is a video game console by Microsoft.
* ''[[Wikipedia:Taxicab Confessions|Taxicab Confessions]]'' is a documentary series which surreptitiously records candid conversations with taxi passengers. At the end of the ride, the cameras are revealed and the passengers are asked to grant permission to use the footage in the documentary.
* ''[[Wikipedia:Adam Sandler|Adam Sandler]]'' is a comedian best known for his work on ''[[Wikipedia:Saturday Night Live|Saturday Night Live]]''. Michael's Diwali song is in the style of Sandler's popular ''[[Wikipedia:The Chanukah Song|Chanukah Song]]'' series.
* ''[[Wikipedia:Loratadine|Claritin]]'' is a brand of antihistamine. A ''[[Wikipedia:flunitrazepam|roofie]]'' (slang for the drug [[Wikipedia:flunitrazepam|flunitrazepam]]) is a "date rape" drug; it has a sedative and hypnotic effect and also results in amnesia.
* Andy and Jim sing the song "Closer to Fine" by the [[Wikipedia:Indigo Girls|Indigo Girls]].
* [[Wikipedia:Beyonce|Beyonce]]'s hit song "Crazy In Love" was heard at the Diwali festival.

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting Cast===
*[[David Denman]] as [[Roy Anderson]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Phyllis Smith]] as [[Phyllis Lapin]]

===Recurring Cast===
*[[Ed Helms]] as [[Andy Bernard]]
*[[Rashida Jones]] as [[Karen Filippelli]]
*[[Charles Esten]] as [[Josh Porter]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Nancy Walls]] as [[Carol Stills]]
*[[Mike Bruner]] as [[Tony Gardner]]

===Guest Cast===
*[[Tanveer Atwal]] as [[Dunder Mifflin Family Members and Loved Ones#Kelly's Sisters|Rupa]] (Kelly's Sister)
*[[Ananya Kepper]] as [[Dunder Mifflin Family Members and Loved Ones#Kelly's Sisters|Tiffany]] (Kelly's Sister)
*[[Avu Chokalingam]] as [[Dunder Mifflin Family Members and Loved Ones#Kelly's Parents|Kelly's Dad]]
*[[Swati Chokalingam]] as [[Dunder Mifflin Family Members and Loved Ones#Kelly's Parents|Kelly's Mom]]
*[[Jaysha Patel]] as [[Dunder Mifflin Family Members and Loved Ones#Kelly's Sisters|Neepa]] (Kelly's Sister)
*Kunal Sharma as Food Server/Teenager #1
*Sunah Bilsted as Waitress
*Nakul Dev Mahajan as Dancer/Choreographer
*Varun Gurunath as Dancer
*Siddharth Jain as Dancer
*Anita Kapila as Dancer
*Sangita Sanyal as Dancer
*Karishma Sawhney as Dancer
*Rita Sehmi as Dancer
*Rohan Vora as Dancer
*Gagandeep Bedi
*Vali Chandrasekaran
<br />

{{Season3}}
