# Product Recall

![](ProductRecall.jpg "ProductRecall.jpg"){width="150"}

**\"Product Recall\"** is the twentieth episode of the third season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 49th overall.
It was written by Brent Forrester and [Justin
Spitzer](Justin_Spitzer "wikilink") and directed by [Randall
Einhorn](Randall_Einhorn "wikilink"). It first aired on April 26, 2007.
It was viewed by 7.6 million people.

Synopsis
--------

[Jim](Jim_Halpert "wikilink") [Halpert](Jim_Halpert "wikilink") enters,
[dressed as](List_of_Jim's_Pranks "wikilink") [Dwight
](Dwight_Schrute "wikilink")[Schrute](Dwight_Schrute "wikilink"). He
explains that he accidentally found Dwight\'s glasses at a drug store for four
dollars, and was unable to resist recreating the rest of Dwight\'s
ensemble for a grand total of eleven dollars. Jim imitates Dwight\'s
mannerisms and tone: \"Fact. Bears eat beets. Bears. Beets. Battlestar
Galactica\" and Dwight starts to get upset: \"Identity theft is not a
joke, Jim! Millions of families suffer every year!\" Jim screams
\"Michael!\" and ran to his office. Dwight, very annoyed, did the exact
same thing.

![The watermark.](Product_recall.jpg "fig:The watermark.")

[Dunder Mifflin Scranton](Dunder_Mifflin_Scranton "wikilink") is thrown
into damage control when some paper is released with an obscene
watermark of a cartoon duck and mouse having [sex](sex "wikilink").
[Michael](Michael_Scott "wikilink") addresses the media while Jim and
[Andy](Andy_Bernard "wikilink") try to calm a school principal who used
the affected paper to send out prom invitations. After arriving at the
high school, Andy notices that his \"girlfriend\" is a student, and this
ruins his day.

Meanwhile, [Kelly](Kelly_Kapoor "wikilink") is entrusted with training
the accountants, [Oscar](Oscar_Martinez "wikilink"),
[Kevin](Kevin_Malone "wikilink"), and
[Angela](Angela_Martin "wikilink"), to handle customer support calls.
Angela is unable to sincerely apologize to customers for the company\'s
mistake, while Oscar seems to handle the calls well and Kevin simply
repeats his apology over and over again.

Michael arranges for a customer to come to the office, so that he may
apologize to her personally and present her with a novelty check for
free paper. With the media present (a single reporter from the *Scranton
Times*), Michael apologizes to the customer. However, she refuses his
apology and asks for his resignation. Dwight attempts to defuse the
situation by insisting that the cartoon sex appears consensual. Michael
refuses to resign and angrily decides to give the check to someone else.
He then decides to make an \"apology video,\" filmed by
[Pam](Pam_Beesly "wikilink"), in which he reiterates the fact that the
mistake was not his fault and that he has become an \"[escape
goat](Wikipedia:scape_goat "wikilink").\"

Meanwhile, [Creed](Creed_Bratton "wikilink"), who as Quality Assurance
failed to catch the error, maneuvers to ensure that he saves his own job
(at the expense of an employee at the paper mill). He gets a farewell
card for her and money from all the employees. On his way out, Creed
pockets the money and tosses the card.

Later, in the car, Jim cajoles Andy into singing \"[The Lion Sleeps
Tonight](Wikipedia:The_Lion_Sleeps_Tonight "wikilink")\" to cheer him
up.  In the closing, Dwight comes to work dressed as Jim and imitates
him very poorly.

Deleted scenes
--------------

-   Ryan is forced to listen to Kelly repeat the same apology over and
    over again.
-   Michael compares this crisis to an incident in which \"an aspirin
    company\" [sold bottles with poison in
    it](Wikipedia:Tylenol_Crisis_of_1982 "wikilink").
-   Dwight calls CNN and tries to reach the Scranton bureau chief, a
    position which does not exist.
-   Alternate takes of Kelly\'s training session.
-   The *Scranton Times* reporter recognizes Creed, whose obituary he
    wrote, at which point Creed thanks him for what he considered a
    flattering portrayal. In a talking head interview, Creed explains
    that he faked his death ten years ago and is collecting life
    insurance benefits as his own widow.
-   Andy reels from the realization that his girlfriend is a high school
    student.
-   Andy describes how he and Jamie met. Jim, mindful of the cameras,
    tries to rephrase Andy\'s story in less incriminating terms but
    eventually gives up.
-   Andy barges into Jamie\'s Spanish class.
-   In a talking head interview, Michael compares dissatisfied clients
    to women.
-   Additional footage from the press conference.
-   In a talking head interview, Dwight explains that he doesn\'t
    believe in apologies.
-   Angela\'s refusal to apologize to customers leads to a fight with
    Kelly.
-   In a talking head interview, Kevin giggles uncontrollably at the
    thought of cartoon characters having sex.
-   In a talking head interview, Michael explains that the customer is
    always right, but Ms. Allen was wrong.
-   In a talking head interview, Pam discusses Michael\'s previous
    apology videos.
-   Jim unsuccessfully tries to dissuade Andy from talking to Jamie in
    the courtyard. Jim tries making small talk with Jamie\'s friend.
-   Creed successfully rebuts Debbie Brown\'s claim that she is being
    framed.
-   Michael prepares for his apology video.
-   Kelly and Angela reconcile, and Kelly excitedly declares them best
    friends. In separate talking head interviews, Kelly expresses
    satisfaction for having softened Angela\'s personality, and Angela
    says that she learned something from Kelly today, but her look to
    the camera shows that she was speaking sarcastically.
-   Additional footage from Michael\'s apology video.
-   Dwight announces that Debbie Brown has been fired and thanks Creed
    for his effort in tracking down the problem.

References to previous episodes
-------------------------------

-   Michael calls the situation \"Threat Level: Midnight\", which is the
    name of his screenplay in *[The Client](The_Client "wikilink")*.
-   Creed tells Dwight that Debbie Brown told him she had an emergency
    dentist appointment. In the episode *[The
    Fight](The_Fight "wikilink")* Michael talks about friends who come
    to work late \"having dentist appointments that aren\'t dentist
    appointments.\" The following year, in the episode *[The
    Coup](The_Coup "wikilink")*, Dwight tells Michael he has a dentist
    appointment so he can secretly meet with Jan.

Michael\'s cue cards
--------------------

-   Card 1

:   Hello, I am Michael Scott, Regional Manager of Dunder Mifflin,
    Scranton. By now you\'re probably sick of hearing about Dunder
    Mifflin and our embarrassing watermark boner.

-   Card 2

:   I have literally apologized an infinite number of times over this,
    and still there are calls for me to resign, calls from an annoying
    woman and possibly even the media alike. Well, let me tell you
    something. Something from the heart. I will not resign. I am not

-   Card 3

:   leaving this office. It will take a SWAT team to remove me from this
    office, and maybe not even then. There is no way I will resign. It
    wouldn\'t be fair. Not to the good workers I work with. Not to my
    clients. And especially not to me. Let\'s not forget who this
    resigning business is really all about.

-   Card 4 (seen on screen but not read)

:   I need this job. My mortgage is hundreds of dollars a month. With
    this job I can barely cover that. I have a company car, but I still
    have to pay for gas. Gas prices are high and I have no savings
    whatsoever. And it wasn\'t even me. It\'s so not fair that they want
    me to resign.

-   Card 5 (only partly visible, text implied by Michael\'s reading)

:   If I could leave you with one thought, remember, it wasn't me. They
    are trying to make me an escape goat. If I am fired, I swear to god,
    that every single piece of copier paper in this town is going to
    have the F-word on it. The F-word. You have one day.

Trivia
------

-   In the opening scene depicting everyone dealing with disgruntled
    customers the camera views numerous employees (Pam, Jim, Andy,
    Phyllis, Stanley and even Michael) on the phone busily talking to
    customers or someone else. Creed is then seen on the phone not
    talking and shiftily looking around the office, indicating that he
    was not really on the phone and only pretending to appear busy like
    everyone else (this despite the fact he was responsible for the
    entire crisis).
-   The writing staff appear to have an odd relationship with the teen
    television show *[The
    Hills](Wikipedia:The_Hills_(TV_series) "wikilink")*. In a deleted
    scene, Jim tries to make small talk by asking about the character
    [Heidi](Wikipedia:Heidi_Montag "wikilink"). At Comic-Con 2008,
    during a writing panel session, the writers compared Jim and Pam to
    other television couples, and [B.J. Novak](B.J._Novak "wikilink")
    jokingly compares them to
    [Spencer](Wikipedia:Spencer_Pratt "wikilink") and
    [Heidi](Wikipedia:Heidi_Montag "wikilink").
-   In the cold open, Jim says he spent 4 dollars on glasses and 7
    dollars on the rest of the outfit. Later in the episode, Angela
    tells Kevin that he didn't know 4 + 7 on a payroll form. That money
    could have been from Jim's spending.

Cultural references
-------------------

-   A part of Jim\'s Dwight costume is a [calculator
    watch](Wikipedia:calculator_watch "wikilink"). They were introduced
    in the mid-1970s and quickly became associated with nerds.
-   *Stat* (short for *statim* =immediately) is medical jargon. Appended
    to a command, it means that the action should be performed
    immediately.
-   The *No Spin Zone* is a segment of [Bill
    O\'Reilly](Wikipedia:Bill_O'Reilly_(commentator) "wikilink")\'s
    political television program *The O\'Reilly Factor* which purports
    to cover the issues of the day without spin (bias).
-   *O.J.* is a former NFL football player. *[O. J.
    Simpson](Wikipedia:O._J._Simpson "wikilink")* and his murder trial
    captured the nation\'s attention during 1994 and 1995.
-   *[Bridget Jones](Wikipedia:Bridget_Jones "wikilink")* is the
    protagonist in a series of books by [Helen
    Fielding](Wikipedia:Helen_Fielding "wikilink"). When the story
    begins, she is an unhappy single woman struggling to find love. *Ice
    cream* is a food stereotypically consumed by women in such
    situations.
-   *Dingo babies* is a corruption of the [Azaria Chamberlain
    disappearance](Wikipedia:Azaria_Chamberlain_disappearance "wikilink")
    in which dingoes in Australia were reported to have taken a baby.
-   Michael announces that the press conference will be *in 45*. This is
    shorthand for *in 45 minutes*.
-   The *Washington Post* is a major national newspaper published in
    Washington, DC.
-   The *Scranton Times-Tribune* is a local Scranton newspaper.
-   *Dickson City* and *Carbondale* are suburbs of Scranton.
-   *Tourette\'s* is slang for *[Tourette
    Syndrome](Wikipedia:Tourette_Syndrome "wikilink")*, a medical
    disorder characterized by motor tics and verbal grunting. In popular
    culture, it is depicted as a disorder in which victims swear
    uncontrollably; in reality, the shouting of obscenities is caused by
    a disorder called
    *[coprolalia](https://en.wikipedia.org/wiki/Coprolalia)*, which is
    present only in a small minority of people with tourette\'s.
-   The *[Better Business
    Bureau](Wikipedia:Better_Business_Bureau "wikilink")* is an
    organization which helps to resolve disputes between consumers and
    its member companies.
-   *[Newsweek](Wikipedia:Newsweek "wikilink")* is a national weekly
    news magazine. *[CNN](Wikipedia:CNN "wikilink")* is an international
    cable television news network.
-   A *[SWAT team](Wikipedia:SWAT "wikilink")* is a police unit
    specializing in operations with a high risk of violence such as
    hostage rescue or subduing heavily armed criminals.
-   Kevin\'s exclamation *Facial!* is a variation of the victory taunt
    *In your face!*
-   Michael says *escape goat* instead of *scapegoat. [Michael\'s
    Botched Phrases](Michael's_Botched_Phrases "wikilink")*
-   When Andy describes in a deleted scene how he met his girlfriend, he
    rattles off various illegal activities. He purchased *[wine
    coolers](Wikipedia:wine_coolers "wikilink")* for them, even though
    alcoholic beverages cannot be sold to people under age 21. He
    \"threw in some *[scratch
    tickets](Wikipedia:Scratchcard "wikilink")*\", sales of which is
    illegal to people under age 18. Finally, Andy talks about \"making
    out in the woods\", which although not strictly illegal, teeters
    close to [statutory rape](Wikipedia:Statutory_rape "wikilink").
-   Jim tries to make small talk with a high school student in a deleted
    scene by talking about *[Heidi](Wikipedia:Heidi_Montag "wikilink")*,
    a cast member on the [MTV](Wikipedia:MTV "wikilink") scripted
    reality show *[The
    Hills](Wikipedia:The_Hills_(TV_series) "wikilink")*. The student
    says that her dad gave her a *[Lexus](Wikipedia:Lexus "wikilink")*,
    a brand of luxury car.

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting Cast

-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Recurring Cast

-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Rashida Jones](Rashida_Jones "wikilink") as [Karen
    Filippelli](Karen_Filippelli "wikilink")

### Guest Cast

-   [Lisa Darr](Lisa_Darr "wikilink") as [Barbara
    Allen](Clients_of_Dunder_Mifflin#Barbara_Allen "wikilink")
-   [Shira Scott Astrof](Shira_Scott_Astrof "wikilink") as
    [Jamie](Jamie "wikilink")
-   [Anthony Russell](Anthony_Russell "wikilink") as [Chad
    Ligh](Chad_Ligh "wikilink")
-   Jim Jansen as The High School Principal

\

Quotes
------

-   [Jim Halpert](Jim_Halpert "wikilink"): Fact, bears eat beets. Bears,
    beets, Battlestar Galactica.

`Andy Bernard: [in Andy's car] Beer me.`\
`Jim Halpert: What's that?`\
`Andy Bernard: Hand me that water.`\
`Andy Bernard: I always say, "Beer me." Gets a laugh, like a quarter of the time.`
