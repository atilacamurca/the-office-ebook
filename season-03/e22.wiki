{{Office episode
|Title      =Beach Games
|Image      =[[Image:BeachGames.jpg|250px]]
|Season     =[[Season 3|3]]
|Episode    =22
|Code       =3022
|Original   =May 10, 2007
|Writer(s)  =[[Jennifer Celotta]] & [[Greg Daniels]]
|Director   =[[Harold Ramis]]
|Prev       =[[Women's Appreciation]]
|Next       =[[The Job]]}}
'''"Beach Games"''' is the twenty-second episode of the third season of ''[[The Office (US)|The Office]] ''and the 51st overall. It was written by [[Jennifer Celotta]] and [[Greg Daniels]] and also directed by [[Harold Ramis]]. It first aired on May 10, 2007. It was viewed by 7.2 million people.

==Synopsis==
In the opening, [[Michael Scott|Michael]] has started to feel sick shortly after being given some paperwork, to [[Dwight Schrute|Dwight]]'s worry; this is a chronic occurrence, according to [[Pam Beesly|Pam]]. Soon, [[Dunder Mifflin]] CFO [[David Wallace]] calls; Michael blindsides him with unsolicited, hollow bragging about having dumped Jan. David tells Michael that he's invited to interview in a week's time for a bigger job at corporate headquarters in New York City. To choose a possible replacement as Scranton's branch manager, Michael decides to turn a scheduled office-wide outing to the beach at Lake Scranton into a ''[[Wikipedia:Survivor (TV series)|Survivor]]''-like contest (which he's convinced will determine the best leader). Michael wants everyone to think of the outing as a day off &mdash; except for Pam, whom he directs to take detailed notes on each participant's behavior and skills. He takes all the employees with him except [[Toby Flenderson|Toby]], telling the camera that the beach trip also serves as the last great memory for the employees of their time spent with Michael—which Toby's very presence would ruin (for Michael).

As they arrive, Michael starts the activities by selecting four employees "at random" to lead different "tribes": [[Jim Halpert|Jim]], [[Dwight Schrute|Dwight]], [[Andy Bernard|Andy]] and [[Stanley Hudson|Stanley]]. The choices were actually deliberate, as they are Michael's considerations for his successor. He explains their pros and cons to the camera: Jim has a great personality but doesn't work hard (since he can finish a project in 30 minutes while the same project takes Michael all day); Dwight is enthusiastic but "an idiot"; Andy "gets" Michael, but he doesn't trust Andy; Stanley represents the "amazing progress of African-Americans." They form different tribes: "[[Team Gryffindor]]," led by Dwight, "[[Team Voldemort]]," led by Jim and named in direct opposition to Dwight, "[[Blue Team]]," led by Stanley, and "[[Team U.S.A]].," led by Andy.

The activities begin with the employees oblivious to Michael's true intentions. In an egg race, Jim tricks a blindfolded [[Karen Filippelli|Karen]] into stepping into the nearby lake; Dwight is overly commanding of [[Ryan Howard|Ryan]]; Andy can't stop [[Kelly Kapoor|Kelly]] from removing her blindfold; and Stanley sits out in his customary apathy. As Pam takes notes for him, Michael grows frustrated with the contestants' failure to stick to his plans. He has Pam prepare a hot dog eating contest (giving her about 10 minutes to prepare eight hundred wieners), but when the employees balk, an exasperated Michael reveals his upcoming interview, and that the winner of the games would replace him. Realizing Michael's seriousness, the four tribe leaders (except for Jim) decide to step up their efforts; even Stanley, since he would detest working for anyone else in the office. Andy wins the hot dog eating contest. [[Creed Bratton|Creed]] returns to the group after eating a raw fish which he caught with his bare hands, unaware that food was going to be provided by Michael.

Privately, Dwight and [[Angela Martin|Angela]] plan to sabotage events by having Angela, who is on Andy's team, pretend to mishear everything that Andy says. Michael reveals the next event to be mock sumo wrestling, using the large movement-limiting costumes common to amusement park-style, recreational sumo wrestling. Dwight emerges victorious, and, as planned, Angela pretends not to hear Andy's direction, even when he slips into the lake in his buoyant costume and begins drifting downstream. Away from the others, Karen and Jim phone headquarters, both making appointments for the same open interview that Michael was invited to. Also, in a talking head segment, [[Oscar Martinez|Oscar]] states that he intends to break up with his partner Gil. He adds that he may (with Angela's encouragement) experiment with heterosexual relationships, although it's not immediately clear if he is serious about that part.

Michael reveals his final event: a [[Wikipedia:Fire-walking|walk across hot coals]]. He decides to walk across the coals himself, but ultimately never takes the first step after much procrastination. Shockingly, Pam wants to try the challenge herself, but Michael stops her since she is not one of the candidates, to her protest. He asks Jim to walk across the coals, but he refuses because he doesn't want to burn his feet. Dwight, in an attempt to both impress and blackmail Michael into giving him the position, steps onto the coals and says he will stay there until he gets the job, at which point his feet begin to burn, and he begins to collapse until he is practically lying on top of the coals in pain while the staff rushes to help him.

Once Dwight is removed from the pit, Michael, again unhappy with the results thus far, announces a sudden death &mdash; "[[Wikipedia:Tribal Council (Survivor)|Tribal Council]]" &mdash; stand-up comedy event. Andy is still adrift in the water on his back before a light shines on him and he calls for help, but is still ignored. Jim again declines to participate, and reveals that he is going to New York City for the open interviews at corporate. Pam, still at the coals, decides to run across barefoot. Empowered by the experience, she interrupts the Tribal Council with her newfound confidence. She expresses her disappointment that (almost) no one attended [[Business School|her art show]] (months earlier) despite being invited, and that they sometimes act as if she doesn't exist. She then directs herself to Jim, and publicly reveals that she called off her wedding because of him, explaining that she had plenty of reasons not to be with [[Roy Anderson|Roy]], but ignored them until she met Jim. Saying that it's "fine" that he's with someone else now, she states that she simply misses him and the fun that they had as best friends before he left for the [[Dunder Mifflin Stamford|Stamford]] branch, and then decides she needs to walk in the water to cool her feet. While the rest of the staff sits in awkward silence, Michael is impressed but reiterates that he is looking for a replacement with sales experience.

The episode ends with footage of the staff on the bus earlier that day, singing the theme song to ''[[Wikipedia:The Flintstones|The Flintstones]]'' with Michael providing sound effects before shouting "WILMA!" at the end of the song.

==Deleted scenes==
The Season Three DVD contains a number of deleted scenes. Notable cut scenes include:
*Michael announces that the bus will be leaving soon, and Phyllis says Bob Vance (of Vance Refrigeration) was going to bring her. Michael doesn't allow this.
*Jim receives a call from David Wallace informing him of the job opening in the Corporate New York office.
*Meredith sees the "No Alcohol" sign on the party bus, runs off the bus to get her drink, and then races after the bus when it starts driving away without her, pounding on the front door until the driver stops and lets her on.
*Later on, Meredith says she needs to use a restroom. Michael says they're almost at the lake site, but Meredith yells for the bus to stop. When it does, she runs to the side of the road and squats down to pee. The camera cuts from her explicitly doing this to everyone on the bus carefully looking the other way—except for Creed, who watches her peeing and nods in approval.
*Michael inflates the sumo suit with his mouth while explaining how he intends to use it. He becomes light-headed and passes out.
*Michael asks Pam (who is busy cooking hot dogs) for her assessment of the egg race. When he criticizes her slow grilling pace, Pam says sharply "There are 800 of them!" and Michael shuts up and walks away from her.
*After Michael tells Phyllis to "dip it in the water so it slides down your gullet more easily," everybody says in unison, "That's what she said."
*Michael provides running commentary on the hot dog eating contest.
*In a talking head interview, Phyllis believes Jim would be the best boss, "plus, he's eye candy."
*Ryan offers to take Andy's place, but Michael won't have it.
*Andy flags down a passing car, but after getting close it drives away, with the driver presumably having seen that Andy is in a sumo suit.

==Amusing details- Caution, here be spoilers==
* Michael says, "I personally have cooked up" the hot dogs, even though it was Pam who did it.
* When Michael says "I am deducting sixty points from Voldemort," Dwight becomes apprehensive upon hearing the name spoken out loud. (See "Cultural References" below for an explanation.)
*At the hot dog eating contest, when Michael tells Phyllis to "dip it in the water so it can slide down your gullet more easily," everyone else sits silently. However, when a flashback to this scene is shown later in [[The Banker]] Season 6, Ep. 14, everyone simultaneously responds with, "That's what she said!"
* Even though he participated in the hot dog eating contest, Dwight later eats a sandwich when he challenges Andy to a sumo rematch.
* With the exception of Stanley, all of Michael's candidates to replace him eventually become manager at some point. Dwight in "[[The Job]]", "[[Dwight K. Schrute, (Acting) Manager]]" and "[[Livin' the Dream]]" through the rest of the series. Jim in "[[Survivor Man]]" and "[[The Promotion]]" through "[[Manager and  Salesman|Manager and Salesman]]", and Andy from "[[The List]]" to "[[Get the Girl]]" and then again from "[[Free Family Portrait Studio]]" to "[[Livin' the Dream]]".
* [[Toby Flenderson|Toby]]'s diappointment is clear when he realizes that he will miss out on seeing Pam in a two-piece swimsuit.
* When Dwight attacks Andy after their sumo-suit fight, it's clear that [[Ed Helms]] is laughing hysterically.
* When [[Meredith]] accidentally exposes her breasts to the camera it's possible to see that her nipples are covered by tape.
* While filming this episode, [[Rainn Wilson]] (Dwight) accidentally kicked sand into [[Leslie David Baker]]'s (Stanley) eye. Baker had to be rushed to the hospital while he had his eye cleaned out, after being diagnosed with a scratched cornea.

==Connections to previous episodes==
* Michael mentions burning his foot in "[[The Injury]]".
* Michael is wearing a shirt and hat with the word "Sandals" on it. This is a reference to his trip to Jamaica that we learn about in "Back from Vacation".
* In "[[Women's Appreciation]]", Michael wished for Pam to have courage.

==Cultural references==
* Michael asks, "David Wallace, and Gromit?" referring to ''[[Wikipedia:Wallace and Gromit|Wallace and Gromit]]'', the main characters in a series of animated films.
* ''Get your freak on'' is urban slang for having sex with someone. Michael appears not to know what it actually means.
* Michael tells Oscar, "You can't swim in leather pants." Leather pants are a stereotypical gay article of clothing.
* Michael tells Oscar, "I'm just yanking your chain," slang for "I'm just harassing you." Michael eventually recognizes the double entendre.
* Michael wants Beach Day to be a memory "after I have passed on." He inadvertently uses a euphemism for dying.
* The office staff sing ''[[Wikipedia:The Gambler (song)|The Gambler]]'' on the bus on the way to the beach.
* Michael tells Stanley to "go to the back of the bus." He eventually realizes that forcing blacks to sit in the back of the bus was one of the injustices of the segregationist era. [[Wikipedia:Rosa Parks|Rosa Parks]] famously stepped on a bus to defy this law, which was used by the media to spark the civil rights movement.
* As he gets off the bus, Michael says, "Watch out for snakes!" This line comes from the movie [[Wikipedia:Eegah!|Eegah!]], which had been spoofed by [[Wikipedia:MST3K|MST3K]].
* Michael says that Beach Day consists of "fourteen strangers who work together, but only one survivor," and breaks the group into four "tribes". Michael is inspired by the ''[[Wikipedia:Survivor (TV series)|Survivor]]'' television series.
* Dwight names his team ''[[Wikipedia:Gryffindor|Gryffindor]]'', the house to which fictional hero [[Wikipedia:Harry Potter|Harry Potter]] belongs. Jim responds by naming his team ''[[Wikipedia:Lord Voldemort|Voldemort]]'', the name of the antagonist in the same book series. Jim further taunts Dwight by saying the name out loud, something which is taboo in the book series.
* Dwight says that the word ''sabotage'' is Dutch. The etymology of the word is disputed, but one of the possible sources is indeed the Netherlands.
* ''[[Wikipedia:Bob Hope|Bob Hope]]'' is a comedian from the mid twentieth century. ''[[Wikipedia:Amanda Bynes|Amanda Bynes]]'' is a comedian from the 2000s & early 2010s. Her movie ''[[Wikipedia:What a Girl Wants (film)|What a Girl Wants]]'' came out in 2003.
* Dwight attempts to tell the joke ''[[Wikipedia:The Aristocrats (joke)|The Aristocrats]]'' but gives away the punch line at the beginning and misses the point of the joke by providing no details on the family's depraved act.
* The [[Wikipedia:The Flintstones|Flintstones]] sing-along at the end of the show recalls a scene in the movie ''[[Wikipedia:Planes, Trains & Automobiles|Planes, Trains &amp; Automobiles]]'' where [[Wikipedia:John Candy|John Candy]]'s character leads a bus full of strangers in singing the song. In the DVD commentary, [[Ed Helms]] mentions that he chose the song specifically as a tribute to the movie.
* In a deleted scene, Michael says, "We are off like a herd of turtles." This is a whimsical way of saying that that they are finally on their way after many delays. He then attributes the phrase to talk show host [[Wikipedia:Johnny Carson|Johnny Carson]] for no apparent reason.
* In a deleted scene, Michael says that he's choosing his apprentice, which is why he modeled the day after ''Survivor''. ''[[Wikipedia:The Apprentice (U.S. TV series)|The Apprentice]]'' is a reality television show in which contestants compete to work for [[Wikipedia:Donald Trump|Donald Trump]].
* In a deleted scene, Phyllis calls Jim "eye candy", slang for an attractive person. She also lists a number of celebrities: [[Wikipedia:George Clooney|George Clooney]], [[Wikipedia:Leonardo DiCaprio|Leonardo DiCaprio]], and "that British guy that got in trouble with the prostitute" (a reference to [[Wikipedia:Hugh Grant|Hugh Grant]] in his 1995 incident).
* In a deleted scene, Michael says that in the old days, finding the next king was done by seeing who could pull a sword out of a stone, referring to the legend of ''[[Wikipedia:Excalibur|Excalibur]]''.

==Cast==
===Main cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting cast===
*[[Ed Helms]] as [[Andy Bernard]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Phyllis Smith]] as [[Phyllis Vance]]

===Recurring cast===
*[[Rashida Jones]] as [[Karen Filippelli]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Andy Buckley]] as [[David Wallace]]
<br />

{{Season3}}
