{{Office episode
|Title      =The Return
|Image      =[[Image:TheReturn.jpg|250px]]
|Season     =[[Season 3|3]]
|Episode    =13
|Code       =3013
|Original   =January 18, 2007
|Writer(s)  =[[Lee Eisenberg]], [[Michael Schur]], & [[Gene Stupnitsky]]
|Director   =[[Greg Daniels]]
|Prev       =[[Traveling Salesmen]]
|Next       =[[Ben Franklin]]}}
'''"The Return"''' (also referred to as '''"Oscar's Return"''') is the thirteenth episode of the third season of ''[[The Office (US)|The Office]] ''and the 42nd overall. It was written by [[Lee Eisenberg]], [[Michael Schur]] and [[Gene Stupnitsky]] and directed by [[Greg Daniels]]. It first aired on January 18, 2007. It was viewed by 10.2 million people.

==Synopsis==
[[Dwight Schrute|Dwight]] attends job interviews, including one where he offers his prospective employer a three volume résumé; one packet represents his professional career, the second, his athletic history, and the last, Dwight Schrute trivia. At another, describing himself in "three words", Dwight offers: "Hardworking. Alpha male. Jackhammer. Merciless. Insatiable..." Dwight settles for a job at Staples to earn money while he continues his job search. Dwight alienates his coworker who has never heard of Dunder-Mifflin.

[[Andy Bernard|Andy]] is obnoxiously testing the last nerve of his coworkers, including [[Michael Scott|Michael]], who is growing weary of Andy's incessant badgering and brown-nosing. He has relocated to Dwight's former desk where he severely irritates [[Jim Halpert|Jim]] and Ryan, whom he attempts to nickname "Big Turkey." Andy casts an invisible fishing line in an pantomimed attempt to hook Jim (the "Big Tuna"), and calls his own cell phone in order to demonstrate his new "Rockin' Robin" ringtone. Andy explains he recorded himself singing all four parts of harmony. He also repeatedly sings one portion of The Cranberries song "Zombie." Despite his earlier claims of a cappella singing experience, his pitch goes flat.

Oscar returns from his paid vacation, and [[Kevin Malone|Kevin]] asks him how he enjoyed his "gay-cation." Oscar takes the joke in good humor and chuckles. This pleases Kevin, who admits he devised the joke shortly after Oscar left and had been waiting weeks for the opportunity to use it. [[Angela Martin|Angela]] pensively invites Oscar to join the party planning committee. At first he is offended, believing she is ridiculing him since the committee consists only of women. However, Angela respectfully continues to ask, apologizing for the way that "certain events" transpired. She breaks into tears explaining how she "would just like to make some changes about certain things and certain situations and certain accountants..." Oscar quickly accepts, reassuring her.

Michael welcomes Oscar by asking the party planning committee to have a Mexican-themed fiesta celebrating his return, offending Oscar in the process. The committee outfits the office with numerous piñatas and Ryan uses a marker to change the spelling on the bottle of lemonade to read "Lemoñadé."

Jim tries to enlist [[Karen Filippelli|Karen's]] help in pulling a prank on Andy, but she is busy with the influx of clients inherited upon Dwight's departure, stating that Dwight's files are password-protected with the names of different mythical creatures. [[Ryan Howard|Ryan]] also gruffly refuses to join in the hi-jinx to Jim's dismay. Finally, Jim approaches [[Pam Beesly|Pam]], who readily agrees. In a carefully choreographed plan, Jim steals Andy's cell phone and leaves it with Pam. Later, Pam hands it back to Jim as he heads for the kitchen and stands guard as Jim pushes up a ceiling tile and launches the phone through the empty space to its final resting place, atop a tile roughly above Andy's desk. Karen briefly notices the actions of the two. The pair trade calls to Andy's phone, frustrating him to the point of accusing [[Phyllis Lapin|Phyllis]] of the theft, who ignores him.

Andy, still happy that Dwight is gone, continues sucking up to Michael. Due to Andy's pursuit of "face time" with his boss, Michael begins avoiding him, stating that Andy is "annoying the bejesus" out of him, even walking with him to the bathroom.

Angela reveals to Michael that the reason Dwight visited the corporate office was to deliver her tax documents, noting that no other employee — Andy in particular — would have done so. Andy offers to hang out with Michael, but Michael declines, requesting that Andy stop his annoying behavior. Michael's rejection of Andy's offered friendship, combined with Pam and Jim's prank, pushes Andy over the edge. He punches a hole in a wall, shocking his co-workers and quickly prompting a petrified Jim to hang up the phone. Michael leaves the office to try to get Dwight back.

Michael finds Dwight working at Staples and admits to him that he was wrong, telling Dwight that if he would go out of his way for a "random coworker" like Angela, he had misjudged him from the beginning. Michael apologizes, and asks Dwight to return to the office. Dwight accepts Michael's apology, asks not to do Michael's laundry anymore (Michael is non-committal), and returns to the office in a daze, finding his coworkers celebrating Oscar's welcome back party, which he presumes is being held for him.

At the party, Karen observes Pam and Jim laughing together about the prank. She confronts Jim, asking him if he still has feelings for Pam. After several seconds of silence, Jim nods and answers "Yes." Karen stands up and walks out, visibly upset.

Dwight is handed a broom and blindfold to break open a piñata. Declining the blindfold, he knocks the piñata to the ground and begins pummeling it into shards. He triumphantly continues throughout the office, decimating the decorative piñatas hung from the ceiling as he goes. Andy sulks and watches Dwight's antics from the kitchen. Michael states that he prefers being sucked up to for reasons of love rather than career advancement.

==Producer's Cut==
A Producer's Cut of this episode was made available on the NBC Web site and via iTunes. It contains the following new scenes:
*Michael's convertible top has mechanical problems and will not go up. When Oscar parks his new car (leased for him by the company as part of his settlement), Michael advises Oscar to "put [[Wikipedia:Rainbow flag (gay movement)|rainbow stickers]] on the back". Michael comments on the "German engineering" of Oscar's [[Wikipedia:Lexus RX 400h|Lexus RX 400h]] (a Japanese vehicle). In a talking head interview, Oscar admits that he is not sure whether he wants everyone to "just keep their mouth shut" about his orientation, or keep going "because I could use a home theater system".
*Oscar finds Creed and Jim in the kitchen and asks them where Dwight is, prompting Creed to describe events from "[[Grief Counseling]]" (completely unrelated to Dwight's departure). Jim, being at Stamford at that time, says he is sure none of that's real. Creed shouts back, "''You're'' not real, man!"
*Jim asks Ryan if he wants to play a prank on Andy. Ryan responds "Not right now, ask me again ten years ago." Jim says he liked him better as the temp and Ryan agrees. (This scene is not on the season 3 DVD, but is in the version available on iTunes.)
*Brief shot of snow collecting in Michael's uncovered convertible.
*Kelly is surprised that Oscar has never heard of [[Wikipedia:Lance Bass|Lance Bass]], suggesting that Oscar should "learn more about his culture."
*Michael asks Jim what he thinks of Andy. Michael claims on paper that he and Andy should be best friends. Jim tells him that Andy will only say what makes Michael happy because he is a [[Wikipedia:Yes man|yes man]]. Michael doesn't understand what this means until Jim tells him that Andy did the same thing with [[Josh Porter|Josh]].
*In a talking head, Michael recalls a childhood memory of being put to bed in Spider-Man pajamas and his mother kissing him on his butt accidentally. Thus he knows what it is like to have one's butt kissed literally, adding, "and it better not be what Andy is doing."
*Angela confides in Pam about missing Dwight. She tells Pam she doesn't want people to know about their relationship. Pam tells her there has to be a way she can get Dwight back without spilling the beans. Angela claims she cannot, as she is "not like Pam, walking around in [her] provocative outfits, saying whatever thought pops in [her] head".
*Sequence of Michael talking about (in talking head format) going to get Dwight being played as he cleans snow out of his car. Michael states that he made a big mistake replacing Dwight with Andy, a person who "loves the company so much he punched a hole in it".
*Michael's search for Dwight in Staples is extended.
*Creed asked Meredith where she got some of the party food. When she mentioned the store name, Creed asked which aisle specifically. Meredith then replied that she couldn't remember, but Creed continues to interrogate by offering her a pen and his hand (instead of paper) and says "Draw me a map, Mama!"
*Michael shakes maracas at Pam while saying "I will shake mine, and then you will shake yours." Pam promptly replies, "No I will not". Phyllis proceeds to shake her chest enthusiastically. Pam smiles and Michael walks away in disgust.
*Michael asks Oscar if this party reminds him of his childhood, Oscar says it reminds him of the movie ''[[Wikipedia:¡Three Amigos!|¡Three Amigos!]]''. Michael is flattered by Oscar's suggestion and takes it as a compliment.
*The episode ends with Andy driving to a ten-week Anger Management program he has been ordered to attend. Andy prefers to think of it as "Management Training." He tells the documentary crew that he intends to complete the program in five weeks by employing a strategy of "name repetition, personality mirroring, and positive reinforcement through nods and smiles," similar to the strategy announced upon his arrival at Scranton. Andy claims "I'll be back, just like [[Wikipedia:Rambo|Rambo]]" as he greets his counselor.

==Deleted scenes==
The Season Three DVD contains deleted scenes beyond those included in the Producer's Cut. Notable cut scenes include:
*Oscar and Toby make awkward small talk.
*Karen reluctantly declines taking part in the prank.
*Extension of the phone-throwing scene.
*Michael learns that Dwight turned down payment for his unused three months of vacation days. "He'd rather the company have the money."
*Andy makes up a new nickname for Michael.
*Dwight is promoted to management ahead of schedule.
*Kevin is amused by the word "ass."
*Kevin hums the [[Wikipedia:Jarabe tapatío|Mexican Hat Dance]].
*Karen feels left out because she doesn't see what's funny about Andy punching a hole in the wall.

==Trivia==
* The man who interviews Dwight is Kevin Reilly, at the time the president of NBC Entertainment. Reilly was a strong supporter of ''The Office'' in its early days despite low ratings.
* The woman who interviews Dwight initially had no name in the script. Writer Michael Schur made up the name Gwendolyn Trundlebed as a placeholder and thought nothing of it. The set designers saw the name and decorated her office in the style seen in the episode, assuming that Schur was trying to convey a message with that unusual name.<ref name="WritersBlock">Schur, Michael. The Office Convention, Writer's Block panel discussion.</ref>
* Andy's plans for hanging out with Michael include watching the television program ''Lost'' (which was on hiatus at the time) and the Cornell-Hofstra game (even though the college football season had already ended).
*Dwight wears a ''Battlestar Galactica'' sweatshirt upon his return to the office. When he left Staples, he was wearing a plain white t-shirt. It is not explained where the shirt came from. ''(The shirt could have been in Dwight's car).'' Michael also wears the same sweatshirt later on in season four's 'Survivor Man' after he cuts up his own suit and Dwight saves him from eating mushrooms.
*According to Greg Daniels, the ''Battlestar Galactica'' sweatshirt was created by the wardrobe department because such shirts were not yet available at the time the episode was filmed.
* After the episode aired, a memo was circulated at the real ''Staples'' that read, "Effective immediately, and to the relief of many, Dwight Schrute is no longer a Staples associate... Despite his promising start in business machines (selling 2 printers in one morning), it was soon clear he wasn't a good fit with the Staples culture."<ref>Parade Magazine, [http://www.parade.com/quiz/the-office/28/c.html The Office Quiz]</ref>
* One of Dwight's coworkers at Staples is played by Yvette Nicole Brown, who would later go on to star in the NBC show Community.

==Analysis==
*Jim remarks "I liked you better when you were the Temp," to which Ryan quietly replies "Me too." Some viewers interpret this to be a reference to Ryan's previous remarks that he did not want to remain with the company. Others consider it a meta-humorous remark by the writers, lamenting that Novak's character was more affable and had more screen time before he was promoted.

==Amusing details==
* Andy has the Dunder Mifflin logo on his cell phone. His efforts to suck up to Michael have no bounds.
* The anger management company is named "Calm Visions".
* Marcy, the trainer at the anger management company, smokes. Some people smoke as a way to calm their nerves.
* When Michael finds Dwight in Staples an instrumental version of ''Up Where We Belong'' is being played in the background. Richard Gere reunites with his lover to the same song at her work place in An Officer and A Gentleman.
* At one point Andy begins singing the song ‘Zombie’ by The Cranberries. It is possible that ‘Zombie’ was one of the mythical creature passwords that Dwight used to encrypt his sales folders.
* Andy calls Ryan "big turkey", just like he calls Jim "big tuna" because he ate a tuna sandwich on his first day in Stamford.
* When Michael and Pam are talking about the plant that is suddenly not doing very well, the plant on the wall between accounting and reception is also dying.
* Dwight goes out of his way to help customers at Staples, knowing that big market chains lack in customer service.
* This is Jim's third prank on Andy that has resulted in him having a meltdown (Putting his calculator in Jello, pretending to not know he went to Cornell, hiding his phone and calling it repeatedly).

==Connections to Previous Episodes==
*When Oscar asks what happened to Dwight, Creed says that he was decapitated, and that the whole office had a funeral for a bird. Jim says that he doesn't believe it, and Creed gets angry. In fact, Creed describes what happened during [[Grief Counseling]] , although it was [[Ed Truck]] who was decapitated, not Dwight. As well, neither Jim nor Oscar could have known; Jim was in Stamford at the time, and Oscar was still on vacation.

==Cultural references==
* Andy says ''TGI Wednesday'', a pun on ''TGIF'', short for "Thank God it's Friday", an expression of relief at the end of the work week.
* Andy's expression ''Get my beer on'' is a snowclone of ''get my X on'' which means ''start doing X''. He continues in the same fashion with ''Get my Lost on''; ''Lost'' is a television drama that takes place on an island. (See also ''get our skate on'' in the episode "[[Michael's Birthday]]".)
* ''[[Wikipedia:Ebonics|Ebonics]]'' is a term for the dialect of English spoken by some African-Americans in the United States. Michael says the word "business" as "bidness" as an example.
* ''Oscar night'' is the nickname given to the [[Wikipedia:Academy Award|Academy Award]] ceremony.
* ''[[Wikipedia:Swanson|Swanson]]'' is a frozen food company that invented the [[Wikipedia:TV dinner|TV dinner]]. As with most frozen dinners, the quality is mediocre.
* Unknown reference: ''Riding in on a donkey like Pepe''.
* ''[[Wikipedia:Marv Albert|Marv Albert]]'' is a sportscaster who in 1997 pled guilty to biting a female sex partner.
* ''[[Wikipedia:Cornell University|Cornell]]'' is Andy's alma mater. ''[[Wikipedia:Hofstra University|Hofstra]]'' is another university also in New York. The two teams did not play each other in football during the 2006-2007 season.
* A ''[[Wikipedia:Piñata|piñata]]'' is a traditional Mexican party accessory, typically filled with candy and toys. Blindfolded guests take turns trying to smash the piñata with a stick in order to release the contents.
* Meredith tells Creed that she bought the chimichangas at Gerrity’s, a chain of groceries stores in eastern Pennsylvania.

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting Cast===
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Phyllis Smith]] as [[Phyllis Lapin]]

===Recurring Cast===
*[[Ed Helms]] as [[Andy Bernard]]
*[[Rashida Jones]] as [[Karen Filippelli]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]

===Guest Cast===
*[[Yvette Nicole Brown]] as [[Paris]]
*Eric Christie as Al
*Charlotte Stewart as Woman in Staples
*Kevin Reilly as Dwight's First Interviewer
*Susie Geiser as Marcy (Andy's Anger Management Counselor, only in the Producer's Cut)

==References==
<references/><br />

{{Season3}}
