# The Job

![](TheJob.jpg "TheJob.jpg"){width="150"}

**\"The Job\"** is the twenty-third episode of the third season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 52nd/53rd
overall. It was written by [Paul
Lieberstein](Paul_Lieberstein "wikilink") and [Michael
Schur](Michael_Schur "wikilink") and directed by [Ken
Kwapis](Ken_Kwapis "wikilink"). It first aired on May 17, 2007 and was
an hour-long episode. It was viewed by 7.9 million people.

Synopsis
--------

[Michael](Michael_Scott "wikilink") is overconfident that he will be
getting the corporate job. He has sold his condo on
[eBay](Wikipedia:eBay "wikilink") and names
[Dwight](Dwight_Schrute "wikilink") as his successor as Regional
Manager. Back in Scranton, [Oscar](Oscar_Martinez "wikilink"),
[Kelly](Kelly_Kapoor "wikilink") and
[Stanley](Stanley_Hudson "wikilink") poke fun at
[Pam](Pam_Beesly "wikilink")\'s speech at the beach (in \"[Beach
Games](Beach_Games "wikilink")\") ([Creed](Creed_Bratton "wikilink")
also claims to have documented the speech on the
[website](Creed_Thoughts "wikilink") he created with
[Ryan\'s](Ryan_Howard "wikilink") help, but in a talking head Ryan
reveals that the \"blog\" is actually a Microsoft Word document, as he
had been unwilling to expose humanity to the innermost workings of
Creed\'s mind), but Pam insists the evening was an important milestone
in her life and reveals that she and Jim had privately agreed to try to
mend their friendship. Pam explains to Karen that she is not sorry about
what she said, but does regret putting Karen in such an awkward
position.

In an attempt to win Michael back, [Jan](Jan_Levinson "wikilink")
arrives at the office telling him that she\'s been more confident and
better than ever after taking a vacation. In a panic, Michael consults
the women of the office. Pam tells him to be strong and to not get back
together with her. Michael attempts to reject Jan\'s request, but
reverses himself immediately when he sees that Jan had a [breast
augmentation](Wikipedia:breast_augmentation "wikilink"). Soon after, Jim
and Karen leave to spend the night in New York City before their
interviews. Karen tells Jim that if either of them gets the job, both
should move to New York. She promises to do so, but is met with awkward
silence when she asks Jim to reciprocate. Back in Scranton, Dwight
begins his new regime, assigning [Andy](Andy_Bernard "wikilink") the
role of his No. 2. Not surprisingly. Dwight\'s new motivational tool
\"[Schrute Bucks](Schrute_Buck "wikilink")\" is not popular. He and Andy
repaint the walls of Dwight\'s new office black in order to instill
fear.

![One Schrute Buck](SchruteBuck.jpg "One Schrute Buck"){width="208"}

During his interview at Corporate, Michael learns from
[CFO](Wikipedia:Chief_financial_officer "wikilink") [David
Wallace](David_Wallace "wikilink") that he is interviewing for the job
currently held by Jan, who will be fired. Michael immediately tells Jan,
who storms into Wallace\'s office while he\'s interviewing Karen and has
an emotional breakdown. Jan refuses to leave, and must be escorted out
by security. As Michael leaves, Wallace tells him that he won\'t be
getting the position. While being driven home by Michael, Jan begins to
realize that she\'s ruined her career and ponders making their
relationship her \"full-time job,\" much to Michael\'s visible
discomfort.

Karen leaves to meet friends for lunch, asking Jim to call her when his
interview is finished. Jim\'s interview with David begins very well.
When asked for his sales report numbers, Jim discovers Pam had slipped
in an encouraging note along with a yogurt lid medal (a reference to a
scene from \"[Office Olympics](Office_Olympics "wikilink")\"). Jim,
visibly distracted, stumbles through the rest of the interview until
Wallace asks him where Jim thinks he will be in ten years. The scene
flashes back to Jim and Pam\'s talk after her confession on the beach.
He tells her why he left and how he feels that he has never really come
back. She tells him that she wishes that he would. After the interview,
Jim drives back to Scranton without Karen.

In her interview, Pam says she is convinced Jim will be hired in New
York. She reaffirms that she is fine with the way things worked out with
her and Jim, and that they just \"never got the timing right.\" Her
interview is interrupted when Jim bursts in and asks her if she is
available that evening for dinner. Pam, who is caught completely
off-guard, answers, \"Yes.\" Jim replies, \"All right, then it\'s a
date,\" and leaves just as quickly as he arrived. Pam smiles with tears
in her eyes.

The episode and season end with Wallace offering the job over the phone
to [Ryan](Ryan_Howard "wikilink"), despite the fact that in the previous
episode \"[Business School](Business_School "wikilink")\" Michael said
that Ryan had never made a sale. Wallace indicates that Ryan\'s
[MBA](Wikipedia:Master_of_Business_Administration "wikilink") was a key
factor. A gleeful Ryan accepts and says without missing a beat to a nosy
[Kelly](Kelly_Kapoor "wikilink"): \"You and I are done.\"

Deleted scenes
--------------

-   Michael takes photographs of the office as mementos. Inspired by
    Pam\'s speech in \"Beach Games\", Michael tells Toby that he hates
    him and then exults at how good it feels to \'get that off (his)
    chest\'. Toby barely reacts to this news. As Michael leaves for his
    interview, Toby wishes him good luck and Michael snarls at Toby for
    jinxing him, adding that everyone in the office hates Toby as much
    as he does, to no response from anyone else.
-   Ryan chuckles at a fax he receives. When Pam asks him what the fax
    said, he grins and says he\'ll tell her if she\'ll promise not to
    make a speech about it to everyone in two years (alluding to Pam\'s
    speech at the end of \"[Beach Games](Beach_Games "wikilink")\") and
    Pam grins good-naturedly. Meanwhile, Kevin continues comparing Pam
    and Karen.
-   Michael bids a confused farewell to the accounting department.
-   Dwight caresses his new office. Michael asks him not to change
    anything for one month to allow the staff to grieve.
-   Michael finds an apartment in Jamaica, Queens, on his transit
    line\'s very last stop, where he will room with Vijay Chokalingham
    and live directly over an Indian restaurant.
-   Additional scenes from Andy\'s interview with Dwight. Andy says,
    among other things, that \"Eli Edison\" invented the cotton mill and
    goes through a several step description of how to do an emergency
    appendectomy.
-   Extension of Andy\'s \"my brain\" talking head interview.
-   In a talking head interview in his new office, Dwight describes his
    management style.
-   Extension of Pam\'s \"Absolutely I do.\"
-   Kelly gives Pam erroneous advice about Jim\'s feelings for her,
    telling Pam that \"he\'s just not that into you.\" In a talking head
    interview, Pam realizes that she and Kelly have nothing in common.
-   Michael answers the interview question, \"Where do you see yourself
    in ten years?\"
-   Dwight sits behind his new desk, and Angela tells him he looks
    \"like Sean Hannity.\"
-   Several of the office workers talk about who will get the Corporate
    job, and Kevin is insistent that Karen will be the winner. In a
    talking head interview, Kevin says that he bet a friend at the
    Albany branch one month\'s salary that Karen would get the job, and
    realizes that he might have a gambling addiction.
-   Additional scenes from Dwight\'s conference room presentation.
-   Creed produces \$2.1 million in counterfeit [Schrute
    Bucks](Schrute_Buck "wikilink") and demands that they be converted
    to their cash equivalent.
-   In a talking head interview, Dwight discusses how he can\'t pal
    around with his colleagues now that he is the boss.
-   Dwight and Andy paint Dwight\'s office.
-   Additional footage of Michael\'s face-saving speech when he returns
    to Scranton.
-   In a talking head interview, Dwight reflects on his one-day reign as
    regional manager.
-   Stanley tells Michael he never thought he\'d say this, but he\'s
    glad Michael is back. Michael opens his arms to offer Stanley a hug,
    but Stanley just looks at him and then walks back to his desk.
-   As Michael settles back into his office, he talks about how he loves
    Scranton, then mentions Jan is moving in with him the next day. Jan
    gives specific orders to the movers while Michael watches her with
    abject horror.

Cultural References
-------------------

-   Michael says, *No further questions* to David Wallace, jokingly
    using a phrase conventionally used by lawyers in the United States
    to indicate to the judge that they are finished eliciting testimony
    from a witness.
-   Michael\'s note for Dwight calls him an *A-wipe*, abbreviating the
    vulgar word *ass*. To *screw the pooch* is an off-color way of
    saying *to mess up something*.
-   Michael sold his condo on *[eBay](Wikipedia:eBay "wikilink")*, an
    online auction site.
-   Dwight\'s ideal choice for his Number Two is *[Jack
    Bauer](Wikipedia:Jack_Bauer "wikilink")*, the fictional government
    agent in the television suspense series
    *[24](Wikipedia:24_(TV_series) "wikilink")*.
-   *[DEFCON](wikipedia:DEFCON "wikilink")* is a measure of the
    activation and readiness level of the United States Armed Forces.
    However, readiness increases as the scale decreases; DEFCON 1 is
    maximum readiness.
-   Michael says *Houston, we have a problem*, a common misquote of the
    message \"Houston, we\'ve had a problem\" sent from [Apollo
    13](Wikipedia:Apollo_13 "wikilink") after an oxygen tank ruptured.
-   *[Swing Low, Sweet
    Chariot](Wikipedia:Swing_Low,_Sweet_Chariot "wikilink")* is a Negro
    spiritual.
-   Andy brags, *Saving the best for first,* turning around the idiom
    \"Saving the best for last.\"
-   The cash value of a [Schrute Buck](Schrute_Buck "wikilink") is 1/100
    of a cent. Some states require that coupons be assigned a cash
    value. Manufacturers set the value high enough to be legal but low
    enough that nobody will actually attempt to redeem them for
    cash.[1](http://www.straightdope.com/classics/a1_329c.html) Creed
    attempts to redeem [Schrute Bucks](Schrute_Buck "wikilink") for cash
    in a deleted scene.
-   When Dwight and Andy are painting Michael\'s office, Dwight says
    \"Abandon all hope ye who enter here.\" referencing Dante
    Alighieri\'s\'\' Divine Comedy*, specifically* Inferno \'\'in which
    the phrase is inscripted on the gates of Hell.
-   Michael says \"I\'m baaaack, for gooood!\" and erroneously
    attributes it to [Kevin Nealon](Wikipedia:Kevin_Nealon "wikilink").
    In the \"Hans and Franz\" series of skits on *[Saturday Night
    Live](Wikipedia:Saturday_Night_Live "wikilink")*, Nealon portrayed
    Hans, one of the two Austrian bodybuilders. The characters were
    spoofs of former bodybuilder [Arnold
    Schwarzenegger](Wikipedia:Arnold_Schwarzenegger "wikilink"). The
    line \"I\'ll be back\" is properly attributed to Schwarzenegger\'s
    character in the movies *The Terminator* and *Terminator 2*.

Trivia- Caution, here be spoilers
---------------------------------

-   Michael\'s potential roommate in Queens is named Vijay Chokalingam,
    a nod to [Mindy Kaling](Mindy_Kaling "wikilink")\'s older brother.
-   Jim and Karen have dinner at [The Spotted
    Pig](http://www.thespottedpig.com/) in New York City.
-   According to the DVD commentary, the episode was originally supposed
    to begin with the Pam/Jim beach flashback, but the scene was moved
    till near the end mainly because, given how happy and right for each
    other Jim and Karen appeared to be in the NYC sequence, it was
    important to show just how deep his feelings for Pam were when he
    realizes where he truly belongs.
-   This is the first time someone other than Michael has a word
    \"bleeped\".
-   Wallace dislikes his HR representative, paralleling Michael\'s
    hatred of Toby.
-   Wallace states that Michael lost no clients after the merger but did
    reduce the budget. The budget reduction can be explained by the loss
    of three people from the Stamford branch after the merger.
-   After Dwight informs Angela about his promotion and leaves the room,
    Angela says to herself, \"Goodbye, Kelly Kapoor.\" Angela\'s disdain
    for Kelly goes back to [Christmas Party](Christmas_Party "wikilink")
    in [Season 2](Season_2 "wikilink") when Kelly got drunk at the
    Christmas Party. Other reasons for Angela\'s dislike of Kelly could
    include a conversation in \"*[Initiation](Initiation "wikilink")*\",
    when Kelly told Angela that Dwight was a freak, and Kelly being very
    talkative and flashy.
-   Also, in \"Initiation\" Michael uses *The Terminator* line, \'I\'ll
    be back\'.
-   In the end of the episode, Michael tells the staff that he will
    \"never, ever\" leave the office, however he does so when he moves
    to Colorado with his fiance [Holly Flax](Holly_Flax "wikilink") in
    \"*[Goodbye, Michael](Goodbye,_Michael "wikilink")\"*
-   With the start of Pam and Jim\'s relationship at the end of this
    episode, all of Jim\'s relationships over the course of the show
    began in a Dunder Mifflin office; [Katy](Katy "wikilink"), *\"[Hot
    Girl](Hot_Girl "wikilink")\" - \"[Booze
    Cruise](Booze_Cruise "wikilink")*\" [Karen
    Filippelli](Karen_Filippelli "wikilink"), \"[*The
    Merger'*\'](The_Merger "wikilink")*\" - \"[The
    Job](The_Job "wikilink")*\" and [Pam
    Halpert](Pam_Halpert "wikilink"), *\"[The
    Job](The_Job "wikilink")\" - \"[Finale](Finale "wikilink")\".*
-   \"The Job\" has a double meaning. One is the job that Karen, Jim,
    and Michael interview for at corporate, and the other is Jan\'s
    breast enhancement, often called a \"boob job.\"

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting Cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Recurring Cast

-   [Rashida Jones](Rashida_Jones "wikilink") as [Karen
    Filippelli](Karen_Filippelli "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Andy Buckley](Andy_Buckley "wikilink") as [David
    Wallace](David_Wallace "wikilink")
-   [Nicholas D\'Agosto](Nicholas_D'Agosto "wikilink") as
    [Hunter](Hunter "wikilink")

### Guest Cast

-   [Vivianne Collins](Vivianne_Collins "wikilink") as
    [Grace](Grace "wikilink")

\
