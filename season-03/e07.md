# Branch Closing

**\"Branch Closing\"** is the seventh episode of the third season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 35th overall.
It was written by [Michael Schur](Michael_Schur "wikilink") and directed
by [Tucker Gates](Tucker_Gates "wikilink"). It first aired on November
9, 2006. It was viewed by 8.1 million people. It is the first episode to
have a \"Producer\'s Cut\" on NBC.com, with deleted scenes edited into
the full episode and broadcast on the website.

Synopsis
--------

At the [Stamford branch](Dunder_Mifflin_Stamford "wikilink"),
[Jim](Jim_Halpert "wikilink") faxes [Dwight](Dwight_Schrute "wikilink")
a message from \"Future Dwight\" on stationery he stole before leaving
[Scranton](Dunder_Mifflin_Scranton "wikilink"). Dwight receives a fax
warning that the coffee is poisoned and responds by dramatically
knocking a mug of coffee out of [Stanley](Stanley_Hudson "wikilink")\'s
hand.

[Jan](Jan_Levinson "wikilink") informs Michael that the board has voted
to close the Scranton branch. A small number of people will be
transferred to Stamford while the rest (including Michael) will receive
severance packages. Michael takes the news badly, and it isn\'t long
before he spills the beans to the office prematurely.

Reactions to the news vary, as revealed in a series of talking head
interviews. [Stanley](Stanley_Hudson "wikilink") is thrilled and looks
forward to retiring and traveling with his wife,
[Ryan](Ryan_Howard "wikilink") notes the irony that the news is
announced the day he receives his business cards,
[Angela](Angela_Martin "wikilink") calmly blames everyone, and a
distraught [Kelly](Kelly_Kapoor "wikilink") threatens to kill herself if
Ryan loses his job while she gets to keep hers, just like *Romeo and
Juliet* (the Claire Danes version). Jim notes Scranton\'s closing with a
tinge of disappointment, and [Pam](Pam_Beesly "wikilink") considers it a
blessing in disguise, since she has started catching herself answering
her home phone with \"Dunder Mifflin, this is Pam.\"
[Roy](Roy_Anderson "wikilink") is unsure whether he wants to keep
working at the warehouse if Pam leaves. But the warehouse workers are
not in danger: The new warehouse owner [Bob Vance](Bob_Vance "wikilink")
(of [Vance Refrigeration](Vance_Refrigeration "wikilink")) offered to
retain the warehouse crew.

Michael has a plan: He will confront the [CFO](David_Wallace "wikilink")
at the New York office to get him to change his mind. As he and Dwight
drive to New York, Michael learns that the CFO is out of the office for
the day, so the confrontation will take place at the CFO\'s home
instead.

In the break room, Ryan breaks up with Kelly, explaining that their
relationship would not work out since they will no longer be working
together. In a talking head interview, Ryan is pleased: He got valuable
work experience, he will get a great recommendation letter from Michael,
and he broke up with Kelly.

Amidst the celebration at the Stamford branch, Jan informs their branch
manager [Josh](Josh_Porter "wikilink") that he will be taking over the
newly-formed division Dunder Mifflin Northeast, which will be all of the
offices north of Stamford. Josh turns down the offer, however, revealing
that he has leveraged the situation to obtain a senior management
position at Dunder Mifflin\'s main competitor
[Staples](Staples "wikilink"). This throws the entire restructuring into
disarray. Jim remarks in a talking head interview, \"Say what you will
about Michael Scott, but he would never do *that*.\"

Michael and Dwight arrive at the CFO\'s residence to find nobody home.
While waiting for the CFO to return, Dwight helps Michael develop \"an
attack plan\" and they rehearse what Michael will say.

Jan returns to the conference room and informs Jim that the new plan is
for Scranton to absorb Stamford. She offers Jim the number two position
at the Scranton branch, but Jim is unsure, as he has unresolved personal
issues at the Scranton branch (which Jan assumes are due to Michael).
[Karen](Karen_Filippelli "wikilink") and Jim discuss the situation, and
Karen says that if asked, she would go to Scranton. Jim is still
undecided but suggests that Karen investigate job opportunities in New
York rather than moving to Scranton. As for
[Andy](Andy_Bernard "wikilink"), he creates a mess in the break room in
frustration.

Jan returns to the Scranton branch, only to find chaos: Michael is gone,
Stanley happily packs up his things, and Ryan flings his new business
cards into the air. Demanding an explanation, Jan learns that Michael
had told them of the branch\'s closing, at which point she informs them
that their branch is not closing after all. With the exception of
Stanley, the office is relieved that their jobs are safe. (Even Kevin
and Angela embrace upon hearing the news.) They decide to go out to
celebrate. Pam anxiously asks Jan whether anyone from Stamford is coming
back to Scranton. Kelly is thrilled that she and Ryan don\'t have to
break up now that their jobs are safe.

While Pam justifies in a talking head interview that keeping her job
saves her the trouble of adjusting to a new one, Jim agonizes over
whether he should return to Scranton or not. He informs Karen that he
has decided to accept the position and suggests that she go there if
offered a job. In a talking head interview, Karen admits that she is
glad he said it, because even though she does not think he is into her,
she is kind of into him.

Night has fallen, and Michael and Dwight continue to wait for the CFO to
come home. Pam calls Michael\'s cell phone to let him know that the
Scranton branch is safe, but he refuses to answer calls from the office
until he can give them \"good news.\" As it gets later, Michael gets
more and more despondent. While Dwight steps away to check his
voicemail, Michael, talking to himself, finally accepts defeat, lying on
the sidewalk and bitterly accepting his failure - only to be interrupted
by a scream from Dwight, who tells him that the Scranton branch has been
saved. As the reality of this statement finally sinks in, the pair
joyfully celebrate their success, unsure exactly how they accomplished
it.

As he heads home from work, [Toby](Toby_Flenderson "wikilink") tells the
documentary crew that he dreamed of selling his house, moving to Costa
Rica, and learning how to surf, plans which had to be put on hold when
the Scranton branch was saved. \"Costa Rica will still be there\... when
I\'m 65.\" which happens (not permanently) in the episode [Goodbye,
Toby](http://theoffice.wikia.com/wiki/Goodbye,_Toby) .

Producer\'s cut
---------------

In subplots, [Meredith](Meredith_Palmer "wikilink") seeks a potential
paramour based on a deal she once made to get physical with a coworker
on the final day of employ, and [Creed](Creed_Bratton "wikilink") sells
the electronics and furniture from his area of the office.

Meredith first believes that she makes the future sexual liaison pact
with Michael, but he says it is not so.
[Toby](Toby_Flenderson "wikilink") is equally unhelpful on the matter.
Finally, Gary Trundle, Meredith\'s former coworker from another
branch\'s warehouse, calls to remind her of a deal he had made with her
long ago. Meredith asks him to be at her place in 20 minutes. She
confirms the closing to Gary even though she knows that the decision to
close has been reversed, initially telling him the truth before cutting
herself off to plan the rendezvous.

Dwight searches through the CFO\'s garbage outside his home, learning
that he is rich (as he has a satellite TV bill) and drinks coffee,
\"possibly to disguise the smell of cocaine.\"

Pam\'s line about the closing being a blessing has been changed from her
being able to no longer answer her home phone \"Dunder Mifflin, this is
Pam\" to her fantasy about quitting.

Jim\'s line about his theory that Scranton would close \"because Michael
sold the building for magic beans\" is changed to an awkward high school
reunion that ends up with everyone moving in with him.

Andy asks Jim what he\'s going to do if he should get laid off. When Jim
asks Andy, he says that Cornell has a great alumni network and he might
go back there to teach. Jim then asks where he went to college, mocking
Andy\'s frequent reference to his alma mater.

In an extended scene, we find out that Ryan and Kelly\'s relationship
may be more than it seems. Kelly appears to have a strange power over
Ryan. He claims he \"can\'t explain\" the attraction and why they remain
together.

Creed takes pictures of office property on his desk and advertises the
items online. He makes several deals throughout the day and ultimately
earns \$1,200 selling Dunder Mifflin property from a branch now
scheduled to remain open (giving proper context to Kevin\'s line, which
remained in the aired version, that Creed would be paying for the
post-work celebration at Poor Richard\'s).

Amusing Details
---------------

-   Most of the office is aware that a few will be transferred to
    [Dunder Mifflin Stamford](Dunder_Mifflin_Stamford "wikilink") and
    that the rest will be given severance even though Michael never
    communicated this to them.
-   Stanley packs up his things even though it is mentioned that the
    process of closing a branch takes a little while.
-   When Jim pesters Josh about people transferring to the Stamford
    branch, Josh becomes noticeably uncomfortable and says \"You know,
    nothing\'s definite, and frankly I wouldn\'t worry about it,\" to
    which Jim replies, \"What is that supposed to mean?\" This
    foreshadows Josh soon departing the company because he leveraged his
    new title into a senior management position at Staples, which ruins
    the company\'s plans for the Stamford branch to absorb the Scranton
    branch.
    -   Josh also repeatedly tells the Stamford employees to not worry
        about the rumors of the Scranton branch shutting down and
        Stamford becoming Dunder Mifflin Northeast, because he would
        soon ruin the plan.

Connections to other Episodes
-----------------------------

-   In [The Office: The
    Accountants](The_Office:_The_Accountants "wikilink") it's revealed
    that Angela has a crush on Roy, this is also unsubtly hinted at in
    this episode.

Cultural references
-------------------

-   Michael sings *Just call me Levinson in the morning* to the chorus
    of the song *[Angel of the
    Morning](Wikipedia:Angel_of_the_Morning "wikilink")*. The actual
    lyrics are \"Just call me angel of the morning, angel.\"
-   *[Romeo and Juliet](Wikipedia:Romeo_and_Juliet "wikilink")* is a
    tragic romance by William Shakespeare. The *[Claire
    Danes](Wikipedia:Claire_Danes "wikilink")* version is the 1996 movie
    *[Romeo + Juliet](Wikipedia:Romeo_+_Juliet "wikilink")*, a
    modernization of the classic play.
-   In the children\'s story *[Jack and the
    Beanstalk](Wikipedia:Jack_and_the_Beanstalk "wikilink")*, the
    protagonist Jack sells the family cow for a handful of *magic
    beans*.
-   *[You Don\'t Know What You Got (Till It\'s
    Gone)](Wikipedia:You_Don't_Know_What_You_Got_(Till_It's_Gone) "wikilink")*
    is a song by the rock group
    *[Cinderella](Wikipedia:Cinderella_(band) "wikilink")*.
-   *[Shotgun](Wikipedia:riding_shotgun "wikilink")* is a protocol for
    determining who sits in the front passenger seat.
-   *[Michael Moore](Wikipedia:Michael_Moore "wikilink")* is a film
    documentarian with an aggressive, confrontational style. His
    documentary *[Bowling for
    Columbine](Wikipedia:Bowling_for_Columbine "wikilink")* deals with
    the culture of guns and violence in the United States. The movie
    *[Kingpin](Wikipedia:Kingpin_(film) "wikilink")* is a 1996 comedy.
    Coincidentally, the lead character in the movie is from Scranton.
-   *[Staples](Wikipedia:Staples_Inc. "wikilink")* is a retail chain of
    office supplies, positioned within the show as one of Dunder
    Mifflin\'s largest competitors.
-   *[DJ\'s](Miscellaneous_Scranton_locations#DJ.27s "wikilink")* is a
    Scranton restaurant that features chicken wings.
    *[Cuginos](Miscellaneous_Scranton_locations#Cugino.27s "wikilink")*
    is a small Italian restaurant in nearby Dunmore.
    *[Cooper\'s](Miscellaneous_Scranton_locations#Cooper.27s_Seafood "wikilink")*
    is a seafood restaurant in Scranton.
-   *[Poor Richard\'s Pub](Poor_Richard's "wikilink")* is a pub in
    Scranton.
-   65 years is the standard retirement age in the United States.
-   Pam\'s P.S. in Kelly\'s company directory, \"What a long, strange
    trip it\'s been\", is the climatic refrain of the song
    [Truckin\'](Wikipedia:Truckin' "wikilink") by the [Grateful
    Dead](Wikipedia:Grateful_Dead "wikilink").

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting cast

-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring cast

-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")
-   [Rashida Jones](Rashida_Jones "wikilink") as [Karen
    Filippelli](Karen_Filippelli "wikilink")
-   [Charles Esten](Charles_Esten "wikilink") as [Josh
    Porter](Josh_Porter "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Calvin Tenner](Calvin_Tenner "wikilink") as
    [Calvin](Calvin "wikilink") (Uncredited)
-   [Philip Pickard](Philip_Pickard "wikilink") as
    [Philip](Philip "wikilink") (Uncredited)

### Guest cast

-   Skyler Caleb as CPU Guy (Producer\'s Cut)

\
