{{Office episode
|Title = Back From Vacation
|Image = [[Image:BackFromVacation.jpg|250px]]
|Season = [[Season 3|3]]
|Episode = 11
|Code = 3011
|Original = January 4, 2007
|Writer(s) = [[Justin Spitzer]]
|Director = [[Julian Farino]]
|Prev = [[A Benihana Christmas]]
|Next = [[Traveling Salesmen]]
}}
'''"Back From Vacation"''' is the eleventh episode of the third season of ''[[The Office (US)|The Office]] ''and the 40th overall. It was written by [[Justin Spitzer]] and directed by [[Julian Farino]]. It first aired on January 4, 2007.

==Synopsis==
[[Jim Halpert|Jim]] leads a meeting of the sales staff while [[Michael Scott|Michael]] is away on vacation in Jamaica. [[Dwight Schrute|Dwight]] turns on a portable tape recorder, explaining that Michael instructed him to record all meetings and type up transcripts. Jim jokes loudly to the recorder that Dwight is "completely nude and is holding a plastic knife to Stanley's neck", over Dwight's angered protest. [[Phyllis Lapin|Phyllis]] and [[Karen Filippelli|Karen]] join in the teasing with their own absurd statements. [[Andy Bernard|Andy]], however, takes the joke too far by shouting, "I am now chopping off Phyllis' head with a chainsaw!" He meekly adds a chainsaw sound effect.

Michael returns from his trip to [[Wikipedia:Jamaica|Jamaica]] rejuvenated, unfazed by the news that [[Hannah Smoterich-Barr|Hannah]] quit while he was away and filed a complaint against the company. [[Stanley Hudson|Stanley]] complains to Michael that his bonus check was $100 short and refuses to do any work until he gets the missing money.

Michael directs the [[Party Planning Committee]] to throw a [[Wikipedia:luau|luau]]-themed party in the warehouse to coincide with the postponed annual inventory project that evening. The committee initially objects, protesting that there is not enough time to plan.

Michael attempts to impart the lessons of his vacation, displaying to the staff a photograph of a sign in Jamaica reading: "No shirt, no shoes, no problem". An astounded [[Pam Beesly|Pam]] notices [[Jan Levinson|Jan]] in the margins of the photo and points it out to her colleagues. Michael states to the camera that Jan didn't want anyone to know they travelled together since it may get them both in trouble. He then reveals he and Jan had an affair in Jamaica.

Michael attempts to send another racy vacation photograph of himself and Jan as proof of the trip to [[Todd Packer]] but instead mistakenly sends it to "packaging". Warehouse supervisor [[Darryl Philbin|Darryl]] receives and promptly forwards the photo to several other employees, and it quickly reaches the entire office. Mortified, Michael attempts damage control, avoiding Jan's phone calls and dodging inquiries from [[Toby Flenderson|Toby]].

Karen has been upset with Jim because he has expressed objections to the prospect of Karen moving into an apartment two blocks away in his neighborhood. When Pam asks Jim what is wrong, Jim reveals that he and Karen have been dating for a month and argues that living on the same street might be "a little close." Pam convinces Jim that he is being unreasonable, and during the luau, Jim hands Karen a piece of paper that appears to be related to the house she wanted to rent. Karen is pleased, asking Jim, "Are you sure?" Karen privately thanks Pam for her help.

Pam tells the documentary crew that she was glad to help Jim, because she likes to help her friends. During inventory, however, Dwight walks in on Pam crying in private. Though Dwight appears to have good intentions, handing Pam a handkerchief and awkwardly trying to comfort her, he mistakes Pam's crying for [[Wikipedia:PMS|PMS]].

As the luau/inventory is held, Jan arrives at the office to speak with Michael in private. Without explicitly revealing whether or not she is aware of the image being sent throughout the email system, she confesses her attraction to him against all reason. She informs Michael that on her psychiatrist's advice, she has decided to give in to her [[Wikipedia:self-destructive|self-destructive]] tendencies and continue their affair. She kisses Michael and instructs him to make an excuse to leave the office party so he can meet her at his condo. In a ''Jerry Maguire'' moment, Michael, dazed and surprised at Jan's assertiveness can only utter, "Jan, You complete me." Jan looks pained for a moment, and leaves.

Back in the warehouse, Pam and [[Roy Anderson|Roy]] joke about their failed engagement and [[Kevin Malone|Kevin]] takes home a large poster printout of the oversized vacation photograph produced earlier by warehouse personnel, stating that he will hang it at home since he doesn't have a lot of art.

==Deleted scenes==
The Season Three DVD contains a number of deleted scenes.

*In a talking head interview, Pam shows some postcards from Michael's past vacations. Michael put ten dollars worth of postage on his postcard from Jamaica.
*The documentary crew meet Michael at the airport. Michael sings the "[[Banana Boat Song]]" in the car and describes events from his vacation, including buying a "chill pill" from a man at the beach. "And after I finished vomiting, I was more relaxed than I've ever been." In that scene, the car passes the front of a business building where the company name is written on the wall in big capital letters mentioning "California".
*Michael has gifts for people in the office. For Dwight, a "genuine [[Wikipedia:Rastafari movement|Rastafarian]] wig"; for Andy, a [[Wikipedia:Milli Vanilli|Milli Vanilli]] t-shirt; for Angela, a lei; for Pam, a coconut bra; and for Stanley, some "[[Wikipedia:Cannabis (drug)|ganja]]." Creed offers to get Stanley some real ganja.
*Jim asks Michael if he is stoned. Kevin thinks Michael looks like he has the munchies in the picture. Kelly does, however, and heads for chocolate wafers.
*Pam requires more evidence than seeing to believe that Michael and Jan went to Jamaica.
*Ryan notes that Michael is into [[Wikipedia:Adobe Photoshop|Photoshopping]], and shows an altered photograph where he and Michael were "in Egypt."
*Stanley won't give Phyllis the list of exchanges for inventory until he has received his bonus check. In a talking head, Phyllis says that she likes Stanley but isn't always sure he reciprocates, which is okay, because she doesn't really like him.
*Michael unsuccessfully tries to guess Jan's email password.
*Michael calls [[Wikipedia:America Online|America Online]] for a "gobbler" that would eat up emails.
*Dwight talks about [[Wikipedia:Spiderman|Spiderman]] and the internet's abilities to spread information.
*The phone rings on Stanley's desk. He does a crossword instead of picking up. Karen stops the call.
*Dwight complains to Michael about Jim's pranks while Michael was on vacation. Michael isn't interested.
*Several brief talking head interviews:
**Dwight admits to having taken one vacation, but never again. Jim had his desk shipped to him in [[Wikipedia:Roswell|Roswell]].
**Meredith explains that when a woman gets older, she'll go anywhere with anyone.
**Angela emphasizes that one's body is a temple. "You can't just whore it out."
**As a result of his skin cancer scare ("[[Michael's Birthday]]"), Kevin just hopes Jan was wearing sunscreen, although he wishes that he could rub it on her.
**Dwight lists advantages and disadvantages of the Internet.
*Jan is asked by an unnamed colleague about her vacation. She maintains the ruse that she was in Scottsdale, unaware that the colleague has a copy of Jamaica Jan Sun Princess on his computer screen. Jan sticks to her story in a talking head interview.
*To help motivate inventory, Dwight announces that he has hidden a deck of cards which can be redeemed for a soft drink. Meanwhile, Jim and Ryan play cards.
*Pam notes that inventory luau '07 is superior to disco audit '05.
*Ryan mentions that they are missing seven boxes of canary yellow copier paper. Roy takes the scanner and scans the same box seven times. "Looks good to me." Ryan begins to use Roy's new inventory technique.
*Stanley says he doesn't want to miss the show when Jan comes.
*Kelly sings as she does inventory. Michael wants her to stop, but she can't help herself.
*Dwight and Andy get into a race stacking boxes, with Jim stuck in the middle of the craziness. Upon extrication, he and Pam observe the race in amusement.
*To get a better look at an upper shelf, Meredith climbs on a shelf, which falls and traps her.
*Michael gives Stanley his bonus in the form of a $1000 [[Wikipedia:Jamaican dollar|Jamaican dollar]]-bill, and mentions that the exchange rate is 65-to-1, although he is unclear which way it converts.
*Toby says he lived in Honolulu for a year. Kevin says that that means he's been to a real luau. Angela says that she tried her best given the short notice.
*Dwight notes that Creed is one short of the black medium-ball points. Creed replaces it, having presumably stolen it. He later says once an acquaintance and he cleaned the place out. The company blamed it on some kid.

==Trivia==
*As of this episode, only two people ([[Andy Bernard|Andy]] & [[Karen Filippelli|Karen]]) from the Stamford branch remain at the Scranton office.
*Ed Helms ([[Andy Bernard|Andy]]) once again shows his musical talent, this time by learning to play the steel drum after only an hour's worth of lessons.<ref name="Sparano">Sparano, John. January 4, 2007. blog.myspace.com/index.cfm?fuseaction=blog.view&friendID=102409815&blogID=213736679 "Back From Vacation", ''MySpace.com''</ref>
*The object that Darryl finds is an iPod dock with speakers.<ref name="Sparano"/> Presumably it was lost during [[Casino Night|&quot;Casino Night&quot;]] when Darryl voiced concerns about something getting stolen during the event.
*This is the second episode of the series where Ryan has no dialogue.
*Michael states that his trip to Jamaica was his first time out of the country. Presumably, his trip to Canada in Business Trip was his second.
*Dwight removes his jacket, as if he were to give it to Pam to comfort her, but instead ties it around his waist, saying, "It's hot in here."
*Toby says he got the email with the photo several times, including from his ex-wife. This doesn't make much sense, since Toby's ex-wife doesn't work at Dunder Mifflin.
*This is one of the several times in the series where Jan says she's visiting her sister in Scottsdale to cover something up.
*Kevin has a sign on the wall next to his desk saying “DON’T EAT THE YELLOW SNOW”

==Cultural references==
* ''Jim Carrey'' is an actor best known for comedic roles.
* The ''Muppet Babies'' is a children's television program featuring characters from "The Muppet Show" (including ''Animal'') as babies.
* ''Hey mon'' is a stereotypical Jamaican greeting.
* The ''Cayman Islands'' is an island nation in the Caribbean. Many businesses are formally incorporated there due to its low taxes and light government regulation.
* A ''luau'' is a Hawaiian (not Jamaican) feast.
* The sign ''No shirt, no shoes, no problem'' is a play on the sign "No shirt, no shoes, no service" commonly seen in United States restaurants. Many health codes require shirts and shoes to be worn in food establishments. There happens to be a song titled ''No Shoes, No Shirt, No Problems'', but the similarity is most likely just a coincidence.
* Todd Packer was in ''Hot-lanta'', a nickname for Atlanta, Georgia.
* ''Montego Bay'' is a resort in Jamaica.
* ''Ice queen'' is a pejorative term for an attractive woman who rejects sexual advances.
* ''Maxim'' is a men's magazine.
* ''Booty'' is slang for the buttocks. To ''hit it'' is slang for "to have sex with that person".
* ''PG-13'' is a movie rating in the United States. PG-13-rated movies are not recommended for viewers under age 13.
* A ''T1 line'' is a type of broadband Internet connection.
* The ''Day's Inn'' is a chain of budget hotels.
* ''PMS'' (known as ''PMT'' in British English) is short for "Premenstrual syndrome", a collection of adverse symptoms tied to a woman's menstrual cycle.
* ''Scottsdale'' is a city in Arizona. The climate there is hot and arid.
* ''Identity theft'' is a crime wherein someone obtains enough information about the victim to be able to impersonate them. The motive can vary, but financial fraud is a common reason.
* ''You complete me'' is a line from the climactic moment of the film ''Jerry Maguire''.

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting Cast===
*[[Melora Hardin]] as [[Jan Levenson]]
*[[David Denman]] as [[Roy Anderson]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Phyllis Smith]] as [[Phyllis Lapin]]

===Recurring Cast===
*[[Ed Helms]] as [[Andy Bernard]]
*[[Rashida Jones]] as [[Karen Filippelli]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[David Koechner]] as [[Todd Packer]] (Voice Only)
*[[Calvin Tenner]] as [[Calvin]] (Uncredited)

==References==
<references/>
{{Season3}}
