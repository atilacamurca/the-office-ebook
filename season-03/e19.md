# Safety Training

![](SafetyTraining.jpg "SafetyTraining.jpg"){width="150"}

**\"Safety Training\"** is the nineteenth episode of the third season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 48th overall.
It was written by [B.J. Novak](B.J._Novak "wikilink") and directed by
[Harold Ramis](Harold_Ramis "wikilink"). It first aired on April 12,
2007. It was viewed by 7.7 million people.

Synopsis
--------

[Andy](Andy_Bernard "wikilink") returns to the office after several
weeks in anger management training, determined to make a fresh start
with all the Dunder Mifflin employees. Meanwhile, it\'s safety training
day in the office, and [Michael](Michael_Scott "wikilink") and
[Dwight](Dwight_Schrute "wikilink") are on a mission to illuminate the
true dangers of the workplace.

Upon his return, Andy requests that the rest of the office refer to him
as Drew. Dwight decides to shun Andy for three years. Michael takes the
staff of the upper office into the warehouse to hear
[Darryl](Darryl_Philbin "wikilink")\'s presentation on warehouse safety.
Darryl repeatedly reinforces that Michael is *not* allowed to operate
the heavy machinery in the warehouse, and Michael only irritates the
warehouse staff more by constantly interrupting Darryl, at which point,
[Lonnie](Lonnie "wikilink") insults Michael in front of everyone. In a
talking head interview, Darryl reveals that he is on crutches because
Michael pulled a ladder out from under him as a joke. The office and
warehouse staff then head upstairs, where
[Toby](Toby_Flenderson "wikilink") reads office safety tips from a book
as Michael frequently interrupts and insults him. Bored with the
presentation and of Michael, the warehouse staff depart, and Michael and
Darryl exchange words, with Darryl calling Michael\'s life and job
completely safe like \"[Nerf](Wikipedia:Nerf "wikilink").\"

Meanwhile, the entire office staff begins
[betting](Wikipedia:gambling "wikilink") on various things, a trend
started by [Kevin](Kevin_Malone "wikilink"), who is bored since [March
Madness](Wikipedia:NCAA_Men's_Division_I_Basketball_Championship "wikilink")
has ended and he has nothing to bet on. On one bet,
[Jim](Jim "wikilink") , [Kevin](Kevin "wikilink") ,
[Karen](Karen_Filippelli "wikilink") and
[Oscar](Oscar_Martinez "wikilink") bet on how many [jelly
beans](Wikipedia:jelly_beans "wikilink") are in
[Pam\'s](Pam_Beesly "wikilink") candy dish. Kevin loses to Jim and calls
this unfair, since Jim has spent \"hours\" standing at Pam\'s desk. A
look of discomfort spreads across Karen\'s face when she hears this.
Other bets include guessing how long [Kelly](Kelly_Kapoor "wikilink")
can talk about [Netflix](Wikipedia:Netflix "wikilink")
([Ryan](Ryan_Howard "wikilink") wins), how many times she says awesome
(twelve times, Pam wins), how many [romantic
comedies](Wikipedia:romantic_comedies "wikilink") she mentions (six
times, Jim wins). The final bet is whether
[Creed](Creed_Bratton "wikilink") will notice if someone switches his
apple with a potato. Karen loses every bet and gets cleaned out.

Offended by Darryl\'s comments, Michael decides to show them that
working in an office is dangerous because the stress of office life can
lead to depression and suicide. Michael plans to demonstrate the dangers
of depression by jumping off the roof and secretly landing on a
trampoline. However, when he and Dwight test the trampoline with a
watermelon, the watermelon bounces off, landing on Stanley\'s car, so
they replace the trampoline with a [bouncy
castle](Wikipedia:bouncy_castle "wikilink"). Dwight calls the office and
warehouse staff to the parking lot, where they see Michael standing on
the roof, talking about the dangers of depression. Initially, they do
not take Michael seriously, but then they find the bouncy castle and
realize that Michael is actually planning to jump, which will probably
injure him badly. Darryl talks Michael out of his fake \"suicide
attempt\", by convincing him that he is braver than Darryl.

At the end of the day, [Stanley](Stanley_Hudson "wikilink") is shocked
when he sees his car covered in watermelon from Michael\'s test.

Deleted scenes
--------------

-   After seeing *An Inconvenient Truth* \"almost twice\", Andy trades
    in his Nissan X-Terra for a Toyota Prius
-   Office workers insult the cookies Andy made.
-   Andy tries to remain calm when Kevin takes the last of the coffee.
-   Angela keys Andy\'s new car.
-   Michael summons the office staff to safety training.
-   Kelly tells Ryan he would \"look so hot\" if he worked out, which he
    responds to by saying \"Yeah, we should both work out.\" Kelly says
    to Ryan, \"Screw you!\"
-   Extension of Michael and Dwight in the hallway discussing how they
    should respond.
-   In a talking head interview, Michael shows how safety training can
    be funny by talking like Borat, however his impression of Borat is a
    poor one.
-   Dwight helps Michael calculate his trajectory from the roof onto the
    trampoline.
-   Karen loses a bet to Stanley over how often Bob Vance calls
    Phyllis - in a talking head, Stanley says that, while his co-workers
    had always annoyed him, he never realized he could make money off
    their behavior, and encourages Vance to call Phyllis any time he
    wants.
-   Several members of the office staff take turns trying to talk
    Michael down from the roof. Kevin, who is still hoping to collect on
    his 10,000:1 wager that Michael would jump, says this shows you
    should always take such a gamble because you never know what\'s
    going to happen, adding that, should *[John
    Mellencamp](wikipedia:John_Mellencamp "wikilink")* ever win an
    [Oscar](wikipedia:Academy_Award "wikilink"), his personal finances
    will take a significant turn for the better.
-   Michael explains how upper middle management is more dangerous than
    working in a coal mine.
-   Andy sees his keyed car.

Trivia
------

-   References or similarities to previous episodes:
    -   When Pam apologizes for calling Andy \"Andy,\" rather than his
        newly preferred nickname, \"Drew,\" he tells her that the
        apology is not accepted, because it was not necessary. In \"[The
        Negotiation](The_Negotiation "wikilink")\", Dwight tells Jim
        that his gratitude is not accepted because it is similarly not
        necessary.
    -   Michael claims that safety training day is going to be
        \"zoppity.\" In the previous episode, Darryl teaches Michael a
        \"black man phrase\" that is \"pippity poppity give me the
        zoppity.\"
-   According to the DVD commentary, Greg Daniels thought of the scene
    where Jim and Pam try to talk Michael down from the roof as if Jim
    and Pam were a divorced couple, speaking in code to their child
    (Michael) which makes the new wife (Karen) feel \"left out\".
-   Rainn Wilson came up with the \"unshun/reshun\" hand gesture.
-   Even though Darryl says that they\'ve never gotten through a full
    year without an accident, in [Boys and
    Girls](Boys_and_Girls "wikilink"), it is shown that the warehouse
    hasn\'t had an accident in than 936 days (far longer than a year)
    before Michael\'s shenanigans with the forklift.
-   Kelly mentions the 2003 romantic comedy\'\' [Love
    Actually](http://www.imdb.com/title/tt0314331/)\'\', a film in which
    the UK *Office* star [Martin Freeman](Martin_Freeman "wikilink") is
    featured in.
-   Michael\'s quote, \"Dwight, you ignorant slut!\" is a reference to
    the 1978 SNL skit \"Point/Counterpoint\" where Dan Akroyd calls Jane
    Curtin the same name.

In the background
-----------------

-   On Creed\'s desk (visible when he eats the potato) is a binder
    labeled *Academy of Tobacco Studies*, which is the fictitious
    tobacco lobby group in the movie *[Thank You for
    Smoking](Wikipedia:Thank_You_for_Smoking_(film) "wikilink")* which
    was directed by Jason Reitman, the son of Harold Ramis\' frequent
    collaborator, Ivan Reitman. Jason Reitman (director of *Thank You
    For Smoking* and *[Juno](Wikipeida:_Juno_(film) "wikilink")*) would
    later direct the season four episode \"[Local
    Ad](Local_Ad "wikilink")\".

Cultural references
-------------------

-   *[Anger management](Wikipedia:Anger_management "wikilink")* is a
    psychotherapy technique which helps a person control their anger.
-   *The grumpies* is a slang term for the state of being grumpy.
-   *[Shunning](Wikipedia:Shunning "wikilink")* (social avoidance) is a
    form of punishment exercised by many groups, including the
    [Amish](Wikipedia:Amish "wikilink").
-   *How\'s it hangin\'?* is a slang greeting.
-   *X-er? I hardly know her?* is a lewd joke pattern using a word
    ending in \"-er\" and reinterpreting the suffix as an elision of
    \"her\". Michael\'s choice of X is not particularly funny, but that
    doesn\'t stop him from joking about it anyway.
-   *March Madness* is the common nickname for the [NCAA Men\'s Division
    I Basketball
    Championship](Wikipedia:NCAA_Men's_Division_I_Basketball_Championship "wikilink"),
    an annual basketball tournament which takes place in March to
    determine the best college basketball team in the United States. It
    is the subject of widespread sports gambling, even among people
    otherwise not interested in basketball.
-   *[Nerf](Wikipedia:NERF "wikilink")* is a brand of toys made from
    soft foam. Darryl\'s \"shenanigans\" remarks consist mostly of
    invented insults.
-   *[Men\'s Wearhouse](Wikipedia:Men's_Wearhouse "wikilink")* is a
    chain of mens business clothing stores.
-   A *[big-box store](Wikipedia:Big-box_store "wikilink")* is a
    warehouse-sized store which sells large items (or small items in
    large quantities).
-   *[Borat](Wikipedia:Borat "wikilink")* is a character portrayed by
    comedian [Sacha Baron
    Cohen](Wikipedia:Sacha_Baron_Cohen "wikilink"). He is a journalist
    from [Kazakhstan](Wikipedia:Kazakhstan "wikilink") whose humor
    derives from his violation of social norms and expression of
    distasteful opinions. Appending *Not!* to a sentence in a thick
    accent is one of Borat\'s catch phrases. (Appending *Not!* was a
    catch phrase from the 1980s.)
-   *[Netflix](Wikipedia:Netflix "wikilink")* is an online movie rental
    service.
-   On his late night program, television host *[David
    Letterman](Wikipedia:David_Letterman "wikilink")* would regularly
    throw objects off a five-story building.
-   *Bingo!* is an exclamation of success, usually associated with
    hitting a target either literally or metaphorically (such as
    guessing a correct answer).
-   *[Watermelons](Wikipedia:Watermelon "wikilink")* were often used in
    racist caricatures of African Americans. This explains Michael\'s
    desire to defend himself against accusations of committing a hate
    crime.
-   A *[bouncy castle](Wikipedia:Inflatable_structure "wikilink")* (also
    known as a *moon bounce*) is an inflatable structure designed for
    children to bounce around in.
-   *Jane, you ignorant slut!* was a catch phrase from the *[Weekend
    Update](Wikipedia:Weekend_Update "wikilink")* segment of sketch
    comedy program *[Saturday Night
    Live](Wikipedia:Saturday_Night_Live "wikilink")*. Michael directs
    the insult at Dwight.
-   *[John Mellencamp](Wikipedia:John_Mellencamp "wikilink")* is a rock
    singer-songwriter.
-   The *[Repliee
    Q1Expo](http://news.bbc.co.uk/1/hi/sci/tech/4714135.stm)* is a
    lifelike female android.
-   *[Braveheart](Wikipedia:Braveheart "wikilink")* is an
    Academy-award-winning historical epic.

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting Cast

-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Recurring Cast

-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Rashida Jones](Rashida_Jones "wikilink") as [Karen
    Filippelli](Karen_Filippelli "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Patrice O\'Neal](Patrice_O'Neal "wikilink") as [Lonnie
    Collins](Lonnie_Collins "wikilink")
-   [Karly Rothenberg](Karly_Rothenberg "wikilink") as
    [Madge](Madge "wikilink")
-   [Calvin Tenner](Calvin_Tenner "wikilink") as
    [Glenn](Glenn "wikilink") (Uncredited)
-   [Philip Pickard](Philip_Pickard "wikilink") as
    [Philip](Philip "wikilink") (Uncredited)

\
