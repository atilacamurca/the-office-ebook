{{Office episode
|Title      =The Coup
|Image      =[[Image:Thecoup.jpg|250px]]
|Season     =[[Season 3|3]]
|Episode    =3
|Code       =3002
|Original   =October 5, 2006
|Writer(s)  =[[Paul Lieberstein]]
|Director   =[[Greg Daniels]]
|Prev       =[[The Convention]]
|Next       =[[Grief Counseling]]
}}
'''"The Coup"''' is the third episode of the third season of ''[[The Office (US)|The Office]] ''and the 31st overall. It was written by [[Paul Lieberstein]] and directed by [[Greg Daniels]]. It first aired on October 5, 2006.

==Synopsis==
[[Pam Beesly|Pam]] supervises microwave popcorn in progress in the breakroom as [[Michael Scott|Michael]] complains of dire hunger. He pulls a DVD copy of ''Varsity Blues'' from his office safe, and calls it the only cure for Monday blues. Pam explains in an interview that Michael began Movie Monday with training videos, and continued the tradition with an ill-received medical video, likely a birthing video, six repeats of a single episode of ''Entourage'' and finally a feature film in thirty-minute increments. Prior to the screening, Michael selects an excited [[Kevin Malone|Kevin]] to recite previous action in the film, to bring the audience up to speed.

[[Angela Martin|Angela]] states her opposition to Movie Monday, and indeed is the only staffer not in attendance when [[Jan Levinson|Jan]] pops in to the branch unexpectedly. Jan scolds Michael despite his explanation that productivity increases following the screenings as employees must work harder due to the lost time.

In interviews, Angela and [[Dwight Schrute|Dwight]] both note that Jan seems to have a vendetta against Michael for his choosing [[Carol Stills|Carol]] over her, and fear it might affect the prospects of the [[Scranton branch]]. Angela summons Dwight to the breakroom, where they speak in their traditional manner with their back to each other. Angela tells Dwight she fears for their jobs under Michael's leadership, and tells Dwight to speak to Jan about taking over the branch. In the parking lot, Dwight reaches Jan on her cell phone one hour out of Scranton. She does not want to speak, but listens when he says it concerns Michael. Dwight instructs her to shop at the nearby Liz Claiborne outlet store — which he has determined to be a favorite brand of hers — until he can reach her. In order to leave, he tells Michael he is visiting a new dentist who works far away, so he will be gone for about three hours.

Jan and Dwight sit at a nearby diner where he orders two plates of waffles and begins to propose taking over the branch. Pouring syrup maniacally and proceeding to eat in a deliberate way for effect, he states that he could do the job better by firing half the staff, to whom he swears no loyalty since he doesn't care about any of his co-workers. As the meeting ends and Dwight sloppily devours his meal in front of her, he urges Jan to check out a new Ann Taylor outlet store nearby, as he has noticed she is fond of the brand's earrings. Jan calls Michael, relays the entire meeting and demands he get his branch under control.

Meanwhile, the [[Stamford branch]] has its own distraction, the computer war game ''Call of Duty'', being played under the guise of a team-building exercise. New to the game, [[Jim Halpert|Jim]]'s play is poor and he mentions video games weren't played at his former office. He does, however, reveal a prank in which he and Pam hummed the same high note in the hopes of Dwight scheduling an appointment to have his hearing checked, an activity Pam dubbed "pretendinitis".

Back in Scranton, Pam shops online for new clothes at [[Kelly Kapoor|Kelly]]'s insistence. Upon the clothes' arrival, Kelly demands through chanting that Pam perform a lunchtime fashion show. Though Kelly, [[Phyllis Lapin|Phyllis]] and [[Meredith Palmer|Meredith]] compliment her on her clothes, Pam claims "it's too much" and considers returning them. [[Roy Anderson|Roy]] enters the breakroom and tells Pam she looks pretty. Kelly asks if that is his third soda of the day, to which he doesn't respond.

In Stamford, Jim manages to kill a player, but the thrill is short-lived as he is told he's killed a teammate on the German side. As another game starts up, [[Josh Porter|Josh]] calls Jim and [[Andy Bernard|Andy]] for an impromptu huddle about game strategy. Andy blames defeat on "the new guy," and as Jim reveals his use of a sniper rifle, both co-workers react in disgust, with Josh scolding Jim for using the weapon on an inappropriate map, and Andy claiming he is going to shoot Jim "for real."

Upon Dwight's return, a calm but clearly offended Michael initially tries to make him admit he didn't go to the dentist by feeding him M&M's and asking for the dentist's name, to which Dwight replies "Crentist." When Michael notes how the name sounds strangely like "dentist," Dwight reasons it may have been why he chose the profession. Later, Michael tells Dwight that Jan has called to demote him to Dwight's position as Assistant (to the) Regional Manager, and Dwight should be expecting a call from Corporate to take over as acting Regional Manager. Announcing it to the office, multiple people question Dwight's selection, and Kevin even expresses concern about Michael possibly losing his condo. Angela is the only one to congratulate Dwight, though later, when she says in private that they could make a difference together, he corrects her to say ''he will'' make a difference. Though Angela seems upset at first, she then smirks as Dwight offers her to be in charge of the female staff.

Michael eventually reveals the trick when Dwight casually refuses the keys to Michael's Corporate Chrysler Sebring convertible and insults the vehicle due to its poor gas mileage and impracticality in Scranton's climate. Taking the remark personally, Michael blows up at Dwight after he refuses to take back what he said, especially after Dwight previously stated that he loved the car (a lie meant to appease Michael). In turn, Dwight begins to beg for his job and grovels at the floor for Michael's forgiveness, offering to do his [[Dwight and Michael's Laundry Agreement|laundry for a month]], then a year, to which Michael responds he has a laundry machine. Michael forces him to "Hug it out, bitch," which, Michael tells the camera, is what men say and do to each other after a fight (a reference to Michael's obsession with ''Entourage''). Dwight returns quickly to being Michael's loyal right-hand man at the next Movie Monday, as Angela looks on in disappointment.

Pam decides to return to dressing her usual way at work when [[Creed Bratton|Creed]] walks over to her desk, blatantly looks down her shirt and refuses to leave the area. Pam covers up with a sweater and decides to keep the clothes for outside of the office.

Jim still can't get the hang of the game, and [[Karen Filippelli|Karen]] catches him jumping in a corner trying to shoot with a smoke grenade. Karen tells the camera to "look at how cute he is," before telling Jim to turn his character around. Requesting any last words, she shoots his character point blank, causing "You killed Jim Halpert" to appear on her screen. Jim then turns to the camera and whispers "psychopath." At the end of the day, Jim pretends to throw a grenade towards her desk when leaving, to which she tosses a few paper clips in the resulting explosion. They laugh, and after he leaves, she still gives a slight wave and continues to stare at the exit.

Deciding he still harbored resentment towards Dwight, Michael forces him to solemnly stand atop a box with a "LIAR" sign dangling from his neck, in addition to one year of indentured servitude as a launderer.

==Deleted scenes==
The Season Three DVD contains a number of deleted scenes.
*Dwight is sent to bring Angela to Movie Monday, but Angela refuses to go and suggests to Dwight that he would be a better manager.
*In a talking head interview, Dwight describes the conflict between his paternal Schrute blood (which is loyal) and his maternal Manheim blood (which knows when to cut and run).
*After Jan leaves Michael's office, Dwight attempts to give Michael advice on cutting costs, suggesting that he fire Meredith and charge Creed rent.
*In a talking head interview (illustrated with footage of Creed in the office at night), Creed explains that he sleeps under his desk four nights a week and spends the remaining three nights in Toronto in order to milk the welfare state. "They don't know about this job up there."
*Michael tries to resume watching the movie, but nobody is interested. Angela shoots Dwight a look of impatience.
*Michael tediously recaps an episode of ''[[Wikipedia:Grey's Anatomy|Grey's Anatomy]]'' for Pam. His summary is interrupted by a phone call. It's Jan. Michael warily returns to his office. In a talking head interview, Pam admits that she once faked a call from Jan to get out of Michael's synopsis of an episode of ''[[Wikipedia:NCIS (TV series)|NCIS]]'', and Michael has been suspicious ever since.
*Pam's computer screen [[Wikipedia:screensaver|blanks due to inactivity]], prompting her to use it as a mirror to see how her new sweater fits. Kevin drops off some documents, then stares at her chest and says, "Nice. Wow!" Pam uses a [[Wikipedia:binder clip|binder clip]] to close up her neckline.
*Roy admits the only time he told Pam how nice she looked was at their senior prom.
*Michael tries to come up with a career that rhymes with his own last name.
*Dwight tells Michael of his plans for changing the office.
*In a talking head interview, Phyllis considers quitting if Dwight is put in charge. Her fiancé told her that her new job is to be Mrs. Bob Vance.
*In a talking head interview, Meredith considers telling Dwight that she has a dependent child to avoid being fired. She undoes a button on her blouse.
*In a talking head interview, Creed doesn't believe working for Dwight will be any different.
*In the video game, Jim kills Karen's character. He turns to gloat, only to discover that Karen isn't playing; she's talking with Jan. Jan comes over to Jim's computer and playfully presses a key that causes Jim's character to fall to its death. As she leaves for a meeting with Josh, she tells Jim, "Remind me to tell you what Dwight said to me earlier. You're going to get a big kick out of it." In a talking head interview, Jim acknowledges that the work environment in Stamford is very different from Scranton. "It's not bad."

==Trivia==
[[File:Call of Duty|thumb|300px|right|Call of Duty!]]
* One of the producers at ''The Office'' installed ''Call of Duty'' on everybody's computers, and they play it at work.<ref>[http://www.inc.com/magazine/20070801/he-knows-where-you-work.html He Knows Where You Work], Inc.com</ref> The game made cameo appearances in the earlier episode "[[Email Surveillance]]" in Jim's house. Although Jim owns two versions of the game at home, he admits, "I really suck at it."

* Dwight calls Jan's cell phone. In the episode "[[Health Care]]", Jan tells Dwight not to call her cell phone again.
* When Michael says, "And I guess I will be Assistant Regional Manager", Dwight corrects him to "Assistant to the Regional Manager", which is ironic because Michael corrected him on this for years.
* Dwight must stand on a desk in front of the whole office wearing a sign that says "LIAR" on it. In "[[Livin' the Dream]]", Dwight will stand on the desk to exclaim to the whole office that he is permanently promoted to Regional Manager.

* When Dwight returns from his meeting, Michael asks him if he wants an M&M. Dwight replies he's "stuffed," which would be impossible since he was supposed to be at the dentist.

* A close inspection of the computer screens reveals that Karen's username in ''Call of Duty'' is KarentheJimSlayer. Andy's is Here Comes Treble, presumably named for [[Here Comes Treble|his a cappella group]]. Jim's is simply Jim Halpert.

==Cultural references==
* ''[[Wikipedia:Varsity Blues (film)|Varsity Blues]]'' is a 1999 film about a high school football team. When a quarterback is ''[[Wikipedia:Quarterback sack|sacked]]'', he is tackled before he can throw the ball. The quarterback is a vulnerable position, and a sack can cause injury, as it did in the movie.
* ''[[Wikipedia:Entourage (TV series)|Entourage]]'' is a television comedy about a young actor and the friends who follow him around.
* ''[[Wikipedia:Milk Duds|Milk Duds]]'' are a candy associated with movie theaters.
* ''[[Wikipedia:Call of Duty|Call of Duty]]'' is a video game set in World War II. The town of ''[[Wikipedia:Carentan|Carentan]]'' in France is a multiplayer map in ''[[Wikipedia:Call of Duty 2|Call of Duty 2]]''.
* ''[[Wikipedia:Liz Claiborne|Liz Claiborne]]'' and ''[[Wikipedia:Ann Taylor (retail chain)|Ann Taylor]]'' are retail chains which sell women's clothing. An ''[[Wikipedia:Outlet store|outlet store]]'' sells irregular, out-of-date, or simply surplus clothing from a major manufacturer at a discount. They are typically located far from major cities. The [http://www.premiumoutlets.com/thecrossings/ Crossings Premium Outlets] in [[Wikipedia:Tannersville, Pennsylvania|Tannersville]] has both a Liz Claiborne and an Ann Taylor outlet store. It's about 45 minutes outside of Scranton along the most direct route between Scranton and New York City. However, Jan was headed to Stamford, which would have taken her along a different route.
* ''[[Wikipedia:M&M's|M&amp;M]]'' is a brand of chocolate candy(Much like Smarties).
* ''Let's hug it out, bitch'' is a line from an episode of ''Entourage''. [http://www.youtube.com/watch?v=0ZvarRe-XVQ Watch it here].
* In a deleted scene, Michael recaps an episode of the television medical drama ''[[Wikipedia:Grey's Anatomy|Grey's Anatomy]]''.

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting Cast===
*[[Melora Hardin]] as [[Jan Levenson]]
*[[David Denman]] as [[Roy Anderson]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Phyllis Smith]] as [[Phyllis Lapin]]

===Recurring Cast===
*[[Rashida Jones]] as [[Karen Filippelli]]
*[[Ed Helms]] as [[Andy Bernard]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Charles Esten]] as [[Josh Porter]]
*[[Ursula Burton]] as [[Hannah Smoterich-Barr]] (Deleted Scenes)

==References==
<references/>
<br />

{{Season3}}
