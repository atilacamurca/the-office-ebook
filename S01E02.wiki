---
title: Diversity Day
---

{{Office episode
|Title       =Diversity Day
|Image       =[[Image:Diversity Day.jpg]]
|Season      =1
|Episode     =2
|Code        =1002
|Original    =March 29, 2005
|Writer(s)   =[[B.J. Novak]]
|Director    =[[Ken Kwapis]]
|Prev        =[[Pilot]]
|Next        =[[Health Care]]
}}'''Diversity Day''' is the second episode of the [[Season 1|first season]] of The Office and the 2nd episode overall. It was written by B.J. Novak and directed by Ken Kwapis. It originally aired on March 29, 2005. It was viewed by 6 million people.

==Synopsis==
[[Michael Scott|Michael]] explains in his talking head that it's Diversity Day and that a guest speaker is at [[Dunder Mifflin Scranton]] to talk about diversity in the workplace. Michael claims that he's wanted this for awhile. Corporate mandated it without Michael having to talk to them about it, though. But Michael still believes it's very important.

[[Jim Halpert|Jim]] is trying to make a sales call when [[Dwight Schrute|Dwight]] begins noisily shredding paper. Jim turns off Dwight's surge strip. In retaliation, Dwight disconnects Jim's phone call. Jim explains to the camera that this client makes up 25% of his commission each year. He brings a small bottle of champagne to the office with him to celebrate.

While Pam plays freecell, Michael walks out of his office, then backs up when he sees the corporate representative is heading out of the conference room. He makes sure to walk out at the same time as the other man and make a point of singling out Oscar Martinez as a friend of his. Jim has made contact with his client just in time for Michael to order him into the meeting. Jim is forced to hang up again.

When the meeting begins, Michael tries to take over by talking about how he doesn't even see Stanley as a black man. The representative explains that's not the point here. No one is expected to be color-blind. That's just fighting ignorance with more ignorance.

Then Michael decides everyone should go around and name an ethnicity that he or she finds sexually attractive. Dwight claims he's attracted to whites and Indians. The representative does not think that's a good start. He asks Michael for permission to run this session himself and suggests it would be easier if Michael sits down.

He explains that he asked each of them to write down an incident that offended them. He would like volunteers to assist him. Dwight asks that they don't do anything involving gays.

The representative attempts to take control again from Michael who asks his name. When he reveals that it's [[Mr. Brown]], Michael thinks it's a trick to see if he will call a black man Mr. Brown. Brown assures him that is his name. He goes on to explain that almost everyone wrote down the same incident and asks if everyone is familiar with the Chris Rock routine.

Michael complains to the camera how Chris Rock can do a hilarious routine that makes everyone laugh, but he gets in trouble from corporate when he does the same routine. Michael volunteers to be the joker, Brown doesn't want the person who was the original offender in the same position. Kevin offers to be the joke teller, but doesn't tell it to Michael's liking. Michael interrupts with a loud, profane rendition of the routine before Brown stops him.

While Jim listens to his phone ring, helpless to answer it, Brown sets up an acronym for Hero: Honesty, Empathy, Respect and Open-Mindedness. Dwight adds additional criteria that essentially describes a super-hero, not a hero. Brown passes out forms, asking everyone sign it. Michael refuses because he didn't learn anything.

Privately, Brown explains that they both know he is here because of Michael's behavior. Michael argues that this office is racially advanced enough that it really doesn't need a diversity lecture. Brown tells him it does because it's Michael that caused the need for it. He only needs Michael's signature, but put the whole office through the seminar because he didn't want Michael to feel singled out. Michael feigns offense that this wasn't really about diversity then.

Michael signs the form and then jokes after the fact that he signed it "Daffy Duck".

In the meantime, Jim has tried to call back his client and has to leave a message.

Michael comes out, tears up the pledge, and criticizes Mr. Brown's lack of an "Oprah moment". He wants everyone to get as much done before lunch as possible.

Jim gets nothing but a busy signal right before Michael forces them all into the conference room again.

The HR representative, [[Toby Flenderson]] asks if they are going to sit Indian style. Michael orders him out for being offensive. Then he shows the staff a video announcing his new initiative for Diversity Tomorrow and quotes Abraham Lincoln as saying, "If you are a racist, I will attack you with the North".

[[Kelly Kapoor]], a young Indian woman, asks to be excused for a customer meeting. Michael lets her go. He introduces himself and describes his ethnic breakdown, including that he is part Native American. Oscar asks him what part he is. Michael asks about Oscar's ethnicity. Oscar explains his parents were born in Mexico, moved here and he was born. Michael wants to know if there's something he would like to be called besides Mexican. Oscar doesn't understand why Michael would think Mexican is offensive.

Jim, hearing his phone ring, runs out to grab it, but is too late.

Michael hands out cards with different ethnicities on them. The employees are to take a card and then try to guess their pretend ethnicity by how their co-workers treat them.

Stanley is inadvertently given Black. Dwight is Asian but can't guess it based upon people telling him they like his people's food or eat lots of rice. Michael, who's chosen Martin Luther King for himself, insists Pam stir up the pot by getting extreme. Pam hesitantly suggests to Dwight that his people aren't very good drivers. Dwight angrily asks if he's a woman.

Jim goes to Pam's desk where Ryan is watching an online sketch. Ryan mentions how cute she is. Jim agrees, but that Pam is engaged. Ryan says he was talking about the girl in the sketch.

Kevin is Italian and is trying to get Angela to guess that she's Jamaican by using the word "mon" and talking about beaches. Angela won't go any further when he asks if she wants to get high.

Irritated that no one is trying very hard to be offensive, Michael immediately jumps on Kelly when she returns from her meeting by throwing himself into a really offensive Indian stereotype until Kelly slaps him. Michael gets visibly upset. He tells the employees how now Kelly knows what it's like to be a minority.

Jim finally gets ahold of his client who was able to close his sale with Dwight instead who gave him a discount. Jim puts the mini champagne bottle on Dwight's desk.

Michael continues mumbling about Mr. Brown and corrects Stanley on how to pronounce the word collard in collard greens. When 5 PM arrives, Pam is asleep on Jim's shoulder. He smiles at the camera, then wakes her up so they can go.

He tells the camera that today was a pretty good day.

==Cultural references==
* ''Diversity'' is a term used in corporate culture to refer to multiple races, cultures, and customs being represented within a company's workforce.
* [[Michael Scott|Michael]]'s exclamation "Celebrate good times, come on!" comes from the chorus of the 1980s song "[[Wikipedia:Celebration (song)|Celebration]]" by the group [[Wikipedia:Kool & the Gang|Kool &amp; the Gang]].
* ''[[Wikipedia:Daffy Duck|Daffy Duck]]'' is a cartoon character.
* [[Toby Flenderson|Toby]] jokes about "sitting in a circle [[Wikipedia:Sitting#Cross-legged|Indian style]]." This is a typical campfire arrangement (and terminology) used by children at camp.
* ''[[Wikipedia:Namaste|Namaste]]'' (with the accompanying hand gesture) is a traditional Indian greeting.
* ''I Have a Dream'' is the refrain of a famous speech by civil rights activist [[Wikipedia:Martin Luther King, Jr.|Martin Luther King, Jr.]]
* The ''[[Wikipedia:Melting pot|melting pot]]'' is an American concept for describing how multiple cultures are assimilated into a single combined culture. The more contemporary concept of ''diversity'' emphasizes how the cultures retain their identity rather than lose it via assimilation (with the new analogy of a salad bowl).
* ''Googy-googy'' is a made-up term.
* In a deleted scene, [[Dwight Schrute|Dwight]] mentions ''[[Wikipedia:James Earl Jones|James Earl Jones]]'', a noted African-American actor and voice-over performer.
* In a deleted scene, Michael imitates the character of Colonel Klink from the 1960s sitcom "[[Wikipedia:Hogan's Heroes|Hogan's Heroes]]". The sitcom is set in a German POW camp. Hogan is the leader of a group of Allied POWs whose activities regularly frustrate camp commandant Klink. Michael compares Jan to General Burkhalter (Klink's superior) and Dwight to Sergeant Schultz (Klink's bumbling aide).
* In a deleted scene, Michael (pretending to be [[Wikipedia:Martin Luther King, Jr.|Martin Luther King, Jr.]]) talks about marching to the Washington Monument, the Lincoln Memorial and the U.S. Mint. King did [[Wikipedia:March on Washington for Jobs and Freedom|march from the Washington Monument to the Lincoln Memorial]], but not the U.S. Mint.

==Quotes==
:''see [[Diversity Day Quotes]]''

==Trivia==
* Originally, producer [[Greg Daniels]] wasn't sure where to use [[Mindy Kaling]] on screen in the series until the point came in this episode's script when Michael needed to be slapped by a minority. "Since then, I've been on the show," Kaling says. Daniels also loves the scene where [[Pam Beesly|Pam]] falls asleep on [[Jim Halpert|Jim]]'s shoulder.
* [[Larry Wilmore]] ([[Mr. Brown]]) is a writer for the show. At the table-read for this show, they hadn't cast the part yet and Daniels just had Wilmore read for the role to fill in. After the read, Daniels thought he was perfect for the role. However, because of stipulations with the Screen Actors Guild, producers still had to have Wilmore formally audition with other actors for the role.
* [[Michael Scott|Michael]]'s attempts to upstage Brown echoes [[David Brent]]'s treatment of a visiting consultant in the episode [[Training]] from [[The Office UK]].
* The diversity training company's name (and Michael's response to it) went through a number of changes before Diversity Today was settled on, including Diversity 360 (Michael would rename it Diversity 365) and Diversity 2000 (Michael would rename it Diversity 3000).
* The routine Michael imitates is "Niggas vs. Black People" from Chris Rock's 1996 HBO special ''Bring the Pain''.
* Michael's diversity exercise with the index cards is similar to one sometimes done in actual diversity training programs.
* This episode was nominated for the Writers Guild of America Award for "Best Episodic Comedy".
* These are the roles played by the [[Dunder Mifflin Paper Company|Dunder Mifflin]] employees in Michael's diversity exercise:
{|border="1"
|-bgcolor="#ccccff"
!Character
!Race/nationality
|-
|[[Michael Scott|Michael]]
|Martin Luther King Jr.
|-
|[[Dwight Schrute|Dwight]]
|Asian (and "Dwight" in a deleted scene)
|-
|[[Stanley Hudson|Stanley]]
|Black (by coincidence)
|-
|[[Angela Martin|Angela]]
|Jamaican
|-
|[[Kevin Malone|Kevin]]
|Italian
|-
|[[Meredith Palmer|Meredith]]
|Brazilian
|-
|[[Pam Halpert|Pam]]
|Jewish
|-
|[[Oscar Martinez|Oscar]]
|Eskimo
|-
|[[Creed Bratton|Creed]]
|Puerto Rican
|-
|[[Phyllis Lapin|Phyllis]]
|Haitian (deleted scene)
|-
|[[Devon White|Devon]]
|West Nile (deleted scene)
|}

* Although it's true that "insect" would not have made sense either, another anagram for "incest" is "nicest", which would have worked well in this context.
* Wilmore appears as Mr. Brown again in the [[Season 3|Season Three]] episode "[[Gay Witch Hunt]]", giving the [[Dunder Mifflin Stamford|Stamford branch]] the Diversity Today sensitivity training. It is implied he was brought in because of events in Scranton.
* Michael says, "Don't take my word for it. Let's take a look at the tape." But the tape consists merely of Michael talking.
* As Dwight gives his explanation of heroes, Jim's face goes through at least four expressions.

==Deleted scenes==
[[Image:Devon's race.JPG|thumb|300px|[[Devon]]'s forehead card.]]
Included on the [[Season 1 DVD]] were various deleted scenes:

* [[Michael Scott|Michael]] and [[Mr. Brown]] discussing erasing racism.
* [[Dwight Schrute|Dwight]] telling the camera he's wasting busy sales hours because of the meeting.
* A scene at the meeting where Michael creates his own acronym for sensitivity (inclusion, new attitude, color blind, expectations, sharing, and tolerance) which ends up spelling I.N.C.E.S.T. After [[Pam Beesly|Pam]] points this out, he tries to explain how the term works in context.
* Pam in a talking head saying Michael could've just used insect as an acronym, though it still wouldn't have made sense.
* [[Ryan Howard|Ryan]] mentioning his childhood neighbor who played pro baseball "before the leagues were integrated" and his stories, though Michael interrupts him and asks him to leave the meeting because he sees this information as irrelevant. He tells Ryan to check if anyone parked in the handicap spots and tries to talk about how that ties in with the new attitude part of his acronym, though Mr. Brown stops him from continuing.
* Pam trying to think of good things about Michael.
* Ryan in the parking lot looking at the empty handicap parking spots.
* An extended scene with Michael showing his Daffy Duck signature.
* Jim helping Pam in her game of solitaire, but Dwight trying to stop them as the game should only be played alone.
* Dwight and Michael filming the short sensitivity video.
* Michael talking about marching on Washington "back in the day."
* [[Devon White|Devon]] smoking on a balcony with "West Nile" on his forehead card.
* When Dwight asks if he's a woman, Jim changes his forehead card from Asian to "Dwight" and continues asking what race he is, though Angela eventually tells him what his card says.
* Pam telling the camera there was once an ethnic festival in Scranton.
* Reactions from the office workers after Kelly slaps Michael.

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Recurring Cast===
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Phyllis Smith]] as [[Phyllis Lapin]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]] (Uncredited)
*[[Devon Abner]] as [[Devon White]] (Only in Deleted Scenes)

{{Season1}}
