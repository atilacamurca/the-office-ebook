#!/bin/bash

#pandoc -f mediawiki -o S01.md S01.wiki
#pandoc -f mediawiki -o S01E01-pilot.md S01E01-pilot.wiki
#pandoc -f mediawiki -o S01E02.md S01E02.wiki

pandoc --toc-depth=1 -o the-office.epub \
    --epub-cover-image=cover.png \
    title.txt Season_1.md Pilot.md Diversity_Day.md Health_Care.md \
    The_Alliance.md Basketball.md Hot_Girl.md \
    season-02/Season_2.md season-02/The_Dundies.md \
    season-02/Sexual_Harassment.md season-02/Office_Olympics.md \
    season-02/The_Fire.md season-02/Halloween.md season-02/The_Fight.md \
    season-02/The_Client.md season-02/Performance_Review.md \
    season-02/Email_Surveillance.md season-02/Christmas_Party.md \
    season-02/Booze_Cruise.md season-02/The_Injury.md \
    season-02/The_Secret.md season-02/The_Carpet.md \
    season-02/Boys_and_Girls.md season-02/Valentines_Day.md \
    season-02/Dwights_Speech.md season-02/Take_Your_Daughter_to_Work_Day.md \
    season-02/Michaels_Birthday.md season-02/Drug_Testing.md \
    season-02/Conflict_Resolution.md season-02/Casino_Night.md \
    season-03/s03.md season-03/e*.md \
    season-04/s04.md season-04/e*.md \
    season-05/s05.md season-05/e*.md \
    season-06/s06.md season-06/e*.md \
    season-07/s07.md season-07/e*.md \
    season-08/s08.md season-08/e*.md \
    season-09/s09.md season-09/e*.md
