# Health Care

**\"Health Care\"** is the third episode of the [first
season](Season_1 "wikilink") of \'\'[The
Office](The_Office_(US) "wikilink") \'\'and the 3rd overall. It was
written by Paul Lieberstein and directed by Ken Whittingham. It first
aired on April 5, 2005. It was viewed by 5.8 million people.

Synopsis
--------

Because he is in charge of choosing a health plan for his employees,
[Michael](Michael_Scott "wikilink") decides that makes him the
equivalent of their doctor.

He wants to choose the Gold Plan which [Jan](Jan_Levinson "wikilink")
explains that even she doesn\'t have. With downsizing possible, it\'s
important that Michael chose an inexpensive plan for his employees.
Michael, wanting to be liked, isn\'t enthusiastic about cutting their
benefits. Jan tells him that part of being a manager involves giving
unpleasant news. This is something he has to do.

Michael tries to give the job to [Jim](Jim_Halpert "wikilink") who
prefers he focus on his own job. He isn\'t planning to stay at Dunder
Mifflin forever so doesn\'t really believe he should be acting the part
of a manager. He suggests [Dwight](Dwight_Schrute "wikilink") who
accepts wholeheartedly, setting up a temporary workspace in the
conference room.

Dwight, who doesn\'t have any health problems and believes in cutting
costs, picks a dirt cheap plan with virtually no benefits. To avoid his
employees\' complaints, Michael hides out in his office, pretending to
have calls that Pam can tell he doesn\'t. After Jim and
[Pam](Pam_Beesly "wikilink") try to get Dwight to have more compassion
toward his co-workers, the employees gather outside the men\'s room and
confront Michael about the plan.

Michael orders Dwight to make changes and promises the crew a surprise
at the end of the day. In the conference room, Dwight orders everyone to
write down what diseases they need covered. Jim explains that such
information is confidential. Michael leaves the office to figure out
what the surprise should be, considering Atlantic City or a trip to a
coal mine.

In the meantime, Jim and Pam have fun making up fake diseases to put on
the forms. When Dwight recognizes the prank, he accuses Jim who refuses
to confess. Jim locks Dwight in the meeting room.

Dwight calls Jan, identifies himself as Assistant Manager and demands
the ability to fire Jim. Jan tells him he\'s not a manager at all.
Dwight explains that Michael put him in charge of the health plan and
isn\'t in the office right now. She lectures Dwight, tells him to never
call her on her cell phone again and to have Michael call her when he
gets back.

When Michael returns, he throws ice cream sandwiches at the staff. They
point out that this isn\'t a very good surprise, considering the day
they\'ve been having. Michael assures them it isn\'t the surprise. He
goes into his office and doesn\'t come out.

Meanwhile, Dwight goes through the questionnaires people submitted and
verifies that [Meredith Palmer](Meredith_Palmer "wikilink") needs
coverage for an inverted vagina, Kevin for anal fissures and Angela for
dermatits. Finally he picks a plan so cheap that
[Oscar](Oscar_Martinez "wikilink") likens it to a pay decrease.

At the end of the day, Michael is confronted again by the employees,
wanting to know why he put Dwight in charge. Michael pretends not to
know that Dwight picked a bad plan and that he did so against his
orders. Since it\'s the end of the day, there\'s nothing to be done
about it now.

They ask about their surprise. While Michael still insists he has one,
they walk out as he continues to kill time trying to think of one. Then
Dwight tells him Jan wants to him to call her back.

Cultural references
-------------------

-   [Michael](Michael_Scott "wikilink")\'s nickname for Pam,
    \"Pama-lama-ding-dong\", is a pun on the song title
    *Rama-lama-ding-dong*, the 1958 hit song by [The
    Edsels](Wikipedia:The_Edsels "wikilink").
-   Michael\'s exclamation \"Makin\' copies!\" is the catch phrase from
    a recurring character portrayed by Rob Schneider on the sketch
    comedy program *[Saturday Night
    Live](Wikipedia:Saturday_Night_Live "wikilink")*.
-   Michael mutters *[Information
    Superhighway](Wikipedia:Information_Superhighway "wikilink")*, a
    term popular in the early 1990s (but now obsolete) to describe the
    future of the nascent internet.
-   *[Trading Spouses](Wikipedia:Trading_Spouses "wikilink")* was a
    reality television program in which two families of different
    socio-economic class trade wives or husbands for one week.
-   Michael describes [Dwight](Dwight_Schrute "wikilink")\'s potential
    failure as a *[strike two](Wikipedia:Strikeout "wikilink")*, a
    reference to the sport of baseball.
-   The *[deductible](Wikipedia:Deductible "wikilink")* in an insurance
    plan is the monetary amount the insurance does not cover. For
    example, given a \$200 expense with a \$50 deductible, the insurance
    pays \$150 and the insured person pays \$50.
-   *How\'s tricks?* is a casual greeting. [Click here to see the origin
    of the
    phrase.](http://www.randomhouse.com/wotd/index.pperl?date=20010103)
-   *[Atlantic City](Wikipedia:Atlantic_City,_New_Jersey "wikilink")* is
    a city in nearby [New Jersey](Wikipedia:New_Jersey "wikilink") known
    for its casinos and beach, about three and a half hours\' drive from
    Scranton.
-   *[Whose Line Is It
    Anyway?](Wikipedia:Whose_Line_Is_It_Anyway? "wikilink")* is an
    improvisational comedy program.
-   *[Count Chocula](Wikipedia:Count_Chocula "wikilink")* is a
    chocolate-flavored children\'s cereal.
-   *Mork from Ork* is the name of Robin Williams\' character in the
    1970s television comedy *[Mork and
    Mindy](Wikipedia:Mork_and_Mindy "wikilink")*. *Na-nu na-nu* was the
    character\'s catch phrase.
-   *[Outbreak](Wikipedia:Outbreak_(film) "wikilink")* is a 1995 movie
    that deal with the outbreak of a fictitious Ebola-like virus.
-   *[Unbreakable](Wikipedia:Unbreakable_(film) "wikilink")* is a 2000
    movie in which one of the lead characters appears to have unusual
    powers, among them being immune to disease.
-   *[The Sixth Sense](Wikipedia:The_Sixth_Sense "wikilink")* is a 1999
    movie in which one of the lead characters is revealed at the end of
    the movie (spoiler) as having been a ghost the entire time. Both
    *Unbreakable* and *The Sixth Sense* are written and produced by [M.
    Night Shyamalan](Wikipedia:M._Night_Shyamalan "wikilink"), and in
    both cases, the character in question was played by [Bruce
    Willis](Wikipedia:Bruce_Willis "wikilink").

Trivia
------

-   Diseases submitted to [Dwight](Dwight_Schrute "wikilink"):
    -   Mad Cow Disease
    -   Ebola
    -   [Spontaneous Dental
        Hydroplosion](Spontaneous_Dental_Hydroplosion "wikilink")
    -   Leprosy
    -   Flesh-Eating Bacteria
    -   Hot Dog Fingers
    -   Anal Fissures
    -   Government Created Nanorobot Infection (a reference to the
        *X-Files* episode \"[S.R.
        819](http://x-files.wikia.com/wiki/S.R._819)\" in which [Walter
        Skinner](http://x-files.wikia.com/wiki/Walter_Skinner) is
        infected with such a disease as a means to blackmail him)
    -   Inverted Penis
-   [Stanley](Stanley_Hudson "wikilink") circles every disease on the
    form.
-   Three elements of this episode borrow from [the original British
    series](Wikipedia:The_Office_(UK_TV_series) "wikilink"):
    -   Dwight and [Michael](Michael_Scott "wikilink") argue over
        whether the workspace is an office.
        [David](Wikipedia:David_Brent "wikilink") and
        [Gareth](Wikipedia:Gareth_Keenan "wikilink") have a similar
        argument in in [Episode Two, Series
        One](Wikipedia:Episode_Two_(The_Office,_Series_One) "wikilink").
    -   [Jim](Jim_Halpert "wikilink") locks Dwight in his workspace.
        [Tim](Wikipedia:Tim_Canterbury "wikilink") does the same to
        Gareth in [The Office Christmas
        Specials](Wikipedia:The_Office_Christmas_specials "wikilink").
    -   In a deleted scene, Jim and Pam look through Dwight\'s old signs
        for his \"workspace\". A similar scene involved Tim and Dawn
        looking through Gareth\'s old signs for his workspace in the
        episode [Work Experience](Work_Experience "wikilink").
-   Dwight claims in this episode he has \"never been sick\" due to his
    \"perfect immune system.\" However, in later episodes, it is
    revealed that he once had walking pneumonia (\"[Performance
    Review](Performance_Review "wikilink")\"), that he has a history of
    kidney stones (\"[Michael\'s
    Birthday](Michael's_Birthday "wikilink")\"), and also has had sinus
    infections (\"[Christmas Party](Christmas_Party "wikilink")\").
-   [Angela Kinsey](Angela_Kinsey "wikilink") (portraying [Angela
    Martin](Angela_Martin "wikilink")) and [Brian
    Baumgartner](Brian_Baumgartner "wikilink") (portraying [Kevin
    Malone](Kevin_Malone "wikilink")) break when
    [Dwight](Dwight_Schrute "wikilink") reads off \"hot dog fingers\".
    The producers thought it was funny, so they kept it.
    [1](http://popgurls.com/2007/04/25/popgurls-interview-the-offices-angela-kinsey/)
-   According to [Paul Lieberstein](Paul_Lieberstein "wikilink") at
    *Paley Fest*, the final scene was scripted as \"The longest pause in
    television history.\" It ultimately went two and a half minutes, so
    long [Steve Carell](Steve_Carell "wikilink") broke into a sweat from
    the awkwardness of the situation.
-   When Michael calls the mine shaft, it is Creed that he talks to.

Deleted scenes
--------------

The [Season One DVD](Season_1_DVD "wikilink") contains a number of
deleted scenes from this episode. Notable cut scenes include:

-   [Michael](Michael_Scott "wikilink")\'s final response at the end
    after everyone has left was that they would all go bowling. This
    suggests he may have mentally shut down out of panic while trying to
    improvise something.
-   An alternate take where [Jim](Jim_Halpert "wikilink") says
    [Dwight](Dwight_Schrute "wikilink") should pick a good health plan
    in order to help himself as well, leading to them making movie
    references in order to argue over his mortality.
-   Jim and [Pam](Pam_Beesly "wikilink") reveal early attempts at the
    sign for the conference room. These include \"Schrute Space\" (which
    is the name of Dwight\'s blog), \"Quiet! Dwight Schrute Working\"
    and \"Dwight Schrute Privates\".
    -   This would have been a third reference to the [original British
        series](The_Office_UK "wikilink"), also from [Episode Two,
        Series
        One](Wikipedia:Episode_Two_(The_Office,_Series_One) "wikilink").
-   An interview between Dwight and [Oscar](Oscar_Martinez "wikilink").
-   Dwight talking about jealousy when a worker is elevated above his
    peers.
-   Oscar trying to confront Michael about the plan.
-   Dwight interviewing Pam which leads to him interviewing Jim,
    demanding he confess.
-   An extended talking head of Michael talking on how he learned improv
    from the masters.
-   Dwight memorizing who he takes health care forms from, only for
    Oscar to give him a bunch of forms at once.

Quotes
------

:   see *[Health Care Quotes](Health_Care_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring Cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited) and Coal mine
    employee (Uncredited)
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink") (Uncredited)

### Guest Cast

-   Charlie Hartsock as Travel Agent

\
