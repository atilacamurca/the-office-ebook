# The Office - eBook

Ideia: criar um eBook (ePUB e Mobi) dos artigos da wiki
<http://theoffice.wikia.com/wiki/Main_Page>.

## Ferramentas

* Pandoc - <https://pandoc.org/>
* nodemw - <https://github.com/macbre/nodemw>

## Ideia

Fazer parse das informações dos artigos da wiki

Baixar imagens relevantes de cada página

Criar caixa com as informações do episódio, semelhante ao
que aparece na página original

Baixar código fonte da wiki e converter para ePUB

Converter ePUB para Mobi com a ferramenta kindlegen
<http://epubsecrets.com/convert-epub-to-kindle-mobi-format.php>

Usar `--toc` para gerar table of contents <http://pandoc.org/MANUAL.html>