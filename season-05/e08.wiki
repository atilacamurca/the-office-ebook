{{Office episode
|Title      =Business Trip
|Image      =[[Image:BusinessTrip.jpg|250px]]
|Season     =[[Season 5|5]]
|Episode    =8
|Code       =5009
|Original   =November 13, 2008
|Writer(s)  =[[Brent Forrester]]
|Director   =[[Randall Einhorn]]
|Prev       =[[Customer Survey]]
|Next       =[[Frame Toby]]}}
"'''Business Trip"''' is the eighth episode of the fifth season of the television series ''[[The Office (US)|The Office]] ''and the 80th episode overall. It was written by [[Brent Forrester]] and directed by [[Randall Einhorn]]. It first aired November 13, 2008. It was viewed by 8.18 million people.

==Synopsis==
CFO David Wallace sends Michael on a foreign sales call to Winnipeg to help alleviate the stress of having Holly taken away from him. He brings along Andy as a foreign language translator and Oscar as the "numbers guy". David Wallace tells him to go to the concierge to find activities to do in the city. Michael finds himself attracted to the concierge (whose name is Marie), and, thinking that the concierge is the Winnipeg version of a Geisha, mistakes her as some kind of prostitute.

Michael runs into Marie at a bar later that night. They have a few drinks and head back to her hotel room to have sex. After supposedly brief and impersonal intercourse, Marie kicks Michael out of her room and he walks away, depressed and barefoot. As he heads down the hall, she opens the door. Michael turns around expectantly, but she opened the door only to return his shoes.

Back at the bar, Oscar wishes to be left alone, but Andy attempts to set him up with a couple of guys. A few drinks later, Oscar asks Andy what he sees in Angela. Andy says he sees something behind her strict behavior before mentioning that they have not had sex yet. Oscar is shocked and tells Andy to call Angela about this. Andy, drunk, calls Angela, who is with Dwight. Andy tells her he does not like the fact they have not had sex yet. Angela berates him for calling her whilst drunk and hangs up on him.

The next morning, Andy and Oscar reminisce about the night before, with Oscar bringing up Andy's call to Angela. Andy, now sober, is horrified to find that he really ''did'' call Angela. (Upon waking that morning, he thought the call had just been a dream, he tells Oscar.) He calls Angela to apologize. He says afterward that Angela has put them back on "first base", which for her means that Andy gets to "kiss her forehead". Andy tells Oscar he appreciated his company on the trip, and Oscar feels the same way about Andy.

Michael secures the sale with the client and calls David Wallace, who congratulates him. Michael then tells him he had a terrible time in Winnipeg and berates him for taking Holly away from him before abruptly hanging up. He then gets on the plane back to Scranton, mentioning he has stayed with the company for a long time because they give him enough respect that he could talk down his boss and nothing would happen to him.

Back in Scranton, Jim and the rest of the staff are eagerly counting down the days until Pam returns from art school, with the staff awkwardly and intrusively showing their excitement for Jim. Pam is upset that she failed a class and calls Jim to tell him that she would need to stay in New York for another three months to retake it. Pam is unsure if she wants to spend another three months away from Jim, but Jim reminds her that she went to New York for her own personal benefit and should only return to Scranton "the right way".

The rest of the staff finds out about her situation and feels sorry for the two of them, save Dwight who criticizes Pam's painting of the office building on the wall. At the end of the day, Jim finds Pam waiting in the parking lot. Pam tells Jim that she is coming back "the wrong way" not because of him, but because she ultimately did not like graphic design and that Scranton is her home. Jim welcomes her back and they kiss.

Meanwhile, Ryan moves back to the annex with Kelly in preparation for Pam's return and starts showing off by doing push ups in front of her. Kelly says she has no intention of breaking up with Darryl and restarting a relationship with Ryan, but the two begin making out passionately. Ryan tells Kelly to break up with Darryl via text message, and Darryl responds immediately saying merely "it's cool". Ryan is shocked at Darryl letting it go easily, and Darryl is shown happily walking to his truck.

At the end of the episode, Ryan and Kelly confirm they are back together, with Ryan visibly uncomfortable like he was before in their relationship.

==Trivia==
* The scenes at the Canadian hotel were shot at the Wilshire Grand Hotel in Los Angeles.
* Scenes at "The Huntsman" were shot at the Seven Grand Bar and Grill in Los Angeles.
* The office possibly runs on wireless internet as in the last scene, on Jim's desk is a linksys wusb54gs, a wireless internet adapter.
* Most of the Winnipeg information is fictitious. There is no sushi restaurant called Matsuki, a pub called "The Huntsman" in the Financial District (which is more commonly known as the Exchange district), or an Astro Cleaners, and the Number 17 bus runs until 1am, not 9pm.
* According to Weight Loss, Pam left for New York the week of July 7th. In this episode, David Wallace says its the middle of November. This would mean that Pam was in New York for 4 months instead of 3.

==Amusing details==
* Andy clearly does not speak fluent French; the French he speaks while saying goodbye to Angela is full of pronunciation mistakes
* When entering the hotel in Canada, Michael does not have the two large blue suitcases he had asked Dwight to carry out for him.

==Connections to previous episodes==
* Andy learned French during his semester at sea, as noted by Angela in a deleted scene from ''[[Dinner Party]]''.
* Andy says beer me for getting drinks for the guys he's trying to hook Oscar up with. This is exactly what Andy says to Jim when asking for something in ''[[Product Recall]]''.

==Cultural references==
* [[Wikipedia:Suicide in Japan|Suicide in Japan]] is a national problem and is actively discouraged by the government.
* Michael talks about "all things international" including pancakes and the man of mystery, referring to [[Wikipedia:IHOP|The International House of Pancakes]], a national restaurant chain specializing in breakfast, and the movie ''[[Wikipedia:Austin Powers: International Man of Mystery|Austin Powers: International Man of Mystery]]''.
* ''[[Wikipedia:Abu Dhabi|Abu Dhabi]]'' is the capital of the [[Wikipedia:United Arab Emirates|United Arab Emirates]] (not [[Wikipedia:England|England]]). The Islamic dress code is widely adhered to (although not compulsory), and Western-style clothing is popular among young people.
* Michael plans to spend his ''[[Wikipedia:Per diem|per diem]]'' money on a sweater. The per diem is a monetary allowance given to an employee on business travel to cover daily expenses such as food and taxicab rides, not as money to be spent on gifts.
* When Dwight says: "I wash my hands of this", he is referring to Pontius Pilate in the Bible. Pilate washed his hands to show that he was not responsible for the execution of Jesus. Dwight says so to show that he is not responsible if Michael gets robbed.
* David sends Michael to ''[[Wikipedia:Winnipeg|Winnipeg]]'', the capital of the Canadian province Manitoba. It is a windy city, and although summers are quite warm, in November, the temperature typically remains below freezing all day.
* Although [[Wikipedia:Canada|Canada]] is a bilingual nation, Winnipeg is in the English-speaking part of Canada, although it does have a sizeable French-speaking community in the neighborhood of ''[[wikipedia:Saint Boniface, Manitoba|Saint Boniface]]''.
* A ''[[Wikipedia:Concierge|concierge]]'' is a hotel employee who assists guests with local services, such as recommending restaurants or giving directions to local attractions.
* Michael introduces the documentary crew to his airplane seat by calling it ''[[Wikipedia:MTV Cribs|Cribs]], business class edition'', referring to the reality television program which provides tours of celebrity houses and mansions.
* Andy plans to watch ''[[Wikipedia:Harry and the Hendersons|Harry and the Hendersons]]'' during the flight, a comedy movie about a family's encounter with the legendary creature [[Wikipedia:Bigfoot|Bigfoot]].
* Michael believes the concierge is like a ''[[Wikipedia:geisha|geisha]]'', a Japanese woman trained in the fine arts. Legitimate geisha flirt with their clients but do not have sex.
* ''[[Wikipedia:Adobe Flash|Flash]]'' is a platform for presenting interactive content on the Web. ''[[Wikipedia:Adobe Acrobat|Acrobat]]'' is a program for creating and managing documents, and ''[[Wikipedia:QuarkXPress|Quark]]'' is a desktop publishing program.
* Andy takes it upon himself to serve as Oscar's ''[[Wikipedia:Wingman (social)|wingman]]'', an informal term for somebody who helps a friend approach potential sexual partners.
* On the bar at The Huntsman are two bags of [[Wikipedia:Old Dutch Foods|Old Dutch]] potato chips. The company's Canadian head office is in Winnipeg.
* Andy says he gave Oscar "the whole nine 'nards", a pun on the phrase "the whole nine yards."
* The potential client says that he can get a better price from ''[[Wikipedia:Catalyst Paper|Catalyst Paper]]'', a Canadian paper company.
* Presumably, this is only Michael's second time out of the country, the first being his trip to Jamaica mentioned in ''[[Back From Vacation]]''.

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting Cast===
*[[Ed Helms]] as [[Andy Bernard]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]

===Recurring Cast===
*[[Andy Buckley]] as [[David Wallace]]

===Guest Cast===
* Wendi McLendon-Covey as Marie the Concierge
* Deb Hiett as Beth the Flight Attendant
* Tony Pasqualini as [[Clients of Dunder Mifflin#Canadian Client|Potential Canadian Client]]
* Frank Maharajh as Not Gay Businessman
* Paul Sass as Not Gay Businessman
* Danilo Di Julio as Plane Passenger
