# Stress Relief

![](StressRelief.jpg "StressRelief.jpg")

**\"Stress Relief\"**, a two-parter, is the fourteenth and fifteenth
episodes of the fifth season of the television series The Office, and
the show\'s 86th and 87th episodes overall. It was written by Paul
Lieberstein and directed by Jeffery Blitz. The episode aired in the U.S.
on February 1, 2009, after the Super Bowl. It was viewed by 22.91
million viewers. (see [The Office Nielsen
Ratings](The_Office_Nielsen_Ratings "wikilink")).

Synopsis
--------

[Dwight](Dwight "wikilink") tries to teach fire safety by example,
forcing all the doors closed and disabling the phones before setting a
fire in an office trash can with a cigarette. He then says, \"today
smoking will save lives.\" He returns to his desk and tries to draw
attention to the smoke, asking if anyone "smells anything smoky in
here?" People don\'t believe him at first but he clears his throat and
Pam notices the smoke. During the subsequent panic, the copy machine is
destroyed, [Michael](Michael "wikilink") smashes a window to call for
help and [Kevin](Kevin "wikilink") takes advantage of the havoc to raid
the snack machine. [Stanley](Stanley "wikilink") collapses from a heart
attack immediately following Dwight's announcement that the whole thing
was a drill. Michael, against the advice of his coworkers, tries to give
CPR to an unconscious Stanley.

As Dwight\'s actions could have burned down the building or killed
Stanley, Corporate is not amused. Michael and Dwight are called on the
carpet in New York, Michael to be reminded of his responsibilities to
his subordinates and Dwight to be warned that he is subject to being
fired. On the drive back home, Michael revokes Dwight's status as safety
advisor. Back in Scranton, because he hadn\'t had any idea how to give
first aid to Stanley, Michael has the American Red Cross send over an
instructor (and most importantly a dummy) to teach CPR. Dwight pouts,
Andy sings, and naturally no one learns anything except the lyrics to
The Bee Gee\'s song \"Stayin\' Alive\" - and that Dwight looked very
realistic wearing the face of the dummy a la "Silence of the Lambs".
Most importantly, Dwight takes the scenario far too seriously and
mutilates the dummy to "harvest the organs." CFO David Wallace and
Corporate HR Kendall, upset that Dwight\'s mutilating the dummy cost the
company \$3500, requires Dwight to apologize to his co-workers and get
their acknowledgement of his apology in writing. Michael signs the
apology without much thought, but the rest of Dwight's coworkers refuse;
everyone except [Phyllis](Phyllis "wikilink") is tricked into signing
the sheet, mistaking it for an attendance sheet (Phyllis only signs when
Dwight has a fake delivery sent to her.)

[Michael](Michael "wikilink") tries a number of ways to get his
employees to unwind, like soft music and meditation, before discovering
that he is the number one cause of stress at work. So that people won\'t
feel afraid of him, he insists on a no-holds-barred roast of himself. He
actually thought that each speaker would also express love for him, so
while the employees had a great time, Michael ends up with his feelings
hurt. Later, after taking a \"Personnel Day,\" he recovers and conducts
a mini-roast of his employees - initially making them somewhat
uncomfortable, before Stanley bursts into laughter about his joke and
the rest of the office jumps in.

While all of this has been going on, [Andy](Andy "wikilink") is
convinced that [Jim](Jim "wikilink") and [Pam](Pam "wikilink") are film
gurus because they are talking about Pam\'s parents as the three watch a
pirated movie, but Andy thinks they\'re talking about the movie---and by
chance, it sounds like it. The episode features movie stars [Jack
Black](Jack_Black "wikilink"), [Cloris
Leachman](Cloris_Leachman "wikilink"), and [Jessica
Alba](Jessica_Alba "wikilink") in the film, *Mrs. Albert Hannaday*,
which features what some would consider a taboo subject, a love triangle
with a \"May - late-December\" relationship. Andy does not know that Jim
and Pam were actually discussing Pam\'s mother and father\'s
deteriorating relationship. Pam\'s father stays with Pam and Jim for
several days. Pam asks Jim to talk to her father and see if her dad will
tell Jim what is wrong with his relationship with Pam\'s mother. After
the two men talk her father says he is going to get his own apartment.
Jim has no idea what he could have said to make Pam\'s father leave her
mother for good. Later Pam is stunned by revelations on two fronts: 1)
that Jim has been deeply in love with her since the beginning and 2)
that her father never felt a deep emotion for her mother - Mr. Beesly
realized the latter after hearing the former from the heart, and *that*
was what Jim had said. In a talking heads interview, Pam says that
although most children expect their parents to be soulmates, Pam and
Jim's children "will be right about that." She also says that love
affairs and relationships look different from the outside, which is
overhead by a frustrated Andy, who (once again) believes she is talking
about the film they watched and decides he is not fit to be a movie
critic.

Trivia
------

-   This is the only episode in which [Leslie David
    Baker](Leslie_David_Baker "wikilink") ([Stanley
    Hudson](Stanley_Hudson "wikilink")), [Brian
    Baumgartner](Brian_Baumgartner "wikilink") ([Kevin
    Malone](Kevin_Malone "wikilink")). [Creed Bratton
    (actor)](Creed_Bratton_(actor) "wikilink") ([Creed
    Bratton](Creed_Bratton "wikilink")), [Kate
    Flannery](Kate_Flannery "wikilink") ([Meredith
    Palmer](Meredith_Palmer "wikilink")), [Mindy
    Kaling](Mindy_Kaling "wikilink") ([Kelly
    Kapoor](Kelly_Kapoor "wikilink")), [Angela
    Kinsey](Angela_Kinsey "wikilink") ([Angela
    Martin](Angela_Martin "wikilink")), [Paul
    Lieberstein](Paul_Lieberstein "wikilink") ([Toby
    Flenderson](Toby_Flenderson "wikilink")), [Oscar
    Nunez](Oscar_Nunez "wikilink") ([Oscar
    Martinez](Oscar_Martinez "wikilink")), [Craig
    Robinson](Craig_Robinson "wikilink") ([Darryl
    Philbin](Darryl_Philbin "wikilink")), and [Phyllis
    Smith](Phyllis_Smith "wikilink") ([Phyllis
    Vance](Phyllis_Vance "wikilink")) are credited during the opening
    theme.
-   [B.J. Novak](B.J._Novak "wikilink") ([Ryan
    Howard](Ryan_Howard "wikilink")) only appears in this episode via
    Archive Footage.
-   Oscar\'s rant in Spanish translates as \"You give me an ulcer every
    time I wake up and I have to come to work. I have to come to work
    for you. For you!\"
-   Jim saying to Michael, \"Multiple times a day Michael says things
    that are way beyond my vocabulary,\" is a reference to the ongoing
    joke of Michael often confusing words and common phrases.
-   Michael does not roast Toby, Kelly, or Phyllis. Neither Toby nor
    Phyllis mock Michael at the roast.
-   The lyrics to Andy\'s song, which is a parody of \"What I like about
    you\" by The Romantics:
    -   What I hate about you
    -   You really suck as a boss
    -   You\'re the losiest, jerkiest, and you\'re dumber than apple
        sauce
    -   We\'re stuck listening to you all day
    -   Stanley tried to die just to get away,
    -   well it\'s true
    -   That\'s what I hate about you
    -   That\'s what l hate about you
    -   Yoooooooooouuouoooouuouoooooooooououououooooouuuu
    -   YEAH!
-   When the employees were meeting in the break room after the roast
    there were no snacks in the vending machine. This can be explained
    by assumming that in the days after Kevin broke the glass the rest
    of the office also decided to steal any snacks that they could.
-   During the roast, Kelly lists things she would rather make out with
    than Michael Scott. When she says \"a fridge\", Bob Vance can be
    heard in the background saying \"Yeah!\"
-   The song in the first scene in the fake movie *Mrs. Albert Hannaday*
    is *You Make My Dreams* by Hall & Oates.

Amusing Details
---------------

-   During the cold open fire scene, Kevin is seen breaking open the
    vending machine. After Stanley faints Kevin is messily eating snacks
    while dropping bags on the floor.
-   When David Wallace states that the CPR dummy costs "thirty-five
    hundred dollars", Michael mistakenly thinks that this means \$5,300.
-   In some of the talking heads after the fire drill, a window is seen
    boarded up which is the one Michael broke with the projector to yell
    for help.
-   When Stanley\'s stress beeper starts going off whenever Michael gets
    near him, you can see John Krasinski (Jim) trying to hold in his
    laughter.
-   In a very quick shot of the top of the filing cabinet next to
    Dwight\'s desk, as Pam walks past, we see what looks to be the duck
    from \'Cocktails\'.
-   Angela panics just like the rest of the office, even though in
    \"[The Fire](The_Fire "wikilink")\" Angela is well informed on how
    to evacuate.
-   Dwight shouting, \"Have you ever seen a burn victim?\" is a call
    back to when he said the same in \"The Fire.\"

Deleted Scenes
--------------

These were notable cuts from this episode as seen on the [Season 5
DVD](Season_5_DVD "wikilink"):

-   Michael congratulates [Darryl](Darryl "wikilink"),
    [Stanley](Stanley "wikilink"), and maybe [Kelly](Kelly "wikilink")
    on the inauguration of President Obama. Michael tells Darryl that
    black people have to be responsible and Darryl jokingly rebuts that
    Public Radio will become private, NASCAR is over, and Morgan Freeman
    movies will be cancelled.
-   Andy gives Stanley a mix of his songs.
-   Kevin offers Stanley to go to the bathroom with him. Stanley looks
    confused.
-   Phyllis explains that it was hard for her when Stanley was gone.
-   Michael shows Stanley that he got a defibrillator.
-   Kelly explains that she came in seventeenth in the Scripps National
    Spelling Bee, and then she gained 40 pounds.
-   Michael tries to shock Toby with the defibrillator but fails.
-   Michael thinks that Stanley is dead, shocks him, and realizes that
    he was just sleeping. He then puts a sign on Stanley saying, NOT
    DEAD.
-   An alternate extended scene of the Rose/CPR scene.
-   The office lists things that are stressful.
-   The office tries to find things that stop stress.
-   Dwight gets Stanley to sign the sheet, however, he fails to get
    Phyllis to. Dwight angrily tells her to sign it.
-   Stanley eats a wheel of brie as his reward for \"letting go of
    \[his\] anger\" by signing Dwight\'s apology letter.
-   Angela and Kevin mess up Meredith\'s hair using the defibrillator.
-   Michael wakes up from sleeping and says that beaches aren\'t
    stressful.
-   Dwight receives a text message and tells the office the Albany
    Branch is closing during Michael\'s relaxation seminar. Angrily,
    Michael confiscates everyone\'s cellphone and places them in a
    plastic garbage bag. Later the bag is seen placed on his desk but
    the vibration of the many phones causes them to fall off the desk.
-   Michael is angry that the mayonnaise is gone as he has the habit of
    eating a spoonful of mayonnaise during tough situations.
-   Dwight tries to get Jim\'s signature and Jim asks him whether or not
    he thinks he\'s part of the problem, which Dwight scoffs at, but
    then asks hypothetically what he could do to improve. Jim tells
    Dwight to smile more often and instructs him on twisting his face
    into a creepy leer. Jim then takes a photo of Dwight with his
    cellphone and happily signs the sheet.
-   Extension of the roast. Kelly says she saw Michael at a screening of
    Twilight, with his mother. Oscar rants to Michael in English - in a
    talking head, he says he came up with the material simply by writing
    down a typical drive-home rant. Jan calls and is placed on
    speakerphone, saying how Michael destroyed her life. Michael then
    rebuts with a crack about her breast augmentation.

Cultural References
-------------------

-   When Michael yells at Stanley not to die because \"Barack is
    president,\" he is referring to [Barack
    Obama](wikipedia:Barack_Obama "wikilink"), the first African
    American president of the United States, who was elected in 2008.
-   During the fire scare at the beginning of the episode, it is
    established that the phones have been disconnected by Dwight.
    However, when the scare escalates, Dwight calmly suggests that they
    call 911, perhaps by using a cell phone.
-   [PETA](wikipedia:People_for_the_Ethical_Treatment_of_Animals "wikilink")
    is an animal rights non-profit in the United States, and stands for
    People for the Ethical Treatment of Animals.
-   The Red Cross ([ARC](wikipedia:American_Red_Cross "wikilink")) is a
    well known charity that teaches CPR classes and does first aid and
    CPR certification.
-   After giving up on practicing CPR, Kevin says \"Call it\". In
    medical TV shows, this is commonly said when they\'ve tried all life
    saving measures and are ready to officially call time of death.
-   When Michael says \"No rest for the sick,\" he is [incorrectly
    quoting](Michael's_Botched_Phrases "wikilink") a well known biblical
    expression, \"No rest for the wicked,\" or \"No rest for the
    weary,\" another common idiom.
-   \"[Stayin' Alive](wikipedia:Stayin'_Alive "wikilink")\" is a famous
    disco song by the Bee Gees. Michael confuses it for the song "[ I
    Will Survive](wikipedia:I_Will_Survive "wikilink")" by American
    singer Gloria Gaynor.
-   An organ donor card is a piece of paper that people can carry in
    their wallets, indicating that after their death they want their
    organs donated. It is also common for people to indicate this on
    their driver\'s license.
-   Dwight removes the face of the dummy and turns to the office,
    saying, \"Clarice?\" mimicking a famous scene from the movie [The
    Silence of the
    Lambs](wikipedia:The_Silence_of_the_Lambs_(film) "wikilink"). Dwight
    tells David Wallace he thought the scene was unrealistic, which is
    why he tried acting it out himself.
-   Jack Black, Nicole Kidman and Cloris Leachman are all famous actors.
-   Chunky Monkey is an ice cream flavor made by Ben & Jerry\'s. It is
    banana flavored with chunks of fudge and walnuts.
-   Although there is not such a thing as the Comedy Central Roast
    Channel, Michael is referring to a series of [famous TV
    specials](wikipedia:Comedy_Central_Roast "wikilink"), aired on
    Comedy Central, which were
    [roasts](wikipedia:Roast_(comedy) "wikilink") of various
    celebrities.
-   YouTube is a well-known video hosting website where users can upload
    their own videos. However, Michael suggests that it is a production
    company that solely makes videos itself.
-   [Sammy Davis Jr.](wikipedia:Sammy_Davis,_Jr. "wikilink") was an
    American actor known for his impersonations. He was roasted on the
    NBC show [The Dean Martin Celebrity
    Roast](wikipedia:The_Dean_Martin_Celebrity_Roast "wikilink")
    in 1975.
-   Lord Voldemort is the villain from the famous children\'s series
    Harry Potter.

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink") (Only appears in Archive Footage)

### Supporting Cast

-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Recurring Cast

-   [Andy Buckley](Andy_Buckley "wikilink") as [David
    Wallace](David_Wallace "wikilink")
-   [Bobby Ray Shafer](Bobby_Ray_Shafer "wikilink") as [Bob
    Vance](Bob_Vance "wikilink")
-   Hidetoshi Imura as [Hidetoshi
    Hasagawa](Hidetoshi_Hasagawa "wikilink") (uncredited)
-   [Calvin Tenner](Calvin_Tenner "wikilink") as
    [Calvin](Calvin "wikilink") (uncredited)
-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levinson](Jan_Levinson "wikilink") (Deleted scene/Voice only)

### Guest Cast

-   John Hartmann as [Kendall](Kendall "wikilink") (Corporate H.R. Rep.)
-   Robin Lynch as Rose the CPR Instructor
-   Lamont Ferrell as Michael the Warehouse Worker
-   Rick Overton as [William Beesly](William_Beesly "wikilink")
-   [Joanne Carlsen](Joanne_Carlsen "wikilink") as [Terri
    Hudson](Dunder_Mifflin_Family_Members_and_Loved_Ones#Terri_Hudson "wikilink")
    (uncredited)
-   James O. Kerry as Deliveryman

### Special Celebrity Guest Cast

-   [Jack Black](Jack_Black "wikilink") as Sam (in a Fake Movie)
-   [Cloris Leachman](Cloris_Leachman "wikilink") as Lily (in a Fake
    Movie)
-   [Jessica Alba](Jessica_Alba "wikilink") as Sophie (in a Fake Movie)
-   [Nate Federman](Nate_Federman "wikilink") as Lily\'s New Boyfriend
    (in a Fake Movie)
