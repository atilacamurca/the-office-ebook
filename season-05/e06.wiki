{{Office episode
|Title      =Employee Transfer
|Image      =[[Image:EmployeeTransfer.jpg|250px]]
|Season     =[[Season 5|5]]
|Episode    =6
|Code       =5006
|Original   =October 30, 2008
|Writer(s)  =[[Anthony Farrell]]
|Director   =[[David Rogers]]
|Prev       =[[Crime Aid]]
|Next       =[[Customer Survey]]}}
'''"Employee Transfer"''' is the sixth episode of the fifth season of ''[[The Office (US)|The Office]] ''and the 78th episode overall. It was written by [[Anthony Farrell]] and directed by [[David Rogers]]. It first aired October 30, 2008. It was viewed by 9.32 million people.

==Synopsis==
[[Pam Beesly|Pam]] is embarrassed when she is the only person at corporate wearing a costume on Halloween. Pam's intended costume is Charlie Chaplin, since her mustache was applied with grease paint. She shows annoyance that she cannot remove her hat, since she then resembles Adolf Hitler. In [[Dunder Mifflin Scranton|Scranton]], [[Stanley Hudson|Stanley]] wears a Creature from the Black Lagoon mask in order to sleep at work and not get caught while [[Creed Bratton|Creed]], [[Kevin Malone|Kevin]] and [[Dwight Schrute|Dwight]] dress up as Heath Ledger's version of the Joker from ''The Dark Knight''.

[[Kelly Kapoor|Kelly]] dresses up as Carrie Bradshaw from ''Sex and the City'', and [[Ryan Howard|Ryan]] tries to woo her. Ryan dresses as Gordon Gekko from the film ''Wall Street'', which Kelly associates with the GEICO gecko. [[Oscar]] dresses up like Uncle Sam, [[Meredith]] is a cheerleader, [[Phyllis Vance|Phyllis]] is Raggedy Ann, and [[Andy Bernard|Andy]] is Rum Tum Tugger from the Broadway show ''Cats''. [[Angela Martin|Angela]] is in the same kitten outfit as she was in "[[Halloween]]". [[Jim Halpert|Jim's]] outfit is minimalist as usual, dressing up as a guy named "Dave" (a costume consisting of his normal attire, with a name tag).

[[Holly Flax|Holly]] has been transferred back to her old branch in [[Dunder Mifflin Nashua|Nashua]], New Hampshire after CFO [[David Wallace]] discovered her relationship with [[Michael Scott|Michael]]. Michael and [[Darryl Philbin|Darryl]] help her move using Darryl's truck. Michael and Holly want to continue their relationship, but as they get closer to Nashua, Holly starts to come to the realization that their relationship is not going to work with the long distance, citing the seven hour drive. Michael continuously begs her to keep their relationship alive as he fears he will go back to [[Jan Levinson|Jan]].

Michael had originally intended to spend the weekend with Holly before heading back to Scranton, but after they move her stuff into her new house, he changes his mind and decides to head back with Darryl. Michael and Holly share one last embrace before he leaves. In the truck, Darryl tries to console Michael by teaching him to sing the blues, which Michael does not understand but ultimately joins in.

Jim meets Pam in New York for lunch with his brothers, Tom and Pete. Pam, Tom, and Pete arrive early to discuss a prank Pam wants to play on Jim that involves her engagement ring. Tom and Pete, however, think of playing a prank involving her interest in art, which she agrees to, though she appears to be mildly offended. When Jim arrives, Tom and Pete begin mocking Pam's interest in art, and Jim continuously comes to her defense.

When the argument becomes heated, Tom and Pete reveal it was a prank, and Jim and Pam stare awkwardly at each other. Later, when Jim and Pam are walking outside of the restaurant, Pam explains her original idea for the prank, which Jim appreciates much more than his brothers' idea. Moments later, Jim receives a text message from his brother stating his approval of Pam and welcoming her to the family.

Meanwhile, back in Scranton, Dwight comes in wearing a Cornell sweatshirt, which irritates Andy, who thinks Dwight is mocking him. Dwight tries to talk about Cornell with Andy and tells him that he is going to apply there. He further installs a Cornell pennant and a Cornell mascot bobblehead in his area. Unable to take any more of Dwight's mocking, Andy calls the university and is given permission to give Dwight his interview for admission.

Andy has no intention of allowing Dwight to pass, but during the interview, Dwight notices inconsistencies with Andy's questions such as asking who was Cornell's eighth president, which Dwight answers correctly as Dale R. Corson, but Andy replies that Cornell's seventh president was James A. Perkins. Then Dwight starts writing down his critique of Andy's interviewing skills, which he tells him he is going to send to the university. Dwight additionally gets to Andy by proclaiming his newfound desire to change his application to the "vastly superior Dartmouth," which becomes the last straw for Andy.

After a heated physical struggle with the conference room table, Andy gives up. At the end of the episode, Andy comes into the office in overalls, a farmer's hat, and a basket of beets, saying he is starting his own beet farm, hence trying to be like Dwight. Dwight, however, is quick to point out Andy's knowledge of beets, or lack thereof. Through the day Angela is seen to be annoyed by both Andy and Dwight's antics, but does not speak up against either one.

==Trivia==
* The scene in New York City with Jim's brothers was filmed at the East 3rd Steakhouse and Lounge in Los Angeles.
* Andy and Dwight mimicking each other at the beginning and end of the episode is similar to Dwight and Jim doing the same. In both episodes, the first imitator leaves the victim so frustrated that the latter humiliates himself with an attempt at payback.
* On the drive back from Nashua, Michael and Darrell are headed south but it appears as if the sun is setting in the east.
* Michael and Holly say that the trip from Scranton, PA to Nashua, NH will take 7 hours. According to Google Maps, the fastest route between the two cities is about 4 hours and 45 minutes, although there is a second route which takes about 5 hours. This could be accounted for by the fact that Michael, Holly, and Darryl travel in Darryl's work truck, which is loaded with all of Holly's possessions, and thus would need to drive at slower speeds, thus lengthening the trip.
* Ryan's Gordon Gekko costume is humorous considering Ryan was also arrested for fraud.

==Cultural References==
*Creed, Dwight and Kevin dress up as [[Wikipedia:Heath Ledger|Heath Ledger]]'s version of The Joker from ''[[Wikipedia:The Dark Knight (film)|The Dark Knight]]'', the sequel to ''[[Wikipedia:Batman Begins|Batman Begins]]'', while Creed has the most impressive costume.[[File:The-office-halloween-2008-creed.jpg|thumb|Creed's Joker Outfit]]
*Kelly dresses as Carrie Bradshaw from ''Sex and the City''.
*Pam dresses up as Charlie Chaplin and points out that without her hat she looks like Adolf Hitler.
*Ryan dresses up as Gordon Gekko from ''Wall Street'', though Kelly confuses that with the GEICO Gecko.
*Andy dresses up as a Rum Tum Tugger from the musical ''Cats''.
* Phyllis is dressed as [[wikipedia:Raggedy_Ann|Raggedy Ann]], and Meredith is wearing a cheerleader costume.
*Oscar dresses up as [[Wikipedia:Uncle Sam|Uncle Sam]].

==Connections to Previous Episodes==
*While in Darryl's truck, Michael and Holly play with the radio until Darryl yells at them to stop. In [[The Negotiation]], while Michael is in Darryl's truck, he jokes about playing with the radio and then promises not to touch it. 

==Amusing Details==
*Jim's brothers both look at the camera with comedic facial expressions and mannerisms similar to Jim's.
* Darryl's front license plate has a sticker that says Rock 107, which is a real Scranton radio station.

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting Cast===
*[[Ed Helms]] as [[Andy Bernard]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]

===Special Guest Star===
*[[Amy Ryan]] as [[Holly Flax]]
===Recurring Cast===
*[[Calvin Tenner]] as [[Calvin]] (Uncredited)

===Guest Cast===
*[[Tug Coker]] as [[Pete Halpert]]
*[[Blake Robbins]] as [[Tom Halpert]]
