# Casual Friday

![](CasualFriday.jpg "CasualFriday.jpg")

**\"Casual Friday\"** is the twenty-sixth episode of the fifth season of
*The Office*, and the 98th episode overall. It was written by [Anthony
Farrell](Anthony_Farrell "wikilink") and directed by [Brent
Forrester](Brent_Forrester "wikilink"). It first aired on April 30,
2009. It was viewed by 7.31 million people.

Synopsis
--------

[Michael Scott](Michael_Scott "wikilink") has returned to his old job as
regional manager of the [Dunder Mifflin
Scranton](Dunder_Mifflin_Scranton "wikilink") branch and announces that
[Pam](Pam "wikilink") and [Ryan](Ryan "wikilink") have joined the sales
team, retaining their old clients from the [Michael Scott Paper
Company](Michael_Scott_Paper_Company "wikilink").
[Dwight](Dwight "wikilink"), [Andy](Andy "wikilink"),
[Phyllis](Phyllis "wikilink") and [Stanley](Stanley "wikilink") are
upset because most of those clients were ones Michael previously stole
from them. They are also upset that Michael treats Pam and Ryan with
more respect because they were the only people to follow him to his new
company, with Michael believing his company operates within the branch
despite the buyout. Dwight calls a secret meeting in the warehouse with
the salespeople, minus Pam and Ryan, to think of a way to get their
clients back.

[Jim](Jim "wikilink"), trying to remain impartial, informs Michael of
the potential mutiny and heads to the break room to play chess with
[Creed](Creed "wikilink") until the conflict is resolved. Things take a
turn for the worse when Dwight interrupts a sales call with Ryan,
causing him to lose the client. Michael steps in and demands an apology,
but Dwight, Andy, Phyllis and Stanley threaten to quit and start their
own paper company. When Michael calls their bluff, Phyllis breaks down
and says that they were the real victims of Michael\'s company, not
corporate, and that they were hurt since Michael had treated them like a
family and then turned on them. Michael is taken aback by her words.

Meanwhile, it is Casual Friday and most people dress too loosely at the
office. [Angela](Angela "wikilink") complains to [Toby](Toby "wikilink")
about [Oscar](Oscar "wikilink") wearing sandals to work and having to
look at his feet. [Meredith](Meredith "wikilink") disgusts the rest of
the employees by wearing a very short dress without any panties or a
bra, thus exposing herself to the rest of the office as she tries to
adjust it to meet Toby\'s requests. Toby calls a meeting with everyone
regarding Casual Friday. When Dwight tries to take charge of the
meeting, Toby is finally stern with him and cancels Casual Friday, much
to everyone\'s disappointment.

Michael calls a meeting with Dwight, Andy, Phyllis and Stanley to give
them a formal apology, but they do not accept it since all they want is
their clients back. Michael reluctantly agrees to give back their
clients. In doing so, however, there are not enough clients to keep both
Pam and Ryan on the sales team, leaving Michael to decide which one to
keep. He talks it over with Jim, who suggests Pam would be better
suited, while Michael thinks Ryan should get it. Michael calls a meeting
with Pam after meeting with Ryan to inform her she did not get the sales
job. Pam is visibly disappointed, but Michael starts laughing and tells
her that she did get it, having offered Ryan a temp position again. Pam
laughs it off and tells him he should stop with the \"fake-firing\"
joke. Michael then decides to pull the joke on the new receptionist,
[Erin Hannon](Erin_Hannon "wikilink").

Trivia
------

-   [Dwight\'s](Dwight_Schrute "wikilink") idea of dressing casually is
    not wearing a necktie. [Angela\'s](Angela_Martin "wikilink") idea of
    dressing casually is wearing short sleeves (exposing her elbow).
-   Dwight does not know Stanley\'s surname.
-   Michael says, \"Do I rent The Devil Wears Prada again?\" referring
    to the cold open in which he acts to Pam as Meryl Streep acts to her
    secretary in the movie because he is renting The Devil Wears Prada.
-   [Pam](Pam_Beesly "wikilink") is the only person
    [Michael](Michael_Scott "wikilink") has fake-fired twice; this was
    the second time. The first time was in the [Season
    1](Season_1 "wikilink") episode \"[Pilot](Pilot "wikilink")\". This
    was [Erin\'s](Erin_Hannon "wikilink") first time being fake-fired.
-   The picture of Toby standing outside a church while he was in
    seminary school is the same church the show uses later on for Jim
    and Pam\'s wedding ([Niagara](Niagara "wikilink")).
-   Michael says he is not to be \"truffled\" with but he means
    \"trifled\" with. (See [Michael\'s Botched
    Phrases](Michael's_Botched_Phrases "wikilink"))
-   When Jim and Michael are discussing if Pam or Ryan will get the
    sales position, Jim suggests creating a Pro and Con list, similarly
    to how he does during [The Promotion](The_Promotion "wikilink").
    This also mirrors Pam making lists in [Dream
    Team](Dream_Team "wikilink").
-   Erin compliments Kelly\'s outfit, the same outfit she asks Kelly for
    when Kelly offers Erin any of her clothes in [Body
    Language](Body_Language "wikilink").

Deleted scenes
--------------

These were deleted scenes from the [Season 5
DVD](Season_5_DVD "wikilink"):

-   [Michael](Michael_Scott "wikilink") explains that
    [Pam](Pam_Beesly "wikilink"), himself, and
    [Ryan](Ryan_Howard "wikilink") have been through a lot, like they
    were in a love triangle.
-   [Oscar](Oscar_Martinez "wikilink") welcomes Michael back, and
    Michael explains what happened.
-   [Dwight](Dwight_Schrute "wikilink") sings the
    [24](wikipedia:24 "wikilink") theme song.
-   [Jim](Jim_Halpert "wikilink") gives Pam a gift with Dwight, Jim, and
    Pam on an island, which Jim tells was from himself and Dwight.
    Dwight says he didn\'t give it and tries to convince Pam.
-   Dwight tells [Andy](Andy_Bernard "wikilink") that urine was the code
    on the paper.
-   Dwight tells Andy, [Phyllis](Phyllis_Vance "wikilink"), Jim, and
    [Stanley](Stanley_Hudson "wikilink") his plan to get Pam and Ryan
    fired.
-   Dwight explains that failure is not an option. Andy and Phyllis tell
    Dwight that it is an option.
-   [Kelly](Kelly_Kapoor "wikilink") asks Dwight what he is doing in her
    nook. Kelly shows Dwight a sign with \"Kelly\'s Nook\" on it and
    Dwight leaves.
-   After [Toby](Toby_Flenderson "wikilink") cancels casual Fridays, the
    office is outraged. Since he is tired, he says that they can wear
    whatever they want, causing the office to celebrate with hugs and
    handshakes. Toby mumbles and sits at his desk.

Cultural references
-------------------

-   Boscov\'s is a department store chain found on the east coast.
-   Michael is unsure whether to rent [Sophie\'s
    Choice](wikipedia:Sophie's_Choice_(film) "wikilink") or [The Devil
    Wears Prada](wikipedia:The_Devil_Wears_Prada_(film) "wikilink") and
    says it is \"what you would call a classic difficult decision\". He
    doesn\'t seem to know that difficult decisions are often compared to
    the movie Sophie\'s Choice.
-   When Michael interrupts Creed and Jim\'s game of Scrabble, he lays
    down the letters \"NOSCRUB.\" This is possibly a reference to the
    TLC song [\'No
    Scrubs.\'](http://www.youtube.com/watch?v=pKV8uSX2nEQ)

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Recurring cast

-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
