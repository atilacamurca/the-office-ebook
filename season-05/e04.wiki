{{Office episode
|Title      =Baby Shower
|Image      =BabyShower.jpg
|Season     =[[Season 5|5]]
|Episode    =4
|Code       =5004
|Original   =October 16, 2008
|Writer(s)  =[[Aaron Shure]]
|Director   =[[Greg Daniels]]
|Prev       =[[Business Ethics]]
|Next       =[[Crime Aid]]
}}'''"Baby Shower"''' is the fourth episode of the fifth season of ''[[The Office (US)|The Office,]] ''and the 76th episode overall. It was written by [[Aaron Shure]] and directed by [[Greg Daniels]]. It first aired on October 16, 2008. It was viewed by 8.07 million people.

==Synopsis==
[[Dwight Schrute|Dwight]] acts out the process of childbirth with a watermelon so [[Michael Scott|Michael]] can be prepared for the arrival of [[Jan Levinson|Jan]]'s baby. Dwight eventually drops the watermelon from his apron, which bursts onto the floor.
Michael insists that the Party Planning Committee give him a Golden Shower just like they gave [[Phyllis Lapin]] for her wedding. [[Oscar Martinez|Oscar]] deems the whole thing pointless as the baby isn't even related to Michael. [[Angela Martin|Angela]] plans a "Guess Whose Baby Picture" game for the shower and gets angry when [[Andy Bernard|Andy]] unintentionally makes fun of her picture. Stanley gets irritated at pregnant women in the office when they complain about swollen ankles, constant hunger, varicose veins, and nipple chafing. He has the same problems and doesn't make them everyone's business.

[[Jim Halpert|Jim]] and [[Pam Beesly|Pam]] have a hard time trying to communicate with each other throughout the day, with Pam telling the documentary crew that the two of them would be having an "off" day even if she were in Scranton, which she is not.

When Jan arrives, everyone's surprised to see that her baby girl, Astrid, has already been born. The shower is held anyway with Michael doing his best to placate Jan by being cold to [[Holly Flax|Holly]] (to whom he gave a head's up prior to the shower).

The office employees pitched in money to buy a stroller for Jan who has already bought a $1200 Orbit stroller. Dwight can't imagine a stroller should cost as much as his bomb shelter. He takes the Orbit out to test its durability. While the shower continues, he straps a watermelon into the stroller and pushes it into fences, throws it over small cliffs and ties it to the back of his car for a bumper test.

Michael loves children. He never misses an opportunity to hold one. But when he holds Astrid, he feels nothing. He seeks advice from another "baby-daddy," [[Darryl Philbin|Darryl]]. Darryl explains that Michael doesn't have any attachment to Astrid because she's not his baby.

Despite Michael's pretense at hostility toward Holly, Jan recognizes his attraction to her. After retrieving Astrid from Angela and Andy's vegetable photo shoot, she prepares to leave, but not before telling Michael not to date Holly. He agrees. Then he goes into the building and hugs Holly. He feels the connection to her he didn't feel with Astrid and asks Holly out on a date. She accepts.

Jim and Pam call each other at exactly the same time and leave similar messages, finishing the day still having not reached each other.

==Deleted scenes==
* Michael reveals that Jan has allowed him to come up with names if she has a boy.
* Michael and Dwight reveal the contents of the "go bag" Michael carries around in case he needs to rush to the hospital for the birth, as written out in "[[Wikipedia:What To ExpectWhen You're Expecting|What To Expect]]" - coming from the 1986 edition, the contents are laughably out of date, though Dwight has added some items of his own.
* Kevin and Phyllis pour orange juice into baby bottles for the shower - Meredith adds her own [[Wikipedia:vodka|special ingredient]], assuring them "it's for me".
* Stanley wonders why all babies seem genetically predisposed to stare at him.
* Jan reluctantly plays a pointless baby shower game. After Holly remarks how beautiful Jan looks (to which Jim mockingly states, "Wait'll you get to know her better"), Michael scolds her for talking during the party.
* Holly talks with Toby (via speakerphone) about what it was like being the HR person.
* Jan finds out that Kevin donated sperm at the same sperm bank she went to. While Kevin gleefully wonders if he might have "done it" with her, in a talking head interview, Jan considers her options if he really is the father: "Sue? ...icide?"
* More of Dwight's "testing" the stroller bought for Jan, ending with Dwight admitting he is very impressed when it holds up.
* Following a discussion regarding Astrid in the otherwise empty conference room, Jim insists that he has no intention of being Michael's connection with reality.
* Kevin, Phyllis and Kelly wonder whether "she" should sleep on her stomach or back, but the camera zooms out to show not Astrid but a passed-out Meredith. In a talking head, Kelly says that, if she makes no mention of Meredith's clothes, there's no reason to bring up her drinking problem.
* Jan breast-feeds Astrid in plain view of the office, as Kevin and Creed happily look on.

==Trivia==
* The episode demonstrates actress Melora Hardin's real-life singing talent.
* After the episode aired, participants in [[Dunder Mifflin Infinity (online game)|Dunder Mifflin Infinity]] were tasked with matching up each baby picture with the corresponding actor.
* Writer [[Michael Schur]]'s wife and son appear in the montage of Michael with babies, when Michael dances with two babies at once.<ref>[http://www.officetally.com/the-office-baby-shower?cp=12#comment-475975 The Office: Baby Shower], Jennie Tan, October 17, 2008.</ref>

==Amusing details==
* When Michael interrupts Phyllis's talking head interview, Angela watches with satisfaction.
* Creed wraps his foot in bandages, presumably the same foot which has only four toes (as revealed in the episode ''[[Take Your Daughter to Work Day]]'').
* Stanley's contribution to the baby shower collection is a single dollar.
* When photographing Astrid, Angela calls the baby "it" instead of "she".
* Creed implies that he fought at Omaha Beach. Assuming his birthdate of November 1, 1925 is correct, this would make him 18 when D-Day occurred and thus is possible.
* Holly wears a different color of nail polish on each hand, in keeping with her quirky personality.
*Michael's "special mark" that he was to put on Astrid when she was born was "MGS", his initials.
*As Jan drives away, the stroller she received from the employees of Dunder Mifflin is seen next to Michael.

*Jan allows Michael to decide the name for the baby if it was a boy, however it is heavily implied that she knew her baby would already be a girl which means she only allowed him to choose the name because she knew he had no chance of actually naming her child.

==Connections to previous episodes==
* Andy calls a baby picture of himself himself "Nard Puppy", a callback to the nickname "Nard Dog" he received in the episode ''[[Local Ad]]''.
* In [[Take Your Daughter to Work Day]], Michael complained about having children around the office and was awkward around Toby's daughter, however, by the end of the episode he calls himself a "little kid lover". In this episode, he talks about how much he likes babies as well.

==Cultural references==
* Michael wanted live ''storks'' for the baby shower. In Western culture, the stork is the [[Wikipedia:Stork#Childbirth|symbol for childbirth]].
* Michael asks for a ''golden shower'', inadvertently using a slang term for [[Wikipedia:Urolagnia|urolagnia]], the practice of urination for sexual pleasure.
* Pam mentions a friend who makes "Murakami-style collages". She is possibly referring to [[wikipedia:Haruki_Murakami|Haruki Murakami]], a Japanese surrealist writer, or more likely, Takashi Murakami, a Japanese contemporary artist.
* On the phone, Pam talks about Sarah, the ''T.A.'', a common abbreviation for [[Wikipedia:Teaching assistant|teaching assistant]].
* Jan wants to talk to Michael about Astrid's ''529''. A [[Wikipedia:529 plan|529 plan]] is a way of saving money for a child's higher education. The joke is that Jan expects Michael to contribute to Astrid's college fund.
* Angela's picture of Astrid in a vegetable patch echoes the style of photographer [[Wikipedia:Anne Geddes|Anne Geddes]].
* [[Wikipedia:Omaha Beach|Omaha Beach]] was one of the five beaches where Allied forces landed on [[Wikipedia:Normandy Landings|June 6, 1944]]. It faced the greatest amount of German resistance and suffered the most casualties.
*Jan is singing [[Wikipedia:Son of a Preacher Man|Son of a Preacher Man]] at her baby shower.

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting Cast===
*[[Ed Helms]] as [[Andy Bernard]]
*[[Melora Hardin]] as [[Jan Levinson]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]
*[[Paul Lieberstein]] as [[Toby Flenderson]] (Uncredited, Voice Only, Deleted Scene)
===Special Guest Star===
*[[Amy Ryan]] as [[Holly Flax]]
===Recurring Cast===
*[[Calvin Tenner]] as [[Calvin]]

===Guest Cast===
*Vanessa Ragland as Linda

==References==
<references /><br />
