# Broke

![](Broke.jpg "Broke.jpg")

**\"Broke\"** is the twenty-fifth episode of the fifth season of *The
Office*, and the 97th episode overall. It was written by [Charlie
Grandy](Charlie_Grandy "wikilink") and directed by [Steve
Carell](Steve_Carell "wikilink") in his directorial debut. It first
aired on April 23, 2009. It was viewed by 7.21 million people.

Synopsis
--------

The strain of early morning paper deliveries takes its toll on the
Michael Scott Paper Company. Ryan, who says he gets sick by mornings
now-a-days, is teased by Michael by moving the van when he reaches for
the handle. Pam is told goodbye by a very sleepy Jim and Michael\'s
honking disturbs their neighbor. Michael, Pam and Ryan visit an
accountant to determine whether they can afford to hire a delivery
person and discover that not only can they not afford one, but
Michael\'s deep discounts and Ryan\'s flawed cash flow model mean that
the company is on the brink of collapse.

Meanwhile, CFO [David Wallace](David_Wallace "wikilink") visits the
Scranton branch to determine what can be done to stop Michael from
stealing Dunder Mifflin\'s clients. Dwight\'s bizarre suggestions on how
to get rid of the Michael Scott Paper Company cast him in a new light in
[Charles Miner](Charles_Miner "wikilink")\'s eyes.

Wallace realizes that he has to buy out the Michael Scott Paper Company.
Jim \"investigates\" the situation (although he knows from Pam the
Michael Scott Paper Company\'s desperate situation) and thwarts
Dwight\'s efforts to warn David Wallace and [Charles
Miner](Charles_Miner "wikilink") that they are offering too much for the
company.

Michael shows an uncharacteristic flash of brilliance during the
negotiations, first bidding up the offer for his company from \$12,000
to \$60,000, and then on top of that demanding (and getting) his old job
back as Regional Manager of [the Scranton
branch](Dunder_Mifflin_Scranton "wikilink"), as well as jobs for both
Pam and Ryan. Wallace is furious that Ryan will be back at Dunder
Mifflin considering the enormous damage he caused the company, and Ryan
doesn\'t seem too thrilled to be working there either.

Amusing details
---------------

-   When Michael honks the horn to pick up Pam, a light turns on in the
    neighbor\'s house.
-   When Dwight runs into the kitchen to tell Charles that the Michael
    Scott Paper Company is broke, nobody is in the conference room, even
    though it happens while Michael, Pam, and Ryan are supposed to be in
    there.
-   The buyout of the Michael Scott Paper Company came not a moment too
    soon: They just ran out of cheese puffs!
-   Michael\'s fish is different yet again. Poor guy can\'t keep a fish
    alive for more than a few days.
-   During the conference between Charles, David Wallace, Jim, and
    Dwight, the two sides dress similarly: David and Jim both wear light
    blue shirts with dark ties, Charles and Dwight wear white/off-white
    shirts with striped ties.
-   As Pam pointed out, 스크랜턴 할렐루야 교회 which is on the side of
    their van actually translates to Hallelujah Church of Scranton from
    Korean. This causes random Korean churchgoers to board the van
    throughout the episode.
-   The shareholder meeting that Michael points out is coming up for
    David Wallace is the one Michael is invited to speak at during
    [Shareholder Meeting](Shareholder_Meeting "wikilink").

Cultural references
-------------------

-   Michael asks, \"It\'s 4:30 in the morning. Do you know where your
    kids are?\" This is a slight misquote of the [public service
    announcement](Wikipedia:public_service_announcement "wikilink")
    popular in the United States in the 1960s to the 1980s. At 10pm or
    11pm, a brief announcement came on the television set: \"It\'s
    (time). [Do you know where your children
    are?](Wikipedia:Do_you_know_where_your_children_are? "wikilink")\".
-   A *paper route* is a job typically held by teenage children
    delivering newspapers to houses early in the morning.
-   Michael says, \"Time to make the donuts!\", the catch phrase from a
    series of television commercials for donut chain [Dunkin\'
    Donuts](Wikipedia:Dunkin'_Donuts "wikilink") featuring [Fred the
    Baker](Wikipedia:Fred_the_Baker "wikilink") waking up before dawn
    muttering the phrase to himself.
-   Ryan says \"Ever since I\'ve gotten clean\...\" To *get clean* is
    slang for to stop taking drugs.
-   Michael shouts, \"Boner patrol!\" *Boner* is slang for a male
    erection.
-   Michael, Pam, and Ryan sitting on the floor of their \"office\"
    sharing confessions echoes a similar scene from *[The Breakfast
    Club](Wikipedia:The_Breakfast_Club "wikilink")*.
-   Jim addresses Michael, Pam, and Ryan as \"Titans of Industry\" when
    asking if they would be receptive to a buyout. This is a common
    phrase applied to industrial giants such as Andrew Carnegie and J.
    P. Morgan. It was also the less common name given to Andrew
    Johnson\'s Reconstruction-era Knights of the Guild. Michael
    misunderstands Jim and informs him that \"not only are \[they\]
    tight ends, \[they\'re\] also quarterbacks,\" referencing two
    positions in American football.

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Special guest star

-   [Idris Elba](Idris_Elba "wikilink") as [Charles
    Miner](Charles_Miner "wikilink")

### Recurring cast

-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Andy Buckley](Andy_Buckley "wikilink") as [David
    Wallace](David_Wallace "wikilink")

### Guest cast

-   Kurt Scholler as Ty Platt
