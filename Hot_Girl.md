# Hot Girl

**\"Hot Girl\"** is the sixth episode of the [first
season](Season_1 "wikilink") of \'\'[The
Office](The_Office_(US) "wikilink") \'\'and the 6th overall. It was
written by Mindy Kaling and directed by Amy Heckerling. It first aired
on April 26, 2005. It was viewed by 4.8 million people.

Synopsis
--------

A purse vendor, Katy, comes in to the office to sell purses for the day.
While solicitors are generally not welcome in the office, Michael allows
her to use the conference room to sell purses because he, as well as
some of the other male staff, like her. Michael continually tries to get
Katy interested in him but fails. Michael offers to give Katy a ride
after work and is disturbed when he discovers her heading out with Jim.

Trivia
------

-   Michael tells Stanley that the coffee machine is easy to clean. But
    two scenes later, Michael talks to Pam in his office, and the coffee
    machine is covered in dirty napkins, as if Michael had made a
    horrible mess.
-   The Dunder Mifflin parking lot isn\'t the same as the other episodes

Deleted scenes
--------------

The Season One DVD contains a number of deleted scenes from this
episode.

-   Dwight describes his perfect girl.
-   Dwight shops for a purse as Michael brings coffee.
-   Michael clarifies his intentions with Katy.
-   Michael makes Toby look bad in front of Katy.
-   Jim assesses Michael\'s chances with Katy.
-   Jim encourages Dwight to use his purse, tricking him into mentioning
    it in front of the other employees.
-   Dwight describes his perfect date.
-   Michael asks Ryan for music advice.
-   Dwight gives his purse to Kelly.

Cultural references
-------------------

-   The *Mets* (New York City) and *Pirates* (Pittsburgh) are
    professional baseball teams.
-   Michael\'s nickname *Pamburger* is a pun on *hamburger*.
-   *American Way* is the in-flight magazine of American Airlines.
-   *Doris Roberts* is an Emmy Award-winning actress.
-   *Starbucks* is an international chain of coffee shops.
-   Michael\'s question \"High test or unleaded?\" is invented slang
    asking whether Katy prefers regular or decaffeinated coffee. Her
    response, \"Bring it on,\" indicates that she wants the more
    challenging alternative.
-   *Joe* is slang for coffee.
-   *Oscar the Grouch* is a character from the children\'s television
    program *Sesame Street*.
-   *[Bishop O\'Hara](Bishop_O'Hara_High_School "wikilink")* is a
    Catholic high school in Dunmore, a suburb of Scranton.
-   *Soccer mom* refers to a stay-at-home upper-middle-class woman with
    school-age children.
-   *NASCAR mom* is a term that was briefly popular in the 2004
    election, referring to the mother of a working-class white family.
-   *GQ* is a men\'s magazine focusing on fashion and style.
-   *Divine Secrets of the Ya-Ya Sisterhood* is a book and subsequent
    movie about a group of women who are close friends.
-   *Mack daddy* is slang for a man who is successful with women.
    Michael applies it to a coffee machine.
-   A *futon* is a foam mattress used as a cheap substitute for a bed.
    In the United States, its use is typically restricted to young
    adults with little money.
-   *Drakkar Noir* is a men\'s cologne. *Rite Aid* is a U.S. chain of
    drugstores. (*Night Swept* is a made-up brand name.)
-   *Filet-O-Fish* is the name of a fried fish sandwich from McDonalds.

Quotes
------

:   see *[Hot Girl Quotes](Hot_Girl_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring Cast

-   [Amy Adams](Amy_Adams "wikilink") as [Katy](Katy "wikilink")
-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink") (Uncredited)
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited)
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink") (Uncredited)
