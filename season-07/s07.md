# Season 7

![Season Seven DVD cover](TheOffice_S7_DVD.jpg "Season Seven DVD cover")

The American television comedy *[The Office](The_Office "wikilink")* was
renewed by [NBC](NBC "wikilink") for a seventh season on March 5,
2010.[^1] The season premiered on September 23, 2010 and ended on May
19, 2011, airing a total of 26 episodes. ET as part of Comedy Night Done
Right.

![](The_office_season-7.jpg "fig:The_office_season-7.jpg")[^2]

The previous season was [Season 6](Season_6 "wikilink") and the
succeeding season was [Season 8](Season_8 "wikilink").

It was the final season to feature series lead [Steve
Carell](Steve_Carell "wikilink") as [Michael
Scott](Michael_Scott "wikilink").

Production
----------

On June 28, 2010, it was confirmed that the seventh season was [Steve
Carell](Steve_Carell "wikilink")\'s last on the show, as his contract
expired.[^3] NBC planned on continuing the series after his departure,
and would welcome any return appearances by Carell.[^4] Actor and
writer/co-executive producer [B.J. Novak](B.J._Novak "wikilink") had
renewed his contract with Universal Media Studios through an eighth
season and would be promoted to full executive producer halfway through
the seventh season.[^5] As of this season, [Zach
Woods](Zach_Woods "wikilink"), who plays [Gabe
Lewis](Gabe_Lewis "wikilink") on the show, has been promoted to a series
regular.[^6] Executive producer/writer/actor [Paul
Lieberstein](Paul_Lieberstein "wikilink") had confirmed that [Amy
Ryan](Amy_Ryan "wikilink"), who portrays [Holly
Flax](Holly_Flax "wikilink"), will appear in eight episodes of the
season. He also confirmed [Kathy Bates](Kathy_Bates "wikilink") will
return as Jo Bennett in the season premiere.[^7] [Melora
Har](Melora_Hardin "wikilink")[din](Melora_Hardin "wikilink"), who
portrays [Jan Levinson](Jan_Levinson "wikilink"), has announced on her
Twitter account that she will return this season.[^8] [Timothy
Olyphant](Timothy_Olyphant "wikilink") will guest star in at least two
episodes as a rival salesman.[^9] It is also revealed that Pam dated
Olyphant\'s character while Jim dated
[Karen](Karen_Filippelli "wikilink").[^10] Holly will return to Dunder
Mifflin in the hour-long Christmas episode that will be written by
[Mindy Kaling](Mindy_Kaling "wikilink") and directed by [Rainn
Wilson](Rainn_Wilson "wikilink").[^11] An upcoming episode will include
the office employees getting together to watch an episode of the
television series *[Glee](Glee_(TV_series) "wikilink")*.[^12]

The seventh season of the show is produced by [Reveille
Productions](Reveille_Productions "wikilink") and Deedle-Dee
Productions, both in association with [Universal Media
Studios](Universal_Media_Studios "wikilink"). The show is based upon the
British series created by [Ricky Gervais](Ricky_Gervais "wikilink") and
[Stephen Merchant](Stephen_Merchant "wikilink"), both of whom are
[executive producers](executive_producer "wikilink") on both the US and
UK versions.[^13] *The Office* is produced by [Greg
Daniels](Greg_Daniels "wikilink"),[^14] who is also an executive
producer. Returning writers from the previous season include [Mindy
Kaling](Mindy_Kaling "wikilink"), [B.J. Novak](B.J._Novak "wikilink"),
[Paul Lieberstein](Paul_Lieberstein "wikilink"), [Brent
Forrester](Brent_Forrester "wikilink"), [Justin
Spitzer](Justin_Spitzer "wikilink"), [Aaron
Shure](Aaron_Shure "wikilink"), Charlie Grandy, [Daniel
Chun](Daniel_Chun "wikilink"), Warren Lieberstein, and Halsted Sullivan.
New writers in the seventh season include Peter Ocko, [Jon
Vitti](Jon_Vitti "wikilink"), Amelie Gillette[^15] and [Steve
Hely](Steve_Hely "wikilink"). Lieberstein serves as executive producer
and [showrunner](showrunner "wikilink"). Kaling, Novak, Shure, Chun and
Ocko are co-executive producers; Forrester and Vitti are consulting
producers; Spitzer and Grandy are supervising producers; and Warren
Lieberstein, Halsted Sullivan and Steve Hely are producers.

Episodes
--------

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Episode Title                                                                           Season Episode Number   Production Code            Original Airdate
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------- ----------------------- -------------------------- --------------------

  ![](Nepotism_infobox.jpg "Nepotism_infobox.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               [Nepotism](Nepotism "wikilink")                                                         1                       701                        September 23, 2010
  Directed and written by [Jeffrey Blitz](Jeffrey_Blitz "wikilink"). Michael upsets the office when he ignores their pleas to fire office assistant Luke, Michael\'s immature nephew. Pam\'s attempt to prank Dwight backfires when Kevin\'s faulty rewiring of an elevator strands the two together. Andy grows even more upset with Gabe and Erin\'s relationship.

  ![](Counseling.jpg "Counseling.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           [Counseling](Counseling "wikilink")                                                     2                       702                        September 30, 2010
  Directed by [Jeffrey Blitz](Jeffrey_Blitz "wikilink"), written by [B.J. Novak](B.J._Novak "wikilink"). Michael is forced to have six hours of counseling with Toby after he physically reprimanded Luke, but Michael refuses to make Toby\'s job easy. Pam attempts to finagle a promotion to office administrator. Dwight boycotts the [Steamtown Mall](Steamtown_Mall "wikilink") after a shop owner refuses to serve him.

  ![](Andy-s-Play-the-office-17734783-425-358.jpg "Andy-s-Play-the-office-17734783-425-358.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 [Andy\'s Play](Andy's_Play "wikilink")                                                  3                       703                        October 7, 2010
  Directed and written by [John Stuart Scott](John_Stuart_Scott "wikilink"), written by [B.J. Novak](B.J._Novak "wikilink"). Andy lands a role in a local production of *[Sweeney Todd](Sweeney_Todd "wikilink")* and invites the entire office to the performance, in hopes to impress Erin. While Michael struggles to put his jealousy aside, Jim and Pam have trouble with their less-than-stellar babysitter.

  ![](Sex_Ed.jpg "Sex_Ed.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   [Sex Ed](Sex_Ed "wikilink")                                                             4                       704                        October 14, 2010
  Directed and written by [Paul Lieberstein](Paul_Lieberstein "wikilink"). Michael comes to work thinking he has a pimple, but it turns out to actually be a [cold sore](cold_sore "wikilink"). When he is told that it is a form of herpes, Michael contacts all his ex-girlfriends---Jan ([Melora Hardin](Melora_Hardin "wikilink")), Holly ([Amy Ryan](Amy_Ryan "wikilink")), Helene ([Linda Purl](Linda_Purl "wikilink")), Donna ([Amy Pietz](Amy_Pietz "wikilink")) and Carol ([Nancy Carell](Nancy_Carell "wikilink")). Andy holds a sex education meeting in the office hoping to appeal to Erin\'s passionate side.

  ![](The_Sting.jpg "The_Sting.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             [The Sting](The_Sting "wikilink")                                                       5                       705                        October 21, 2010
  Directed by [Randall Einhorn](Randall_Einhorn "wikilink"), written by [Mindy Kaling](Mindy_Kaling "wikilink"). When a Dunder Mifflin client is stolen by a rival salesman named Danny ([Timothy Olyphant](Timothy_Olyphant "wikilink")), Michael, Dwight and Jim decide to set up a sting in order to uncover his sales secret. Andy starts a band with Darryl when he learns that one of his old college friends has a successful music career.

  ![](Costume_Contest.jpg "Costume_Contest.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 [Costume Contest](Costume_Contest "wikilink")                                           6                       706                        October 28, 2010
  Directed by [Dean Holland](Dean_Holland "wikilink"), written by [Justin Spitzer](Justin_Spitzer "wikilink"). Michael freaks out when Darryl goes over his head by taking an idea to corporate. The employees partake in a Halloween costume contest in the office. Meanwhile, Pam tries to get the truth from Danny about their dating history.

  ![](Christening.jpg "Christening.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         [Christening](Christening "wikilink")                                                   7                       707                        November 4, 2010
  Directed by [Alex Hardcastle](Alex_Hardcastle "wikilink"), written by [Peter Ocko](Peter_Ocko "wikilink") Pam and Jim\'s baby, Cece, gets [christened](christened "wikilink") and Michael invites the entire office to celebrate. Michael joins a church group of high school graduates on a mission to Mexico, with Andy following along to impress Erin.

  ![](Viewing_Party.jpg "Viewing_Party.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     [Viewing Party](Viewing_Party "wikilink")                                               8                       708                        November 11, 2010
  Directed by [Ken Whittingham](Ken_Whittingham "wikilink"), written by [John Vitti](John_Vitti "wikilink"). Erin and Gabe invite the office over to Gabe\'s house for a Glee viewing party. Michael can\'t handle the fact that the office workers think of Gabe as their boss instead of him. Growing more jealous of Gabe and Erin\'s relationship, Andy goes to extremes in order to impress her. Dwight helps Pam with Cece, much to Jim\'s chagrin.

  ![](Wuphf.jpg "Wuphf.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     [WUPHF.com](WUPHF.com "wikilink")                                                       9                       709                        November 18, 2010
  Directed by [Danny Leiner](Danny_Leiner "wikilink"), written by [Aaron Shure](Aaron_Shure "wikilink"). Michael helps Ryan by charming people to invest in his internet company, WUPHF.com. Dwight creates a hay festival in the parking lot for the Thanksgiving holiday. Jim learns of a new Sabre capping policy that prevents him from earning too much commission.

  ![](China.jpg "China.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     [China](China "wikilink")                                                               10                      710                        December 2, 2010
  Directed by [Charles McDougall](Charles_McDougall "wikilink"), written by [Warren Lieberstein](Warren_Lieberstein "wikilink") & [Halsted Sullivan](Halsted_Sullivan "wikilink"). When Michael reads an article about China growing as a global power, he decides they must be stopped before they take over the United States. Pam threatens to move Dunder Mifflin to a new building after everyone in the office complains about Dwight\'s building standards.

  ![](Officeclassychristmas.jpg "Officeclassychristmas.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     [Classy Christmas, Part 1](Classy_Christmas "wikilink")                                 11                      711                        December 9, 2010
  Directed by [Rainn Wilson](Rainn_Wilson "wikilink"), written by [Mindy Kaling](Mindy_Kaling "wikilink"). Michael couldn\'t be happier when Toby takes a leave of absence, leaving corporate to send Holly Flax ([Amy Ryan](Amy_Ryan "wikilink")) to cover for him. Michael forces Pam to plan a second Christmas party on the day Holly returns to Scranton. Jim agrees to a snowball fight with Dwight, which he later regrets.

  ![](Officeclassychristmas.jpg "fig:Officeclassychristmas.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 [Classy Christmas, Part 2](Classy_Christmas "wikilink")                                 12                      712                        December 9, 2010
  Directed by [Randall Einhorn](Randall_Einhorn "wikilink"), written by [Mindy Kaling](Mindy_Kaling "wikilink") Michael couldn\'t be happier when Toby takes a leave of absence, leaving corporate to send Holly Flax ([Amy Ryan](Amy_Ryan "wikilink")) to cover for him. Michael forces Pam to plan a second Christmas party on the day Holly returns to Scranton. Jim agrees to a snowball fight with Dwight, which he later regrets.

  ![](Ultimatum.jpg "fig:Ultimatum.jpg"){width="150"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          [The Ultimatum](The_Ultimatum "wikilink")                                               13                      713   January 20, 2011
  Directed by [David Rogers](David_Rogers "wikilink"), written by [Carrie Kemper](Carrie_Kemper "wikilink") Michael prepares himself for the good or bad news about Holly and her boyfriend. Pam decides to put up a resolution board for everyone to post their New Year's resolutions. Meanwhile, Michael calls for a counseling session with Holly.

![](The-office-the-seminar_article_story_main.jpg "fig:The-office-the-seminar_article_story_main.jpg"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                                       [The Seminar](The_Seminar "wikilink")                                                   14                      714   January 27, 2011
  Directed by [B.J. Novak](B.J._Novak "wikilink"), written by [Steve Hely](Steve_Hely "wikilink") With Andy's sales at his lowest ever, he gets desperate and holds a small business seminar with the help of some special guests. Michael and Holly turn the event into an improv challenge. Meanwhile, Erin enlists some of her co-workers in her Scrabble battle with Gabe.

![](The_office_season_7_search.jpg "fig:The_office_season_7_search.jpg"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      [The Search](The_Search "wikilink")                                                     15                      715   February 3, 2011
  Directed by [Micheal Spiller](Micheal_Spiller "wikilink"), written by [Brent Forrester](Brent_Forrester "wikilink") Dwight, Erin and Holly go on a search for Michael when he goes missing. Gabe becomes frustrated when Pam\'s cartoons are given captions that make fun of Sabre.

![](The-office-pda-alert.jpg "fig:The-office-pda-alert.jpg"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 [PDA](PDA "wikilink")                                                                   16                      716   February 10, 2011
  Directed by [Greg Daniels](Greg_Daniels "wikilink") and [Robert Padnick](Robert_Padnick "wikilink"). It\'s Valentine\'s Day in the office and everyone is getting sick and tired of Michael and Holly\'s PDAs. Andy helps Erin with a Valentine\'s Day scavenger hunt from Gabe.

![](Threat_Level_Midnight.jpg "fig:Threat_Level_Midnight.jpg"){width="150"}

[Threat Level Midnight](Threat_Level_Midnight "wikilink")                               17                      717   February 17, 2011
  Directed by [Tucker Gates](Tucker_Gates "wikilink"), written by [B.J. Novak](B.J._Novak "wikilink") Michael finally screens his epic film masterpiece \"Threat Level Midnight\" for the office - but Holly reacts poorly. The office thinks back to the filming of the movie.

![](The_Office_Todd_Packer.jpg "fig:The_Office_Todd_Packer.jpg"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              [Todd Packer](Todd_Packer_(episode) "wikilink")                                         18                      718   February 24, 2011
  Directed by [Randall Einhorn](Randall_Einhorn "wikilink") and written by [Amelie Gillette](Amelie_Gillette "wikilink") Michael\'s favorite Dunder Mifflin traveling salesman, Todd Packer (David Koechner), shows up at the office wanting a desk job causing everyone except Michael to protest the new member in the office.

![](The_Office_Garage_Sale.jpg "fig:The_Office_Garage_Sale.jpg"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             [Garage Sale](Garage_Sale "wikilink")                                                   19                      719   March 24, 2011
  Directed by [Steve Carell](Steve_Carell "wikilink"), written by [John Vitti](John_Vitti "wikilink") Michael finally pops the question to Holly while the office holds a garage sale in the warehouse. Dwight forces people to trade with him while Andy, Darryl, and Kevin play a board game while creating their own rules.

![](The-Office-Training-Day-Will-Ferrell-e1302838328855.jpg "fig:The-Office-Training-Day-Will-Ferrell-e1302838328855.jpg"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                    [Training Day](Training_Day "wikilink")                                                 20                      720   April 14, 2011
  Directed by [Paul Lieberstein](Paul_Lieberstein "wikilink"), written by [Daniel Chun](Daniel_Chun "wikilink") Michael\'s replacement appears in the office, to start receiving training from Michael. The new manager, [Deangelo Vickers](Deangelo_Vickers "wikilink") (Will Ferrell), has everyone hoping to make good first impressions: Andy finds himself awkwardly typecast while Jim and Pam worry that they\'ve come on too strong. Only Dwight is apathetic about the new leader.

![](The-office-michaels-last-dundies_article_story_main.jpg "fig:The-office-michaels-last-dundies_article_story_main.jpg"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                    [Michael\'s Last Dundies](Michael's_Last_Dundies "wikilink")                            21                      721   April 21, 2011
  Directed by [Mindy Kaling](Mindy_Kaling "wikilink"), written by [Mindy Kaling](Mindy_Kaling "wikilink") Michael trains his office replacement, Deangelo (Will Ferrell), for hosting the Dundie Awards. Meanwhile, Erin deals with her dislike of her boyfriend, Gabe.

![](THE-OFFICE-Goodbye-Michael-Part-2-Season-7-Episode-22-5-550x366.jpg "fig:THE-OFFICE-Goodbye-Michael-Part-2-Season-7-Episode-22-5-550x366.jpg"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                            [Goodbye, Michael](Goodbye,_Michael "wikilink")                                         22                      722   April 28, 2011
  Directed by [Paul Feig](Paul_Feig "wikilink"), written by [Greg Daniels](Greg_Daniels "wikilink"). Michael prepares to leave for Colorado with Holly, and spends his last day in the office saying goodbye to everyone individually, wanting no drama to ensue. Meanwhile, new manager Deangelo and Andy try to keep Michael\'s biggest clients.

![](Theinnercircle.png "fig:Theinnercircle.png"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                [The Inner Circle](The_Inner_Circle "wikilink")                                         23                      723   May 5, 2011
  Directed by [Matt Sohn](Matt_Sohn "wikilink"), written by [Charlie Grandy](Charlie_Grandy "wikilink") New office manager Deangelo picks favorites among the staff, revealing his true management style.

![](Dwight_K_Schrute.png "fig:Dwight_K_Schrute.png"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            [Dwight K. Schrute, (Acting) Manager](Dwight_K._Schrute,_(Acting)_Manager "wikilink")   24                      724   May 12, 2011
  Directed by [Troy Miller](Troy_Miller "wikilink"), written by [Justin Spitzer](Justin_Spitzer "wikilink") Dwight becomes the interim regional manager, after Jim refused, instituting a typically heavy-handed management style. Meanwhile, Gabe tries to win back Erin.

![](The-office-search-committee_article_story_main.jpg "fig:The-office-search-committee_article_story_main.jpg"){width="150"}
                                                                                                                                                                                                                                                                                                                                                                                                                                              [Search Committee](Search_Committee "wikilink")                                         25                      725   May 19, 2011
  Directed by [Jeffrey Blitz](Jeffrey_Blitz "wikilink"), written by [Paul Lieberstein](Paul_Lieberstein "wikilink") Deangelo\'s new replacement is sought out through a search committee process led by Jim.

[^1]:

[^2]:

[^3]:

[^4]:

[^5]:

[^6]:

[^7]:

[^8]:

[^9]:

[^10]:

[^11]:

[^12]:

[^13]:

[^14]:

[^15]: <http://www.avclub.com/articles/nepotism,45181/>
