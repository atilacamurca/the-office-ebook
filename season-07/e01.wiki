{{Office episode
|Title       =Nepotism
|Image       =[[File:Nepotism infobox.jpg|250px]]
|Season      =[[Season 7|7]]
|Episode     =1
|Code        =7001
|Original    =September 23, 2010
|Writer(s)   =[[Daniel Chun]]
|Director    =[[Jeffrey Blitz]]
|Prev        =[[Whistleblower]]
|Next        =[[Counseling]]
}}
'''"Nepotism"''' is the first episode of [[Season 7]] and the 127th episode of ''[[The Office]]'' overall. It was written by Daniel Chun and directed by [[Jeffrey Blitz]]. The episode originally aired on September 23, 2010. It was viewed by 8.40 million people.

==Synopsis==
The opening begins with various members of [[Dunder Mifflin Scranton|the office]] participating in a lip dub, with the exception of Toby, who is recording it.

[[Michael Scott|Michael]] reminisces over his summer, one in which he contracted West Nile virus and lost some weight. [[Gabe Lewis|Gabe]] reveals that he started dating [[Erin Hannon|Erin]] over the summer, and both parties agree that they are enjoying the experience. [[Andy Bernard|Andy]] tries to remain calm at the prospect, but it is obvious he is mad at Gabe for stealing his girlfriend. He tries to calm himself by picturing himself at a calm beach, in which he sees a whale in the ocean...eating Gabe.

[[Dwight Schrute|Dwight]] discloses that he became the owner of the building over the summer and carries a key ring everywhere he goes. [[Kelly Kapoor|Kelly]] participated in the executive training program at Yale, which she claims has made her smart.

The new assistant, [[Luke Cooper]], arrives late with the employees' coffee, and accidentally mixed up [[Darryl Philbin|Darryl's]] order. Darryl begins to talk about his summer, but expresses his dislike for Luke instead. In the break room, [[Oscar Martinez|Oscar]] and [[Phyllis Vance|Phyllis]] complain that they gave Luke packages to deliver to clients, none of which arrived at their destination. Michael defends Luke, however, much to the displeasure to the rest of the office.

[[Jim Halpert|Jim]] tries to play a prank on Dwight by adding more keys to his key ring, but [[Pam Beesly|Pam]] laughs and exposes Jim's plan. Pam says she is sorry, but Jim is upset that he was unable to prank Dwight.

As everyone in the office becomes increasingly annoyed with Luke, Michael continues to defend him. He holds a meeting for the purpose of telling everyone, "Don't not bother Luke," and gets frustrated when his use of a double negative is pointed out. In the ensuing discussion, Michael makes an announcement: "One more thing I did over the summer... I hired my nephew."

Michael tries to justify hiring Luke by explaining that he had an ulterior motive; namely, he hoped that doing so would end hostilities between him and his half-sister (Luke's mother). He also reveals the cause of said hostilities, explaining that there was a time when he lost Luke in a forest.

The rest of the office still persists in their demands for Michael to fire Luke, but he once again refuses.

Pam, trying to make it up to Jim, decides to come up with a plan to prank Dwight. She seeks the help of [[Kevin Malone|Kevin]] to rewire the elevator, so the buttons do other things than what they really should. After text messaging Dwight, she boards the elevator to show him "something in the lobby," but the plan backfires when they become trapped in the elevator due to the rewiring. Dwight starts to urinate in the elevator, which he states is due to all the water he has been drinking.

In the parking lot, members of the office discover the packages that were never delivered all sitting in Luke's car. [[Meredith Palmer|Meredith]] is able to break into the car so they retrieve the packages. With Luke now costing the office clients, Gabe and Michael confer with [[Sabre]] CEO [[Jo Bennett]] about the problem, and she sternly tells Michael to deal with the issue. When Luke continues to irritate the office, Michael gets fed up and repeatedly spanks him in front of the entire office.

Luke runs away crying and quits his job, with the rest of the office satisfied that Luke will no longer be a problem to them.

People from building maintenance are required to get Dwight and Pam out of the elevator. As it lowers to ground level, Jim is impressed with the prank Pam pulled on Dwight, despite the fact it had the opposite effect that Pam intended.

In the final scene, Gabe informs Michael that because he spanked Luke (which was "physical assault" of an employee) he must either have a counseling session or be fired immediately. Michael takes the former option, but finds out that the session will be with [[Toby Flenderson|Toby]], greatly upsetting Michael.

==Goofs==
Jim is able to put three keys on Dwight's key ring before Pam laughs and blows the prank. However, when Dwight grabs it back and throws the keys at Jim, he throws too many (one for every word; Ha. Ha. Not. funny. Jim). He would have actually thrown two of his own keys off the ring. However, it's possible this isn't the first time Jim has done this and Dwight is removing and throwing all of the keys he doesn't recognize.

==Trivia==
*Toby is the one filming the lip dub, on a lower quality camera than is typical for the show. This is presumably because the filming of the lip dub took place over the documentary crew's "summer break" when the production crew was not present at the office.
*Stanley drinks out of a plastic water cup even though everyone is supposed to be using metal Sabre bottles.
*Kevin refers to the circuit board of the elevator as the "circus board".
*Michael says he stepped on a piece of glass which, "Got infected, even though I peed on it." He was likely thinking of the common rule of peeing on a jellyfish sting.
*Angela remains in Darryl's office for the rest of the lip dub.
*Jo asks why Michael can't fire Luke and he says, "I love him." Jo says, "Oh Michael, how far has it gone?" believing them to be in a romantic relationship.
*There are 3 references in this episode to the film ''[[wikipedia:Kill Bill|Kill Bill]]'':
*#In the first scene with Evan Peters as Luke Cooper (Michael's nephew), Luke returns from a coffee run for the members of the office. When handing Darryl his order, Luke refers to him as "Darryl Hannah". Daryl Hannah is the name of the actress who plays Elle Driver, a character in Kill Bill.
*#In the cold open, The Office is lip-syncing to "Nobody But Me" by The Human Beinz. This song is also played in the Showdown scene, with O-Ren Ishii (Lucy Liu) facing off with The Bride (Uma Thurman).
*#At the end of that same scene, The Bride, as an alternative to killing a younger boy who joined the Yakuza gang, instead spanks him a number of times, much like the way Michael Scott reprimands Luke towards the end of the episode.

==Cultural References==
* The episode opens with a [[Wikipedia:lip dub|lip dub]] to [[wikipedia:The Human Beinz|The Human Beinz]]'s version of ''[[wikipedia:Nobody but Me (The Human Beinz song)|Nobody But Me]]''. A lip dub is a type of video which became wildly popular starting in late 2009. Performers lip-sync to an existing song shot in a single take.
* During the lip dub Creed is shown "playing the guitar". It may be a reference to the fact that Creed Bratton played lead guitar for the band The Grass Roots.
* Luke watches his computer monitor and says "Yay, strawberries are ripe." He is most likely playing [[Farmville]], a game on Facebook.
* Meredith tells Luke to stop listening to [[wikipedia:Chumbawamba|Chumbawamba]], a British band mainly known for their 1997 song "Tubthumping".
* Michael talks about open auditions for the band [[wikipedia:Hanson (band)|Hanson]] ("What if no one named Hanson showed up?"), not realizing that it is a family band consisting of three brothers.
* Creed says he follows Luke on Twitter because he doesn't talk about [[wikipedia:Betty White|Betty White]], the American actress who made a comeback in 2009. White is famous for her raunchy sense of humor and old age (she turned 90 in 2012).
* Pam says, "They don't call me the [[wikipedia:Bart Simpson|Bart Simpson]] of Scranton for nothing!" Bart is the ten-year-old troublemaker on the long running TV show ''The Simpsons''.
* Michael wonders how [[wikipedia:Ringling brothers|The Ringling Brothers]] manage to "still work with their family" every night all over America. The ''Ringling Brothers Circus'' was a circus founded in the United States in 1884. The company is still active, but the last founding brother died back in 1936.
* Michael saw the movie ''[[wikipedia:Inception|Inception]]'', a 2010 film about a man who can infiltrate other people's dreams.
* [[Wikipedia:Orville Redenbacher|Orville Redenbacher]] was a businessman best known for his brand of unpopped popcorn. Erin confuses him with "the guy who invented the peanut", presumably thinking of [[Wikipedia:George Washington Carver|George Washington Carver]] who developed peanut agricultural research.
* Gabe tells Erin he's going to [[Wikipedia:Siberia|Siberia]], a part of Russia known for its inhospitable climate and its use as a place of exile.
* Andy imagines himself on [[Wikipedia:Cape Cod|Cape Cod]], the easternmost portion of Massachusetts and known as the home to many upper-class families.
* Erin finds the pants that Michael wanted to return to ''Talbot's'', a chain of high-end women's clothing stores. The nearest Talbot's is at Montage Mountain. This scene refers to a previous episode [[The Negotiation]], when Michael wears a womans suit.
* Luke causes Phyllis to lose a potential sale to ''Lehigh Motors''. Lehigh is the name of a town and a county not too far from Scranton, although there is no such company.
* On the break room refrigerator is a magnet for [[Wikipedia:Sheetz|Sheetz]], a chain of gas station convenience stores.
* When the Office discovers their undelivered packages in Luke’s car, Michael (trying to defend his nephew) says Luke might own an eBay store. This could be a reference to the film ''[[wikipedia:40-Year-Old Virgin|The 40-Year-Old Virgin]],'' in which Steve Carell (who portrays Michael) falls in love with a women who owns an eBay store.

==Behind the Scenes==
* Writer [[Daniel Chun]] [http://www.officetally.com/the-office-lip-dub-qa answered some questions about the making of the lip dub].

==Connections to previous episodes==
* In "[[Body Language]]", Darryl drops out of the running for the Sabre Minority Executive Training Program because it would interfere with his softball schedule saying that he "only has about a year left in these knees". Turns out he had even less than that.
* Andy states in a talking head that Gabe stole Erin, however in "[[Sex Ed]]" we learn that Gabe asked Andy before he began to date Erin. We also see at the end of "[[Whistleblower]]" that Erin makes a move toward Andy suggesting Andy had a chance with Erin before Gabe but procrastinated and lost his shot.

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Halpert]]
*[[B.J. Novak]] as [[Ryan Howard]]
*[[Ed Helms]] as [[Andy Bernard]]

===Supporting Cast===
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]
*[[Zach Woods]] as [[Gabe Lewis]]

===Recurring Cast===
*[[Kathy Bates]] as [[Jo Bennett]]
*[[Hugh Dane]] as [[Hank Tate]]

===Guest Cast===
*[[Evan Peters]] as [[Luke Cooper]]
