# Search Committee

![](The-office-search-committee_article_story_main.jpg "fig:The-office-search-committee_article_story_main.jpg")

**\"Search Committee\"** is the hour-long finale of the
[seventh](Season_7 "wikilink") season of the American television comedy
series *The Office* and is the show\'s 151st/152nd episode overall. It
is the season\'s 25th/26th episode. It was written by Paul Lieberstein
and directed by Jeffrey Blitz. It originally aired on May 19, 2011. It
was viewed by 7.29 million people.

Synopsis
--------

With Deangelo Vickers still in a coma, Dunder Mifflin-Sabre Scranton is
left with Creed as interim regional manager. Jo Bennett puts Jim, Toby
and Gabe into a committee to interview candidates for the manager
position, including Andy, Darryl and a number of outsiders, including a
personal friend of hers, Nellie Bertram, who suggests various strange
methods for running an office, including a \"zen garden\" theme and
removal of all official titles for Dunder Mifflin Scranton. Pam takes it
upon herself to protect the office and their clients from Creed\'s
haphazard managerial style, such as distracting him with activities and
pretending to be their own clients when Creed starts calling around with
the false information that Dunder Mifflin is going out of business.

Dwight, still upset over being demoted by Jo, stops taking care of
himself and openly looks through want ads in the office. His attitude
changes when he speaks with applicant [Robert
California](Robert_California "wikilink"), who disparages the position
and the office to another manager candidate, Merv Bronte, in a
manipulative effort to make him abandon his interview. Merv ruins his
interview, and Dwight becomes incensed that the position might go to
someone who doesn\'t take it seriously. He demands to be interviewed,
and while Jim balks, after Dwight\'s persistent efforts to acquire an
interview, Jo instructs Jim to grant him one, as she likes \"a little
bit of crazy.\"

Other interviewees include David Brent, who video conferences from
England and raves about his own personal qualifications, Fred Henry, who
claims he has a \"three-step plan\" for the office but will not reveal
it, and Warren Buffett, who asks suspicious questions about
long-distance calls policies and gas incentives.

Darryl thinks his popularity with the staff (and his race) will make him
a shoe-in, so he thinks he doesn\'t have to do an interview like
everyone else. He is caught off guard when he actually must do an
interview and they ask for his resume. Darryl writes up an
overly-extensive four page resume, which Jo quickly derides, though Jim
encourages Darryl by saying that the interview and resume are mere
formalities for him. Darryl later brings in his daughter and attempts to
use her to gain sympathy points, though he quickly realizes that it was
a bad idea.

When Andy interviews for the manager position, Gabe hijacks the meeting
in an attempt to sabotage him by asking random trivia questions, most of
which Andy is able to successfully answer, although Andy is still
frustrated in the end. Gabe is also dismissive of Kelly during her
interview. Kelly exacts revenge by telling Jo about his relationship
with Erin and his harassing behavior after their breakup. In response,
Jo sends Gabe back to Sabre\'s Florida headquarters, and installs Kelly
to Gabe\'s position on the search committee. Jim eventually takes the
discussion to the entire office (after Kelly accepts a bribe from Dwight
to cast her vote for him), but the discussion gets out of control, and
Jim brings the committee back to the conference room.

Phyllis and Erin await the results of a DNA test to see if Erin is the
daughter previously given up by Phyllis in high school. The results are
negative, but Phyllis holds off on telling Erin after Andy rejects
Erin\'s proposal to start dating again, and continues to show maternal
affection toward her.

Meanwhile, Angela becomes engaged to her state senator boyfriend,
Robert. Oscar, who strongly believes that Robert is gay, shares his
feelings with Pam, and the rest of the office. They eventually decide
not to tell Angela out of respect for her feelings, despite Angela\'s
constant obnoxious and upbeat attitude about her forthcoming nuptials.

At the end of the episode, the cameras interview the people who were
interviewed for the manager position. Andy\'s sure that he failed, and
so is the guy with the glasses. The Finger Lakes guy is sure he
succeeded, and so is Robert California. Dwight Schrute vows that he will
either \"run this branch or destroy this branch.\" He then proceeds to
storm out of the lobby.

Cultural references
-------------------

-   Darryl calls [Microsoft](wikipedia:Microsoft "wikilink") and asks
    whether they still have [Clippy](wikipedia:Clippy "wikilink"), an
    unpopular feature from [Microsoft
    Office](wikipedia:Microsoft_Office "wikilink").
-   Phyllis mentions that many babies were born in 1982 due to the
    release of the comedy film,
    *[Porky\'s](wikipedia:Porky's "wikilink")*.
-   She also tells Erin how Bob Vance and her engaged in *bestiality*,
    sexual intercourse involving a human and a lower animal. This is not
    the correct term in this case because there were really no animals
    involved.
-   Robert California mentions he learned how paper is made on the PBS
    program *[Sesame Street](wikipedia:Sesame_Street "wikilink")*.
-   Angela says \"It\'s a little flashy. I mean, what am I, [Naomi
    Judd](wikipedia:Naomi_Judd "wikilink")?\", a reference to the
    [country music](wikipedia:Country_music "wikilink") singer.
-   Ryan insults Pam, saying that she would prefer [Rachael
    Ray](wikipedia:Rachael_Ray "wikilink") or the hosts of *[The
    View](wikipedia:The_View_(U.S._TV_series) "wikilink")* as the new
    manager.
-   Andy has trouble liking the AMC drama series *[Mad
    Men](wikipedia:Mad_Men "wikilink")*.

Cameos
------

-   [Jim Carrey](Jim_Carrey "wikilink"), [Will
    Arnett](Will_Arnett "wikilink"), and [Ray
    Romano](Ray_Romano "wikilink") make cameos in interviewing for
    Michael\'s job. They are all famous comedic actors like Steve Carrel
    and Will Ferrell.
-   [Ricky Gervais](Ricky_Gervais "wikilink") appears as his character
    from The Office (U.K.), [David Brent](David_Brent "wikilink"),
    interviewing for the position.
-   [Catherine Tate](Catherine_Tate "wikilink") ([Nellie
    Bertram](Nellie_Bertram "wikilink")) and [James
    Spader](James_Spader "wikilink") ([Robert
    California](Robert_California "wikilink")) make cameos as characters
    that play major roles in the next season.
-   [Warren Buffett](wikipedia:Warren_Buffett "wikilink") makes a cameo
    as a penny pincher.

Connections to previous episodes
--------------------------------

-   [David Brent](David_Brent "wikilink") from the original UK *The
    Office* returns via satellite. He previously asked Michael if there
    were any openings in the Scranton Branch in the episode [The
    Seminar](The_Seminar "wikilink").
-   Like Jan Levinson and David Wallace, Dwight seems to have crashed as
    a result of losing his job. This is the second time we\'ve seen him
    with facial hair as a result of being depressed about his life
    circumstances, the first being after his breakup with Angela.
-   Andy is shown doing a common anger management exercise in his car.

Amusing details/Trivia
----------------------

-   Creed is the only manager who did not come from the sales
    department.
-   Robert California is one of the only characters to blatantly
    acknowledge the documentary crew the way he does.
-   Ryan says that [Senator Robert Lipton](Robert_Lipton "wikilink") is
    \"totally gay\" because he liked his Facebook posts at three in the
    morning. In a later episode, Lipton is seen by the doc crew staring
    at Ryan\'s butt.
-   Kelly\'s desk has a name plate on it that says \"Minority Executive
    Trainee\"
-   Ryan casually takes cash from Kelly\'s wallet while talking to Oscar
    and Pam.
-   On several occasions it is possible to notice that Jenna Fischer is
    pregnant. Only in season 8, when hiding the pregnancy would be more
    of a hassle did the show introduce Pam\'s second pregnancy.
-   When talking about Dwight\'s possible interview, he and Jim are
    merely walking in a circle in the parking lot.
-   Dwight attempts to text Jo his resume line by line, and the text
    shown is poorly spelled.
-   Jo is angry because Dwight\'s texts are costing her US\$0.10 a piece
    despite being extremely wealthy, a characteristic not unlike that of
    Warren Buffet, who made a cameo as one of the candidates for the
    position of Regional Manager.
-   Jo\'s Cadillac has a Pennsylvania license plate, even though she
    lives in Florida. (Although, this is probably because it is a
    rental, as she flies everywhere.)
-   Andy is wearing a campaign-style button that says \"Andy for
    Manager?\"
-   The episode has two different end tags, depending on whether it is
    aired as a single episode or as a two-parter: in the former, Pam is
    seen distracting Creed some more, improvising an entire conversation
    between fake clients; in the latter, the episode ends with a series
    of talking heads in which several employees mention their
    expectations from the new manager. The extended producer\'s cut
    shows both end tags in succession.
-   This is the first appearance of the new reception sign.
-   The following people interview for the regional manager position:
    -   [Fred Henry](Fred_Henry "wikilink"), [Warren
        Buffett](wikipedia:Warren_Buffett "wikilink"), [Robert
        California](Robert_California "wikilink"), [Darryl
        Philbin](Darryl_Philbin "wikilink"), [Howard
        Klein](wikipedia:Howard_Klein "wikilink"), Deshaun Williams,
        [Merv Bronte](Merv_Bronte "wikilink"), [Andy
        Bernard](Andy_Bernard "wikilink"), [Kelly
        Kapoor](Kelly_Kapoor "wikilink"), [David
        Brent](David_Brent "wikilink"), [Nellie
        Bertram](Nellie_Bertram "wikilink"), \"[Finger Lakes
        Guy](Finger_Lakes_Guy "wikilink"),\" and [Dwight
        Schrute](Dwight_Schrute "wikilink").

Cast
----

### Main Cast

-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Halpert](Pam_Halpert "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")

### Supporting Cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")
-   [Zach Woods](Zach_Woods "wikilink") as [Gabe
    Lewis](Gabe_Lewis "wikilink")

### Recurring Cast

-   [Kathy Bates](Kathy_Bates "wikilink") as [Jo
    Bennett](Jo_Bennett "wikilink")
-   [Cody Horn](Cody_Horn "wikilink") as [Jordan
    Garfield](Jordan_Garfield "wikilink")
-   [Hugh Dane](Hugh_Dane "wikilink") as [Hank
    Tate](Hank_Tate "wikilink")

### Guest Cast

-   [James Spader](James_Spader "wikilink") as [Robert
    California](Robert_California "wikilink")
-   [Will Arnett](Will_Arnett "wikilink") as [Fred
    Henry](Fred_Henry "wikilink")
-   [Ray Romano](Ray_Romano "wikilink") as [Merv
    Bronte](Merv_Bronte "wikilink")
-   [Catherine Tate](Catherine_Tate "wikilink") as [Nellie
    Bertram](Nellie_Bertram "wikilink")
-   [Jim Carrey](Jim_Carrey "wikilink") as The [Finger Lakes
    Guy](Finger_Lakes_Guy "wikilink")
-   [Warren Buffett](Warren_Buffett "wikilink") as Interviewee
-   [Taylar Hollomon](Taylar_Hollomon "wikilink") as Jada
-   [Ricky Gervais](Ricky_Gervais "wikilink") as [David
    Brent](David_Brent "wikilink") (Uncredited)
