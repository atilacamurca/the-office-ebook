# China

![](China.jpg "China.jpg")

**\"China\"** is the tenth episode of the [seventh
season](Season_7 "wikilink") of *The Office* and the 136th episode
overall. The episode was directed by [Charles
McDougall](Charles_McDougall "wikilink") and was written by Halsted
Sullivan & Warren Lieberstein. It first aired on December 2, 2010. It
was viewed by 7.31 million people.

Synopsis
--------

After reading a magazine article about the rise of China\'s economy,
[Michael](Michael "wikilink") gets frightened that the United States
will fall victim to an evil ploy by the Chinese government due to its
immense size. In an announcement, Michael asks the staff for ideas on
how to boost the American economy and states a figure about how many
cities China has that have over a million people.
[Oscar](Oscar "wikilink") tries to correct him, but
[Ryan](Ryan "wikilink") and Stanley research it which proves Michael
correct.

The entire staff jokes with Oscar for the rest of the day about being
less intelligent than Michael, so Oscar invites Michael to coffee to
one-up him in another conversation about China. [Jim](Jim "wikilink")
and [Andy](Andy "wikilink") try to coach him, but Michael fumbles the
conversation and Oscar nearly walks away victorious and smug from the
debate. Michael, at the last minute, rebuts with an argument about
friendship and conversation which has no relevance to the conversation
but is delivered so passionately that the audience of
[Erin](Erin "wikilink"), [Kelly](Kelly "wikilink"), Ryan, Jim and Andy
all applaud and congratulate him.

As the new building owner, [Dwight](Dwight "wikilink") has made
financial cutbacks, including installing motion-activated light sensors
and half-ply toilet paper. [Pam](Pam "wikilink") starts to argue against
these changes, and as the new office administrator decides to find a new
building to move into. Pam finds a new space for the office in a plush
building. Dwight begins to get worried, but finds out she made up the
office as leverage. He starts to put pressure on her, and she confides
in [Jim](Jim "wikilink") that she doesn\'t want to be a failure as
office administrator.

Dwight overhears and sets up a ploy for Pam to find out about tenant
rights, which she uses against Dwight. He makes the changes and confides
in the camera that it was because he is **not** a compassionate person,
although a candid smile may reveal otherwise.

[Darryl](Darryl "wikilink") confronts [Andy](Andy "wikilink") about
sending him stupid text messages, but Andy continues to text him. Darryl
draws the line and gives Andy one more do-or-be-blocked message. Andy
texts him about pigeons in the parking lot, and Darryl reluctantly goes
but ends up enjoying the birds eating ice cream.

Cultural references
-------------------

-   Michael is initially worked up about China after reading an article
    in *[Newsweek](wikipedia:Newsweek "wikilink")*, an American weekly
    news magazine.
-   Michael notes that he was forced to read *Newsweek* in the waiting
    room at his dentist\'s office because some \"kid had the magazine I
    wanted to read\", hinting that the magazine Michael wanted to read
    was the children\'s magazine *[Highlights for
    Children](wikipedia:Highlights_for_Children "wikilink")*.
-   [Dognapping](wikipedia:Dognapping "wikilink") is the kidnapping or
    stealing of a dog owned by someone else, with the intention of
    demanding a ransom. According to Nate, Dwight is being questioned by
    the police in connection with a string of dognappings.
-   Jim played a game called *[Zombie
    Soccer](http://www.bored.com/game/play/150751/Zombie_Soccer.html)*
    for two hours.
-   Andy sends Darryl a text that only reads \"[Megan
    Fox](wikipedia:Megan_Fox "wikilink")?\", a reference to the American
    actress and model.
-   Michael does an impression of the then-governor of California,
    [Arnold Schwarzenegger](wikipedia:Arnold_Schwarzenegger "wikilink").
-   Creed notes that he understands, but cannot speak [Pirate
    slang](wikipedia:International_Talk_Like_a_Pirate_Day "wikilink").
-   *Tibet* is a plateau region in Asia, north-east of the Himalayas.
-   *Mao* *Zedong* was a Chinese communist revolutionary, dictator,
    political theorist and politician.
-   Andy attempts to motivate Michael with a pep talk taken from the
    1979 movie *[Rocky II](wikipedia:Rocky_II "wikilink")*.

Connections to previous episodes
--------------------------------

-   During Michael and Oscar\'s discussion in the lobby of the office
    building, a copy of *[Call of
    Duty](wikipedia:Call_of_Duty "wikilink")* can be seen behind the
    counter ([The Coup](The_Coup "wikilink")).
-   Michael returns from dentist appointments many times during the
    series, due to his soft teeth.

Amusing details
---------------

-   A box for the game \"Call of Duty\" is tucked away in the security
    desk in the lobby, visible during Oscar and Michael\'s
    debate.[1](http://www.officetally.com/the-office-call-of-duty) The
    video game \"Call of Duty\" made appearances in earlier episodes
    [The Coup](The_Coup "wikilink") and [Email
    Surveillance](Email_Surveillance "wikilink").
-   When Dwight and Nate are locating the building, Nate reads out
    \"10706\", but the sign reads \"10735\".
-   When Kevin is coming up with the antacid pill ideas, Stanley says,
    \"Why not just go one for the year?\" and Kevin says, \"It\'s too
    big a pill to swallow.\" It appears Kevin was just making the pills
    bigger instead of using a stronger antacid.
-   Michael says, \"Yes, Pam this is exactly why I hired you as Office
    Administrator, handle it.\" Michael did not hire her as Office
    Administrator, and he didn\'t pay attention to what Pam was saying
    when he signed off on it.
-   Michael says that hitting ctrl-p will open a new tab as long as the
    printer isn\'t hooked up. This is false.

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Halpert](Pam_Halpert "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")

### Supported cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")
-   [Zach Woods](Zach_Woods "wikilink") as [Gabe
    Lewis](Gabe_Lewis "wikilink")

### Recurring cast

-   [Hugh Dane](Hugh_Dane "wikilink") as [Hank
    Tate](Hank_Tate "wikilink")
-   Mark Proksch as Nate

### Guest cast

-   Sarah Zimmerman as \"Other Pam\"
