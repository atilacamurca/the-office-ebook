{{Office episode
|Title      =Viewing Party
|Image      =[[File:Viewing Party.jpg|253px]]
|Season     =[[Season 7|7]]
|Episode    =8
|Code       =7008
|Original   =November 11, 2010
|Writer(s)  =[[Wikipedia:Jon Vitti|Jon Vitti]]
|Director   =[[Ken Whittingham]]
|Prev       =[[Christening]]
|Next       =[[WUPHF.com]]
}}
'''"Viewing Party"''' is the eighth episode of the seventh season of ''[[The Office (US)|The Office]] ''and the 134th overall. It was written by Jon Vitti and directed by [[Ken Whittingham]]. It first aired on November 11, 2010. It was viewed by 7.15 million people.

==Synopsis==
The office employees are watching the pursuit of "The Scranton Strangler", live on television. They see that the chase is heading down their street, so [[Michael]] races to the windows, just in time to get a glimpse of the chase. He then goes outside and collects the gravel which the chase occurred on, thinking that it may be of value someday.

Erin and Gabe invite the office over to Gabe's house for a ''Glee'' viewing party. Before the party, Michael becomes frustrated when Kevin refers to Gabe as his boss. When the show starts at Gabe's house, Michael and Gabe get into an argument over how high the volume should be. After taking turns raising and lowering the volume, Michael retreats to Gabe's bedroom, hoping that his employees will follow him there. Meanwhile, Pam has been having a hard time with getting Cece to go to sleep at night, so Dwight picks her up to try to calm her down. Pam is stunned to find out that Cece becomes completely quiet in Dwight's hands.

Jim changes the channel from the ''Glee'' episode so he can check sports scores. Kelly yells at Jim to change the channel back to ''Glee'', but discovers much to her horror that Erin had forgotten to record the episode. In order to avoid being the target of Kelly's anger, Jim goes into the bedroom to see Dwight holding a sleeping Cece. He is visibly uncomfortable when Pam says, "It's a miracle. She loves him." Angela comes into the room and demands that Dwight meet her outside for fulfillment of their sex contract. Dwight is about to leave, but Pam persuades him to stay with the baby. Jim is then forced to feed Dwight a pizza and a beer while Pam goes outside to tell Angela that Dwight cannot see her.

Andy is growing increasingly jealous of Gabe's relationship with Erin. While observing Gabe's room, he becomes intoxicated with a Chinese virility supplement (made from seahorse) Gabe has in his bedroom. The supplement initially works but soon makes Andy sick, prompting him to later admit his jealousy to Phyllis, who decides to help Andy by casually talking to Erin about her relationship with Gabe. Phyllis attempts this by describing the first time she and her husband Bob saw each other naked. She says that they just stared at each other for hours before finally falling asleep. Erin becomes disgusted and walks off. Becoming irritated over the attention people are paying to the TV show, Michael goes outside and pulls the cable connection, causing it to turn off. Hysteria erupts from Kelly and many of the other ''Glee'' fans. Michael becomes self-conscious over the commotion he has caused and goes outside to fix the cable.

Outside, Michael is discovered by Erin, who throughout the evening has been trying to encourage Michael to bond with Gabe. Michael, still indignant over the idea that his office views Gabe as his boss, questions Erin as to why she needs his approval, since he's not her father—a statement that is met with complete silence from Erin. Michael, realizing that Erin views him as a father figure, jokes around and tells her to go to her room, leading to a sweet moment between the two. As he leaves the party, Michael warns Gabe that he will, both figuratively and literally, kill Gabe if he breaks Erin's heart.

==Amusing details==
* Michael says that his favorite character on ''Glee'' is "the invalid." Wheelchair-bound Artie Abrams is portrayed by actor [[Kevin McHale]], who appeared in [[Launch Party]] as the [[Pizza Delivery Kid|pizza delivery boy]] kidnapped by Michael after he refused to honor a coupon.
* In a talking head interview, Kelly points out inconsistencies in the writing of two characters on ''Glee''. This is possibly a tongue-in-cheek inclusion by the writers, who may be aware of the irony that the character Kelly herself was not initially a chatterbox, but was later written to be one.
*An iMac and a MacBook Pro can be seen on Gabe's desk.
*Ryan notes Gabe's Marantz tube stereo in his bedroom during the party. The turntable Gabe owns in this episode happens to be a Sony PS212.
*Kevin thinks Gabe is the boss even though Michael made him cry because he was accused of thinking Gabe was the boss in [[Costume Contest]].
*Kelly is extremely interested in the show at the viewing party even though she rants to Erin about how she doesn't want to go to the party because the show is terrible.

==Cultural references==
*When Erin invites Michael to her party, they engage in a walk-and-talk popularized by Aaron Sorkin in ''The West Wing''.
*Jim mentions events like the [[wikipedia:Balloon boy|Balloon boy hoax]] and the [[wikipedia:Michael Jackson memorial service|funeral]] of pop star [[wikipedia:Michael Jackson|Michael Jackson]].
*''[[wikipedia:Glee (TV series)|Glee]]'' is an American musical comedy-drama TV series.
*Oscar says an actress on ''Glee'' also appeared on the American sports drama TV series ''[[wikipedia:Friday Night Lights (TV series)|Friday Night Lights]]''. The actress he pointed to was Dianna Agron, who has never appeared on ''Friday Night Lights''.
*Phyllis wears ''White Diamonds'' by Elizabeth Taylor perfume, just like Andy's nanny used to.
* When arguing with Gabe about the volume on the TV set, Michael exclaims: "Turn it up to eleven!", which is a reference to the 1984 comedy mockumentary ''This Is Spinal Tap'' (Inaccurately referred to as "Spinal Cord" by Michael). ''This Is Spinal Tap'' was also one of the main inspirations for the original UK version of [[The Office UK|The Office]] according to [[Ricky Gervais]].
* When Pam tries to go talk to Angela for Dwight, he says, "She is in heat. She will eat your face off." Heat is a term for female mammals in a part of their reproductive cycle when they are trying to reproduce. Animals can become easily agitated in this time.

==Goofs==
*Several of the references to the ''Glee'' episode playing (Duets) is not correct. Kelly tells the group that 'Blinded by the Light' was performed by a blind person, this song has never been performed on the show nor has it ever featured a blind individual. Creed also tells the group that Puck and Finn 'worked it out' and that a solo song was changed into a duet. Puck was absent from this episode and it is unclear which song he is referring to being changed into a duet. However, it's possible Creed was deliberately misleading the group.
*When Jim changes the channel to check basketball scores, one of the games is listed as "Los Angeles vs Philadelphia." Seeing as there are two NBA teams in Los Angeles, neither team would ever be listed as only the name of the city. They probably had to do this for copyright reasons, but it's still unrealistic.
*Michael mentions that he liked Ed Truck, but in "The Carpet", he strongly expressed how he disliked his old boss.

==Connections to previous episodes==
*Michael lists better men than Gabe who have tried to be his boss and got fired:
#"David got fired" ([[Secret Santa]])
#"Charles got fired" ([[Secret Santa]])
#"Jan went crazy" ([[The Job]])
#"Ed Truck got decapitated" ([[Grief Counseling]])
*Creed's Chinese skills were mentioned before in [[The Fight]]. (Creed says, when translating for Andy, "haima buyao gen jiuluan gao," which means, "do not mix the seahorse with alcohol." [海马不要跟酒乱搞])
*Dwight asks Jim to "beer" him, just like Andy asked Jim in [[Traveling Salesmen]].
*Dwight is helpful with CeCe. In "Baby Shower" he says that babies are one of his 'areas of expertise'.

==Cast==
===Main cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Halpert]]
*[[B.J. Novak]] as [[Ryan Howard]]
*[[Ed Helms]] as [[Andy Bernard]]

===Supporting cast===
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]
*[[Zach Woods]] as [[Gabe Lewis]]
