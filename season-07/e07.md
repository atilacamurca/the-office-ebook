# Christening

![](Christening.jpg "Christening.jpg")

**\"Christening\"** is the seventh episode of the
[seventh](Season_7 "wikilink") season of *The Office* and the 133rd
episode overall. It was written by Peter Ocko and directed by Alex
Hardcastle. It originally aired on November 4, 2010. It was viewed by
7.65 million people.

Synopsis
--------

Office Administrator Pam tries to discuss
[Sabre\'s](Sabre_(company) "wikilink") tips on staying healthy to the
office, but is constantly interrupted by Dwight, who believes no
attempts should made to improve sanitation, and instead let his immune
systems work. He then invites everyone to sneeze on him, and Jim, Erin,
and Andy happily fulfill his request.

Jim and Pam\'s daughter Cece, is being christened, and Michael has
invited the entire office to the service, much to their disappointment.
Michael believes he is Cece\'s godfather but is disappointed when he
learns this is not the case. Toby, who remarks on his history between
himself and \"the big Man\", hesitates to enter the church. Over the
course of the episode, Ryan frequently hints at his distaste for
religion. He paraphrases Karl Marx\'s \"Opium of the masses\" statement
and mentions \"Drinking the Kool-Aid\" while the church\'s youth group
is speaking. During the service, Cece\'s dress is ruined when Jim
changes her diaper. Jim scrambles to replace the gown, and Cece is
eventually baptized in an Arcade Fire t-shirt he found in the back of
his car. At the end of the mass, the minister announces there will be a
reception hosted by Jim and Pam, who panic because they were expecting
only family and are not prepared for so many guests.

At the reception, food quickly runs out and Cece goes missing after Jim
asks Pam\'s grandmother to keep an eye on her. After finding out from
guests that Cece was last seen with a \"short blond woman\" (Pam\'s
mother Helene, who was changing Cece), Jim thinks Angela (who has
insulted Jim and Pam but adored Cece all day) has kidnapped Cece, and
calls her out as she is leaving. Believing her to be the kidnapper,
Kevin attacks Angela\'s purse, where she has hoarded some of the food
meant for the guests.

While Dwight uses the event to try to sell paper and printers, Toby
finally enters the church after the ceremony. He looks at the crucifix
and simply, achingly asks, \"Why do you have to be so mean to me?\"

Michael is inspired by the fellow churchgoers, especially the youth
ministry, which is going to Mexico to help build a school for
underprivileged children. As the kids leave, Michael shakes hands with
every one of them and wishes them luck, before hopping on the bus
himself. The other employees try to talk him out of it (with the notable
exception of Darryl), but Michael refuses. Just before they leave, Andy
(in another attempt to impress Erin) boards the bus with him. Both
realize their mistake 45 minutes into the trip, and force the bus to
stop. They (along with a kid from the youth ministry who left with them)
then have Erin drive them home while having a good laugh about it, and
Michael is happy to hear that the rest of his staff went out together to
see a movie.

Cultural references
-------------------

-   In the cold opening meeting, Kelly throws in a \"yo mama\" joke,
    also known as [maternal
    insult](wikipedia:Maternal_insult "wikilink").
-   [Transylvania](wikipedia:Transylvania "wikilink") is a historical
    region in the central part of Romania, often associated with
    vampires and the horror genre in general. Dwight claims that the
    wool there is expensive because of the Euro, but Romania is not in
    the Eurozone (it has a different currency).
-   Ryan misstates religion as \"the opium of the masses\"; it was Karl
    Marx who said \"[Religion is the opium of the
    people](wikipedia:Opium_of_the_people "wikilink")\".
-   Michael introduces the question about who the godfather of the child
    is by doing his somewhat vague
    *[Godfather](wikipedia:The_Godfather "wikilink")* impression.
-   Cece wears an adult size [Arcade
    Fire](wikipedia:Arcade_fire "wikilink") shirt. Arcade Fire is a
    Canadian indie rock band.
-   [Teach For America](wikipedia:Teach_For_America "wikilink") is an
    American non-profit organization that enlists high-achieving recent
    college graduates and professionals to teach for at least two years
    in low-income communities throughout the United States. Ryan says
    they have hotter girls than the ones at the baptism.
-   Michael suggests that they hold a car wash to send some cheerleaders
    to Regionals. This is a reference to the
    *[Glee](wikipedia:Glee_(TV_series) "wikilink")* episode
    \"[Acafellas](wikipedia:Acafellas "wikilink")\".
-   Michael says that his coworkers are like the mean girls in the movie
    *[Mean Girls](wikipedia:Mean_Girls "wikilink")*.
-   While driving [Michael](Michael "wikilink") and
    [Andy](Andy "wikilink"), [Erin](Erin "wikilink") turns on the radio
    because \"Lake Wobblegon\" is on. She is referring to [Lake
    Wobegon](wikipedia:Lake_Wobegon "wikilink"), the setting for the
    radio show [A Prairie Home
    Companion](wikipedia:A_Prairie_Home_Companion "wikilink"), on
    [NPR](wikipedia:NPR "wikilink"). Its host, [Garrison
    Keillor](wikipedia:Garrison_Keillor "wikilink"), is heard.

Amusing Details
---------------

-   When Michael enters the bus, one of the kids asks him, \"Are you
    coming?\" Michael surprisingly doesn\'t reply with his usual
    \"That\'s what she said!\" Apparently his new-found faith made him
    temporarily less nasty.

Deleted Scenes
--------------

-   Kevin comments that the Cece looks just like him.
-   Angela, Kelly, Ryan and Oscar all talk about having a child. 
    -   Angela: She wants a child and believes she would be a great
        mother.
    -   Kelly/Ryan: Kelly wants a mixed-race child (much to Ryan\'s
        discomfort) and Ryan attempts to talk Kelly down from the idea.
    -   Oscar: He says he doesn\'t want a kid.
-   A family photo is taken, and Michael mistakenly believes he is
    supposed to be in the photo (when he\'s actually supposed to take
    the photo).
-   Dwight puts his business card in all the bibles and hymnals.
-   Erin claims that Gabe \"adopts families\" and in a talking head he
    flips through photos of many African children. He then explains that
    when he sent them a photo, the village sent him food (mistakenly
    believing him to be famished).
-   Ryan talks about his experience volunteering at the Gulf shore,
    calling it a \"total f\--kfest\".
-   Andy runs on the bus and shouts, \"Beer me a shirt!\" However, they
    are out of shirts and can only give him a pen.
-   Jim and Pam give a toast about Cece, but Pam gets a text from
    Michael, asking someone to pick them up. Erin immediately grabs her
    things and bolts out the door.
-   Toby sits on a tree in front of the church, smiling, he says \"You
    know what Toby? You\'re gonna be all right.\" He then looks up and
    talks to a bird happily.

Connections to previous episodes
--------------------------------

-   Toby is reluctant to enter the church and says that \"It\'s been a
    while\". This could be because Toby was in a seminary for a year but
    dropped out because he wanted to have sex with a girl, as referenced
    in [Casual Friday](Casual_Friday "wikilink"). It can also be
    considered a goof as Toby was laying down in a church pew in the
    episode *[Niagara](Niagara "wikilink")*, while waiting for Jim and
    Pam to proceed with their wedding, before being interrupted by
    Dwight, although he could be nervous about attending Sunday service
    instead of just entering the church.
-   In [The Whistleblower](The_Whistleblower "wikilink"),
    [Nick](Nick "wikilink") the IT guy leaves Sabre to teach through
    Teach for America.
-   In a deleted scene, Andy says, \"Beer me a shirt!\" which is most
    likely a reference to [Product Recall](Product_Recall "wikilink")
    where Andy explains that he likes to say \"Beer me\" to get a laugh.

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Halpert](Pam_Halpert "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")

### Supporting cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")
-   [Zach Woods](Zach_Woods "wikilink") as [Gabe
    Lewis](Gabe_Lewis "wikilink")

### Recurring cast

-   [Bobby Ray Shafer](Bobby_Ray_Shafer "wikilink") as [Bob
    Vance](Bob_Vance "wikilink")
-   [Linda Purl](Linda_Purl "wikilink") as [Helene
    Beesly](Helene_Beesly "wikilink")

### Guest cast

-   Peggy Stewart as [Sylvia](Sylvia "wikilink") \"Meemaw\"
-   Robert Pine as Gerald Halpert
-   Perry Smith as Betsy Halpert
-   Rick Overton as [William Beesly](William_Beesly "wikilink")
-   Zoe Jarman as [Carla](Carla "wikilink")
