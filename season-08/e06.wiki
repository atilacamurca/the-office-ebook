'''"Doomsday"''' is the sixth episode of the eighth season of the American television comedy series ''The Office'' and is the show's 158th overall. It was directed by Troy Miller and written by Daniel Chun. It originally aired on November 3, 2011.

In the episode, [http://en.wikipedia.org/wiki/Dwight_Schrute Dwight] installs a doomsday device that will fire all of the employees and effectively close down the branch if they make too many mistakes. Meanwhile, [http://en.wikipedia.org/wiki/Gabe_Lewis Gabe] attempts to court the new warehouse worker, [[Val Johnson|Val]].

"Doomsday" received mixed-to-positive reviews from critics, with some reviews critiquing the episode's resolution. According to [http://en.wikipedia.org/wiki/Nielsen_Media_Research Nielsen Media Research], the episode was viewed by 6.15 million viewers and received a 3.2 rating/8% share among adult between the ages of 18 and 49, marking an increase in the ratings from the previous episode. The episode ranked third in its time slot and was the highest-rated NBC show of the night.

==Synopsis==
In the cold open, [[Andy Bernard|Andy]], to commemorate and "bring closure" to the end of each work day, sings [http://en.wikipedia.org/wiki/Closing_Time_%28Semisonic_song%29 "Closing Time"], much to the dismay of his visibly annoyed co-workers. Andy, frustrated that no one but he and [[Erin Hannon|Erin]] enjoy the tradition, starts to scold the workers until [[Stanley Hudson|Stanley]] comes in happily singing the song alongside him.

When [[Robert California]] confronts Andy about the number of mishaps and blunders made in the office, [[Dwight Schrute|Dwight]] devises a system to find mistakes made by employees in the office. To improve efficiency, he installs a "[http://en.wikipedia.org/wiki/Doomsday_device doomsday device]" that will send incriminating emails to Robert if they make five mistakes in one day, effectively causing them to lose their jobs. After only one day, however, the group manages to make five mistakes, and Dwight reveals that the emails will be automatically sent to Robert at 5:00 pm. Andy pleads with Dwight to deactivate the machine, but Dwight refuses, belittles the rest of the office staff for being so careless, and retreats to [[Schrute Farms]].

After Dwight abandons the group, Andy tasks [[Pam Beesly|Pam]], Erin, and [[Kevin Malone|Kevin]] to try and change Dwight's mind. The group find Dwight digging a horse grave and offer to help him out. Andy tries to talk to Dwight about deactivating the machine, but Pam consistently quiets him, saying she knows Dwight will come to understand if they aren't pushing it. After complimenting Dwight numerous times and having a small meal, Pam makes a joke that emphasizes that everyone is human and makes mistakes. Dwight, after hearing this, deactivates the machine after the party leaves Schrute Farms. Meanwhile, [[Jim Halpert|Jim]] is tasked with tracking down Robert at a [http://en.wikipedia.org/wiki/Squash_%28sport%29 squash court] and intercepting the doomsday emails before Robert has a chance to view them. To distract him, Jim plays several games, demonstrating his complete lack of skill at squash. Eventually, he discovers that the email has not come through, and prepares to leave, but Robert makes him stay and continue playing.

Meanwhile, [[Gabe Lewis|Gabe]] unsuccessfully tries to strike up a relationship with a new warehouse worker, Val. Gabe believes that he and Val have a connection, and constantly pursues and tries to impress her throughout the day. [[Darryl Philbin|Darryl]], observing Gabe's pitiful attempts, tries to imply that Val doesn't feel the same way, but to no avail. When Gabe does ask Val out, she politely declines and says she does not date co-workers at all. Darryl hears all of this and decides he won't pursue Val either.

==Cultural references==
*Andy sings "[http://en.wikipedia.org/wiki/Closing_Time_%28Semisonic_song%29 Closing Time]" by the American alternative rock band [http://en.wikipedia.org/wiki/Semisonic Semisonic] at the end of every workday.
*Andy says [[Oscar Martinez|Oscar]] is "the ''[http://en.wikipedia.org/wiki/Sex_%26_The_City Sex & The City]'' gang", referring to the group of girlfriends in the HBO comedy series.
*[http://en.wikipedia.org/wiki/Tweedle_Dee_%26_Tweedle_Dum Tweedledee and Tweedledum] has become slang for any two people who look and act in identical ways, generally in a derogatory context. Andy calls Kevin "both". 
*''[http://en.wikipedia.org/wiki/Iron_Chef_America Iron Chef]'' is a cooking show.
*Andy tells Dwight, "You're the deuce I never want to drop," to literally mean that Dwight is a good #2. Dropping a deuce is also a euphemism for pooping.
*Jim and Darryl talk about the [http://en.wikipedia.org/wiki/2011_NBA_lockout 2011 NBA lockout].
*Jim jokes that Robert's favorite songs are "[http://en.wikipedia.org/wiki/Creep_%28TLC_song%29 Creep]" by [http://en.wikipedia.org/wiki/TLC_%28band%29 TLC] and "[http://en.wikipedia.org/wiki/Creep_%28Radiohead_song%29 Creep]" by [http://en.wikipedia.org/wiki/Radiohead Radiohead].
*Dwight briefly compares himself to [http://en.wikipedia.org/wiki/Doctor_Frankenstein Doctor Frankenstein], [http://en.wikipedia.org/wiki/Dr._Jekyll_and_Mr._Hyde_%28disambiguation%29 Doctor Jekyll], and [[wikipedia:The_Island_of_Doctor_Moreau|Doctor Moreau]], but the doctor he is searching for is Dr. No.
*[[Angela Martin|Angela]] says, "Where were you two hours ago, ''[http://en.wikipedia.org/wiki/A_Beautiful_Mind_%28film%29 Beautiful Mind]''?" referring to the 2001 American biographical drama film based on the life of [http://en.wikipedia.org/wiki/John_Forbes_Nash,_Jr. John Nash], a Nobel Laureate in Economics.

==Amusing details/Trivia==
* Dwight tells Kevin to keep his shoes on when entering Dwight's house referencing Kevin's medical condition with his feet.
*Gabe asks Val to a date in the cemetery, further emphasizing his love for all things scary/creepy.
*Despite his teasing, it appears Darryl did get Gabe a frappaccino after all.
*Dwight flies the Historical 15 Star 15 Stripe US flag from his house rather than the current 50 Star 13 Stripe design.
*When Oscar makes a mistake on purpose to see if the doomsday device is legit, the device says that they made a late delivery, however the mistake Oscar claimed he made was sending an order down to shipping before payment was received for the order.
*Dwight says the email doesn't go out until 5:00 P.M., however when Oscar gets the fifth strike, the camera pans on Dwight's computer loading up several files, presumably to be sent to Robert.
*Robert doesn't question Jim being at the squash court instead of at work.
*Pam is once again one of the only people who is able to get through to Dwight as she did with her "Pobody's nerfect" saying.
*Jim tells Robert that he has the iPhone that "nobody has," then proceeds to use a phone that isn't an iPhone at all.
*Pam doesn't know the lyrics to "Closing Time." Oddly enough, while the term is traditionally about a bar, the song is about being anxious at the arrival of a new baby (which is why the room will not be open again "til your brothers or your sisters come").  Pam, being so far along in her pregnancy, would probably get a kick about this if she knew of it.

==Goofs==
* When Jim leaves the court after being asked if he was interested in Robert's business, the clock on the wall says it is about 5:10. Because time plays a huge role in this episode, it's quite a noticeable mistake.

==Cast==

===Main Cast===
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly|Pam Halpert]]
*[[B.J. Novak]] as [[Ryan Howard]]
*[[Ed Helms]] as [[Andy Bernard]]
*[[James Spader]] as [[Robert California]]
===Supporting Cast===
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin]]-Lipton
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]
*[[Zach Woods]] as [[Gabe Lewis]]
===Recurring Cast===
*Ameenah Kaplan as [[Val]]
*Mark Proksch as [[Nate Nickerson|Nate]]
===Guest Cast===
*Michael Daniel Cassady as Gideon
*Mike E. Winfield
*Tom Virtue

{{Season8}}
