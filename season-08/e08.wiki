'''"Gettysburg"''' is the eighth episode of the [[Season 8|eighth season]] of the American comedy television series ''The Office'', and the show's 160th episode overall. The episode aired on [[wikipedia:NBC|NBC]] in the United States on November 17, 2011. It was written by [[Robert Padnick]] and directed by [[Jeffrey Blitz]].

==Synopsis==
[[Andy Bernard|Andy]] decides to motivate and inspire everyone by taking them on a field trip to Gettysburg. Much to Andy's chagrin, half of the office volunteers to stay behind and [[Robert California]] asks them to come up with [[Dunder Mifflin Paper Company|Dunder Mifflin]]/[[Sabre]]'s next big idea. However, the office fails to impress Robert with their ideas. [[Kevin Malone|Kevin]] details a very basic idea he has about cookie placement in the vending machine, but Robert gets the impression that he is extremely clever and is speaking only in metaphors. [[Ryan Howard|Ryan]] later tricks Kevin into explaining an idea he has involving the Big Mac, and Robert realizes that there was no subtext to Kevin's ideas.

Meanwhile, at Gettysburg, Andy is unhappy with the lack of morale and belief in his leadership and attempts to make an analogy between running a paper company and fighting in the Civil War. Andy starts to lead a haphazard tour, and eventually most of office members sit down to rest. [[Jim Halpert|Jim]] and [[Darryl Philbin|Darryl]] follow Andy after he presses on, and eventually tell him to stop trying to impress the office. They reinforce the fact that his co-workers like him the way he is.

During the tour, [[Dwight Schrute|Dwight]] argues with [[Oscar Martinez|Oscar]] about what he claims is the northernmost battle of the Civil War: [[the Battle of Schrute Farms]]. Dwight maintains that its absence from the history books is an example of rewriting history, whereas Oscar maintains that the battle is a fictitious creation. After tracking down a historian, Dwight and Oscar learn that the Battle of Schrute Farms did in fact take place. However, the "battle" was really a code term. During the Civil War, Schrute Farms was a safe haven for artists and poets (and, as heavily implied, homosexuals). Oscar finds this fascinating, whereas Dwight leaves in disgust.

[[Gabe Lewis|Gabe]] is sidetracked by another tour group, who assume that he is an Abraham Lincoln impersonator (which Gabe notes is a frequent occurrence when he is seen by children), and he is forced to give an improvised but applauded performance.

==Cultural references==
*On the bus ride to Gettysburg, Darryl brings the movie ''[[Wikipedia:Limitless|Limitless]]'' starring [[Wikipedia:Bradley Cooper|Bradley Cooper]], who co-starred with Ed Helms in ''[[Wikipedia:The Hangover|The Hangover]]'' and ''[[Wikipedia:The Hangover Part II|The Hangover Part II]]''.
*''[[wikipedia:Source Code|Source Code]]'' is a 2011 American sci-fi techno-thriller film starring Jake Gyllenhaal.
*Oscar sarcastically calls Dwight historian [[wikipedia:Gore Vidal|Gore Vidal]].
*Ryan's idea involves [[wikipedia:Origami|origami]] and [[wikipedia:Sushi|sushi]].
*Gabe does an impression of [[wikipedia:Abraham Lincoln|Abraham Lincoln]], the 16th president of the United States, and his [[wikipedia:Assassination of Abraham Lincoln|assassination]].
*Kevin has an idea involving the [[wikipedia:Big Mac|Big Mac]] hamburgers.
*During Andy's "Food-for-Thought" rant Erin says, "hunger for chicken chimichangas!" and then looks at Darryl for approval. Craig Robinson played [http://shrek.wikia.com/wiki/Cookie_the_Ogre Cookie the Ogre] in ''Shrek Forever After'' whose signature dish was chimichangas.

==Goofs==
*Gabe says that ''Limitless'' is called "The Man with Many Capabilities" in France. In reality, the movie is known in France simply as ''Limitless'' (original English name, not even translated).
*The true northernmost battle of the [[wikipedia:Civil_War|Civil War]] occurred in [[wikipedia:Vermont|Vermont]] unlike the [[wikipedia:Battle_of_Gettysburg|Battle of Gettysburg]] like Oscar said.
*In the scene where Andy attempts to get the employees to steal his flag, Gabe can be seen standing under the big tree even though he is supposed to be giving his Lincoln presentation.

==Trivia==
*"Gettysburg" was the last episode to feature [[Jenna Fischer]] before she went on maternity leave.

==References to Previous Episodes==
*This is the second time the new boss mistakenly believed Kevin to be intelligent. The first time was Deangelo in the episode "Inner Circle".
*In "The List," Robert California calls half of the office staff winners and the other half losers. He says "winners, prove me right, and losers, prove me wrong." This was shown not to be forgotten when many of the "winners" go on the field trip, and Robert states to the remainder of the office staff, mostly "losers," "You guys have shown yourselves to be the free thinkers in the office."
*In "Casino Night," Michael says, "the Lincoln assassination just became funny. I need to see this play like I need a hole in the head." When Gabe is pretending to be Lincoln, he says, "I need her like I need a hole in the head", referring to Lincoln's wife, Mary Todd Lincoln.

==Cast==
===Main Cast===
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John  Krasinski|John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly|Pam Halpert]]
*[[B.J. Novak]] as [[Ryan Howard]]
*[[Ed Helms]] as [[Andy Bernard]]
*[[James Spader]] as [[Robert California]]

===Supporting Cast===
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin]]-Lipton
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]

===Recurring Cast===
* [[Lindsey Broad]] as [[Cathy Simms]]

===Guest Cast===
*Phil Hawn as Park Ranger
*Jonathan Nail as Tourist
*Robin Dalea as Chelsea's Mother
*Nancy Lantis as Amanda Fields-Shad
*Alyssa Suzanne Larsen
*Weston Nathanson
*Duane Shepard Sr.
*Jim Wisniewski

{{Season8}}
