# Get the Girl

![](The-office-get-the-girl-nbc.jpg "fig:The-office-get-the-girl-nbc.jpg")

**\"Get the Girl\"** is the nineteenth episode of the eighth season of
the American television comedy series *The Office* and is the show\'s
171st overall. It was written by Charlie Grandy and directed by Rainn
Wilson. It originally aired on NBC March 15, 2012. It was viewed by 4.87
million people.

Synopsis
--------

[Andy](Andy_Bernard "wikilink") drives all the way to Florida to try and
woo back [Erin](Erin_Hannon "wikilink"), where she has been living with
the elderly Irene and her middle-aged grandson, Glenn (Brad Morris), as
a live-in maid. He surprises Erin by popping out of a delivery box and
singing \"[Signed, Sealed, Delivered I\'m
Yours](wikipedia:Signed,_Sealed,_Delivered_I'm_Yours "wikilink")\", but
she is not particularly excited to see him and states she does not want
to go back to Scranton with him. The situation is further complicated
when Andy reveals to Erin that he has not yet broken up with current
girlfriend, Jessica. Andy tells Erin he loves her, but she rejects him;
Andy subsequently leaves, his feelings crushed. Irene, who had been
treating Andy with disdain over the way Erin spoke of him, sees that he
is a nice person and encourages Erin to go back with him. She runs up to
him in his car and they share a kiss before heading back to Scranton.

Meanwhile, [Nellie](Nellie_Bertram "wikilink") shows up in Scranton
after [Robert California](Robert_California "wikilink") allows her to
have a job there. She is invited to take an open desk, and she takes
Andy\'s manager desk, as he is currently in Florida.
[Jim](Jim_Halpert "wikilink") says she cannot take Andy\'s job, but
Robert is interested in her spontaneous behavior. As acting manager,
Nellie decides to give everyone performance reviews. Both Jim and
[Dwight](Dwight_Schrute "wikilink") refuse to let her proceed, as she is
unfamiliar with the other employees. To counter this, Nellie offers
Dwight a raise on the spot, and eventually, he and the other employees
relent, save for Jim. When it is Jim\'s turn, Jim still refuses.
Everyone in the office, save for Jim and [Pam](Pam_Beesly "wikilink"),
applaud her. Nellie promptly begins rearranging the office.

Cultural References {#cultural_references}
-------------------

-   Nellie is not impressed with illusionist [Harry
    Houdini](wikipedia:Harry_Houdini "wikilink") or his museum.
-   While shaving along the sea shore, Andy picks up a horseshoe crab
    and sarcastically thanks [BP](wikipedia:BP "wikilink"), a reference
    to the 2010 [Deepwater Horizon oil
    spill](wikipedia:Deepwater_Horizon_oil_spill "wikilink").
-   Andy pops out of the box, singing \"[Signed, Sealed, Delivered I\'m
    Yours](wikipedia:Signed,_Sealed,_Delivered_I'm_Yours "wikilink")\"
    by [Stevie Wonder](wikipedia:Stevie_Wonder "wikilink").
-   Irene asks Andy, \"Where\'s the ring,
    [Lancelot](wikipedia:Lancelot "wikilink")?\" According to Arthurian
    legend, Lancelot had an adulterous affair with Queen Guinevere.
-   \"Chumbo\" is a cross between [Dumbo](wikipedia:Dumbo "wikilink")
    and [Jumbo](wikipedia:Jumbo "wikilink"), with a hint of chubby.
-   Robert sarcastically asks Jim if he wants to take the family to
    \"Disneytown\". The Disney parks in the United States are named
    [Disneyland](wikipedia:Disneyland "wikilink") and [Walt Disney
    World](Wikipedia:Walt_Disney_World "wikilink"), commonly known as
    \"Disney World\". Jim attempts to correct Robert.
-   Irene mentions that her grandson is going to sue [Home
    Depot](wikipedia:Home_Depot "wikilink"), a popular retailer of home
    improvement and construction products and services.
-   Nellie compares herself to [Tinker
    Bell](wikipedia:Tinker_Bell "wikilink"), telling the office that, in
    order for her to work for them, they need \"to believe in her.\" In
    the play *[Peter and Wendy](Wikipedia:Peter_and_Wendy "wikilink")*,
    the character of Tinker Bell is revived from near-death by the
    belief of the audience (expressed through applause). The [Tinkerbell
    effect](Wikipedia:Tinkerbell_effect "wikilink") describe the
    phenomenon of something that exists because people believe in it.
-   Nellie dreamed she could breathe underwater like [Jacques
    Cousteau](wikipedia:Jaques_Cousteau "wikilink"), inventor of the
    modern [scuba](Wikipedia:Scuba_set "wikilink") apparatus.

Connections to previous episodes {#connections_to_previous_episodes}
--------------------------------

-   Robert\'s \"everything is sex\" theory is practically the same as
    the one he presented at his job interview in \"[Search
    Committee](Search_Committee "wikilink")\".

Trivia
------

-   Nellie\'s nameplate is obviously Andy\'s old one - the \"Ber\" in
    Bertram is the one in Bernard.
-   In this episode, Nellie mocks Harry Houdini. In the next episode, it
    is revealed that she harbors an intense hatred for stage magicians
    as a result of a painful event in her past.
-   The episode has a 6.3/10 rating on
    [IMDb](https://www.imdb.com/title/tt2267999/?ref_=adv_li_tt), making
    it the lowest-rated *The Office* episode on IMDb.

Cast
----

### Main Cast {#main_cast}

-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Halpert](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")
-   [James Spader](James_Spader "wikilink") as [Robert
    California](Robert_California "wikilink")

### Supporting Cast {#supporting_cast}

-   [Catherine Tate](Catherine_Tate "wikilink") as [Nellie
    Bertram](Nellie_Bertram "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Recurring Cast {#recurring_cast}

-   Georgia Engel as [Irene](Irene "wikilink")

### Guest Cast {#guest_cast}

-   Brad Morris as Glenn
-   Alexander Sibaja as [Alonzo](Alonzo "wikilink")

```{=mediawiki}
{{Season8}}
```
