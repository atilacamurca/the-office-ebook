'''"After Hours"''' is the sixteenth episode of the eighth season of the American television comedy series ''The Office'' and is the show's 168th overall. It was directed by Brian Baumgartner and written by Halsted Sullivan & Warren Lieberstein. It originally aired on NBC on February 23, 2012.

"After Hours" received mixed to positive reviews from critics. According to the [[wikipedia:Nielsen Media Research|Nielsen Media Research]], "After Hours" was viewed by an estimated 5.02 million viewers and received a 2.6 rating/7% share among adults between the ages of 18 and 49, marking a 16% rise in the ratings from the series low ratings of the previous episode, "[[Tallahassee]]". The episode also ranked as the highest-rated NBC program of the night.

==Synopsis==
[[Dwight Schrute|Dwight]] and [[Todd Packer]] compete to become VP under [[Nellie Bertram]] by seducing her after work at the hotel bar. For most of the night, Packer seems to be the most successful. Dwight has to leave the bar momentarily and he enlists [[Gabe Lewis|Gabe]] to keep an eye on Packer and Nellie. Gabe takes it upon himself to spike Packer's beer with his asthma inhaler, which causes Packer to vomit and leaves Dwight alone with Nellie. Dwight eventually succeeds in seducing Nellie as she asks for a key to his room. Dwight scratches off the magnetic strip on his hotel card before giving it to Nellie, stating the seduction was only to gain approval and that he doesn't intend to have sex with her.

[[Andy Bernard|Andy]] has everyone stay late to cover for their co-workers in Florida, which turns into an awkward situation when Val's boyfriend, Brandon, arrives and accuses [[Darryl Philbin|Darryl]] of having an affair with her, after having read Darryl's text messages to her. [[Kelly Kapoor|Kelly]] demands that Darryl read them aloud since it provides something interesting to happen during the long night. They eventually agree that his text messages are suggestive of Darryl wanting to be with Val, but both of them brush it off as being ridiculous. [[Pam Beesly|Pam]] suggests that he try a bold step and he does, privately telling Val that a potential relationship is not ridiculous.

[[Erin Hannon|Erin]] confides in [[Ryan Howard|Ryan]] that she's intending to stay in Tallahassee. Ryan interprets this as an attempt to hook up with him so he attempts to seduce her by taking her to the kitchen to help her make a waffle. They end up hiding under a table when the chefs come back. After Ryan compliments Erin, she suggests that they could become roommates in Florida and possibly start dating in six months if things go well between them. Not wanting that long timeline, he resorts to saying he loves Kelly.

Meanwhile, [[Jim Halpert|Jim]] has started spending more time with [[Cathy Simms|Cathy]] because the adulterous and lecherous actions of [[Stanley Hudson|Stanley]] made Jim uncomfortable around him. Things become awkward when Cathy wants to spend time with him in his room. When Jim gets really uncomfortable with her, he calls Dwight saying he has bed bugs and asks him for help. Dwight arrives and forces Cathy off the bed, making Jim think she will leave, but she steps into the bathroom to take a shower instead. Dwight leaves before Cathy gets out of the shower and she comes out wearing a bath robe.

After ordering some desserts from room service and showing off her legs to Jim, he finally says that he's happily married, and doesn't want to be with her. Cathy gets defensive and insists that she did not have any romantic intentions whatsoever, so Jim relents, and the two continue watching TV together, but Cathy seems to get uncomfortably close to him. After Jim goes to the bathroom, he comes out to find her in only her underwear under his blanket. Fed up, he demands that she leave. Suddenly, Dwight charges in with chemicals to spray bed bugs. Jim keeps asking him to squirt Cathy to scare her away, which he does. However, because of the chemicals, Dwight tells Jim to sleep in Cathy's room. Jim instead spends time with Dwight in his room, eating Cathy's desserts and watching TV, while a drunken Nellie is outside, trying unsuccessfully to get in.

==Cultural references==
*Packer and Nellie discuss the [[wikipedia:Jason Bourne|Jason Bourne]] and [[wikipedia:James Bond|James Bond]] series of films.
*Dwight thinks [[wikipedia:Genghis Khan|Genghis Khan]] "could take them both down because he's not afraid to kill children".
*Darryl says, "I'm stranded on ''[[wikipedia:Shutter Island (film)|Shutter Island]]'' here."
*Nellie says Packer looks like [[wikipedia:Ed Harris|Ed Harris]].
*Andy invites the Dunder Mifflin staff to watch ''[[wikipedia:Romy and Michele's High School Reunion|Romy and Michele's High School Reunion]]''.

==Amusing details/Trivia==
*Gabe's middle name is Susan.
*Dwight continues his tactic of trying to insert his name to make Nellie think of Dwight as a good Vice President, by saying "Dwight, VP, Dwight, VP," over and over again. Nellie then comments she can not be hypnotized.
*James Spader is credited, but does not appear.
==Analysis==
This episode appears to have 4 romantic plot lines, with someone “going for it”.
*Todd and Dwight “going for it” and seducing Nellie. Dwight ends up successfully doing so, but nothing comes of it.
*Cathy “going for it” by seducing Jim and failing.
*Brandon confronting Darryl about his relationship with Val, but Darryl eventually “going for it” with Val.
*Erin and Ryan trying to find waffles as Ryan “goes for it.”

==Cast==
===Main cast===
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly|Pam Halpert]]
*[[B.J. Novak]] as [[Ryan Howard]]
*[[Ed Helms]] as [[Andy Bernard]]

===Supporting cast===
*[[Catherine Tate]] as [[Nellie Bertram]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin]]-Lipton
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]
*[[Zach Woods]] as [[Gabe Lewis]]

===Recurring cast===
*[[David Koechner]] as [[Todd Packer]]
*[[Lindsey Broad]] as [[Cathy Simms]]
*[[Jerry C. Minor]] as [[Brandon]]
*[[Ameenah Kaplan]] as [[Val]]

{{Season8}}
