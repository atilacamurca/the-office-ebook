'''"Last Day in Florida"''' is the eighteenth episode of the eighth season of the American television comedy series ''The Office'' and is the show's 170th overall. It was written by Robert Padnick and directed by Matt Sohn. It originally aired on NBC March 8, 2012. It was viewed by 4.89 million people.

==Synopsis==
[[Dwight Schrute|Dwight]] celebrates his winning of the Vice President position on a golf outing with [[Jim Halpert|Jim]], [[Robert California]], and [[Nellie Bertram]]. After playing, Robert tells Jim that he dislikes the business plan for the Sabre store; he only approved it because Jo Bennett wanted it, but he plans to sandbag it at a high-level meeting, and Jim is stunned when Robert strongly hints he's going to fire Dwight over it. Jim makes several attempts to stop Dwight from attending the meeting with Robert, but Dwight is heedless and continually insults Jim, who finally decides to leave Dwight to his fate. However, a guilt-inducing phone call to [[Pam Beesly|Pam]] makes Jim decide he has to keep parsing Dwight's insults until he directly tells him that he's going to lose his job. Dwight ignores the news, thinking Jim is jealous of him, so Jim resorts to wrestling with Dwight outside the conference room. In the meantime, Nellie has [[Todd Packer]] stand in as VP in Dwight's absence. When Dwight makes it into the conference room, he hears Robert planning to fire the VP, so he sneaks out of the conference room and lets Packer take the fall in front of those present. Dwight then silently extends a hand to Jim, and they head back to Scranton with [[Stanley Hudson|Stanley]], who has reverted to his old grumpy self over the thought of leaving Florida.

Back in Scranton, [[Darryl Philbin|Darryl]] and [[Toby Flenderson|Toby]] are both trying to sell girl scout cookies for their daughters. When Toby's requests clash with Darryl's, Darryl tells him they need their own sections of the office to sell to in order to not interfere with each other. Darryl takes accounting while Toby gets every other section. Darryl asks for accounting because [[Kevin Malone|Kevin]] buys more cookies than everyone else put together. Toby eventually realizes Darryl's plan and the two end up fighting over who will sell Kevin cookies, with Kevin coming up with absurd competition ideas. After doing a song and dance for Kevin, Kevin still can't make up his mind, so Darryl and Toby contemplate giving up until Kevin mentions he plans to buy hundreds of boxes of cookies, at which point they continue the competition. They finally give up for good when Kevin wants to ride them like a pony, citing they still want to maintain their dignity and will not go beyond the limit to what they'd do for their daughters. When they walk away, Kevin says he'll do any absurd thing for them to continue, kissing [[Meredith Palmer|Meredith]] to prove it, but they still refuse.

Meanwhile, [[Andy Bernard|Andy]] learns that [[Erin Hannon|Erin]] isn't coming back to Scranton after she takes a job helping an elderly lady she met at the Sabre store opening, leaving Andy very distraught. When Dwight, Jim, and Stanley return to Scranton and Andy sees Jim happily reunite with Pam, Andy decides to travel to Florida to try and bring Erin back.

==Connections to previous episodes==
*The way Andy tells the office that Dwight is staying in Florida, mirrors the same way Michael told the office that he hit Meredith with his car in [[Fun Run]].
==Cultural References==
*Included on Andy's whiteboard of possible items inside Dwight's treasure chest is the phrase "[[wikipedia:Citizen Kane|Rosebud]]-type scenario" and "''[[wikipedia:Star Wars|Star Wars]]'' stuff."
*Erin walks through Irene's front door and, imitating [[wikipedia:Ricky Ricardo|Ricky Ricardo]], says, "Hey, Lucy! I'm home!"
*Kevin says he wants to be "wined and dined and [[wikipedia:69 (sex position)|69'ed]]" (metaphorically).
*In the episode, Ryan mentions that he told everybody through his [[wikipedia:Tumblr|Tumblr]] account that Erin was not coming back from Florida.<sup class="reference" id="cite_ref-newyork_13-0">[http://en.wikipedia.org/wiki/Last_Day_in_Florida#cite_note-newyork-13]</sup>
*Kevin forces Toby and Darryl to sing "[[wikipedia:Hello! Ma Baby|Hello! Ma Baby]]" in the style of [[wikipedia:Michigan J. Frog|Michigan J. Frog]].
*Oscar uses reverse psychology on himself and then realizes, "I'm [[Wallace Shawn]] in ''[[wikipedia:The Princess Bride (film)|The Princess Bride]]!''"
*Instead of making tea, Erin boils [[wikipedia:Gatorade|Gatorade]], the sports drink.

==Cast==
===Main Cast===
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly|Pam Halpert]]
*[[B.J. Novak]] as [[Ryan Howard]]
*[[Ed Helms]] as [[Andy Bernard]]

===Supporting Cast===
*[[Catherine Tate]] as [[Nellie Bertram]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]
*[[Zach Woods]] as [[Gabe Lewis]]

===Reccuring Cast===
*[[David Koechner]] as [[Todd Packer]]
*[[Lindsey Broad]] as [[Cathy Simms]]
*Georgia Engel as [[Irene]]

{{Season8}}
