[[File:Cast.jpg|400px|right|]]
'''Season 1''' of ''[[The Office]]'' began on March 24, 2005 on [[NBC]]. It is based on the BBC series created by [[Ricky Gervais]] and [[Stephen Merchant]], and developed for American television by [[Greg Daniels]].

''The Office'' on NBC . There are only 6 episodes in Season 1. It aired from March 24, 2005, to April 26, 2005.

Season 1 was released on [[DVD]] in North America on August 16, 2005.

==Season overview==
The first season was a mid-season replacement and thus did not include a full set of episodes. The season had six episodes that began airing from March 24, 2005 to April 26, 2005. The premiere episode was written near word for word from the first episode of the British series, though names and cultural references were changed and some small extra scenes were added.

The season begins with a documentary crew that arrives at the offices of Dunder Mifflin to observe the employees and learn about modern management. The main characters are introduced as well as some minor characters. Rumors of downsizing begin to spread in the office and the possible closure of the branch. [[Michael Scott]], Dunder Mifflin Scranton's regional manager, denies the rumors to boost morale.

In the season finale, [[Jim Halpert]], a sales representative, begins dating [[Katy]], a purse saleswoman who visited and set up shop briefly in the office. Jim also has an interest in the office receptionist [[Pam Beesly]], though she is engaged to Roy Anderson, which expands later in [[season 2]] of the show.

==Episodes==
{| cellpadding="6" style="border: 1px solid #999999; margin: 0; border-collapse: collapse; font-size: 95%; background: #f9f9f9;"
|- style="background: white;"
! style="text-align: center; color: #000;"| Picture !! style="text-align: center; color: #000;"| Episode Title !! style="text-align: center; color: #000;"| Season Episode Number !! style="text-align: center; color: #000;"|Production Code !! style="text-align: center; color: #000;"| Original Airdate
|-
|colspan="5" style="background: #999999;"| <!-- Putting in a nice space between episodes -->
|-
|rowspan="2" style="vertical-align: middle;"|[[Image:pilot.jpg|150px]] || align="center"| [[Pilot]] || align="center"| 1 || align="center"| RP006 ||align="center"| March 24, 2005
|-
|colspan="4"| An introduction to the staff of the [[Dunder Mifflin Scranton|Scranton, Pennsylvania branch]] of the [[Dunder Mifflin Paper Company]].
|-
|colspan="5" style="background: #999999;"| <!-- Putting in a nice space between episodes -->
|-
|rowspan="2" style="vertical-align: middle;"|[[Image:diversityday.jpg|150px]] || align="center"| [[Diversity Day]] || align="center"| 2 || align="center"| R1101 || align="center" | March 29, 2005
|-
|colspan="4"| After the corporate office discovers that [[Michael]] has been making insensitive remarks to his co-workers, they send a [[Mr. Brown|sensitivity trainer]] to the Scranton branch, but when Michael doesn't agree with everything the trainer says, he sets up his own sensitivity workshop.
|-
|colspan="5" style="background: #999999;"| <!-- Putting in a nice space between episodes -->
|-
|rowspan="2" style="vertical-align: middle;"|[[Image:healthcare.jpg|150px]] || align="center"| [[Health Care]] || align="center"| 3 || align="center"| R1105 || align="center" | April 5, 2005
|-
|colspan="4"| Michael passes off the responsibility of picking a new health care plan to [[Dwight]].
|-
|colspan="5" style="background: #999999;"| <!-- Putting in a nice space between episodes -->
|-
|rowspan="2" style="vertical-align: middle;"|[[Image:thealliance.jpg|150px]] || align="center"| [[The Alliance]] || align="center"| 4 || align="center"| R1103 || align="center" | April 12, 2005
|-
|colspan="4"| [[Jim]] jokingly forms an "alliance" with Dwight when rumors of downsizing come about.
|-
|colspan="5" style="background: #999999;"| <!-- Putting in a nice space between episodes -->
|-
|rowspan="2" style="vertical-align: middle;"|[[Image:basketball.jpg|150px]] || align="center"| [[Basketball]] || align="center"| 5 || align="center"| R1104 || align="center" | April 19, 2005
|-
|colspan="4"| It's the office versus the [[Warehouse|warehouse]] in a game of basketball.
|-
|colspan="5" style="background: #999999;"| <!-- Putting in a nice space between episodes -->
|-
|rowspan="2" style="vertical-align: middle;"|[[Image:hotgirl.jpg|150px]] || align="center"| [[Hot Girl]] || align="center"| 6 || align="center"| R1102 || align="center" | April 26, 2005
|-
|colspan="4"| When an attractive handbag [[Katy|salesgirl]] comes to the office, Michael and Dwight compete for her attention.
|-
|colspan="5" style="background: #999999;"| <!-- Putting in a nice space between episodes -->
|-
|}
{{seasonnav|prev=[[Christmas Specials]]|current=Season 1|next=[[Season 2]]}}
