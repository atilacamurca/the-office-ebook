---
title: Diversity Day
---

**Diversity Day** is the second episode of the [first
season](Season_1 "wikilink") of The Office and the 2nd episode overall.
It was written by B.J. Novak and directed by Ken Kwapis. It originally
aired on March 29, 2005. It was viewed by 6 million people.

Synopsis
--------

[Michael](Michael_Scott "wikilink") explains in his talking head that
it\'s Diversity Day and that a guest speaker is at [Dunder Mifflin
Scranton](Dunder_Mifflin_Scranton "wikilink") to talk about diversity in
the workplace. Michael claims that he\'s wanted this for awhile.
Corporate mandated it without Michael having to talk to them about it,
though. But Michael still believes it\'s very important.

[Jim](Jim_Halpert "wikilink") is trying to make a sales call when
[Dwight](Dwight_Schrute "wikilink") begins noisily shredding paper. Jim
turns off Dwight\'s surge strip. In retaliation, Dwight disconnects
Jim\'s phone call. Jim explains to the camera that this client makes up
25% of his commission each year. He brings a small bottle of champagne
to the office with him to celebrate.

While Pam plays freecell, Michael walks out of his office, then backs up
when he sees the corporate representative is heading out of the
conference room. He makes sure to walk out at the same time as the other
man and make a point of singling out Oscar Martinez as a friend of his.
Jim has made contact with his client just in time for Michael to order
him into the meeting. Jim is forced to hang up again.

When the meeting begins, Michael tries to take over by talking about how
he doesn\'t even see Stanley as a black man. The representative explains
that\'s not the point here. No one is expected to be color-blind.
That\'s just fighting ignorance with more ignorance.

Then Michael decides everyone should go around and name an ethnicity
that he or she finds sexually attractive. Dwight claims he\'s attracted
to whites and Indians. The representative does not think that\'s a good
start. He asks Michael for permission to run this session himself and
suggests it would be easier if Michael sits down.

He explains that he asked each of them to write down an incident that
offended them. He would like volunteers to assist him. Dwight asks that
they don\'t do anything involving gays.

The representative attempts to take control again from Michael who asks
his name. When he reveals that it\'s [Mr. Brown](Mr._Brown "wikilink"),
Michael thinks it\'s a trick to see if he will call a black man Mr.
Brown. Brown assures him that is his name. He goes on to explain that
almost everyone wrote down the same incident and asks if everyone is
familiar with the Chris Rock routine.

Michael complains to the camera how Chris Rock can do a hilarious
routine that makes everyone laugh, but he gets in trouble from corporate
when he does the same routine. Michael volunteers to be the joker, Brown
doesn\'t want the person who was the original offender in the same
position. Kevin offers to be the joke teller, but doesn\'t tell it to
Michael\'s liking. Michael interrupts with a loud, profane rendition of
the routine before Brown stops him.

While Jim listens to his phone ring, helpless to answer it, Brown sets
up an acronym for Hero: Honesty, Empathy, Respect and Open-Mindedness.
Dwight adds additional criteria that essentially describes a super-hero,
not a hero. Brown passes out forms, asking everyone sign it. Michael
refuses because he didn\'t learn anything.

Privately, Brown explains that they both know he is here because of
Michael\'s behavior. Michael argues that this office is racially
advanced enough that it really doesn\'t need a diversity lecture. Brown
tells him it does because it\'s Michael that caused the need for it. He
only needs Michael\'s signature, but put the whole office through the
seminar because he didn\'t want Michael to feel singled out. Michael
feigns offense that this wasn\'t really about diversity then.

Michael signs the form and then jokes after the fact that he signed it
\"Daffy Duck\".

In the meantime, Jim has tried to call back his client and has to leave
a message.

Michael comes out, tears up the pledge, and criticizes Mr. Brown\'s lack
of an \"Oprah moment\". He wants everyone to get as much done before
lunch as possible.

Jim gets nothing but a busy signal right before Michael forces them all
into the conference room again.

The HR representative, [Toby Flenderson](Toby_Flenderson "wikilink")
asks if they are going to sit Indian style. Michael orders him out for
being offensive. Then he shows the staff a video announcing his new
initiative for Diversity Tomorrow and quotes Abraham Lincoln as saying,
\"If you are a racist, I will attack you with the North\".

[Kelly Kapoor](Kelly_Kapoor "wikilink"), a young Indian woman, asks to
be excused for a customer meeting. Michael lets her go. He introduces
himself and describes his ethnic breakdown, including that he is part
Native American. Oscar asks him what part he is. Michael asks about
Oscar\'s ethnicity. Oscar explains his parents were born in Mexico,
moved here and he was born. Michael wants to know if there\'s something
he would like to be called besides Mexican. Oscar doesn\'t understand
why Michael would think Mexican is offensive.

Jim, hearing his phone ring, runs out to grab it, but is too late.

Michael hands out cards with different ethnicities on them. The
employees are to take a card and then try to guess their pretend
ethnicity by how their co-workers treat them.

Stanley is inadvertently given Black. Dwight is Asian but can\'t guess
it based upon people telling him they like his people\'s food or eat
lots of rice. Michael, who\'s chosen Martin Luther King for himself,
insists Pam stir up the pot by getting extreme. Pam hesitantly suggests
to Dwight that his people aren\'t very good drivers. Dwight angrily asks
if he\'s a woman.

Jim goes to Pam\'s desk where Ryan is watching an online sketch. Ryan
mentions how cute she is. Jim agrees, but that Pam is engaged. Ryan says
he was talking about the girl in the sketch.

Kevin is Italian and is trying to get Angela to guess that she\'s
Jamaican by using the word \"mon\" and talking about beaches. Angela
won\'t go any further when he asks if she wants to get high.

Irritated that no one is trying very hard to be offensive, Michael
immediately jumps on Kelly when she returns from her meeting by throwing
himself into a really offensive Indian stereotype until Kelly slaps him.
Michael gets visibly upset. He tells the employees how now Kelly knows
what it\'s like to be a minority.

Jim finally gets ahold of his client who was able to close his sale with
Dwight instead who gave him a discount. Jim puts the mini champagne
bottle on Dwight\'s desk.

Michael continues mumbling about Mr. Brown and corrects Stanley on how
to pronounce the word collard in collard greens. When 5 PM arrives, Pam
is asleep on Jim\'s shoulder. He smiles at the camera, then wakes her up
so they can go.

He tells the camera that today was a pretty good day.

Cultural references
-------------------

-   *Diversity* is a term used in corporate culture to refer to multiple
    races, cultures, and customs being represented within a company\'s
    workforce.
-   [Michael](Michael_Scott "wikilink")\'s exclamation \"Celebrate good
    times, come on!\" comes from the chorus of the 1980s song
    \"[Celebration](Wikipedia:Celebration_(song) "wikilink")\" by the
    group [Kool & the Gang](Wikipedia:Kool_&_the_Gang "wikilink").
-   *[Daffy Duck](Wikipedia:Daffy_Duck "wikilink")* is a cartoon
    character.
-   [Toby](Toby_Flenderson "wikilink") jokes about \"sitting in a circle
    [Indian style](Wikipedia:Sitting#Cross-legged "wikilink").\" This is
    a typical campfire arrangement (and terminology) used by children at
    camp.
-   *[Namaste](Wikipedia:Namaste "wikilink")* (with the accompanying
    hand gesture) is a traditional Indian greeting.
-   *I Have a Dream* is the refrain of a famous speech by civil rights
    activist [Martin Luther King,
    Jr.](Wikipedia:Martin_Luther_King,_Jr. "wikilink")
-   The *[melting pot](Wikipedia:Melting_pot "wikilink")* is an American
    concept for describing how multiple cultures are assimilated into a
    single combined culture. The more contemporary concept of
    *diversity* emphasizes how the cultures retain their identity rather
    than lose it via assimilation (with the new analogy of a salad
    bowl).
-   *Googy-googy* is a made-up term.
-   In a deleted scene, [Dwight](Dwight_Schrute "wikilink") mentions
    *[James Earl Jones](Wikipedia:James_Earl_Jones "wikilink")*, a noted
    African-American actor and voice-over performer.
-   In a deleted scene, Michael imitates the character of Colonel Klink
    from the 1960s sitcom \"[Hogan\'s
    Heroes](Wikipedia:Hogan's_Heroes "wikilink")\". The sitcom is set in
    a German POW camp. Hogan is the leader of a group of Allied POWs
    whose activities regularly frustrate camp commandant Klink. Michael
    compares Jan to General Burkhalter (Klink\'s superior) and Dwight to
    Sergeant Schultz (Klink\'s bumbling aide).
-   In a deleted scene, Michael (pretending to be [Martin Luther King,
    Jr.](Wikipedia:Martin_Luther_King,_Jr. "wikilink")) talks about
    marching to the Washington Monument, the Lincoln Memorial and the
    U.S. Mint. King did [march from the Washington Monument to the
    Lincoln
    Memorial](Wikipedia:March_on_Washington_for_Jobs_and_Freedom "wikilink"),
    but not the U.S. Mint.

Quotes
------

:   *see [Diversity Day Quotes](Diversity_Day_Quotes "wikilink")*

Trivia
------

-   Originally, producer [Greg Daniels](Greg_Daniels "wikilink") wasn\'t
    sure where to use [Mindy Kaling](Mindy_Kaling "wikilink") on screen
    in the series until the point came in this episode\'s script when
    Michael needed to be slapped by a minority. \"Since then, I\'ve been
    on the show,\" Kaling says. Daniels also loves the scene where
    [Pam](Pam_Beesly "wikilink") falls asleep on
    [Jim](Jim_Halpert "wikilink")\'s shoulder.
-   [Larry Wilmore](Larry_Wilmore "wikilink") ([Mr.
    Brown](Mr._Brown "wikilink")) is a writer for the show. At the
    table-read for this show, they hadn\'t cast the part yet and Daniels
    just had Wilmore read for the role to fill in. After the read,
    Daniels thought he was perfect for the role. However, because of
    stipulations with the Screen Actors Guild, producers still had to
    have Wilmore formally audition with other actors for the role.
-   [Michael](Michael_Scott "wikilink")\'s attempts to upstage Brown
    echoes [David Brent](David_Brent "wikilink")\'s treatment of a
    visiting consultant in the episode [Training](Training "wikilink")
    from [The Office UK](The_Office_UK "wikilink").
-   The diversity training company\'s name (and Michael\'s response to
    it) went through a number of changes before Diversity Today was
    settled on, including Diversity 360 (Michael would rename it
    Diversity 365) and Diversity 2000 (Michael would rename it Diversity
    3000).
-   The routine Michael imitates is \"Niggas vs. Black People\" from
    Chris Rock\'s 1996 HBO special *Bring the Pain*.
-   Michael\'s diversity exercise with the index cards is similar to one
    sometimes done in actual diversity training programs.
-   This episode was nominated for the Writers Guild of America Award
    for \"Best Episodic Comedy\".
-   These are the roles played by the [Dunder
    Mifflin](Dunder_Mifflin_Paper_Company "wikilink") employees in
    Michael\'s diversity exercise:

  Character                                Race/nationality
  ---------------------------------------- -------------------------------------------
  [Michael](Michael_Scott "wikilink")      Martin Luther King Jr.
  [Dwight](Dwight_Schrute "wikilink")      Asian (and \"Dwight\" in a deleted scene)
  [Stanley](Stanley_Hudson "wikilink")     Black (by coincidence)
  [Angela](Angela_Martin "wikilink")       Jamaican
  [Kevin](Kevin_Malone "wikilink")         Italian
  [Meredith](Meredith_Palmer "wikilink")   Brazilian
  [Pam](Pam_Halpert "wikilink")            Jewish
  [Oscar](Oscar_Martinez "wikilink")       Eskimo
  [Creed](Creed_Bratton "wikilink")        Puerto Rican
  [Phyllis](Phyllis_Lapin "wikilink")      Haitian (deleted scene)
  [Devon](Devon_White "wikilink")          West Nile (deleted scene)

-   Although it\'s true that \"insect\" would not have made sense
    either, another anagram for \"incest\" is \"nicest\", which would
    have worked well in this context.
-   Wilmore appears as Mr. Brown again in the [Season
    Three](Season_3 "wikilink") episode \"[Gay Witch
    Hunt](Gay_Witch_Hunt "wikilink")\", giving the [Stamford
    branch](Dunder_Mifflin_Stamford "wikilink") the Diversity Today
    sensitivity training. It is implied he was brought in because of
    events in Scranton.
-   Michael says, \"Don\'t take my word for it. Let\'s take a look at
    the tape.\" But the tape consists merely of Michael talking.
-   As Dwight gives his explanation of heroes, Jim\'s face goes through
    at least four expressions.

Deleted scenes
--------------

![[Devon](Devon "wikilink")\'s forehead
card.](Devon's_race.jpg "fig:Devon's forehead card."){width="300"}
Included on the [Season 1 DVD](Season_1_DVD "wikilink") were various
deleted scenes:

-   [Michael](Michael_Scott "wikilink") and [Mr.
    Brown](Mr._Brown "wikilink") discussing erasing racism.
-   [Dwight](Dwight_Schrute "wikilink") telling the camera he\'s wasting
    busy sales hours because of the meeting.
-   A scene at the meeting where Michael creates his own acronym for
    sensitivity (inclusion, new attitude, color blind, expectations,
    sharing, and tolerance) which ends up spelling I.N.C.E.S.T. After
    [Pam](Pam_Beesly "wikilink") points this out, he tries to explain
    how the term works in context.
-   Pam in a talking head saying Michael could\'ve just used insect as
    an acronym, though it still wouldn\'t have made sense.
-   [Ryan](Ryan_Howard "wikilink") mentioning his childhood neighbor who
    played pro baseball \"before the leagues were integrated\" and his
    stories, though Michael interrupts him and asks him to leave the
    meeting because he sees this information as irrelevant. He tells
    Ryan to check if anyone parked in the handicap spots and tries to
    talk about how that ties in with the new attitude part of his
    acronym, though Mr. Brown stops him from continuing.
-   Pam trying to think of good things about Michael.
-   Ryan in the parking lot looking at the empty handicap parking spots.
-   An extended scene with Michael showing his Daffy Duck signature.
-   Jim helping Pam in her game of solitaire, but Dwight trying to stop
    them as the game should only be played alone.
-   Dwight and Michael filming the short sensitivity video.
-   Michael talking about marching on Washington \"back in the day.\"
-   [Devon](Devon_White "wikilink") smoking on a balcony with \"West
    Nile\" on his forehead card.
-   When Dwight asks if he\'s a woman, Jim changes his forehead card
    from Asian to \"Dwight\" and continues asking what race he is,
    though Angela eventually tells him what his card says.
-   Pam telling the camera there was once an ethnic festival in
    Scranton.
-   Reactions from the office workers after Kelly slaps Michael.

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring Cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited)
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink") (Only in Deleted Scenes)
