---
title: Pilot
---

**\"Pilot\"**, alternately titled **The Office: An American Workplace**,
is the first episode of the [first season](Season_1 "wikilink") of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the first episode
overall. It was written by Ricky Gervais and Stephen Merchant and Greg
Daniels, and directed by Ken Kwapis. It first aired on March 24, 2005
and was viewed by 11.2 million people.

Synopsis
--------

A documentary crew gives a firsthand introduction to the staff of the
[Scranton branch](Scranton_branch "wikilink") of the [Dunder Mifflin
Paper Company](Dunder_Mifflin_Paper_Company "wikilink"), managed by
[Michael Scott](Michael_Scott "wikilink").

![Michael\'s mug.](World'sBestBossMug.jpg "Michael's mug.")

During a meeting, [Jim Halpert](Jim_Halpert "wikilink") tells his boss,
Michael, that he couldn\'t close a sale with the library. Michael closes
the sale easily, but accidentally calls the woman he\'s speaking with,
\"Sir\". He thinks she might have been a smoker.

Michael tells the camera that he\'s worked for Dunder-Mifflin for 12
years and has been Regional Manager for 4. The branch has the entire
second floor of the building. He introduces the camera to receptionist
[Pam Beesly](Pam_Beesly "wikilink") and mentions how much better she
looked a couple of years ago. She gives him a fax from corporate which
he crumples up and tosses into the trash, calling it his special file
for messages from the corporate office. He shows the camera his World\'s
Best Boss mug and tells how he bought it from Spencer Gifts.

Jim tries to explain his job selling people, but admits it\'s boring him
even talking about it.

Michael comes out of his office and shouts the years-old gag
\"Whassssuuupp?\" to Jim and [Dwight](Dwight "wikilink") who play along.

Michael\'s direct supervisor, [Jan
Levinson-Gould](Jan_Levinson-Gould "wikilink"), then enters the office
for a meeting with Michael. Michael admits he calls her Hillary Rodham
Clinton, but not because he\'s afraid of her. Jan asks if Michael has
any questions about the agenda which he claims not to have seen and
proceeds to lecture Pam who then reveals, in front of Jan, that Michael
threw it away. Michael backpedals that it was just a joke.

Jan explains that corporate doesn\'t see the point in having both the
Scranton and Stamford branches. Michael immediately assumes the worst.
Jan tries to assure him that it\'s not a foregone conclusion that
Scranton will be shut down. Michael wonders if
[Josh](Josh_Porter "wikilink"), the regional manager at Stamford, is
worried about downsizing.

Then Michael takes a call from [Todd Packer](Todd_Packer "wikilink"),
the branch\'s traveling salesman, who, not realizing he\'s on
speakerphone in Jan\'s presence, calls her \"Godzillary\" and asks
Michael if the carpet matches the drapes. Michael quickly realizes his
mistake and mumbles about how inappropriate that was.

Jan concludes the meeting by asking Michael to keep all of this
information under wraps for now.

In the break room, [Oscar Martinez](Oscar_Martinez "wikilink"), [Phyllis
Lapin](Phyllis_Lapin "wikilink"), and [Stanley
Hudson](Stanley_Hudson "wikilink") are at a table talking about the
possibility of downsizing. Oscar admits he\'s updating his resume.
[Angela Martin](Angela_Martin "wikilink") worries she\'ll be let go and
[Kevin Malone](Kevin_Malone "wikilink") agrees. Jim and Pam are too busy
laughing about whether or not they will attend Angela\'s cat party on
Sunday.

![The wall of pencils between Jim and Dwight\'s
desks.](Wallofpencils.jpg "The wall of pencils between Jim and Dwight's desks.")

In the midst of all this, [Ryan Howard](Ryan_Howard "wikilink") arrives
from the temporary employment agency. Michael demonstrates his humor by
imitating Moe, from [The Three
Stooges](Wikipedia:The_Three_Stooges "wikilink") and [Adolf
Hitler](Wikipedia:Adolf_Hitler "wikilink"). It\'s clear that Michael\'s
idea of humor is lost on his employees.

Pam admits it wouldn\'t be the worst thing if she was laid off. She
enjoys working with watercolor and oil pencils. Jim likes her work.

While Jim is trying to make a sales call, Dwight distracts him by using
his ruler to push some of Jim\'s overlapping papers off of his desk. In
retaliation, Jim [built a pencil fence](Jim's_Pencil_Fence "wikilink")
between his desk and Dwight\'s, which Dwight smashes with his phone.
Because of this, Jim tells the camera he\'s not afraid of the downsizing
issue. Dwight, too, tells the camera he has no problem with downsizing
and that he has been recommending it since he first arrived at Dunder
Mifflin.

Pam hands Michael some of his messages. He pretends to act like the Six
Million Dollar Man for several long moments. He decides that six million
dollars would be a great salary for him and asks Pam\'s opinion. Pam
mentions her salary could use a boost, too. Suddenly, Michael becomes
serious, lecturing Pam she should take up her salary concerns with HR if
she thinks there\'s a problem.

The staff is called into the conference room. Dwight, desperate to help,
wants to back up Michael as Assistant Regional Manager. Michael will
only acquiesce to Assistant to the Regional Manager. Michael explains
that he\'s aware of the rumors and can confirm that Jan has indicated to
him that there might be downsizing which could involve either Scranton
or Stamford. Both Oscar and Stanley question Michael\'s assurances that
he will not permit this branch to be closed. To change the subject, he
tries to field a question from Pam who he claims looks like she wants to
ask a question. Pam mentions that, being present in the meeting with
Jan, it certainly did sound like Scranton could be shuttered. Michael,
who doesn\'t seem to recognize that this whole meeting is breaking
Jan\'s request for confidentiality, lectures Pam about leaking
information. Out in the office, Ryan expresses his concerns by phone
that he\'s hearing rumors the branch might be shut down.

Jim wonders what he will do with all this useless information he\'s
gathered about paper. He also knows Pam\'s favorite yogurt flavor: mixed
berry. Pam admits to the camera that he\'s right.

Michael introduces Ryan to the office. When he gets to Dwight, he asks
him to tell Ryan about his car and his martial arts. Dwight starts to
grab some photos of his car when he finds that someone has [encased his
stapler](Puts_Items_In_Jell-O "wikilink") in
[Jell-O](Jell-O "wikilink"). He\'s furious because this is the third
time and insists that Michael, as a witness, should discipline Jim. Jim,
eating Jell-O, wants to know why Dwight thinks it\'s him. He and Ryan
distract Michael by making dessert puns. Michael can\'t think of one.

Jim tries to convince Pam to join the gang for some drinks after work.
Her fiancé, [Roy](Roy_Anderson "wikilink"), one of the warehouse
workers, arrives. Pam admits that she and Roy have been engaged for
three years. They are supposed to be married in September, but she
believes it\'ll probably not be until Spring. Roy tells her he just
wants to go home. She leaves her desk to finish up some work. Jim tries
to make small talk, but Roy isn\'t having it.

Jim tells the camera that he doesn\'t think he\'ll be invited to the
wedding.

While Michael conveys his management style to Ryan (friend first, boss
second), he decides to pull a prank on Pam. When she comes in, he tells
her that she\'s made downsizing easier because he has to fire her for
stealing Post-It notes. She denies having stolen anything and bursts
into tears. Ryan sits awkwardly until Michael reveals the gag. Pam calls
him a jerk and storms out.

Michael tells the camera that people are more important than money. The
proudest day of his life was when one of his employees made him the
godfather of his child. It didn\'t work out as they had to fire the guy
because he wasn\'t very good.

Pam and Jim say good-bye at the end of the day. They are interrupted by
Roy\'s angry honking from outside.

Before Jim leaves, he walks into Michael\'s office and leaves a Jell-O
mold with Michael\'s World\'s Best Boss mug inside.

Cultural references
-------------------

-   [Michael](Michael_Scott "wikilink") calls
    [Jim](Jim_Halpert "wikilink") *grasshopper*, the nickname given to
    the protagonist in the television series \"[Kung
    Fu](Wikipedia:Kung_Fu_(TV_series) "wikilink")\" by one of his
    teachers.
-   *[Spencer Gifts](Wikipedia:Spencer_Gifts "wikilink")* is a chain of
    stores that sells novelty items. There is a store in Scranton.
-   Michael says \"Pam-Pam\" in a high-pitched voice like
    [Bamm-Bamm](Wikipedia:Bamm-Bamm_Rubble "wikilink"), [Barney
    Rubble\'s](Wikipedia:Barney_Rubble "wikilink") son on *[The
    Flintstones](Wikipedia:The_Flintstones "wikilink")*.
-   Michael shouts *[Whassup?](Wikipedia:Whassup? "wikilink")* in the
    style of a series of
    [Clio-Award](Wikipedia:Clio_Awards "wikilink")-winning
    [Budweiser](Wikipedia:Budweiser "wikilink") beer advertisements
    from 2000. (Five years before the episode takes place, not seven as
    Jim claims.)
-   *[Downsizing](Wikipedia:Layoff "wikilink")* is a corporate euphemism
    for firing employees. Other corporate terms employed in the episode
    are *compensation* (pay) and *HR* (*human resources*, the department
    responsible for employee issues such as hiring and salary).
-   *[The Six Million Dollar
    Man](Wikipedia:The_Six_Million_Dollar_Man "wikilink")* was a
    television program popular in the 1970s. The title character was a
    cyborg, and footage of the character performing superhuman feats
    were played in slow motion and accompanied by a sound effect similar
    to the one Michael makes.
-   *[The Jamie Kennedy
    Experiment](Wikipedia:The_Jamie_Kennedy_Experiment "wikilink")* and
    *[Punk\'d](Wikipedia:Punk'd "wikilink")* are television programs
    from the early 2000s. Both programs play pranks on unsuspecting
    victims. Michael\'s exclamation \"You\'ve been X\'d, punk!\"
    combines the catch phrase \"You\'ve been X\'d\" from first program
    with the name of the second program.
-   Michael peers from behind a plant and says \'\"Verrry interesting\"
    in the style of [Arte
    Johnson\'s](Wikipedia::Arte_Johnson "wikilink") recurring character
    on the late 1960s program
    *[Laugh-In](Wikipedia:Rowan_&_Martin's_Laugh-In "wikilink")*.

Trivia
------

-   This episode was adapted from the first episode of the [original
    British series](Wikipedia:The_Office_(UK_TV_Series "wikilink"), but
    \"Americanized\" by executive producer [Greg
    Daniels](Greg_Daniels "wikilink"). Most scenes are very similar to
    the British version, though some are very different.
-   Most of this episode was filmed more than six months before the
    other episodes of the season.
-   At the start of each day, director Ken Kwapis instructed the actors
    to pretend to be at work for 30 minutes. Cameras did not roll; the
    \"work sessions\" were an exercise to build an appropriate office
    atmosphere and dynamic. It was during these work sessions that the
    relationship among the three accountants was developed.[^1]
-   During the \"demarcation\" scene with Jim and Dwight, Jim\'s tie has
    a white label (made using a label maker) that reads, \"SHUT UP\".
-   The scene where [Jim](Jim_Halpert "wikilink") tapes pencils to his
    desk and [Dwight](Dwight_Schrute "wikilink") says they are a safety
    violation was originally filmed as the British version was, in which
    [Tim](Wikipedia:Tim_Canterbury "wikilink") stacks up cardboard boxes
    in front of [Gareth](Wikipedia:Gareth_Keenan "wikilink") so he
    can\'t see him. The script originally used the box-stacking joke
    from the British *Office*. After shooting was complete, Greg Daniels
    decided to replace it with the pencil-fence scene, and [John
    Krasinski](John_Krasinski "wikilink") and [Rainn
    Wilson](Rainn_Wilson "wikilink") were called back to the set (notice
    that no other actors other than Stanley are in the office during the
    pencil-fence scene). When [Michael](Michael_Scott "wikilink")
    announces the meeting in the conference room, the original boxes can
    still be seen stacked on Jim\'s desk.
-   The \"cat party\" scene was improvised by [Jenna
    Fischer](Jenna_Fischer "wikilink") and [John
    Krasinski](John_Krasinski "wikilink"). It was inspired by an
    incident that took place during one of the \"work sessions\":
    [Angela Kinsey](Angela_Kinsey "wikilink") (as [Angela
    Martin](Angela_Martin "wikilink")) gave Jenna Fischer (as [Pam
    Beesly](Pam_Beesly "wikilink")) a Post-It note inviting Pam to her
    cat\'s birthday party. When Ken Kwapis instructed Jenna Fischer and
    John Krasinski to flirt, Jenna Fischer saw the note and improvised
    upon it.[^2]
-   The two women in blue sweaters at the staff meeting were actual
    accountants that worked on the production. Greg Daniels decided not
    to use them regularly because their job involved a lot of telephone
    calls, the sound of which would disrupt filming.
-   Henriette Mantel (credited) is one of the unidentified office
    workers in the conference room scene; she is seated beside Dwight.
    Henriette has appeared in numerous sitcoms and notably played the
    role of Alice in the *Brady Bunch* movies from the 1990s. Henriette
    is an actress, not an \"actual accountant\'.
-   [Michael](Michael_Scott "wikilink") asks
    [Ryan](Ryan_Howard "wikilink") if he likes *[The Jamie Kennedy
    Experiment](Wikipedia:The_Jamie_Kennedy_Experiment "wikilink")* and
    *[Punk\'d](Wikipedia:Punk'd "wikilink")*, which Ryan says he does.
    [B.J. Novak](B.J._Novak "wikilink"), who plays Ryan, was a prankster
    on the second season of Punk\'d.
-   The work computers are all different than in the rest of the series.
    They have much fatter flat panel monitors and are running on Windows
    98, whereas in later episodes they are running on Windows XP.
-   This episode marks the first mention of [Josh
    Porter](Josh_Porter "wikilink"), [Todd
    Packer](Todd_Packer "wikilink"), and the [Stamford
    branch](Dunder_Mifflin_Stamford "wikilink"). Also mentioned is the
    possibility of a merge of the
    [Scranton](Dunder_Mifflin_Scranton "wikilink") and Stamford
    branches, which later happens in the [Season 3](Season_3 "wikilink")
    episode \"[The Merger](The_Merger "wikilink")\".
-   Michael identifies Dane Cook as one of his comedic influences. In
    real life, [Steve Carell](Steve_Carell "wikilink") co-starred with
    Cook in the romantic comedy *Dan in Real Life* as Cook\'s brother.
    Both Carell and Cook are from Massachusetts.
-   The establishing shot of the [Dunder
    Mifflin](Dunder_Mifflin "wikilink") offices at the beginning of act
    two is the Paper Magic building at the corner of Adams and Mulberry
    streets in Scranton. (The Paper Magic building appears at timecode
    2:35 in [this
    slideshow](http://www.boston.com/travel/getaways/us/specials/showusscranton/).)
-   Michael says to the camera, \"We have the entire floor.\" We
    discover in later episodes, however, that the floor is shared with
    [Vance Refrigeration](Vance_Refrigeration "wikilink").
-   Demonstrating their casual attitudes toward their jobs,
    [Pam](Pam_Beesly "wikilink") and [Jim](Jim_Halpert "wikilink") talk
    about [Angela\'s](Angela_Martin "wikilink") cat party while
    everybody else in the office discusses downsizing.
-   During the meeting where Michael talks about the possibility of
    downsizing, [Stanley](Stanley_Hudson "wikilink") can be seen
    standing in the background holding what appears to be a bottle of
    Pedialyte.
-   Near the beginning of the episode, Dwight is singing \"The Little
    Drummer Boy\". In the Season 3 Christmas episode, \"[A Benihana
    Christmas](A_Benihana_Christmas "wikilink")\", Dwight holds the
    microphone for Angela while she sings the same song and in the
    Season 5 premier episode, \"[Weight Loss](Weight_Loss "wikilink")\",
    Angela reveals that it\'s her favorite song.
-   Pam is the only person Michael has fake-fired twice; this was the
    first time. The second time was in the [Season
    5](Season_5 "wikilink") episode \"[Casual
    Friday](Casual_Friday "wikilink")\".
-   In this episode, four pranks were pulled; one from Michael, and
    three from Jim. This episode features the most pranks ever pulled.

Goofs
-----

-   Boxes from an original take of the \"pencil-fence scene\" are
    visible when [Michael](Michael_Scott "wikilink") announces the
    meeting. ([See above](Pilot#Trivia "wikilink") for more about
    reshooting the scene.)
-   In the conference room scene, look carefully at
    [Jim](Jim_Halpert "wikilink")\'s desk when
    [Michael](Michael_Scott "wikilink") says, \"Corporate has deemed it
    appropriate to enforce an ultimatum upon me.\" The shadow is that of
    producer [Greg Daniels](Greg_Daniels "wikilink").[^3]

Deleted scenes
--------------

Included on the [Season One DVD](Season_1_DVD "wikilink") were various
deleted scenes:

-   An interview with [Dwight](Dwight_Schrute "wikilink") describing his
    family background. This would later be used in the [Season
    2](Season_2 "wikilink") episode \"[Drug
    Testing](Drug_Testing "wikilink")\".
-   [Michael](Michael_Scott "wikilink") holding a
    [Dundie](Dundie "wikilink") saying he\'s the best boss in the world.
-   [Jim](Jim_Halpert "wikilink") self-patronizingly says, without him
    dozens of small businesses would be paperless.
-   An alternate take of Michael introducing
    [Pam](Pam_Beesly "wikilink"), where he says every guy in the office
    has sprayed on Pam.
-   An interview with Dwight explaining how his human touch affects
    sales figures.
-   An alternate take of Michael welcoming
    [Ryan](Ryan_Howard "wikilink") to the office.
-   An interview with Michael talking about downsizing and lying to his
    co-workers about it.
-   Michael showing the Homer Simpson doll and introducing Ryan to
    [Oscar](Oscar_Martinez "wikilink"),
    [Kevin](Kevin_Malone "wikilink"), and
    [Angela](Angela_Martin "wikilink").
-   Interviews with Angela, Oscar, and Kevin, who all talk about the
    downsizing issue and themselves.
-   Michael telling Pam he thinks he has testicular cancer as she eats
    her lunch.
-   Michael playing with the boom mic.
-   After being intimidated by the boom mic in an interview (it\'s in a
    blind spot), Dwight goes into detail about his self-defense
    experience.

Quotes
------

:   see *[Pilot Quotes](Pilot_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited)
-   [David Koechner](David_Koechner "wikilink") as [Todd
    Packer](Todd_Packer "wikilink") (Voice only)

### Guest cast

-   Mike McCaul as [Male
    Employee](Mystery_Dunder_Mifflin_Employees#Pilot_employees "wikilink")
-   Henriette Mantel as [Female
    Employee](Mystery_Dunder_Mifflin_Employees#Pilot_employees "wikilink")
-   Randall Barnwell as Madsen (Uncredited)

References
----------

<references />

[^1]: [The Sound of Young America interviews Jenna
    Fischer](http://www.officetally.com/the-sound-of-young-america-interviews-jenna-fischer),
    OfficeTally, March 2, 2009.

[^2]:

[^3]: Season 1 DVD commentary.
