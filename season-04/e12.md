# The Deposition

![](TheDeposition.jpg "TheDeposition.jpg")

**\"The Deposition\"** is the 12th episode of the fourth season of *[The
Office](The_Office_(US) "wikilink")* and the 65th overall. It was
written by [Lester Lewis](Lester_Lewis "wikilink") and directed by
[Julian Farino](Julian_Farino "wikilink"). It first aired November 15,
2007. It was viewed by 8.86 million people.

Synopsis
--------

Michael shows us how he impresses people by having Pam give him fake
messages telling him there\'s a call for him so he can say he\'ll call
back later no matter how important the call or caller may be. On one
occasion, Ryan is visiting, and Pam brings the usual fake message.
Michael initially passes it up, but Ryan insists that customer service
is first priority and has Michael take the call, which requires Michael
to do a surprise bit of improv.

Jan and Michael drive to the corporate office in New York City to do a
deposition for Jan\'s lawsuit over her dismissal. They rehearse
Michael\'s testimony in the car, and he makes up absurd mnemonic devices
so he won\'t forget what to say. He has also memorized Jan\'s answers
and plans to throw in some \"errs\" and \"ahhs\" for the deposition to
appear not so scripted.

Meanwhile, back at the warehouse, Darryl is wiping Jim out in a ping
pong game while Pam and Kelly watch. Kelly goes all trash talk on Pam,
about how Pam\'s boyfriend \"sucks\" at ping pong, but Kelly calls it
\"talking smack.\" Pam gets ticked off and sets up a practice ping pong
table in the conference room so she can get back at Kelly by way of Jim
and Darryl. She gets Kevin and then Dwight as \"sparring partners\" for
Jim, but Dwight easily defeats Jim. Dwight reveals for the first time
that he is a huge lifelong table tennis fan, and he reels off the names
of immortals of the sport whom he idolizes.

At corporate, Michael is (maybe) all set to support Jan during the
deposition hearing, but Ryan then takes him aside and urges him to do
the right thing and not hurt Dunder-Mifflin. Michael agrees and then is
left with a terrible conflict. Michael\'s testimony starts well when he
mentions the inappropriate way corporate handled Jan\'s situation, but
his comments about her drinking and about apparent conflicts in
statements he made about the nature of their relationship and when it
started do cause problems.

When he finds out that Jan has stolen his diary in order to prove the
timeline of their relationship, Michael is extremely upset. Jan points
out that he emailed a nude photo of her to the entire company. Toby
tries to comfort him, to no avail.

Then, to add insult to injury, Michael, who always felt that he was a
front-runner for the job at Corporate, learns that David Wallace
reluctantly admitted, under his own testimony that, while Michael is a
nice guy, he was never a serious contender for the job.

But David has shown more consideration to Michael than Jan so, when
asked if he believes Dunder-Mifflin really showed a pattern of
mistreatment to Jan, Michael declares that it did not. He tells David
that he thinks the CFO is a nice guy, too.

On the way home, Jan suggests they pick up some food. Michael states
that it should be something cheap.

Trivia
------

-   The [Hiya
    buddy!](http://www.flickr.com/photos/hanapbuhay/2037436322/) drawing
    was sketched freehand by producer [Greg
    Daniels](Greg_Daniels "wikilink"). The crew then had to try several
    times to reproduce the drawing on a Post-It note.[^1] The original
    drawing can be seen in the introductory video to the Writer\'s Block
    on the Season 4 DVD: Look on the whiteboard behind [Mindy
    Kaling](Mindy_Kaling "wikilink") when she throws the football. Greg
    Daniels\' artistic talent also appeared in the episode *[The
    Client](The_Client "wikilink")*.
-   Jan and Michael drive to NYC in a PT Cruiser.
-   The [Robert Mifflin](Robert_Mifflin "wikilink") memorial conference
    room is just the Corporate reception area set with different wall
    paneling and furniture.[^2]
-   For the final scene, [Rainn Wilson](Rainn_Wilson "wikilink") and
    [Michael Schur](Michael_Schur "wikilink") merely mimed playing ping
    pong, and the ball and sound effects were added later. In some
    takes, they dove wildly for the ball.[^3]
-   This is the first time Mose has left [Schrute
    Farm](Schrute_Farm "wikilink"), assuming Michael\'s description in
    *[Office Olympics](Office_Olympics "wikilink")* is correct and Mose
    hasn\'t left the farm in the interim.
-   This episode marks a change in Kelly\'s style. She is seen wearing a
    brightly colored wrap dress here and in several subsequent episodes,
    which is a bit of a stylistic change from her earlier outfits.
-   In the scene where Dwight is listing off professional table tennis
    players, a white square is visible in the reflection of his glasses.
    It is possible that the names were memorized but it is also possible
    that the reflection is that of a piece of paper from which he is
    reading the names.

Amusing details
---------------

-   The talking head interviews are conducted in the annex because the
    conference room is being used for ping pong.

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting Cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levinson](Jan_Levinson "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Recurring Cast

-   [Andy Buckley](Andy_Buckley "wikilink") as [David
    Wallace](David_Wallace "wikilink")
-   [Michael Schur](Michael_Schur "wikilink") as [Mose
    Schrute](Mose_Schrute "wikilink")
-   [Calvin Tenner](Calvin_Tenner "wikilink") as
    [Calvin](Calvin "wikilink") (Uncredited)

### Guest Cast

-   [Holly Maples](Holly_Maples "wikilink") as [Diane
    Kelly](Diane_Kelly "wikilink")
-   [Patrick O\'Connor](Patrick_O'Connor "wikilink") as [Lester
    Schneider](Lester_Schneider "wikilink")
-   Michael Potter as Court Reporter
-   Valeri Ross as Stenographer

References
----------

<references/>
\

[^1]: Season 4 DVD commentary for *The Deposition*.

[^2]:

[^3]: The Scrum, [Podcast XIX: Michael
    Schur](http://thescrumbrandon.com/v1/2008/05/01/podcast-xix-michael-schur/)
