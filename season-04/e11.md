# Survivor Man

![](SurvivorMan.jpg "SurvivorMan.jpg")

**\"Survivor Man\"** is the eleventh episode of the fourth season of
*[The Office](The_Office_(US) "wikilink")* and the 64th overall. It was
written by [Steve Carell](Steve_Carell "wikilink") and directed by [Paul
Feig](Paul_Feig "wikilink"). It first aired November 8, 2007. This
episode is part of NBC\'s Green Week. It was viewed by 8.29 million
people.

Synopsis
--------

Toby returns to the office talking about a recent wilderness retreat he
took with Ryan and many other branch managers. Michael, however, had not
been invited and he was upset to learn of it. In order to show that he
is capable of surviving in the wilderness, Michael decides to strand
himself in the wilderness for a few days, leaving Jim in charge of the
office. Michael blindfolds himself and has Dwight take him to a location
deep in the forest. Michael instructs Dwight to leave after dropping him
off, however, knowing that Michael does not have proper wilderness
survival skills Dwight does not leave, instead staying and keeping an
eye on Michael (even through the scope of his rifle).

Meanwhile, back at the office, Jim learns that it is \"birthday month\",
a stretch of days that includes the birthdays of Creed, Meredith, and
Oscar, Creed\'s being on the particular day that Jim is left in charge.
In his first order of business, Jim forgoes organizing a party for
Creed, instead announcing that the three parties will be celebrated
together. He begins to run into problems, however, when Creed and
Meredith request two different types of cake. Additionally, Andy refuses
to attend unless an ice cream cake is made. Toby also requests to be
included in the party, as his party two months earlier was poorly dated
and thus scarcely attended. In a talking head interview, Jim belittles
Toby\'s request.

Michael proves to be completely incapable of living out in the wild by
himself. With the only materials he has, he is only able to fashion
himself more clothes out of a cut up pair of pants, eventually crudely
creating shelter out of them. Unable to find food, Michael begins to try
to eat a group of wild mushrooms, unaware that they are highly
poisonous. Dwight blows his cover to tackle Michael and pull the
mushrooms from his mouth.

Back at the office, after a brief meeting, Jim finds that no one likes
his office-wide party idea, even provoking Phyllis to mistakenly refer
to him as Michael. Once this has happened, Jim decides to have a
traditional party solely for Creed. Michael and Dwight return in time
for the lighting of the cake, with Michael expressing that he no longer
has any desire to return to the wilderness, and Jim expressing his
relief that Michael has returned to run the office.

Deleted scenes
--------------

The Season 4 DVD contains a number of deleted scenes.

-   In a talking head interview, Ryan explains that he wanted to go
    camping with some friends and invented the idea of a \"green
    retreat\" to obtain funding. He also claims that all the cute
    secretaries are impressed by his environmental stance, but B roll
    footage of Ryan smugly cruising the hallways at Corporate past
    several young women shows nobody paying him any attention. He
    explains that \"No one ever does anything for the environment just
    to help the environment. If someone tells you they have a green
    initiative, it\'s a scam.\"
-   Michael asks Pam, \"hypothetically, would you go camping with me?\"
    Pam declines. In a talking head interview, Pam explains, \"When
    Michael invents a hypothetical situation, he eventually turns it
    into an actual situation.\"
-   In a talking head interview, Michael professes that he\'s glad he
    wasn\'t invited. That way, he can experience the wild on his own. He
    conducts an imaginary conversation in which he regales an audience
    with his tales of the wild. At the end of the talking head
    interview, Michael pretends that Ryan is one of the people
    captivated by his stories.
-   Dwight explains to Michael the properties of one particular knife,
    but Michael isn\'t interested and just asks for the knife. Dwight
    offers other camping gear such as a tent and food, but Michael
    explains that he will simply use his instincts. Dwight moans
    doubtfully.
-   In a talking head interview, Angela explains that there are four
    birthdays in one month because, if you count back nine months, it\'s
    Valentine\'s Day. \"Stagger your sin, people.\"
-   In a talking head interview, Kelly notes that everyone else who
    works at Dunder Mifflin is \"elderly,\" and adds, in confidence,
    \"Pam lies about her age.\"
-   In a talking head interview, Kevin says that he celebrates his
    birthday with friends. \"Who do not work here.\"
-   In a talking head interview, Phyllis claims that this year, she\'s
    going to celebrate her birthday by calling in sick.
-   In the wilderness, Michael gives wildly incorrect descriptions of
    the wilderness. He identifies a bird call as a falcon, but it\'s
    actually just Dwight pretending to be a bird.
-   In a talking head interview after returning to the office, Michael
    describes that nature was beautiful, yet also repulsive \"because I
    threw up all over Dwight\'s hand.\"

Trivia
------

-   [B.J. Novak](B.J._Novak "wikilink") ([Ryan
    Howard](Ryan_Howard "wikilink")) is credited but does not appear in
    this episode
-   The weaponry hidden in the office include a
    [Jian](Wikipedia:Jian "wikilink") (Chinese sword) in the ceiling, a
    [Bowie knife](Wikipedia:Bowie_knife "wikilink") in the filing
    cabinet, a pair of Japanese [Sai](Wikipedia:Sai_(weapon) "wikilink")
    daggers behind the water cooler, and a blowgun in the toilet tank.
    Dwight even claims to have a can of pepper spray taped under his
    chair, which he \"saved Jim\'s life with\" (seen used on [Roy
    Anderson](Roy_Anderson "wikilink") from the episode \"[*The
    Negotiation*](The_Negotiation "wikilink") \")
-   The rifle Dwight uses to watch Michael is a .22 caliber rifle.
-   Michael is inspired by the television program
    *[Survivorman](wikipedia:Survivorman "wikilink")*; see *Cultural
    references* below.
-   The mushroom Michael was about to consume was a species known as:
    *[<u>Hypholoma
    fasciculare</u>](http://en.wikipedia.org/wiki/Hypholoma_fasciculare)*,
    most commonly known as the \"clustered woodlover\", a type of
    mushroom common in woodland areas that grow on rotting trees, stumps
    or trunks. Likewise, it is poisonous and symptoms of consumption
    would result in: diarrhea, nausea, vomiting,
    [proteinuria](http://en.wikipedia.org/wiki/Proteinuria) and
    collapse.
-   Continuity Error: In the season one episode \"*[The
    Alliance](The_Alliance "wikilink")*\", Meredith\'s birthday was in
    April (based on the airdate and Pam\'s planner) and Creed\'s
    birthday was not directly before hers (Pam states that Meredith has
    the next birthday in the office, a month after the events of that
    episode).

Amusing details
---------------

-   Dwight probably keeps his hunting rifle either stashed in the office
    or in his car, as it is unlikely Michael would have tolerated him
    stopping home to get it.
-   Michael\'s sense of time is completely wrong throughout the entire
    show. When he says he\'s been out in the wilderness for \"several
    hours now\" he\'s only been out for an hour and 45 minutes or so.
    And when he says he\'s gone \"about three hours\" without food it\'s
    only been two.
-   In addition, when Michael is recording himself in the woods, he
    looks up at the sky as though calculating the time based on the
    sun\'s location, glances at his watch, and gives an approximate
    time.
-   When Dwight is walking through the forest, the cameraman\'s hand can
    be seen moving branches out the way.
-   When Michael tells Pam he is leaving, the way Dwight holds the knife
    and other objects changes between cuts.
-   When Dwight leads a blindfolded Michael through the forest, he walks
    along a path and intentionally leads Michael through the brush.
-   In the flashback to Kevin\'s birthday, Michael dances with the doll
    from \"[Sexual Harassment](Sexual_Harassment "wikilink")\" but the
    doll has a \"[Stacy](Stacy "wikilink")\" sticker on its chest.
    Consequently, Kevin is not particularly amused by Michael\'s antics.
-   The trees in the forest include Ponderosa Pine Trees, Jeffery Pine
    Trees, and Incense Cedar Trees. These trees are not native to
    Pennsylvania, but are native to the Transverse Ranges of Southern
    California where the forest scenes were most likely shot.
-   After fashioning a shelter from his clothes, Michael proudly
    announces that he \"tented \[his\] pants!\" Given that \"tenting
    \[one\'s\] pants\" is a euphemism for getting an erection, it is
    unusual that Michael does not say \"That\'s what she said!\",
    especially given his conversation with Jim about the phrase at the
    end of the episode.
-   In the flashback of Michael surprising Phyllis in the parking lot,
    it looks like she is trying hard not to laugh after dropping the
    cake.
-   In the birthday party scene, Michael is wearing a Battlestar
    Galactica sweatshirt, which he must have borrowed from Dwight.
-   Meredith\'s birthday was at the end of the month, after everyone
    else\'s. However, in \"The Alliance\", they throw a party for
    Meredith, whose birthday isn\'t for another month. This means that
    Kelly and Creed were not considered when they were choosing the next
    office birthday. In addition, later on in the episode, Creed implies
    Jim\'s birthday was in this time period, as he mentions that Jim\'s
    birthday was celebrated 3 weeks prior.
-   Even Toby seems happy to see Michael when he arrives in the
    conference room at the end of the episode.
-   There appears to be matches and an ashtray in the men\'s bathroom,
    as well as a magazine with a topless woman. In a previous episode,
    when the picture of topless Jan was plastered everywhere, Dwight
    mentioned that there were a lot in the bathroom.

Cultural references
-------------------

-   Andy tells Jim he wants a Fudgie the Whale cake. [Fudgie the
    Whale](wikipedia:Fudgie_the_Whale "wikilink") is a type of ice cream
    cake made by Carvel.
-   Michael mentions the television program
    *[Survivorman](Wikipedia:Survivorman "wikilink")*, a reality program
    that focuses on the emphasis of survival in various situations,
    which Michael, in this episode, claims to have been \"kidnapped by a
    serial killer\". In this case, Michael attempts to imitate the
    survival expert [Les Stroud](Wikipedia:Les_Stroud "wikilink"), the
    star of the show, but has absolutely no idea on how to use survival
    skills, such as his focus on the uses of his clothes (such as
    converting his pants to shorts, or even his pants to a tent) and his
    consumption of a poisonous mushroom, which Dwight interferes in time
    before the mushrooms are ingested. Additionally, Michael imitates
    the way the show is also filmed as well, by using a home video
    camera rather than a TV crew.

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")

### Supporting Cast

-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

\
