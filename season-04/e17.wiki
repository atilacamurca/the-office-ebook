{{Office episode
|Title      =Job Fair
|Image      =[[Image:JobFair.jpg|250px]]
|Season     =[[Season 4|4]]
|Episode    =17
|Code       =4017
|Original   =May 8, 2008
|Writer(s)  =[[Lee Eisenberg]] & [[Gene Stupnitsky]]
|Director   =[[Tucker Gates]]
|Prev       =[[Did I Stutter?]]
|Next       =[[Goodbye, Toby]]}}
'''Job Fair''' is the seventeenth episode of the fourth season of ''[[The Office (US)|The Office]] ''and the 70th episode overall. It was written by [[Lee Eisenberg]] and [[Gene Stupnitsky]]. It was directed by [[Tucker Gates]]. It first aired May 8, 2008. It was viewed by 7.22 million people.

==Synopsis==
Michael, Pam, Oscar and Darryl set up a booth at a local job fair at Valley View High School, which happens to be Pam's alma mater, to find the "Best and the Brightest" for Dunder-Mifflin's summer internship. Meanwhile, Jim hits the links with Andy and Kevin to try and land his biggest sale after receiving a formal warning from Ryan regarding his job performance.

Michael sets up the company's booth at the job fair by placing one sheet of white paper on the table and nothing else. The client Jim is trying to impress, Phil Maguire, suggests they bet on the golf game, which excites Kevin, as he both loves to gamble and proves to be an excellent golfer. Andy, on the other hand, is quite a poor golfer. The employees realize there is no reason for them to be in the office with so many people gone and want to go home. Stanley quietly packs his things and walks out as Dwight becomes outraged that everyone wants to leave.

At the job fair, Michael turns away the only student interested in their booth because he is not the type of person Michael was interested in recruiting. He then gets mad that the student signed his name on the sheet of paper and tells Pam to put out another. Pam only brought one, and when Michael gets mad at her, she points out that he told her to only bring one sheet. Michael pushes the blame back on her and makes her go find another piece of paper.

Jim tries to convince the client to switch paper companies, but Phil is content with the supplier he is using and is not interested in hearing a sales pitch. Michael tries to pull other students in to the booth but no one is interested. Back at the office, Angela and Dwight find themselves alone as the rest of the employees decide to go home.

Pam finds a piece of paper from her old art room and returns to the booth with it, but Michael is mad that it is not Dunder-Mifflin paper and makes Pam go back to the office to retrieve another sheet. Meanwhile, Dwight calls Michael with the news that the employees left but Michael agrees that no one should have to be there as both he and Jim are gone and hangs up.

The sales pitch does not seem to be going well for Jim, but he is determined to give it his best shot. After getting shot down again, Andy, in an attempt to be funny, suggests racing to the next hole and crashes the golf cart into a sand trap. Michael becomes disgusted with how well the rest of the booths are doing. Michael tries to re-recruit Justin, the only student who showed an interest in the company, but Justin tells Michael he was a jerk and walks away.

After the golf game, Jim delivers one last desperate sales pitch and Phil finally breaks down and gives Dunder-Mifflin his business. As the job fair starts to wrap up, Michael grabs the microphone and, like Jim, delivers one last desperate sales pitch to get anyone to intern at the company, but he gets no takers.

In his final talking head, Michael justifies his failure at the job fair by accepting credit for Jim's success, as Jim chooses to work at Dunder-Mifflin.

The final scene shows Pam walking over to a graphic design booth during the job fair and asking for an application. Although Pam is intimidated when the graphic design interviewer lists several programs that she is unfamiliar with, the interviewer states that it should be easy to find adult education programs to help Pam learn them.

==Deleted scenes==
* In a talking head interview, Michael explains that kids today have a short attention span, preoccupied with texting, video games and sex. And today, they are being distracted by other booths.
* Michael calls a student to the booth and tries to convince him that Dunder Mifflin is a lot of fun. Pam arrives with the paper, and Michael points out that the student could look at Pam all day. He gives the student the paper and says he can keep it. Pam is stunned.
* Michael tries to convince that student (who is clearly not interested) that Dunder Mifflin is a fun, sexy place. The student manages to escape, but Michael follows him. The student goes to the bathroom and is disturbed to find Michael waiting for him. The student finally tells Michael directly that he doesn't want to waste his life "selling paper for your stupid company".
* Kevin anticipates that the winnings from the golf game will help cover his gambling debts.
* Jim confides that inviting Andy was a calculated move: The client hates Cornell.
* Pam sees a picture of Roy (in the school's trophy case, along with many accolades from his time on the football team) and says, "9 years of my life".

==Trivia==
* The booths at the job fair are real organizations except for [[Dunder-Mifflin]] itself: [[Wikipedia:Office Depot|Office Depot]] (office supplies), [http://www.simplexhomes.com/ Simplex Homes], [[Wikipedia:WICK|WICK Sports Radio "The Game"]], [http://www.ecweekend.com/ Electric City] magazine, [[Dunder Mifflin Scranton]], [[Wikipedia:H&R Block|H&amp;R Block]] (tax preparation), [RLG], the [[Wikipedia:United States Air Force|Air Force]], [http://pennstarbank.com Pennstar Bank], the [http://www.hilton.com/en/hi/hotels/groups.jhtml?ctyhocn=SCRCCHF Hilton Scranton & Conference Center], Allegheny Rent-A-Car, Starlight Models, [http://www.caregiversamerica.com/ Care Givers America] (Clarks Summit), [http://www.the-funeral-home-directory.com/funeralhomes/Pennsylvania/Scranton/35692/service.ashx Frank M. Regan Funeral Home], [http://www.solidcactus.com/ Solid Cactus], L. R. Constanzo Construction, the Greater Scranton Chamber of Commerce, and [http://www.lamar.com/main/default.cfm Lamar Advertising].
* Actor Trevor Einhorn ([[Justin]]) is better-known to American television audiences as [[Wikipedia:Minor characters on Frasier#Frederick Gaylord Crane|Frederick Crane]], son of [[Wikipedia:Frasier Crane|Frasier Crane]] from the television situation comedy ''[[Wikipedia:Frasier|Frasier]]''. (No relation to ''The Office'' Director of Photography [[Randall Einhorn]].<ref name="LeeQA">[http://www.officetally.com/lee-eisenberg-answers-job-fair-questions Lee Eisenberg answers 'Job Fair' questions], [http://www.officetally.com/ Office Tally]</ref>)
* The writers considered sending [[Dwight]] to the job fair (where he would interact with the students) or adding him to [[Jim]]'s golf foursome (where he was eventually kicked out and played golf with a bunch of old ladies instead) before settling on what happens in this episode.<ref name="LeeQA" />
* [[Andy]], [[Kevin]] and [[Phil Maguire]] (the client) all use the [http://www.dickssportinggoods.com/product/index.jsp?productId=2557216 Odyssey White Hot XG 2 Ball Center Shaft Putter].
* The make up department carefully researched the correct placement of Andy's golfing blisters.<ref name="LeeQA" />
* [[Brian Baumgartner]] is an accomplished golfer.<ref name="LeeQA" /> This episode once again demonstrates the actor's talent for sports.
* The graphic design company representative that Pam meets with was played by [[Nelson Franklin]], the same actor who portrays [[Nick]] the I.T. Guy when [[Sabre]] takes over in [[Season 6]].[http://www.imdb.com/name/nm1815689/]

==Connections to other episodes==
* [[Pam]] tells the intern candidate [[Justin]], "We uh eat a lot of cake." In the episode [[Survivor Man]], we learn that every employee in the office gets an individual birthday cake.
* Michael previously used the "And don't call me Shirley" joke with Jan in [[Valentine's Day]]. But in that episode, he used it correctly.
* Jim and Pam share a polite handshake even though they would rather kiss - and after already sharing a kiss. In [[The Return]], Dwight and Angela share a polite handshake even though they would rather kiss.
* Pam does not seem to recognize the graphic design interviewer, [[Nick]], in the episode [[Sabre]], who has been hired as an IT specialist.
*Pam tells the camera that she has fond memories from her high school years in the gym, including “pretending to have PMS to get out of volleyball.” However, in the Season 5 finale “Company Picnic,” Pam is the most skilled volleyball player on the team and tells the camera that she played a little in junior high, high school, college, “and went to volleyball camp most summers.”

==Amusing details==
*[[B.J. Novak]] ([[Ryan Howard]]) and [[Paul Lieberstein]] ([[Toby Flenderson]]) are credited but do not appear in this episode
*[[Kevin]] once again gambles the petty cash. He did so previously in the Webisode ''[[The Accountants: Things Are Getting Tense|Things Are Getting Tense]]''.
* While in the gym, [[Pam]] recounts how she used to pretend to have PMS to get out of playing volleyball. However, in the season 5 finale [[Company Picnic]], not only is Pam shown to be an excellent volleyball player, she proudly states that she used to play in jr. high, high school, and college; in addition to spending most of her summers at volleyball camp.

==Goofs==
* [[Pam]] returns to the office for a sheet of card stock, but she takes it from the copy machine. It is very unusual to use card stock in a copy machine. However, it did appear to be heavy duty paper when she held it up. Also, it was from a lower drawer of the copier where paper other than standard 20 pound may be kept.
* Andy's hands are fully healed when he is playing air guitar when Jim and Pam kiss despite being covered in blisters and sores at the golf course earlier that day.

==Cultural references==
* [[Michael]] says that he wants to ''youthanize'' the office, a made-up word combining "youth" and the verb suffix "-ize". He inadvertently says the word "euthanize", meaning "to kill in a humane way."
* [[Wikipedia:Valley View High School (Pennsylvania)|Valley View High School]] is located in Archbald, Pennsylvania, about nine miles north of [[Scranton]].
* [[Kelly]] tells [[Darryl]], ''You look like [[Wikipedia:Barack Obama|Barack Obama]].'' Obama was a leading presidential candidate at the time the episode aired.
* [[Andy]] mentions his grandfather ''spinning in his urn''. The idiom "spinning in one's grave" is used to describe a situation which would have made a dead person furious if they were still alive. Andy adapts this idiom to accommodate the fact that his grandfather was cremated.
* Andy refers to the client as a ''Dartmouth boy''. [[Wikipedia:Dartmouth College|Dartmouth College]] is a university in [[Wikipedia:New Hampshire|New Hampshire]], a member of the [[Wikipedia:Ivy League|Ivy League]] of prestigious universities (which includes Cornell) in the Northeast United States. (Incidentally, writer/actor [[Mindy Kaling]] went to Dartmouth).
* [[Kevin]] lists several golf betting games: [http://www.bestcoursestoplay.com/html/skins.html Skins], [http://www.playgolfforfun.com/Acey_Deucy.html Acey Deucey], [http://www.bestcoursestoplay.com/html/bingo-bango-bongo.html Bingo-Bango-Bongo], Sandies, Barkies, [http://golf.about.com/cs/golfterms/g/bldef_arnie.htm Arnies], and [http://www.bestcoursestoplay.com/html/wolf.html Wolf].
* ''[[Wikipedia:Premenstrual syndrome|PMS]]'' (known as PMT in British English) is short for "Premenstrual syndrome", a collection of adverse symptoms tied to a woman's menstrual cycle.
* [[Dwight]] says ''the American workday ends at 5pm.'' The traditional American workday runs from 9am to 5pm.
* ''Junk mail'' is an informal term for unwanted advertisements received in the mail.
* Michael blames ''Dateline'' for his lack of success at the job fair. ''[[Wikipedia:Dateline NBC|Dateline NBC]]'' is a television news magazine known for its "[[Wikipedia:To Catch a Predator|To Catch a Predator]]" segment which identifies child sexual predators.
* Michael suggests that [[Justin]] could be a ''migraine worker''. He means "migrant worker."
* Michael says ''And don't call me Shirley'', repeating a joke from the movie ''[[Wikipedia:Airplane!|Airplane!]]''. His use of the joke is incorrect, however, since [[Pam]] merely said "Are you serious?" instead of "Surely you can't be serious."
* Michael describes Justin as ''the ugly girl in the movie, who takes off her glasses and she's hot! And you realize she was always hot, she was just wearing glasses.'' This is a movie cliche [http://tvtropes.org/pmwiki/pmwiki.php/Main/TheGlassesGottaGo] a special case of the ''[http://tvtropes.org/pmwiki/pmwiki.php/Main/BeautifulAllAlong Beautiful All Along]'' cliche.
* Michael derides the job fair as ''[[Wikipedia:List of baseball jargon (B)#bush league|bush league]]'', originally a derogatory term for the minor leagues of professional baseball, but which has entered general use as slang for "substandard."
* When Pam and Jim kiss, Kevin and Andy add musical accompaniment in a style [[Wikipedia:Porn groove|characteristic of pornographic movies]].
* The graphic designer lists a variety of computer graphic software: ''[[Wikipedia:Adobe Photoshop|Photoshop]]'', ''[[Wikipedia:Adobe Dreamweaver|Dreamweaver]]'', ''[[Wikipedia:Corel Painter|Corel Painter]]'', ''[[Wikipedia:Adobe Illustrator|Illustrator]]'', and ''[[Wikipedia:Adobe After Effects|After Effects]]''.
* ''[[Wikipedia:Adult education|Adult education]] classes'' are non-credit evening classes typically offered by small colleges to adults in the community.
* Michael kicks a ball like Nick Nolte in Blue Chips movie from 1990

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting Cast===
*[[Ed Helms]] as [[Andy Bernard]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]

===Guest Cast===
*[[Phil Reeves]] as [[Clients of Dunder Mifflin#Phil Maguire|Phil Maguire]]
*[[Trevor Einhorn]] as [[Justin]]
*Emerson Brooks as Air Force Representative
*Charlie McDermott as High School Student
*Lori Murphy Saux as The Announcer

===Recurring Cast===
*Nelson Franklin as [[Nick]]
*Elvy Yost as [[Megan]] (deleted scene)

==References==
<references/>
{{Season4}}
No
