# The Alliance

**\"The Alliance\"** is the fourth episode of the [first
season](Season_1 "wikilink") of \'\'[The
Office](The_Office_(US) "wikilink") \'\'and the 4th overall. It was
written by Michael Schur and directed by Bryan Gordon. It first aired on
April 12, 2005. It was viewed by 5.4 million people.

Synopsis
--------

Although time has dragged on, the downsizing rumors at [Dunder
Mifflin](Dunder_Mifflin "wikilink") have not ceased.
[Michael](Michael "wikilink") will still not accept that there will be
any staff cuts. [Dwight](Dwight "wikilink") feels particularly
threatened by the impending crisis, and, in an act of desperation, forms
an alliance with his office nemesis [Jim](Jim_Halpert "wikilink"). Jim
sees the alliance as an opportunity with great potential and agrees as a
lark. He immediately enlists [Pam](Pam "wikilink")\'s help in the
situation.

Meanwhile, Michael tries to boost morale in the office by having an
office birthday party for Meredith, even though her birthday is a month
away. Since this isn\'t really about [Meredith](Meredith "wikilink"),
Michael has no problem despite ordering an ice cream cake even though
she\'s lactose intolerant. He also agonizes over writing an insulting
joke in her birthday card that he thinks will be hilarious. In the
meantime, Oscar asks Michael for a donation for his nephew\'s cerebral
palsy walk-a-thon. Michael looks over the \$2 and \$3 contributions and,
feeling generous, signs up for \$25. When Jim points out that the dollar
amounts are per mile which adds up, Michael goes to Oscar to explain his
misunderstanding. Oscar reminds him what a walk-a-thon is and thinks
Michael is being cheap for retracting a charitable donation.

Meanwhile, Dwight, following Jim and Pam\'s efforts, ends up in a box in
the warehouse to overhear information. The party falls flat when
Michael\'s insulting jokes fail to amuse Meredith or anyone else.

At the end of the day, after a breakthrough in his pranks on Dwight, Jim
giddily grabs Pam\'s hand in an attempt to explain what has just
happened. However, Pam\'s fiance [Roy](Roy_Anderson "wikilink") walks in
and immediately assumes Jim is hitting on her. Jim tries to explain that
they were just playing a joke on Dwight by pretending to be in an
alliance with him. Dwight, not finding it funny, denies any knowledge.

Deleted scenes
--------------

The Season One DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Dwight brags to Jim how secure his computer is. Jim successfully
    guesses Dwight\'s password (\"Frodo\"). Dwight changes it, and Jim
    guesses it again (\"Gollum\").
-   Michael critiques Pam\'s phone-answering delivery. (This is the
    scene used in the title sequence when Jenna Fischer\'s name
    appears.)
-   Dwight throws old food out of the office fridge.
-   Jim complains to Dwight that he threw out his lunch as Dwight plays
    with his [Phillies](Wikipedia:Philadelphia_Phillies "wikilink")
    [bobbleheads](Wikipedia:bobblehead "wikilink").
-   Oscar asks Angela to sign up for the walk-a-thon.
-   Extension of Jim and Dwight\'s conversation in the parking lot.
-   Toby signs Meredith\'s card in Michael\'s office, but Michael
    prevents him from writing any jokes.
-   Jim hopes to get Dwight to say \"immunity\" (a la
    *[Survivor](Wikipedia:Survivor_(TV_series) "wikilink")*).
-   Extended scene with Dwight trying to help Michael with Meredith\'s
    card but only with jokes referencing to her hysterectomy. When
    Dwight asks Michael for immunity from downsizing, Jim quietly
    celebrates.
-   Michael tries to get Meredith to inconspicuously recall recent funny
    moments with him.
-   Michael explains to Oscar how he shouldn\'t have deceived him with
    the walk-a-thon.
-   Michael presents his alternate (and just as bad) card jokes to
    Meredith and when she starts crying he asks the cameras if they can
    just edit to the best one.
-   Dwight questions Ryan about his alliances to which Ryan says
    \"what?\" and Dwight says \"well played\".

Trivia
------

-   This episode features three alliances, a fake alliance between Jim
    and Dwight, a real alliance between Jim and Pam, and a minor
    alliance between Dwight and [Roy](Roy "wikilink").
-   In the warehouse scene with Pam, the role of Dwight in the box was
    played not by actor [Rainn Wilson](Rainn_Wilson "wikilink") but by a
    crew member.
-   The [Dundie](Dundie "wikilink") awards are first mentioned in this
    episode. We finally get to see the Dundies in the episode \"[The
    Dundies](The_Dundies "wikilink")\".
-   The first cut of the episode ran 37 minutes long. Producers
    considered making the episode a two-parter, one focusing on the
    Alliance and another focusing on Meredith\'s birthday party.[^1]
-   The car that Dwight kicks actually belongs to actress [Phyllis
    Smith](Phyllis_Smith "wikilink"), who plays Phyllis on the show.
    [Rainn Wilson](Rainn_Wilson "wikilink") stated that after every take
    she would tell him, \"Please don\'t kick the hubcap *too*
    hard.\"[^2]
-   There were over a dozen takes of the scene where Michael wolfs down
    cake. Steve Carell ate so much (at 7:30 in the morning) that he got
    sick.[^3] Jenna Fischer considers shooting the scene as \"the
    absolute worst\" experience on the show. \"We all got so sick \[from
    eating ice cream cake\]\".[^4]
-   Not much of the footage made the final cut, but during the party,
    Ryan talks to a different woman in the background of each scene. The
    producers thought this was a nice character touch for the new
    employee.[^5]
-   A sign seen in this episode gives Dunder Mifflin\'s address as 1725
    Slough Avenue, a reference to the UK *[The
    Office](The_Office_UK "wikilink")* locale in Slough, Berkshire,
    England.
-   On the Season One DVD, the synopsis for this episode mistakenly
    refers to Jim as John, the name of the actor portraying him ([John
    Krasinski](John_Krasinski "wikilink")).
-   When Dwight asks Jim if he would like to form an alliance, Jim
    responds by saying \"Absolutely, I do\". This is later referenced in
    [The Job](The_Job "wikilink"), in which Pam says that Jim told her
    that if Dwight ever asks you to be in something secret, you respond
    with \"Absolutely, I do\".
-   This episode marks the first appearance of the staff newsletter. The
    text of the staff newsletter is the same each time it appears (since
    it is merely a [prop](theatrical_property "wikilink")); only the
    picture and headline change.

### Top Salesman Award

Welcome to yet another exciting edition of the Dunder Mifflin
Employee Newsletter. Thanks to all of the staff and new
contributing writers for putting this together for all of you,
and also many thanks to the folks at Designtown for printing
this up for us. Hopefully you will find a lot of useless
information contained herein that will help you do your job
better, faster and quicker and cheaper and happier.
As anybody can easily tell, this newsletter doesn\'t really have
a lot to say. It\'s really just a prop to fill some space and
sort of look like a newsletter without really being much of a
newsletter at all. By typing a lot of words in two columns on
the front of this page, we can achieve the look of a newsletter
without really reporting much news or provide any real
information to the reader at all. In fact, at times we can
probably get away with not using real English words, such as
kjgowbiwiwpo, ovcviqvck, or the much beloved dfbiouvsulegphaelk.
These words can also be strung together to form a sentence,
paragraph or even a whole prop book, magazine or newspaper.

Amusing details
---------------

-   When Jim begins taping up the box, there is a rat caught in a trap
    under the rolling cart behind him. (The production team even went to
    the effort of getting an animatronic rat.)
-   In a conversation between Toby and Ryan it is revealed that it is
    actually Ryan\'s birthday. Toby and Ryan are in mid-conversation and
    Toby says \"really? today?\" and Ryan says \"yeah\" Toby says \"want
    me to say something\" and Ryan says \"don\'t do that.\"
-   In Meredith\'s birthday card, Michael reads what Jim wrote.
    \"You\'re an accountant, just fudge the numbers\". Meredith is in
    supplier relations not accounting.

Cultural references
-------------------

-   The terms *alliance* and *immunity* come from the early 21st century
    television reality game show
    *[Survivor](Wikipedia:Survivor_(franchise) "wikilink")*. Contestants
    are eliminated one by one, although a contestant can win
    \"immunity\" from elimination, and contestants typically form
    \"alliances\" to engineer the elimination of a rival. Randall
    Einhorn, the camera man for this episode and several other, was a
    camera man for *Survivor.*
-   *Come on down!* is the catch phrase from the television game show
    *[The Price Is Right](Wikipedia:The_Price_Is_Right "wikilink")*.
-   \"You\'re fired!\" is the catch phrase from the television reality
    game show *[The
    Apprentice](Wikipedia:The_Apprentice_(U.S._TV_series) "wikilink")*,
    hosted by [Donald Trump](Wikipedia:Donald_Trump "wikilink").
-   Michael uses his cell phone like a communicator from *[Star
    Trek](Wikipedia:Star_Trek:_The_Original_Series "wikilink")*, a 1960s
    science fiction television program.
-   *Beeyotches* is an intentional mispronunciation of *bitches*, a
    derogatory term for women.
-   *[Baskin-Robbins](Wikipedia:Baskin-Robbins "wikilink")* is a chain
    of ice cream parlors.
-   *Guns* is a slang term for muscles, most commonly the biceps.
-   *Bird day* is a pun on *birthday*.
-   *[Mary had a little
    lamb](Wikipedia:Mary_Had_a_Little_Lamb "wikilink")* is the opening
    line of a nursery rhyme.
-   A *[walkathon](Wikipedia::walkathon "wikilink")* is a
    [fundraising](Wikipedia:Fundraiser "wikilink") event where donors
    pay a pledged amount for each mile (or other distance) the
    participant walks, up to a pre-set limit.
-   *[Liz Taylor](Wikipedia:Elizabeth_Taylor "wikilink")* is an actress
    who has been divorced seven times.
-   A *brain fart* is slang for a momentary mental lapse.
-   *Frodo* and *Gollum* are characters from the epic fantasy novel
    *[The Lord of the
    Rings](Wikipedia:The_Lord_of_the_Rings "wikilink")*.
-   *[Michael Jackson](Wikipedia:Michael_Jackson "wikilink")* is a
    musician known for, among other things, undergoing large quantities
    of cosmetic surgery.

Quotes
------

:   See *[The Alliance Quotes](The_Alliance_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring cast

-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (uncredited)
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink") (uncredited)

References
----------

<references/>

[^1]: Daniels, Greg (Producer). 2005. \"The Alliance\" \[Commentary
    track\], *The Office* Season One (U.S./NBC Version) \[DVD\], Los
    Angeles, CA: [Universal](Universal_Studios "wikilink").

[^2]: Wilson, Rainn (Actor). 2005. \"The Alliance\" \[Commentary
    track\], *The Office* Season One (U.S./NBC Version) \[DVD\], Los
    Angeles, CA: [Universal](Universal_Studios "wikilink").

[^3]: Fischer, Jenna (Actor), Krasinski, John (Actor), and Daniels, Greg
    (Producer). 2005. \"The Alliance\" \[Commentary track\], *The
    Office* Season One (U.S./NBC Version) \[DVD\], Los Angeles, CA:
    [Universal](Universal_Studios "wikilink").

[^4]: [Celebrity Parade: Jenna
    Fischer](http://www.parade.com/celebrity/celebrity-parade/archive/pc_0165.html),
    Parade Magazine

[^5]: Novak, B. J. (Actor/Writer). 2005. \"The Alliance\" \[Commentary
    track\], *The Office* Season One (U.S./NBC Version) \[DVD\], Los
    Angeles, CA: [Universal](Universal_Studios "wikilink").
