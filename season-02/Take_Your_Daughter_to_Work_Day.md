# Take Your Daughter to Work Day

**\"Take Your Daughter to Work Day\"** is the eighteenth episode of the
second season of \'\'[The Office](The_Office_(US) "wikilink") \'\'and
the 24th overall. It was written by [Mindy
Kaling](Mindy_Kaling "wikilink") and directed by [Victor Nelli
Jr.](Victor_Nelli_Jr. "wikilink") It first aired on March 16, 2006. It
was viewed by 5.42 million people.

Synopsis
--------

It is \"[Take Your Daughter To Work
Day](Wikipedia:Take_Our_Daughters_And_Sons_To_Work_Day "wikilink")\" at
Dunder Mifflin Scranton, which [Michael](Michael_Scott "wikilink")
laments, as he feels he\'s not good with kids.
[Toby](Toby_Flenderson "wikilink") and
[Stanley](Stanley_Hudson "wikilink") bring their daughters
([Sasha](Sasha_Flenderson "wikilink") and
[Melissa](Melissa_Hudson "wikilink") respectively),
[Kevin](Kevin_Malone "wikilink") brings his fiancée\'s daughter
[Abby](Abby "wikilink"), and [Meredith](Meredith_Palmer "wikilink")
brings her son [Jake](Jake_Palmer "wikilink").
[Pam](Pam_Beesly "wikilink") is determined to become one child\'s friend
by the end of the day. Michael starts to win over Toby\'s daughter Sasha
when she visits him in his office. Stanley\'s daughter develops a crush
on [Ryan](Ryan_Howard "wikilink"), but a jealous
[Kelly](Kelly_Kapoor "wikilink") alerts Stanley, leading him to
reprimand Ryan for his \"motives\". [Jim](Jim_Halpert "wikilink") makes
friends with Kevin\'s future step-daughter, who asks Jim over for
dinner. However, Jim declines because he has a date, presumably
[Brenda](Brenda_Matlowe "wikilink") from the [Booze
Cruise](Booze_Cruise "wikilink").

Michael shows the office his childhood appearance on the kid\'s show,
*[Fundle Bundle](Fundle_Bundle "wikilink")*, where he revealed his
childhood dream was to \"get married and have 100 kids, so I can have
100 friends, and no one can say \'no\' to being my friend.\" He becomes
depressed and retreats into his office when the children remind him that
he never achieved that dream (or anything resembling it). Toby then
talks to him and Michael decides to start online dating (with the
unfortunate username of \"LittleKidLover\"). After tormenting
[Dwight](Dwight_Schrute "wikilink") all day (and calling him \"Mr.
Poop\"), Meredith\'s son is insulted by him, much to
[Angela](Angela_Martin "wikilink")\'s pleasure. Pam ends up winning over
Meredith\'s son with the paper shredder. [Jim](Jim_Halpert "wikilink")
leaves the pizza party early to go on a date, to Pam\'s chagrin. Michael
and Dwight end the party with their duet of [Crosby, Stills, Nash &
Young](Wikipedia:Crosby,_Stills,_Nash_&_Young "wikilink")\'s \"Teach
Your Children\".

Trivia
------

-   Alec Zbornak, who plays the blonde kid interviewed by Edward R.
    Meow, is the son of producer [Kent
    Zbornak](Kent_Zbornak "wikilink").[1](http://www.tvrage.com/The_Office/episodes/321344/02x18)
-   Two of Greg Daniels\' kids, Haley and Spencer, guest star in this
    episode as Kevin\'s step-daughter and Meredith\'s son, respectively.
-   At the end of the episode when Michael and Dwight are singing, Jim
    comments why would Michael own a guitar if he doesn\'t play it. In
    the episode Email Surveillance, Jim has a guitar sitting in his room
    and he does not play. Further confirmed in \"Local Ad\" when Pam
    inquires about Jim\'s Second Life avatar having a guitar and he
    admits he does not play.
-   During his talking head interview Kevin says to the camera that he
    doesn\'t want Abby to look on his computer implying he may have
    explicit content.
-   Meredith\'s reason for bringing Jake into work is that he was
    suspended from school however it is never explained why he was
    suspended.
-   Michael wears a suit when he was on the TV show as a child. This is
    one of the many references in the show to Michael dressing formally
    as a child/teenager.
-   Toby has his daughter with him, but in other episodes it is
    suggested that Toby only has custody of Sasha on weekends, like most
    single fathers do.
-   When Michael takes the kids over to Creeds desk, you can see Toby
    watching them through the kitchen.

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   The Party Planning Committee squabbles over what food to serve.
-   In a talking head interview, Jim suspects Toby\'s daughter Sasha may
    not think he\'s cool because she\'s seen him play with dolls.
-   Dwight and Meredith\'s son have an argument over whether Optimus
    Prime will have flames in the upcoming Transformers movie. It turned
    out that Optimus was given flames, thus making Dwight correct.
-   In a talking head interview, Stanley complains that his daughter is
    spoiled.
-   Angela won\'t let Kevin pretend to be the head accountant just for
    the day.
-   Roy and Darryl joke around with Jake, but when Darryl touches the
    boy, Michael angrily intervenes. In a talking head interview,
    Michael admits that he didn\'t do background checks on the warehouse
    crew.
-   Jake asks for and gets a quarter from Michael for the vending
    machine. He then extracts a dollar. And then asks, \"Will you marry
    my mom?\"
-   Oscar finds Melissa using his computer. She refuses to give it up.
-   Abby playfully chases Sasha around the office.
-   Dwight discovers an eraser in his coffee, put there by Jake.
-   Abby notices Pam\'s drawing and notes that she wants to be an artist
    when she grows up. Pam tells her she shares the same dream. Abby
    responds, \"You *are* grown up.\"
-   In the break room, Kelly complains to Pam that \"Stanley\'s daughter
    is such a slut.\"
-   Ryan finds Sasha playing under his desk.
-   Jim gives Abby an official certificate of appreciation. To satisfy
    Dwight, Jim shows him the certification number. In a talking head
    interview, Jim admits that he just made up the numbers, \"but you do
    what you have to do.\"

Cultural references
-------------------

-   Michael\'s discussion about how nobody ever rebels against the fun
    uncle is a reference to Hamlet.
-   *Take Our Daughters to Work* (now *Take Our Daughters and Sons to
    Work*) is an annual program that encourages employees to bring a
    pre-teen to work to expose them to career opportunities.
-   *Pam. Ms. Beesly if you\'re nasty* is a modification of *Janet. Ms.
    Jackson if you\'re nasty*, a line from the Janet Jackson song
    *Nasty*.
-   *R* and *G* are movie ratings in the United States. G-rated movies
    are suitable for all ages and typically target young children.
    R-rated movies require that patrons under age 17 be accompanied by
    an adult; it is the most restrictive movie rating in common use.
-   *Eddie Murphy* is a comedian and actor. His stand-up performance in
    *Raw* was notable for its prolific use of profanity. His 2003 movie
    *Daddy Day Care* was decidedly more family-friendly.
-   The super-hero *Superman* lives in the city Metropolis. As Jim and
    Dwight correctly note, *Gotham City* is the home of *Batman*.
    *Aquaman*, as his name suggests, lives in the sea.
-   Students in *eighth grade* are approximately 13 or 14 years old.
-   A student is *suspended* from school as a form of punishment. When
    on suspension, one of the most severe punishments available in the
    United States, the student cannot attend regular classes.
-   *From the Mixed-Up Files of Mrs. Basil E. Frankweiler* is an
    award-winning children\'s book. In it, the protagonist hides in the
    Metropolitan Museum of Art.
-   Michael\'s announcement \"*Next stop: Cu\...\...camonga*\" recalls a
    recurring joke from the 1940s radio show \"Jack Benny Program\",
    wherein a train announcer calls off a list of stops and takes a long
    pause between the first and second syllables of the name Cucamonga.
-   Jake calls his mother by her first name. As Dwight notes, this is
    considered disrespectful in United States culture. Even adults
    address their parents by title and not by name.
-   *Struwwelpeter* is indeed a collection of cautionary tales for
    children.
-   *Dane Cook* is an American stand-up comedian and actor.
-   *[Fundle Bundle](Fundle_Bundle "wikilink")* with *Miss Trudy* is a
    made-up children\'s show based on the real-life show \"Hatchy
    Milatchy\" and its host \"Miss Judy\".
-   *Brunetti\'s Pizza* is a real pizza restaurant in Scranton.
-   The name of the puppet *Edward R. Meow* is a pun on legendary
    newscaster Edward R. Murrow.

Quotes
------

:   see *[Take Your Daughter to Work Day
    Quotes](Take_Your_Daughter_to_Work_Day_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting cast

-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring cast

-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")

### Guest cast

-   [Jazz Raycole](Jazz_Raycole "wikilink") as [Melissa
    Hudson](Dunder_Mifflin_Family_Members_and_Loved_Ones#Melissa_Hudson "wikilink")
-   [Delaney Ruth Farrellas](Delaney_Ruth_Farrell "wikilink") [Sasha
    Flenderson](Dunder_Mifflin_Family_Members_and_Loved_Ones#Sasha_Flenderson "wikilink")
-   [Spencer Daniels](Spencer_Daniels "wikilink") as [Jake
    Palmer](Dunder_Mifflin_Family_Members_and_Loved_Ones#Jake_Palmer "wikilink")
-   [Haley Daniels](Haley_Daniels "wikilink") as [Abby](Abby "wikilink")
-   [Sue Nelson](Sue_Nelsonmandela "wikilink") as [Ms.
    Trudy](Ms._Trudy "wikilink")
-   [Kevin Carlson](Kevin_Carlson "wikilink") as [Edward R.
    Meow](Edward_R._Meow "wikilink") (voice)
-   [Damani Roberts](Damani_Roberts "wikilink") as [Chet
    Montgomery](Chet_Montgomery "wikilink")
-   [Jake Kalendar](Jake_Kalendar "wikilink") as Young Michael Scott
-   Alec Zbornak as Blonde Kid

\
