# Michael\'s Birthday

**\"Michael\'s Birthday\"** is the nineteenth episode of the second
season of \'\'[The Office](The_Office_(US) "wikilink") \'\'and the 25th
overall. It was written by [Lee Eisenberg](Lee_Eisenberg "wikilink") and
[Gene Stupnitsky](Gene_Stupnitsky "wikilink") and directed by [Ken
Whittingham](Ken_Whittingham "wikilink"). It first aired on March 30,
2006. It was viewed by 7.8 million people.

Synopsis
--------

[Michael](Michael_Scott "wikilink") is excited to be celebrating his
birthday and tries to get the employees excited with him. Unfortunately
for Michael, [Dwight](Dwight_Schrute "wikilink") seems to be the only
one joining in, as the rest of the employees are more concerned with
[Kevin](Kevin_Malone "wikilink"), who is awaiting his results from his
[skin cancer](Wikipedia:melanoma "wikilink") screening.
[Pam](Pam_Beesly "wikilink") and [Jim](Jim_Halpert "wikilink") meanwhile
decide to sneak out and go to the store to shop for gifts for Kevin.

When Michael finds out about Kevin\'s predicament, he gives Kevin his
condolences, but subtly remains bitter that Kevin is ruining his
birthday fun. Dwight and [Angela](Angela_Kinsey "wikilink") are less
subtle than they think they are being when discussing their secret
relationship within earshot of [Ryan](Ryan_Howard "wikilink"). After
goofing around at the store, Jim and Pam return to the office. To \"make
Kevin feel better\", Michael takes the employees out ice skating. At the
rink, he also runs into his real estate agent
[Carol](Carol_Stills "wikilink") with her children and he entertains
them, which puts a smile on Carol\'s face. Kevin gets the call about his
results, which turn up negative, to everyone\'s relief except for
Michael\'s who believes that negative meant he did have cancer. He later
explains that \"Apparently, in the medicine community, negative means
good, which makes absolutely no sense. In the real world community,
ye-that would be\... chaos.\" The gifts are passed out to Kevin and
Michael and Pam expresses her feelings on the \"great day\".

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   In a talking head interview, Dwight explains his duties on
    Michael\'s birthday.
-   In a talking head interview, Michael explains how good bosses share
    their birthdays with their employees.
-   Michael tells Ryan that he does not need to get him a present but is
    clearly upset when he does not.
-   In a talking head interview, Michael tells a story of his worst
    birthday.
-   In a talking head interview, Dwight admires the concept of
    someone\'s skin turning on them.
-   Michael makes Ryan read to the camera information about
    [melanoma](melanoma "wikilink") and belittles every fact.
-   In a talking head interview, Angela says that Kevin should be
    concerned about his fate in the afterlife. \"Gluttony?\"

Trivia
------

-   Jim and Pam shop at a [Rite
    Aid](Miscellaneous_Scranton_locations#Rite_Aid "wikilink") pharmacy.
    Based on store size and the Thrifty Ice Cream sign seen in the
    background, it is a West Coast location. East Coast stores are
    smaller and do not sell Thrifty Ice Cream (west coast locations sell
    the ice cream because it was inherited from a chain Rite Aid
    acquired in 1996).
-   Kevin can be seen with a small bandage on the right side of his neck
    throughout the episode. This is likely there to show that he has
    recently had a biopsy of a mole, a common test for skin cancer.
-   According to Jenna Fischer, the writers had been looking for a
    reason to use Carell\'s ice-skating skills in an episode. Carell
    used to play hockey; Fischer learned to skate in preparation for the
    movie Blades of Glory but decided that her character cannot.
-   Since Michael has indirectly said before that his birthday is March
    15, this episode takes place on March 15, 2006.
-   Ice skating scenes were filmed at Pickwick Gardens in [Burbank,
    California](Wikipedia:Burbank,_California "wikilink").
-   Michael says that on his sixteenth birthday, he was supposed to go
    on a date with a girl named Julie, but she got it mixed up with
    another Michael in the class, and he heard all about it in school
    the next day. March 15, 1980, Michael\'s sixteenth birthday, was a
    Saturday, so the next day would be Sunday, Michael not having
    school.

References to previous episodes
-------------------------------

-   Michael creates a fake *Livestrong* bracelet. He wore a real one in
    the episode \"[Basketball](Basketball "wikilink")\".
-   This episode reintroduces the character of [Carol
    Stills](Carol_Stills "wikilink"), first seen in the episode
    \"[Office Olympics](Office_Olympics "wikilink")\".
-   *Nightswept* is Michael\'s cologne, as established in the episode
    \"[Hot Girl](Hot_Girl "wikilink")\".
-   Oscar executes a spin at the ice rink. In the episode \"[The
    Secret](The_Secret "wikilink")\", Oscar pretended to be sick and
    spent the day with his boyfriend [Gil](Gil "wikilink"). Among the
    things they did was go ice skating.

Cultural references
-------------------

-   *Calling cards* are a form of pre-paid long distance telephone
    service. They became wildly popular in the 1990s, but the growth of
    mobile telephones with free long distance led to a decline in both
    popularity and social cachet. (Ryan remarks, \"Who uses calling
    cards any more?\") By the mid-2000s, calling cards were used
    primarily by lower-income people with poor credit.
-   Michael lost money in an attempt to help *the son of the deposed
    king of Nigeria*. Michael was a victim of the *Nigerian Letter*
    scam, one of the most common forms of [advance fee
    fraud](Wikipedia:Advance_fee_fraud "wikilink"). Incidentally, the
    last King of Nigeria was the British King, George VI who died in
    1952 whilst Nigeria was under British colonial rule. Nigeria,
    following independence from the United Kingdom in 1960, declared
    itself a republic in 1963.
-   *Eva Longoria* is an actress best known for her role on the show
    *Desperate Housewives*. *Teri Hatcher* is another actress from that
    program.
-   *eBay* is a large Internet auction site which allows anyone to put
    an item up for sale.
-   *24/7* is shorthand for *24 hours a day, 7 days a week*. Michael
    somewhat erroneously applies it to the efforts of the Party Planning
    Committee, who started working on his party only the previous day
    (and presumably did not stay up all night).
-   *Grey\'s Anatomy* is a television medical drama.
-   *Princess Diana* (formally, Diana, Princess of Wales) was the first
    wife of Charles, Prince of Wales, heir apparent to the British
    throne. She died in an automobile accident, and her funeral was
    watched by an estimated 2.5 billion people.
-   *The Longest Time* is a song performed by *Billy Joel*. Dwight
    mis-identifies the song as *For the Longest Time* and the singer as
    *William Joel*.
-   Michael mis-identifies actor *James Dean* in *Rebel Without a Cause*
    as actor *Luke Perry*.
-   *M&Ms* is a type of candy, *American Pie 2* is a 2001 teen sex
    comedy, *Cup Noodles* (misstated as *Cup of Noodles*) is an instant
    ramen snack, and *69* is the nickname for a sexual position.
-   Pam\'s announcement *Luke, this is your father* is a play on the
    line *No, I am your father* from the movie *The Empire Strikes
    Back*.
-   Michael creates a yellow wristband from Post-it notes, essentially a
    counterfeit *Livestrong* wristband, a fundraising item for the Lance
    Armstrong Foundation. (Later in the episode, he shows off the
    wristband and says, \"Live strong.\")
-   Michael\'s *get our skate on* is a snowclone of *get one\'s X on*,
    originally *get one\'s groove on* or *get one\'s freak on*, meaning
    to dance in a sexually provocative manner. The original sense has
    been largely forgotten. The phrase *get one\'s X on* typically means
    simply *start doing X*.
-   The *NHL* is the National Hockey League, a North American
    professional hockey league.
-   Michael refers to Pam\'s breasts as *ticking time-bags*, a
    combination of the phrase *ticking time-bomb* (a disaster poised to
    occur without warning) and *fun bags* (somewhat crude slang for
    female breasts).
-   Dwight\'s birthday gift for Michael is a jersey for the
    *Wilkes-Barre/Scranton Penguins*, a professional hockey team in the
    AHL, a subsidiary league of the NHL.
-   *Nightswept* is a fake brand of cologne.
-   Michael\'s ringtone is *My Humps* by the group *Black Eyed Peas*.

Quotes
------

:   see *[Michael\'s Birthday
    Quotes](Michael's_Birthday_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring cast

-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Nancy Walls](Nancy_Walls "wikilink") as [Carol
    Stills](Carol_Stills "wikilink")

### Guest cast

-   Susan Foley as Delivery Woman
-   Justin Meloni as Delivery Man
-   Susan Van Horn as Clerk
-   Eric La Barr as Cashier (uncredited)

\
