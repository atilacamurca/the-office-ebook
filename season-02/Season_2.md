# Season 2

![](Cast2.jpg "fig:Cast2.jpg"){width="400"}

**Season 2** of *[The
Office](The_Office_(US) "wikilink")* began airing on September 20, 2005
on [NBC](NBC "wikilink"). It is based on the BBC series created by
[Ricky Gervais](Ricky_Gervais "wikilink") and [Stephen
Merchant](Stephen_Merchant "wikilink"), and developed for American
television by [Greg Daniels](Greg_Daniels "wikilink"). It ended on May
11, 2006 and consisted of 22 episodes.

The Season 2 DVD was released in North America on September 12, 2006.

The previous season was [Season 1](Season_1 "wikilink") and the next
season was [Season 3](Season_3 "wikilink").

Season Overview
---------------

Season 2 marks the beginning of a substantial break in style from the
original BBC series, developing new storylines and a more American sense
of humor. Relationships become a focus in Season 2; the most significant
is between [Jim](Jim_Halpert "wikilink") and
[Pam](Pam_Halpert "wikilink"), but relationships between
[Michael](Michael_Scott "wikilink") and [Jan](Jan_Levinson "wikilink"),
[Ryan](Ryan_Howard "wikilink") and [Kelly](Kelly_Kapoor "wikilink"), and
also [Dwight](Dwight "wikilink") and [Angela](Angela_Martin "wikilink")
begin to develop. Rumors of downsizing continue to haunt the [Scranton
branch](Dunder_Mifflin_Scranton "wikilink"), resulting in the firing of
one employee. As the season progresses, the tension in Jim and Pam\'s
relationship continues to build, leading to a shocking finale.

Episodes
--------

  Picture                                                                                                                                                                                                                                      Episode Title                                                                 Season Episode Number   Production Code   Original Airdate
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------- ----------------------- ----------------- --------------------

  ![](thedundies.jpg "thedundies.jpg"){width="150"}                                                                                                                                                                                            [The Dundies](The_Dundies "wikilink")                                         1                       02004             September 20, 2005
  The embarrassing, annual Dundie Awards take place at a Chili\'s restaurant.                                                                                                                                                                                                                                                                                          

  ![](sexualharassment.jpg "sexualharassment.jpg"){width="150"}                                                                                                                                                                                [Sexual Harassment](Sexual_Harassment "wikilink")                             2                       02002             September 27, 2005
  The arrival of Michael\'s best friend, Todd Packer, results in even more inappropriate jokes than usual.                                                                                                                                                                                                                                                             

  ![](officeolympics.jpg "officeolympics.jpg"){width="150"}                                                                                                                                                                                    [Office Olympics](Office_Olympics "wikilink")                                 3                       02003             October 4, 2005
  When Michael is out purchasing real estate, Jim sets up the office Olympics.                                                                                                                                                                                                                                                                                         

  ![](thefire.jpg "thefire.jpg"){width="150"}                                                                                                                                                                                                  [The Fire](The_Fire "wikilink")                                               4                       02001             October 11, 2005
  When a fire forces everyone to remain outside for much of the day, Jim tries to entertain his co-workers with some fun games. Meanwhile, Michael spends most of his time trying to impress Ryan, which upsets Dwight.                                                                                                                                                

  ![](halloween.jpg "halloween.jpg"){width="150"}                                                                                                                                                                                              [Halloween](Halloween "wikilink")                                             5                       02006             October 18, 2005
  Everybody is dressed up for Halloween, but unfortunately Michael has to fire somebody.                                                                                                                                                                                                                                                                               

  ![](TheFight.jpg "TheFight.jpg"){width="150"}                                                                                                                                                                                                [The Fight](The_Fight "wikilink")                                             6                       02007             November 1, 2005
  Dwight\'s constant bragging about his martial arts abilities leads to a lunch-time match between Michael and Dwight at Dwight\'s dojo.                                                                                                                                                                                                                               

  ![](theclient.jpg "theclient.jpg"){width="150"}                                                                                                                                                                                              [The Client](The_Client "wikilink")                                           7                       02005             November 8, 2005
  While Michael and Jan are closing a deal with a client, Jim and Pam lead the office in a reading of Michael\'s screenplay.                                                                                                                                                                                                                                           

  ![](performancereview.jpg "performancereview.jpg"){width="150"}                                                                                                                                                                              [Performance Review](Performance_Review "wikilink")                           8                       02009             November 15, 2005
  Michael meets with each of the employees to discuss their work performance, but tends to spend more time thinking about and talking about his kiss with Jan. Jim and Pam work on convincing Dwight that it is Friday rather than Thursday.                                                                                                                           

  ![](emailsurveillance.jpg "emailsurveillance.jpg"){width="150"}                                                                                                                                                                              [Email Surveillance](Email_Surveillance "wikilink")                           9                       02008             November 22, 2005
  Pam suspects Dwight and Angela are dating. Michael crashes Jim\'s karaoke party.                                                                                                                                                                                                                                                                                     

  ![](christmasparty.jpg "christmasparty.jpg"){width="150"}                                                                                                                                                                                    [Christmas Party](Christmas_Party "wikilink")                                 10                      02010             December 6, 2005
  Michael overspends on his gift for \"Secret Santa\" and is disappointed by the gift he receives. As a result, he forces everyone into an impromptu game of \"Yankee Swap.\"                                                                                                                                                                                          

  ![](boozecruise.jpg "boozecruise.jpg"){width="150"}                                                                                                                                                                                          [Booze Cruise](Booze_Cruise "wikilink")                                       11                      02011             January 5, 2006
  The Dunder-Mifflin crew goes on a \"motivational\" cruise to Lake Wallenpaupack. A drunken Roy is inspired to announce a date for his wedding with Pam. Jim is crushed and confesses to Michael his feelings for Pam.                                                                                                                                                

  ![](theinjury.jpg "theinjury.jpg"){width="150"}                                                                                                                                                                                              [The Injury](The_Injury "wikilink")                                           12                      02013             January 12, 2006
  When Dwight hears about Michael\'s George Foreman Grill-related foot injury, he rushes to the rescue, but crashes his car and suffers a concussion. Michael is upset by his staff\'s lack of compassion toward his own \"disability.\"                                                                                                                               

  ![](thesecret.jpg "thesecret.jpg"){width="150"}                                                                                                                                                                                              [The Secret](The_Secret "wikilink")                                           13                      02014             January 19, 2006
  Michael has trouble keeping Jim\'s secret about his feelings for Pam. When the cat gets out of the bag, Jim has to make excuses. Meanwhile, Dwight investigates Oscar\'s absence from work.                                                                                                                                                                          

  ![](thecarpet.jpg "thecarpet.jpg"){width="150"}                                                                                                                                                                                              [The Carpet](The_Carpet "wikilink")                                           14                      02012             January 26, 2006
  When feces is discovered on the floor of Michael\'s office, he takes over Jim\'s desk, moving Jim to the other side of the office, and therefore, distant from Pam.                                                                                                                                                                                                  

  ![](boysandgirls.jpg "boysandgirls.jpg"){width="150"}                                                                                                                                                                                        [Boys and Girls](Boys_and_Girls "wikilink")                                   15                      02015             February 2, 2006
  To counter the corporate-appointed \"women in the workplace\" seminar being conducted by Jan, Michael assembles all the men in the warehouse for a \"men in the workplace\" seminar.                                                                                                                                                                                 

  ![](valentinesday.jpg "valentinesday.jpg"){width="150"}                                                                                                                                                                                      [Valentine\'s Day](Valentine's_Day "wikilink")                                16                      02017             February 9, 2006
  Michael is in a corporate meeting in New York with Jan. Pam is irritated that Roy didn\'t buy her anything special for Valentine\'s Day. Dwight and Angela exchange V-Day gifts.                                                                                                                                                                                     

  ![](dwightsspeech.jpg "dwightsspeech.jpg"){width="150"}                                                                                                                                                                                      [Dwight\'s Speech](Dwight's_Speech "wikilink")                                17                      02016             March 2, 2006
  Dwight is being awarded Dunder Mifflin Salesman of the Year and is coaxed by Jim into basing it on a speech by Mussolini. Jim contemplates going on a vacation.                                                                                                                                                                                                      

  ![](takeyourdaughter.jpg "takeyourdaughter.jpg"){width="150"}                                                                                                                                                                                [Take Your Daughter to Work Day](Take_Your_Daughter_to_Work_Day "wikilink")   18                      02018             March 16, 2006
  When the employees bring their children to work, Michael shows he can be good-natured, but also reveals failed childhood dreams.                                                                                                                                                                                                                                     

  ![](michaelsbirthday.jpg "michaelsbirthday.jpg"){width="150"}                                                                                                                                                                                [Michael\'s Birthday](Michael's_Birthday "wikilink")                          19                      02019             March 30, 2006
  Michael is aggravated that his birthday isn\'t getting more attention than Kevin\'s skin cancer test.                                                                                                                                                                                                                                                                

  ![](drugtesting.jpg "drugtesting.jpg"){width="150"}                                                                                                                                                                                          [Drug Testing](Drug_Testing "wikilink")                                       20                      02022             April 27, 2006
  Dwight starts an investigation when he discovers half of a joint in the parking lot. Following a jinx, Jim is unable to speak until he buys Pam a Coke, but the machine is sold out.                                                                                                                                                                                 

  ![](conflictresolution.jpg "conflictresolution.jpg"){width="150"}                                                                                                                                                                            [Conflict Resolution](Conflict_Resolution "wikilink")                         21                      02020             May 4, 2006
  Michael takes over the conflict resolution responsibilities from Toby and ends up announcing all of the complaints aloud to the office, which only stirs things up more. Jim contemplates transferring to another branch.                                                                                                                                            

  ![](casinonight.jpg "casinonight.jpg"){width="150"}                                                                                                                                                                                          [Casino Night](Casino_Night "wikilink")                                       22                      02021             May 11, 2006
  Michael converts the warehouse into a casino for a charity casino night, but ends up with two dates - Jan and his realtor, Carol. Jim has something to tell Pam.                                                                                                                                                                                                     
