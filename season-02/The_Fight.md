# The Fight

**\"The Fight\"** is the sixth episode of the second season of \'\'[The
Office](The_Office_(US) "wikilink") \'\'and the 12th overall. It was
written by [Lee Eisenberg](Lee_Eisenberg "wikilink") and [Gene
Stupnitsky](Gene_Stupnitsky "wikilink"), and directed by [Ken
Kwapis](Ken_Kwapis "wikilink"). It first aired on November 1, 2005. It
was viewed by 7.9 million people.

Synopsis
--------

Jim convinces Michael to fight Dwight, now a karate sempai. When Michael
is injured by Dwight, he becomes embarrassed and initiates a rematch at
Dwight\'s dojo. When Michael \"wins\" the rematch, he decides to finally
promote a humiliated Dwight to Assistant Regional Manager.

Trivia- Caution, here be spoilers
---------------------------------

-   Kevin comes out of the bathroom stall holding a scented candle. In a
    previous episode, \"[The Dundies](The_Dundies "wikilink")\", Kevin
    won a Dundie award in honor of the horrid smell he leaves behind
    after using the bathroom.
-   Dwight tells Kevin to wash his hands in the cold open, however later
    in the series when he waters down the soap and is criticized for it,
    he says, \"Why do you even need to wash your hands? Are you really
    that bad at going to the bathroom?\" And when Pam tries to place
    hand sanatizing stations all over the office, Dwight advocates for
    placing hand \"de-sanitizing\" stations around instead, insisting
    that the human immune system shouldn\'t be coddled.
-   In a talking head interview, Michael talks about friends who come to
    work late \"having dentist appointments that aren\'t dentist
    appointments.\" This hypothetical situation comes to pass in the
    future episode \"[The Coup](The_Coup "wikilink")\".
-   Dwight botches the phrase \"tit for tat\" by saying \"tit for tit\"
    instead. He made the same mistake in a previous episode,
    \"[Diversity Day](Diversity_Day "wikilink")\", where Jim corrects
    him.
-   According to the logos on Dwight\'s Dojo, the school is not supposed
    to be teaching Karate, but Tae Kwon Do, due to the [World Taekwondo
    Federation](Wikipedia:World_Taekwondo_Federation "wikilink") logos
    (See picture). Additionally, the sparring gear that the training
    dummy adorned in the background is actually Tae Kwon Do sparring
    gear, not Karate, as the user usually isn\'t supposed to wear any
    gear, in [Kumite](Wikipedia:Kumite "wikilink"), or \"full body
    contact\".
-   It is likely that this episode takes place on September 30, 2005.
    Since Jim mentions downsizing, it must take place before
    \"Halloween.\" September 30, 2005 was at the end of the month, a
    Friday, and the end of the quarter, meeting all the requirements
    given by Pam.
-   The actor who plays Sensei Ira, Lance Krall, is an actual martial
    art expert; he holds a third degree black belt in Taekwondo, and in
    1992, he was ranked the sixth best in the nation for his weight
    class.
-   It is also interesting to note that in the episode\'s closing scene,
    not only is the staff laughing at Michael beating up Dwight, but so
    is Dwight\'s Sensei.
-   Dwight\'s use of \"sempai\" is (purposely) incorrect, it is an
    honorific to indicate someone of a higher rank. When talking to
    someone who is a higher rank than you (in this case, sensei), you do
    not call yourself \"sempai\".
-   The address that Jim provides for his emergency contact who is
    presumably his mother is different from the address of when he
    purchases his parent\'s home.

Deleted scenes
--------------

Notable cut scenes include:

-   At the dojo, Dwight\'s attempt at a kick fails miserably. Dwight is
    disciplined by sensei Ira when his pager goes off during class.
-   In a talking head interview, Dwight explains that he has not bonded
    with his classmates. \"I\'m not there to make friends. I\'m there to
    attack people.\"
-   Alternate take of the scene in which Jim asks Dwight whom he could
    take on in a fight.
-   Angela accuses Oscar of eating her pudding snacks. Oscar denies it,
    as does Kevin. (Kevin snickers to the camera.)
-   Kelly invites Meredith to Happy Hour. Meredith is unsure. \"I\'m
    still recovering from last night.. but maybe.\"
-   Scenes at the dojo of Michael and Dwight putting on equipment,
    trying to psych each other out, and ultimately fighting like idiots.
-   Ira introduces his young senpai Alyssa. In a talking head interview
    at the dojo, Dwight explains that Alyssa may technically be the
    senpai, but \"the only reason she got into regionals was because her
    competition was a bunch of thirteen-year-old girls.\"
-   Michael brags about the fight to Stanley, who is more interested in
    Michael signing the purchase orders. Pam pointedly puts the forms on
    Michael\'s chair.
-   In a talking head interview, Dwight explains that Michael has no
    honor and would be an outcast in Japan. \"Well, that\'s not totally
    true, because Asians worship chest hair.\"
-   Michael delivers Pam \"part one\" of the forms, but it is just the
    emergency contact information.

Cultural references
-------------------

-   Jim plays the hotter/colder game with Dwight. In this children\'s
    game, warmer temperatures indicate that the player is closer to the
    target.
-   *Arigato gozaimashita* and *Hai*, loosely translated, mean \"Thank
    you\" and \"Yes\", respectively, in Japanese.
-   *The Perfect Storm* is a book and subsequent movie about a powerful
    storm that struck North America in 1991. It subsequently became used
    to refer to the disastrous effect of multiple events which coincide
    by chance.
-   Michael sings *I don\'t want to work. I want to bang on this mug all
    day* to the tune of *[Bang the Drum All
    Day](Wikipedia:Bang_the_Drum_All_Day "wikilink")*.
-   *Chillax* is a slang term combining *chill* (meaning \"to relax\")
    and *relax*.
-   *Pam-M-S\'ing* is a pun on *PMS* (known as *PMT* in British
    English).
-   *Catch-22* is the title of a novel by Joseph Heller. The term refers
    to a no-win situation, although it is clear that Michael has no idea
    what it means.
-   *Michael Jackson\'s Wonderland* is Michael\'s mistaken reference to
    Jackson\'s property the *Neverland Ranch*.
-   *Mike Tyson* is a professional boxer with a high-pitched voice.
-   A *Jet* is a member of one of the rival street gangs in the musical
    *West Side Story*. \"When you\'re a Jet, you\'re a Jet all the way\"
    is the opening line to *The Jet Song*. Jim snaps his fingers in the
    same way as the gang members.
-   *[Bedtime for Bonzo](Wikipedia:Bedtime_for_Bonzo "wikilink")* is a
    movie notable primarily for its star, future president [Ronald
    Reagan](Wikipedia:Ronald_Reagan "wikilink"). Michael\'s use is
    nonsensical.
-   *You **are** the weakest link* is the catch phrase from the game
    show *Weakest Link*.
-   *Queer Eye* (originally *Queer Eye for the Straight Guy*) is a
    make-over reality television program featuring five openly gay men
    dispensing fashion and style advise.
-   *Armageddon* is a 1998 science fiction action movie starring Bruce
    Willis.
-   *Tit for tit* is an incorrect version of *tit for tat*, an
    expression meaning that one can respond to an offense in the same
    manner as the original offence. Dwight first uses this incorrectly
    in the [Season 1](Season_1 "wikilink") episode, [Diversity
    Day](Diversity_Day "wikilink").
-   Michael\'s *Two punches: me punching you and you hitting the floor*
    is a botched quote from the classic 1985 movie [*The Breakfast
    Club*](http://www.imdb.com/title/tt0088847/quotes). Actual quote:
    \"Two hits; me hitting you, you hitting the floor.\"
-   Jim\'s \"Bring it\" is a challenge (short for \"bring it on\", a
    phrase used by Katy in [Hot Girl](Hot_Girl "wikilink")).
-   Jim mocks Pam by doubting her status as an *Ultimate Fighter*. *The
    Ultimate Fighter* is a reality television series in the form of a
    martial arts competition.
-   Kevin\'s advice to *sweep the leg* comes from the movie *[The Karate
    Kid](Wikipedia:The_Karate_Kid "wikilink")*.
-   Michael\'s \"*You talkin\' to me?*\" is a line performed by Robert
    De Niro in the movie *Taxi Driver*. Michael misattributes it to Al
    Pacino in *Raging Bull*.
-   *Phone tag* is a term applied to the phenomenon of two people trying
    to reach each other by phone but always missing each other.
-   Although Dwight\'s martial arts style is Japanese Karate, or
    \"[Gōjū-ryū](Wikipedia:Gōjū-ryū "wikilink")\" Karate, the dojo is
    obviously not Japanese, as the logos on the windows are actually
    logos belonging to the [World Taekwondo
    Federation](Wikipedia:World_Taekwondo_Federation "wikilink"), which
    is in charge of hosting the Tae Kwon Do games of the Olympic games;
    Taekwondo itself is a Korean martial art.
-   Adding to the above, the scoring mechanism used in the episode (one
    point for a clean hit to the body) is more similar to the scoring
    system for \"Shotokan\" Karate; Goju-ryu tends to reward points for
    knocking the opponent on the ground, while the Taekwondo\'s point
    system relies on how many hits the contestants can land on their
    opponents in two minutes.
-   *Fudge packer* is a rude term for a homosexual male.
-   *[The Karate Kid](Wikipedia:The_Karate_Kid "wikilink")* is a 1984
    movie about a boy who becomes an accomplished karate fighter. The
    *Hilary Swank version* is *[The Next Karate
    Kid](Wikipedia:The_Next_Karate_Kid "wikilink")*, the fourth movie in
    the *Karate Kid* series, starring Swank as the first female student
    of sensei Miyagi.
-   Michael suggests, \"Let\'s *gangbang* this thing,\" unaware that
    *gangbang* refers to a single person having sex with multiple
    partners in rapid succession.
-   Michael poses a question at the end of the episode, about whether he
    would rather be feared or loved. This comes from Machiavelli\'s
    \"[The Prince](http://en.wikipedia.org/wiki/The_Prince) \", which
    poses the same question of leaders.

Quotes
------

:   see *[The Fight Quotes](The_Fight_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring Cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Guest Cast

-   [Lance Krall](Lance_Krall "wikilink") as [Sensei
    Ira](Sensei_Ira "wikilink")
