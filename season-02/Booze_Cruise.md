# Booze Cruise

**\"Booze Cruise\"** is the eleventh episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 17th overall.
It was written by [Greg Daniels](Greg_Daniels "wikilink") and directed
by [Ken Kwapis](Ken_Kwapis "wikilink"). It first aired on January 5,
2006. It was viewed by 8.6 million people.

Synopsis
--------

The episode opens with Dwight, who is incensed that Jim has put all of
his possessions in the [vending
machine](Puts_Dwight's_Items_In_Vending_machine "wikilink"). Pam puts in
some money and buys Dwight\'s pencil cup, to his great dismay.  Michael
sends out a vague memo telling the staff to pack a number of random
items, including a swimsuit, a toothbrush, rubber-soled shoes and a
ski-mask, in preparation for the company\'s first quarter leadership
training trip. The surprise trip turns out to be a \"booze cruise\" on
Lake Wallenpaupack, and the staff is not happy\--it\'s January, and much
too cold for a cruise. Michael tells the staff: \"Leader\... ship. The
word \"ship\" is hidden inside the word \"leadership,\" as its
derivation. So if this office is, in fact, a ship, as its leader, I am
the captain!\" When they arrive on the boat, however, Michael
immediately feels threatened by the ship\'s captain, [Captain
Jack](Captain_Jack "wikilink"), who declares that *he* will be the
ship\'s \"party captain\" for the evening. A hilarious battle for top
authority on the ship ensues, with Michael echoing everything Captain
Jack says.

[Jim](Jim_Halpert "wikilink") brings [Katy](Katy "wikilink"), his new
girlfriend, (played by [Amy Adams](Amy_Adams "wikilink")) as his date,
and [Pam](Pam_Beesly "wikilink") brings[ Roy](Roy_Anderson "wikilink")
along.  The four soon start talking, and it becomes clear that Katy and
Roy enjoy each other more than they do their respective partners. Pam
and Jim are left alone together, and the romantic tension is palpable.
As the party inside begins, Michael tries unsuccessfully to motivate his
staff and is disappointed when Captain Jack does a better job. The
awkwardness Michael creates is magnified by the fact Dunder Mifflin\'s
group is not the only group on the boat. When
[Dwight](Dwight_Schrute "wikilink") volunteers to hold the limbo stick,
Captain Jack sends him outside to \"steer the ship.\"
[Angela](Angela_Martin "wikilink") asks Dwight to come inside and talk
to her, but he refuses, saying \"I can\'t. Do you want us to run
aground, woman?!\" (The wheel Dwight is so seriously manning turns out
to be fake). When Captain Jack leaves the festivities with
[Meredith](Meredith_Palmer "wikilink"), Michael claims wildly that the
ship is sinking and will be at the bottom of the lake in mere minutes.
At first no one believes him, but soon people start to panic and one
passenger jumps overboard. Captain Jack returns and, after discovering
what Michael has done, handcuffs him to the railing outside to prevent
him from causing any more trouble. Roy, motivated by Captain Jack\'s
stories about commitment (and slightly drunk), announces to Pam that
it\'s time to set a date for their wedding. He suggests June 10th, and
Pam happily accepts. After seeing this, Jim becomes depressed and soon
breaks up with Katy, who is shocked and upset. Jim then admits to
Michael that he has strong feelings for Pam. Michael more surprised than
anything tells him he usually has a radar that kind of thing. Jim looks
down and seems ready to let Pam go, Michael tells him that if he really
likes her then he shouldn\'t give up. Michael also tells Jim: \"engaged
ain\'t married,\" encouraging him never to give up on Pam.

Trivia
------

-   According to [Greg Daniels](Greg_Daniels "wikilink")\'
    [DVD](DVD "wikilink") commentary, the original script included a
    storyline depicting [Oscar](Oscar_Martinez "wikilink") as a nasty
    drunk. It was cut for time.
-   A real January booze cruise on Lake Wallenpaupack would be
    impossible. The lake is frozen over, and all boats must be removed
    from the lake by October.
-   The official name of the ship is *Lake Wallenpaupack Princess*.
-   Captain Jack claims to have been a captain of a US Navy PC-1 Cyclone
    class patrol ship during Operation Desert Storm.
-   Actor Rob Riggle (plays Captain Jack) served as an Officer in the
    Marine Corps from 1990 until 2013.

Quotes
------

**Michael:** Now, on this ship that is the office, what is a sales
department? Anyone?\
**Darryl:** How about the sales department is the sails?\
**Michael:** Yes, Darryl, the sales department makes sales. Good. Let me
just explain. I see the sales department as the furnace.\
**Phyllis:** A furnace?\
**Jim:** Yeesh, how old is this ship?\
**Pam:** How about the anchor?\
**Phyllis:** What does the furnace do?\
**Michael:** All right, let\'s not get hung up on the furnace. This
just\... it\'s the sales\... I see the sales department down there.
They\'re in the engine room, and they are shoveling coal into the
furnace, right? I mean, who saw the movie Titanic? They were very
important in the movie Titanic. Who saw it? Show of hands!\
**Jim:** I\'m not really sure what movie you\'re talking about. Are you
sure you got the title right?\
**Michael:** Titanic?\
**Pam:** I think you\'re thinking of The Hunt for Red October.\
**Michael:** No, I\'m Leo DiCaprio! Come on!

**Phyllis:** Michael, everyone in the engine room drowned.\
**Michael:** No! Thank you, spoiler alert. You saw the movie, those of
you who did. They\'re happy down there in the furnace room. And they\'re
dirty and grimy and sweaty, and they\'re singing their ethnic songs,
and\... actually, that might be warehouse.\
**Darryl:** What?\
**Michael:** The\... no, no. No, I didn\'t\... okay. Well, okay, in a
nutshell, what I\'m saying is\... leadership. We\'ll talk more about
that on the boat. Ship.

------------------------------------------------------------------------

**Captain Jack:** Not now, Mike, we\'re doing the limbo! That\'s right,
partiers, it\'s time to limbo, limbo, limbo!\
**Michael:** So, okay.\
**Dwight:** Limbo, whoo!\
**Captain Jack:** All right! I need a volunteer to come up here and hold
my stick. Who\'s it gonna be?\
**Meredith:** Me.\
**Captain Jack:** Okay\...\
**Dwight:** Me! Me, me, me.\
**Captain Jack:** Uh\... usually it\'s a woman.\
**Dwight:** I\'m stronger.

------------------------------------------------------------------------

**Michael:** Sometimes you have to take a break from being the kind of
boss that\'s always trying to teach people things. Sometimes you have to
just be the boss of dancing.

------------------------------------------------------------------------

**Kelly:** Wait, Michael?\
**Michael:** Yeah?\
**Kelly:** Why did you tell us to bring a bathing suit?\
**Michael:** To throw you off the scent.\
**Kelly:** Yeah, but I bought a bathing suit.\
**Michael:** Well, just keep the tags on and you can return it.\
**Kelly:** I took the tags off already.\
**Michael:** Well, that\'s not my fault, okay? Just.. we\'re not going
to pay for a bathing suit.

**Jim:** She\'s really funny and..she\'s warm and she\'s just\...anyway.
**Michael:** Well if you like her so much, don\'t give up.

**Jim:** She\'s engaged..

**Michael:** Pfft BFD, engaged ain\'t married.

**Jim:** Huh\...

**Michael:** Never ever, ever give up.

Deleted scenes
--------------

The [Season Two](Season_Two "wikilink") [DVD](DVD "wikilink") contains a
number of deleted scenes from this episode. Notable cut scenes include:

-   Jan and Brenda meet with Michael in his office. When Michael asks
    Brenda for a moment alone with Jan, she responds that Jan had
    specifically told her to say no if he asked that question - when he
    asks again soon after, Brenda is happy to comply before Jan tells
    her not to. After Jan leaves, Michael offers to make Brenda a fake
    ID.
-   [Toby](Toby_Flenderson "wikilink") intentionally arrives late to the
    booze cruise. [Michael](Michael_Scott "wikilink"), of course,
    refuses to have the boat go back to get him - though as the boat
    pulls away from the dock, Toby smiles and admits that stopping to
    have dinner might not\'ve been a good idea.
-   Michael tries to play guitar with the band, but fails.
    [Creed](Creed_Bratton "wikilink") offers to play and begins playing
    very well.
-   Creed explains how he was a member of the band, [The Grass
    Roots](Wikipedia:The_Grass_Roots "wikilink"), which is the real band
    the real Creed Bratton played with.
-   [Dwight](Dwight_Schrute "wikilink") states he was conflicted between
    Michael and [Captain Jack](Captain_Jack "wikilink") as he boarded
    the boat, but now \"he will follow Captain Jack to hell and back.\"
-   Michael gives [Ryan](Ryan_Howard "wikilink") useless advice on how
    to study in the current situation.
-   In separate shots Ryan and [Meredith](Meredith_Palmer "wikilink")
    throw up.
-   A drunken [Darryl](Darryl_Philbin "wikilink") orders a drink then
    says to [Angela](Angela_Martin "wikilink"), \"What you say,
    bitch?\", realizing he\'s had enough.

Amusing details- Caution, here be spoilers
------------------------------------------

-   [Kevin](Kevin_Malone "wikilink") wears the requested ski mask to the
    booze cruise. It obscures his vision so much, he bumps into
    [Michael](Michael_Scott "wikilink") as he walks up the ramp.
-   When [Katy](Katy "wikilink") sits and pouts after being dumped by
    [Jim](Jim_Halpert "wikilink"), [Kelly](Kelly_Kapoor "wikilink") is
    behind her with her head on the table, sleeping.
-   After [Roy](Roy_Anderson "wikilink") finally sets a date for his and
    [Pam](Pam_Beesly "wikilink")\'s wedding, [Captain
    Jack](Captain_Jack "wikilink") offers to marry them aboard the ship.
    Pam politely declines, saying she wants her parents to be there.
    Four years later, in \"[Niagara](Niagara "wikilink")\", she and Jim
    slip away from the wedding with their families and get married on a
    boat.

Cultural references
-------------------

-   [Michael](Michael_Scott "wikilink")\'s recitation which begins
    *Stanley Bo-Banley* is a verse from *[The Name
    Game](Wikipedia:The_Name_Game "wikilink")* adapted to
    [Stanley](Stanley_Hudson "wikilink")\'s name.
-   *[Tony Robbins](Wikipedia:Tony_Robbins "wikilink")* is an author and
    motivational speaker who conducts seminars around the country.
-   The *-ship* suffix in the word *leadership* is not part of its
    derivation as Michael claims. It is merely a noun-forming suffix
    meaning \"the qualities of\".
-   The idiom *all in the same boat* means that everyone shares the same
    problem and needs to work together to solve it.
-   The idiom *to bowl over* means *to make someone happy by
    overwhelming with surprise*. One can be bowled over by a gift, for
    example. Michael\'s use of the idiom in *to bowl over the
    competition* is somewhat misguided; one would more likely wish to
    bowl over one\'s customers. The phrase originates from the sport of
    [cricket](Wikipedia:Cricket "wikilink"), although Michael is more
    likely to have taken his employees
    [bowling](Wikipedia:Bowling "wikilink").
-   Michael fails to recognize that [Darryl](Darrly_Philbin "wikilink")
    is making a pun on *sales* and *sails*.
-   *[Titanic](Wikipedia:Titanic_(1997_film) "wikilink")* is an
    Oscar-winning blockbuster film about the ship [RMS
    Titanic](Wikipedia:RMS_Titanic "wikilink"), which sank after hitting
    an iceberg on its maiden voyage. The film is well known for
    *[Leonardo DiCaprio](Wikipedia:Leonardo_DiCaprio "wikilink")*\'s
    line \"I\'m the king of the world!\", shouted while standing on the
    prow of the ship, a scene which Michael re-creates.
-   *[The Hunt for Red
    October](Wikipedia:The_Hunt_for_Red_October_(film) "wikilink")* is a
    film (based on [a book of the same
    title](Wikipedia:The_Hunt_for_Red_October "wikilink")) about a
    Soviet submarine whose captain wishes to defect to the United
    States.
-   Michael sings \"A three hour tour\" from the theme song to
    *[Gilligan\'s Island](Wikipedia:Gilligan's_Island "wikilink")*, a
    comedy about a group of castaways stranded on a tropical island. He
    also matches up office employees with characters from the program:
    -   [Pam](Pam_Beesly "wikilink") is *[Mary
        Ann](Wikipedia:Mary_Ann_Summers "wikilink")*, a sweet, innocent
        girl.
    -   [Jim](Jim_Halpert "wikilink") is *[The
        Professor](Wikipedia:The_Professor_(Gilligan's_Island) "wikilink")*,
        a genius who could make anything out of
        [coconuts](Wikipedia:coconuts "wikilink").
    -   [Katy](Katy "wikilink") is *[Ginger
        Grant](Wikipedia:Ginger_Grant "wikilink")*, a movie star.
    -   [Angela](Angela_Martin "wikilink") is *[Mrs.
        Howell](Wikipedia:Lovey_Howell "wikilink")*, a millionairess.
    -   [Kelly](Kelly_Kapoor "wikilink") is a native from a nearby
        island.
    -   Stanley is a
        [Globetrotter](Wikipedia:Harlem_Globetrotters "wikilink"),
        referring to the African-American exhibition basketball team who
        appeared in *[The Harlem Globetrotters on Gilligan\'s
        Island](Wikipedia:The_Harlem_Globetrotters_on_Gilligan's_Island "wikilink")*,
        one of one of many sequels to the television show.
    -   Michael is *[The Skipper](Wikipedia:The_Skipper "wikilink")*,
        the captain of the ship that ran aground.
    -   [Dwight](Dwight_Schrute "wikilink") is
        *[Gilligan](Wikipedia:Gilligan_(fictional_character) "wikilink")*,
        the bumbling first mate whose ineptitude ruins many attempts to
        escape the island.
-   *Nebulose* is not a real word. Michael intended to say *nebulous*.
-   Michael uses a variation of the slang saying *If the van\'s
    a-rockin\', don\'t come a-knockin*\'. In the saying, the van is
    rocking because the occupants are having sex.
-   *[Bishop O\'Hara](Bishop_O'Hara_High_School "wikilink")* is a high
    school in nearby Dunmore.
-   *Pan Am* is a now-defunct airline. Before airline security became
    more stringent, pilots would occasionally allow young children to
    visit the cockpit and let them pretend to fly the plane.
-   Dwight sings *[What do you do with a drunken
    sailor?](Wikipedia:Drunken_Sailor "wikilink")*, a traditional sea
    shanty.
-   A *snorkel shot* is a drinking game invented for this episode. It
    appears to consist of drinking a shot of liquor through a snorkel
    tube.
-   *Operation Desert Storm* is another name for the [Gulf
    War](Wikipedia:Gulf_War "wikilink").
-   Michael offers to *give away the bride* at Pam\'s wedding. This is a
    role traditionally played by the bride\'s father.
-   *BFD*, short for *Big F---ing Deal*, is a derisive expression.
-   *I hope I die before I\'m old* are lyrics from the song *[My
    Generation](Wikipedia:My_Generation "wikilink")* by [The
    Who](Wikipedia:The_Who "wikilink").
-   [The Grass Roots](Wikipedia:The_Grass_Roots "wikilink") was a real
    group, a successful folk rock act active from 1966 to 1975 who
    garnered 29 hit singles on the Billboard Top 100, and sold over 30
    million records. [Creed Bratton](Creed_Bratton "wikilink") is
    portrayed by the [actor of the same
    name](Creed_Bratton_(actor) "wikilink"), who was the group\'s
    guitarist. Creed appears to exhibit a level of bitterness over his
    \'fall from grace\'.
-   Songs played on the cruise were \'Get Busy\' by Sean Paul and \'Just
    The Two Of Us\' by Bill Withers.

Quotes
------

:   see *[Booze Cruise Quotes](Booze_Cruise_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring Cast

-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Amy Adams](Amy_Adams "wikilink") as [Katy](Katy "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Joanne Carlsen](Joanne_Carlsen "wikilink") as [Terri
    Hudson](Dunder_Mifflin_Family_Members_and_Loved_Ones#Terri_Hudson "wikilink")

### Guest Cast

-   [Rob Riggle](Rob_Riggle "wikilink") as [Captain
    Jack](Captain_Jack "wikilink")
-   [Brenda Withers](Brenda_Withers "wikilink") as [Brenda
    Matlowe](Brenda_Matlowe "wikilink")
-   Eliza Coleman as Passenger
-   Roger Huse as Tequila Man
-   R.C. Ormand as Panicked Man
