# Halloween

**\"Halloween\"** is the fifth episode of the second season of \'\'[The
Office](The_Office_(US) "wikilink") \'\'and the 11th overall. It was
written by [Greg Daniels](Greg_Daniels "wikilink") and directed by [Paul
Feig](Paul_Feig "wikilink"). It first aired on October 18, 2005. It was
viewed by 8 million people.

Synopsis
--------

When [Michael](Michael_Scott "wikilink") enters the office,
[Pam](Pam_Beesly "wikilink") tells him [Jan](Jan_Levinson "wikilink")
has called. Michael knows it\'s because he was supposed to fire someone
by the end of the month. So far, he has been putting it off hoping
someone would quit, move, or die so he wouldn\'t have to do it. Of
course, he has no idea who he is going to let go. Michael goes to the
Accounting Department ([Kevin](Kevin_Malone "wikilink"),
[Angela](Angela_Martin "wikilink"), and
[Oscar](Oscar_Martinez "wikilink")) and asks them to find \$50,000 in
the budget (basically, a salary and benefits), who come to the
conclusion that the Accounting Department has three people doing the
work of two, but don\'t tell Michael.
[Dwight](Dwight_Schrute "wikilink") gets on
[Jim](Jim_Halpert "wikilink")\'s nerves so he and Pam decide to post
Dwight\'s resume on the web. Angela gets mad at Pam for bringing
brownies instead of chips and dip to the office party. Dwight\'s resume
gets a hit from a company in Maryland, so Jim plays Michael and gives
Dwight a great reference. When the company, Cumberland Mills, calls
Dwight to set up an interview, Dwight immediately ruins his chances by
having a rather stern argument with the caller over the importance and
relevance of martial arts.

Pam thinks Jim should be the one to be getting better job offers and
mentions to Jim that, given the salary and growth potential there, he
should apply for the Cumberland Mills job. Jim is clearly hurt by the
suggestion, thinking that perhaps Pam wouldn\'t care if he left. Pam
tries to back-pedal but Jim makes clear his feelings. Michael tries to
practice his firing skills with Jim, but doesn\'t fire him. Michael
decides to fire [Creed](Creed_Bratton "wikilink"), after unsuccessfully
trying to fire Pam and [Stanley](Stanley_Hudson "wikilink"). Creed is
unwilling to be fired so he talks Michael into letting
[Devon](Devon "wikilink") go, which he does. Devon retailiates by
smashing a pumpkin on Michael\'s car. As Jim is leaving to go home, Pam
apologises for pushing him into taking the Cumberland job and reassures
him that she would \'blow her brains out\' if he ever left. In a talking
head, Jim brushes off the remark, saying that if Pam were the one
leaving, he wouldn\'t necessarily blow his brains out\...though he would
take the Cumberland Mills job. At the end of the episode, we see a
guilty Michael alone at home on
[Halloween](Wikipedia:Halloween "wikilink"), handing out candy to
trick-or-treaters.

Trivia- Caution, here be spoilers
---------------------------------

-   Dwight sends the supplement for his resume to \"Charles Weed\" (at
    Cumberland Mills).
-   When Jim answers the phone in Michael\'s office, he indicates that
    it is Toby on the phone; Michael tells Jim that he has to take the
    call, but promptly hangs up on Toby. This is an early indication in
    the series of Michael\'s hatred of Toby.
-   Michael tells Oscar he probably wishes he could wear his female
    costume all the time, possibly foreshadowing the fact that Oscar is
    gay.
-   While Dwight was not one of the co-workers Devon invited to Poor
    Richard\'s after being fired, in \"[Finale](Finale "wikilink")\"
    Dwight states that he likes Devon, and has re-hired him following
    Creed\'s \"death\".

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Michael walks sadly through the office, unconvincingly reassuring
    everyone that \"everything\'s going to be fine.\"
-   Toby asks Michael for the name of the employee that will be fired so
    he can start the paperwork. Michael tries to fire Toby.
-   As Michael takes the elevator downstairs. Leo and Gino from Vance
    Refrigeration mug for the cameras. At the security desk, Michael
    unsuccessfully asks the guard to be available to subdue an employee
    that has \"gone postal.\" (He also tries to fire the guard.) Michael
    is determined to have made his decision by the time the elevator
    returns him to the second floor, but the door opens before he is
    ready.
-   Michael\'s proposal that everyone take a 10% pay cut is not met with
    enthusiasm.
-   Additional footage of Michael trying to fire Creed.
-   Michael tells a long, rambling story about a deer hunting trip. The
    deer did not die instantly, and \"it\'s hard to hit another living
    thing in the face with a shovel for about an hour.\"
-   Michael calls Jan, angry that he was forced to betray \"one of my
    best buddies.\"
-   Michael sulks at the lame office Halloween party, along with those
    not invited to Poor Richard\'s by Devon (Dwight, Angela, and Creed).
-   In a talking head interview, Dwight explains that he is destined to
    sell paper at Dunder Mifflin.

Cultural references
-------------------

-   A *Sith Lord* is a type of warrior priest from the *Star Wars* movie
    franchise. Dwight later refers to characters *Anakin Skywalker* and
    *Darth Vader* from the same movie series.
-   Kelly is dressed like *Dorothy*, the heroine of the movie *The
    Wonderful Wizard of Oz*.
-   *Bend It Like Beckham* is, like Kelly says, \"the movie about the
    Indian girl who plays soccer.\"
-   *Monster.com* is a web site devoted to job placement, *Google* is a
    search engine, and *Craig\'s List* is a free classified ad web site.
-   Stanley says \"You\'re fired!\" in the style of Donald Trump, 45th
    President of The United States.
-   *Cumberland Mills* is a fictional paper company. The name may have
    been inspired by the Westvaco paper company in Cumberland, Maryland.
    Coincidentally, paper manufacturer Sappi has a factory in Cumberland
    Mills, Westbrook, Maine.
-   *Chili\'s* and *Poor Richard\'s* previously appeared in the episode
    [The Dundies](The_Dundies "wikilink").
-   *Janet Jackson\'s boob* refers to a controversial Super Bowl
    halftime show in 2004 which accidentally exposed the singer\'s
    breast. *Monica Lewinsky* and the *stained dress* refer to the 1999
    sex scandal involving then-president Bill Clinton. *O.J.* refers to
    former football great O. J. Simpson, whose 1995 murder trial
    garnered intense publicity.
-   *Sopranos style* refers to the television series *The Sopranos*
    about a fictional Mafia family.
-   The term *going postal* entered United States slang after a series
    of incidents in the 1980s and 1990s of postal service workers
    shooting and killing colleagues and members of the public. It can
    refer to workplace massacres, or, more generally, any violent
    outburst.

Quotes
------

:   see *[Halloween Quotes](Halloween_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring Cast

-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Hugh Dane](Hugh_Dane "wikilink") as [Hank
    Tate](Hank_Tate "wikilink")
-   [Lee Eisenberg](Lee_Eisenberg "wikilink") as [Gino](Gino "wikilink")
    (Deleted Scenes)
-   [Gene Stupnitsky](Gene_Stupnitsky "wikilink") as
    [Leo](Leo "wikilink") (Deleted Scenes)

### Guest Cast

-   [Ava Nisbet](Ava_Nisbet "wikilink") as [Sherry](Sherry "wikilink")
-   George Gaus as Lion
-   Annabelle Kopack as Fairy Princess
-   Alec Zbornak as Bumble Bee
