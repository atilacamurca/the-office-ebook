{{Office episode
|Title      =Valentine's Day
|Image      =[[Image:Valentinesday.jpg|250px]]
|Season     =[[Season 2|2]]
|Episode    =16
|Code       =2017
|Original   =February 9, 2006
|Writer(s)  =[[Michael Schur]]
|Director   =[[Greg Daniels]]
|Prev       =[[Boys and Girls]]
|Next       =[[Dwight's Speech]]
}}'''"Valentine's Day"''' is the sixteenth episode of the second season of ''[[The Office (US)|The Office]] ''and the 22nd overall. It was written by [[Michael Schur]] and directed by [[Greg Daniels]]. It first aired on February 9, 2006. It was viewed by 8.95 million people.

==Synopsis==
[[Michael Scott|Michael]] spends Valentine's Day at "[[Dunder Mifflin Corporate Office|Corporate]]" in New York City, which he describes as Scranton "on steroids." Before the meeting with the new [[David Wallace|CFO]] begins, Michael spills the beans to the other branch managers that he and Jan "hooked up." The CFO and Jan arrive, and the presentation of Josh Porter ([[Dunder Mifflin Stamford|Stamford]]) goes well. Michael shows a video titled "The Faces of Scranton" before reluctantly providing the financial information of the Scranton branch. Craig ("Craiggers") from the [[Dunder Mifflin Albany|Albany]] branch is completely unprepared and attempts to cover for it by suggesting that "maybe I should've slept with [Jan], too."

Meanwhile, Angela gives [[Dwight Schrute|Dwight]] a [[Wikipedia:bobblehead doll|bobblehead doll]] of himself for Valentine’s Day, which he proclaims is the best gift he’s ever received. After consulting with [[Pam Beesly|Pam]] for advice, he gives Angela a key, presumably to his beet ranch home; she is visibly touched by this. Phyllis is inundated with gifts from her boyfriend Bob Vance, Vance Refrigeration, while Pam is irritated with Roy when the only thing he gives her for Valentine's Day is the promise of the "best sex of [her] life." [[Jim Halpert|Jim]] is forced to witness [[Ryan Howard|Ryan]] turning Kelly down for a date after they hooked up the previous night.

In a private conversation with Michael, Jan is convinced that her career is over, but Michael assures her that he will "fix it." In an unusual moment of tact, Michael defuses the situation by explaining to the CFO that the whole thing was a bad joke (which Craig stupidly misinterpreted as the truth) and accepts responsibility for the situation. Jan plays along by accepting Michael's apology and agreeing to drop the matter. Before Michael leaves, Jan catches Michael by the elevator and kisses him, but groans when she realizes they were caught on camera.

===The Faces of Scranton===
<!-- tricky bits:

Pam wears the same outfit in "Performance Review" and "Boys and Girls"; must use other cues to determine which episode the clip is from.

Jim wears very similar outfits in "Performance Review" and "Boys and Girls", but there is a subtle difference in the necktie pattern. In "Boys and Girls" it has a diamond pattern.

Michael wears the same outfit in "Performance Review" and "Boys and Girls"; must use other cues.

-->
{{Col-begin}}{{Col-2}}
* Pull-back from Dunder Mifflin delivery truck.
* Michael Scott's parking space.
* Title card: ''"The Faces of Scranton" by Michael Scott''
* Pam sits meekly at her desk. (Wearing clothes from "[[Boys and Girls]]".)
* Jim waves off the camera. (Wearing clothes from "[[Boys and Girls]]".)
* Dwight gives two enthusiastic thumbs up. (Wearing clothes from "[[The Carpet]]".)
* ''Life moves a little slower in Scranton, Pennsylvania.''
* Phyllis comes out of the kitchen with a cup of coffee -- slow motion. (Wearing clothes from "[[The Carpet]]".)
* ''And that's the way we like it, because at Dunder Mifflin, Scranton...''
* Michael, talking head in his office. (Wearing clothes from "[[Boys and Girls]]".)
* ''... we're not just in the paper business...''
* Meredith smiles nervously for the camera. (Wearing clothes from "[[Boys and Girls]]".)
* ''... we're in the people business.''
* Oscar and Kevin wave reluctantly for the camera. Ryan hides in the kitchen. (Wearing clothes from "[[The Injury]]".)
* ''Let's meet some of the folks...''
* Angela smiles unconvincingly. (Wearing clothes from "[[Boys and Girls]]".)
* ''... who make the Scranton branch so darn special.''
* Roy, driving the forklift, waves Michael out of the way. ("[[Boys and Girls]]")
* ''This is Stanley Hudson, one of our talented salesmen.''
* Close-up of Stanley working at his desk.
* ''An African-American father of two...''
* Stanley glares at the camera.
* ''Stanley has worked here for eleven years.''
* A different shot of Stanley at his desk. He continues to eye the camera suspiciously.
* Michael shoots a live interview with Stanley.<br />
Michael: "Just... I'm doing a thing for the CFO, okay? A little movie."<br />
Stanley: "Did they ask for a movie?"<br />
Michael: "Just act like you're working, okay?"<br />
Stanley: "I ''am'' working!"
* ''Stanley's dedication is no doubt one of the hallmarks of the foundation of the business we're hoping to build our basis on.''
* Michael, talking head.
{{Col-2}}
* Dwight holds up a Dunder Mifflin catalog. (Wearing clothes from "[[The Carpet]]".)
* ''And finally, Pam Beesly.''
* Pull back from receptionist nameplate to Pam's desk. (Wearing clothes from "[[Boys and Girls]]".)
* ''Look at her. Look how cute.''
* A different shot of Pam slightly frustrated with Michael. (Wearing clothes from "[[Boys and Girls]]".)
* ''Not bad at all.''
* Michael nuzzles up next to Pam as she talks on the phone. (Wearing clothes from "[[Boys and Girls]]".)
* ''As the receptionist...''
* Pam works on the phone.
* ''Pam is truly the gateway to our world.''
* Pam and Jim sit and chat in the break room. Pam gives the camera a thumbs-up. Jim joins her. (Wearing clothes from "[[Boys and Girls]]".)
* ''Well, I hope this gave you a little taste of what life is like here in Dunder Mifflin, Scranton.''
* Michael, in the kitchen, dips a tea bag into an empty cup. (Wearing clothes from "[[Boys and Girls]]".)
* ''What it's like to walk a mile in Oscar's shoes.''
* Oscar walks to his desk. (Wearing clothes from "[[The Injury]]".)
* ''Or try on Phyllis's pants.''
* Phyllis sits at her desk. (Wearing clothes from "[[The Carpet]]".)
* ''Next time you're in town, give us a call.''
* Warehouse crew wave for the camera.
* ''Stop on by.''
* Kelly (at Ryan's desk) and Ryan wave for the camera. (Wearing clothes from "[[Boys and Girls]]".)
* ''I'm sure you'll be greeted by a big smile...''
* Kevin gives a quick smile for the camera.
* ''... and a "How're you doin', pal?"''
* Dwight gives an enthusiastic thumbs up. Creed walks past. (Wearing clothes from "[[The Carpet]]".)
* ''Maybe even one of Angela's famous brownies.''
* Angela works at her desk with a plate of brownies. (Wearing clothes from "[[Boys and Girls]]".)
* ''And you'll know that you're home.''
* Michael, in the kitchen, dips a tea bag into an empty cup. (Wearing clothes from "[[Boys and Girls]]".)
* Title card: ''A Michael Scott Joint''
* ''Great Scott!''
* Title card: ''Great Scott Films International'' with lightning bolt, and decorated with pictures of Steve Martin and Robin Williams.
{{Col-end}}

==Deleted scenes==
The Season Two DVD contains a number of deleted scenes from this episode. Notable cut scenes include:
*Michael delivers cheap plastic roses to the women of the office. In a talking head interview, Michael explains that the most attractive part of a women is her brain, because it is the brain that comes up with "nasty ideas for bedroom stuff."
*Kevin explains that his fiancée is out of town, but he doesn't know where.
*Creed calls everybody "Ace". In a talking head interview, he admits, "I'm not good with names."
*Many improvised scenes of Michael on the streets of New York.
*After delivering Phyllis's present, the Vance Refrigeration guys get into a fistfight.
*In a talking head interview, Michael can't decide whether there is any significance that his meeting with Jan is scheduled for Valentine's Day.
*Dwight asks Jim for spelling help but won't reveal what he's writing. (In a blooper, we learn that he's writing a love poem to Angela.)
*In a talking head interview, Jim says that Dwight having a girlfriend proves that "there really is someone for everybody."
*Kevin gets a phone call from Stacy and learns that she's back in town.
*Michael is chased down the street by Devon, who is now homeless in New York City.

==Trivia- Caution, here be spoilers==
*David Wallace has a mug on his desk that says "World's Best Boss"
*The [[Wikipedia:Tina Fey|Tina Fey]] joke was based on Michael Schur's observation that nearly every woman that wore glasses walking around Rockefeller Center was mistaken for Tina Fey. Ironically, when he went outside with the real Tina Fey, nobody recognized her.<ref name="Schur_DVD">Schur, Michael (Writer). 2006. "The Client" [Commentary track], ''The Office'' Season Two (US/NBC Version) [DVD], Los Angeles, CA: [[Universal Studios|Universal]].</ref>
*The location Michael is sitting in one of the New York scenes is actually about two blocks away from where the building is actually located as shown in Season 3 during "The Job". He is sitting two blocks on the opposite side of the Hilton hotel. In "[[The Job]]", you can clearly see the building they focus on is on the right side of the Hilton.
* Michael says, "New York is like Scranton [...] on steroids". On his September 26, 2007 appearance on ''[[Wikipedia:The Tonight Show with Jay Leno|The Tonight Show with Jay Leno]]'', while speaking about expressions that annoy him, Steve Carell said, "people refer to things being on steroids... that's pretty stupid."
*When the Dwight bobblehead dolls went on sale on NBC.com, they sold out their initial order of 5000 units.
*First appearance of [[David Wallace]].
*Jim and Pam do not speak to each other until the end of episode when Jim leaves for the day.
*During all of Jim's talking heads Pam will wander by delivering Phyllis gifts and in Pam’s talking heads Jim is sitting at the back talking to someone.
*First time Pam wears her hair in a headband instead of half-up half-down.
*First time Kelly and Ryan hook up.
*Michael says Rockefeller Center was founded by Theodore Rockefeller, confusing that with Theodore Roosevelt.
*Both Dwight and Angela seek relationship advice about the other person from Pam.
*Craig had to fire four people in September, however Michael only had to fire one person in October.

==Analysis==
*When Oscar receives a delivery of flowers, he quickly dismisses them as coming his mother. Oscar was revealed to the documentary crew as gay in the earlier episode "[[The Secret]]" but had not revealed it to the office. It is likely that the flowers were sent by his partner Gil.

==Amusing details==
*In the background of Jim's talking head at the beginning of the episode, Pam delivers Phyllis yet another gift.
*While Michael walks around Times Square, he claims that while other people go straight to the Empire State Building when they come to New York, that is too “touristy” for him. Ironically, Times Square is one of the most popular tourist spots in the city.
*In the "Faces of Scranton" video, Michael repeatedly dips his tea bag into an empty cup.
*In the "Faces of Scranton" video, when Michael first shows Pam, the camera darts to Craig, who shows much more interest in the video.

==Cultural references==
* ''[[Wikipedia:Valentine's Day|Valentine's Day]]'' is a holiday that celebrates romantic love.
* Michael calls New York the ''City of Love''. That nickname more traditionally belongs to [[Wikipedia:Paris|Paris]].
* Michael's interjection ''Fuggedaboutit'' is the phrase "Forget about it" in an exaggerated Italian New York accent. The phrase was popularized by the movie ''[[Wikipedia:Donnie Brasco (film)|Donnie Basco]]''. Michael's use of the phrase is somewhat nonsensical.
* ''The city so nice they named it twice'' is a saying applied to New York City because its mailing address is often written "New York, New York" (New York City in the state of New York). Michael doesn't understand this and says that Manhattan (the central borough) is its other name. (Etymologist Barry Popik traced the phrase back to 1975.[http://www.barrypopik.com/index.php/new_york_city/entry/new_york_new_york_so_nice_they_named_it_twice/])
* ''[[Wikipedia:Sbarro|Sbarro]]'' is a national chain of pizza restaurants. It is not a "local New York pizza joint" as Michael believes. Similarly, Michael identifies ''[[Wikipedia:Bubba Gump Shrimp Company|Bubba Gump Shrimp]]'' and ''[[Wikipedia:Red Lobster|Red Lobster]]'', both national seafood restaurant chains, as local New York restaurants.
* ''[[Wikipedia:Rockefeller Center|Rockefeller Center]]'' is named after philanthropist [[Wikipedia:John. D. Rockefeller, Jr|John. D. Rockefeller, Jr]], not "Theodore Rockefeller". Michael was probably thinking of [[wikipedia:Theodore Roosevelt|Theodore Roosevelt]]. The ''[[Wikipedia:New York Rangers|Rangers]]'' are a professional hockey team; they do not practice in the Rockefeller Center ice rink, which is far too small to play hockey.
* ''Tina Fey'' was at the time the head writer for the sketch comedy program ''Saturday Night Live''. The man who walks past is Conan O'Brien, host of a popular nighttime talk show.
* Michael identifies a river as "either the Hudson or the East", rivers which are located on opposite sides of Manhattan.
* ''Michael Jordan'' is a well-known basketball player. ''Stormin' Norman Schwarzkopf'' was the general who commanded Coalition forces in the 1991 Gulf War.
* Michael's ''Faces of Scranton'' video concludes with the phrase ''A Michael Scott Joint''. Director Spike Lee identifies his films as "A Spike Lee Joint". it ends with "''Great Scott!''" (an exclamation of surprise and a pun on Michael's name) and the images of Robin Williams and Steve Martin, two of Michael's comedy idols. The song that plays in the video is "With or Without You" by U2.
* Michael says ''And don't call me Shirley'', repeating a joke from the movie ''[[Wikipedia:Airplane!|Airplane!]]''. The joke is a pun, relying on the homophones "surely" and "Shirley", so when Jan says “Surely, you cannot be serious” it sounds like “Shirley, you cannot be serious”.
* Michael's exclamation ''Oy vey” is a shortening of the Yiddish phrase ''Oy vey's mir'' (oh woe is me) The word ''schmear'' which is a term for cream cheese spread on a bagel (a stereotypically Jewish food).

==Quotes==
:see ''[[Valentine's Day Quotes]]''

==Cast==
===Main cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Supporting cast===
*[[Melora Hardin]] as [[Jan Levenson]]
*[[David Denman]] as [[Roy Anderson]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Phyllis Smith]] as [[Phyllis Lapin]]

===Recurring cast===
*[[Andy Buckley]] as [[David Wallace]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Charles Eston|Charles Esten]] as [[Josh Porter]]
*[[Devon Abner]] as [[Devon White]] (Deleted Scenes)

===Guest cast===
*[[Dan Cole]] as [[Dan Gore]]
*[[Craig Anton]] as [[Craig]]
*[[Lee Eisenberg]] as [[Gino]] (Delivery Man #1)
*[[Jason Kessler]] as Delivery Man #2
*Jeremy Radin as Delivery Man #3
*[[Gene Stupnitsky]] as [[Leo]] (Delivery Man #4)
*[[Vivianne Collins]] as [[Grace]] (Deleted Scenes)
*Miriam Tolan as Fake Tina Fey (Uncredited)
*[[Conan O'Brien]] as Himself (Uncredited)

==References==
<references/>

{{Season2}}
