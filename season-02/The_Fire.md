# The Fire

**\"The Fire\"** is the fourth episode of the second season of \'\'[The
Office](The_Office_(US) "wikilink") \'\'and the 10th overall. It was
written by [B.J. Novak](B.J._Novak "wikilink") and directed by [Ken
Kwapis](Ken_Kwapis "wikilink"). It first aired on October 11, 2005. It
was viewed by 7.6 million people.

Synopsis
--------

[Pam](Pam_Beesly "wikilink") transfers a call from
[Katy](Katy "wikilink") to [Jim](Jim_Halpert "wikilink")\'s desk and the
two make lunch plans. In a talking head interview, Pam awkwardly
explains that Jim and Katy (the purse saleswoman from the episode \"[Hot
Girl](Hot_Girl "wikilink")\") have started dating, Pam then asks Jim to
give Katy the extension to his phone.

[Michael](Michael_Scott "wikilink") gives [Ryan](Ryan_Howard "wikilink")
a glowing checkpoint review. When Ryan expresses his interest in
starting his own business someday, Michael takes it upon himself to
teach Ryan the first of the ten rules of business. The fire alarm
sounds, and while Dwight and Angela both attempt to take charge of the
evacuation, Michael pushes others out of the way in his escape out of
the building.

In a talking head interview, Ryan explains that he doesn\'t want to have
a nickname or be known for anything at the office. Once everyone is
safely outside, Jim begins a game of \"three books on a desert island\".
At Dwight\'s turn, he misses the point of the exercise and asks for an
axe and a hollowed-out book filled with survival gear.

Jim changes the topic from books to DVDs. In a joint talking head
interview, Jim and Pam mock [Meredith](Meredith_Palmer "wikilink")\'s
choices, although Pam admits that she too likes *Legally Blonde*. Jim
teases Pam for liking that movie.

Michael learns that Ryan is going to business school at night. Michael
invites Ryan to quiz him but doesn\'t even understand the first
question. In a talking head interview, Michael explains that he learned
more about business from working at a fast-food restaurant than Ryan
will ever learn in school. Dwight, still jealous of the attention Ryan
is getting from Michael, attempts to bully the young employee but merely
earns a scolding from Michael. Ryan becomes increasingly uncomfortable
with Michael\'s friendliness.

Jim moves on to the next game, \"Who would you do?\", and Kevin
instantly responds, \"Pam.\" Jim and Pam excuse themselves from the game
to talk to Dwight, who is moping in his car. Pam suggests that Ryan
would be impressed to learn that Dwight is a volunteer sheriff\'s
deputy, and Jim tries to keep a straight face when he suggests that
Dwight quit, \"and then that would stick it to both of them.\" Dwight
thanks them for their concern but asks to be left alone.

Jim, Pam, Michael, Ryan and even Roy join the game of \"Who would you
do?\" Roy answers \"that tight-ass Christian chick.\" Jim jokingly
chooses Kevin, and Michael says, \"Well, I would definitely have sex
with Ryan.\" When Michael mentions that he left his cell phone in the
office, Dwight rushes back into the building to fetch it.

The women of the office continue playing \"Who would you do?\" Meredith:
\"Jim.\" Phyllis: \"Definitely Jim.\" Kelly: \"Definitely, definitely
Jim.\" Pam considers her response and says, \"Oscar\'s kind of cute.\"
She also mentions that Toby is cute and the others groan in disapproval,
except for Meredith, who nods and smiles.

Michael asks Ryan to call his cell phone to help Dwight find it. The
phone rings. It\'s in Michael\'s pocket. Dwight emerges, coughing, from
the building and reveals that the fire was started by Ryan, who left a
cheese pita in the toaster-oven set to \"oven\" instead of \"toaster\".
Dwight and Michael proceed to mock Ryan and dub him *The Fire Guy*.

Katy arrives for lunch and joins the DVD game. Her first movie: *Legally
Blonde*.

The fire out, everybody heads back inside. Michael promises Ryan,
\"I\'ll give you the rest of the ten tomorrow.\"

Trivia- Caution, here be spoilers
---------------------------------

-   According to the DVD commentary, this episode was originally
    intended to be the season opener, but NBC decided that *[The
    Dundies](The_Dundies "wikilink")* would be a better opening episode.
    Some story lines remain from the original episode order:
    -   Michael\'s man-crush on Ryan begins in this episode. It would
        explain why Michael awarded Ryan the *Hottest in the Office*
        Dundie award.
    -   The introduction of Katy as Jim\'s girlfriend flows more
        naturally from the immediately-preceding episode *[Hot
        Girl](Hot_Girl "wikilink")*.
-   Michael running out of the burning building, pushing shoving
    everyone out of his way, is somewhat similar to a Seinfeld episode,
    The Fire, where the character George Costanza is pushing and shoving
    everyone out of his path.
-   The Lyrics Dwight sings towards the end are \"Joe McCarthy, Richard
    Nixon, Studebaker, Television, North Korea, South Korea, Marilyn
    Monroe\--RYAN STARTED THE FIRE!!!!!\"
-   As the camera pans out from Michael running out of the burning
    building, a microphone boom arm can briefly be seen at the top right
    of the screen; this is almost definitely unintentional as the
    purposeful revealing of the camera crew began only in Season 9.
-   09:21 into the episode a camera crew and lighting set can be seen
    briefly before the camera zooms into Michael, Dwight and Ryan.
-   When the fire bell goes off, Dwight asks Phyllis and Stanley \"Have
    you ever seen a burn victim?\" Dwight asked the same question when
    he conducted a fire drill in the [Stress
    Relief](Stress_Relief "wikilink") episode in season 5.
-   During the \"Who would you do?\" game, Oscar joins the other guys in
    immediately saying Pam\'s name, much to her annoyance. This
    contradicts Oscar\'s character in later seasons, not only because he
    is gay, but also because he is too sensitive to say something that
    would offend others in such a way. Alternatively, this scene could
    be explained by the fact that Oscar was still in the closet at the
    time, and was simply trying to fit in with the other straight men in
    the office.
-   The fire in the kitchen was hot enough to apparently dry Dwight\'s
    shirt from the point he is dragging Kelly away to the following
    moment where he crawls to safety.
-   The song \"Ryan started the fire\" is based off the song \"we
    didn\'t start the fire\".
-   The windows at the front of the building seem cloudly/matte colored,
    but they\'re clear in any other episodes where the front of the
    building is shown.
-   When playing Desert Island, Dwight says \"Question: Did my shoes
    come off in the plane crash?\" It would be impossible for him to
    store anything in his shoes with airport security.
-   Early in the episode, Ryan says that he doesn't want to be "a guy
    here". By the end of the episode, he is deemed "The Fire Guy".

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Jim sees a weather report of \"70 and clear\" on Pam\'s computer
    screen and is surprised when she tells him that is the current
    weather in Scranton. He then attempts to look out a window to see
    this, until realising there are no windows in the office.
-   Several brief talking head interviews with Dwight, in which he
    confirms his position as Number Two behind Michael, admits to
    writing a book about Michael until Michael made him stop, and
    considers Michael\'s new friendship with Ryan.
-   Michael and Dwight try to develop the list of ten pieces of business
    advice he promised to give Ryan.
-   Dwight awkwardly tries to befriend Ryan.
-   Kevin does some accounting and chuckles when he reaches the
    number 69.
-   Talking-head interviews outside:
    -   Jim and Pam talk about how great it was when the teacher held
        class outside.
    -   Angela talks about how smoothly the building evacuation went,
        then breaks down crying.
    -   Alternate take of Michael justifying why he left the building
        first.
    -   Jim explains that he and Toby used to sit together, but Michael
        moved them because they talked too much.
    -   Michael describes the jobs he held when he was Ryan\'s age.
-   Toby and Kevin list their five favorite movies.
-   Ryan tells Jim that his investment in business school is \"worth
    it,\" with the implication that it would allow him to quit his job
    at Dunder Mifflin.
-   In a talking head interview, Michael gives a rambling answer to the
    question \"If you could change the life of one person, who would it
    be?\".

Cultural references
-------------------

-   *Mr. Miyagi* is the karate master in the *[Karate
    Kid](Wikipedia:The_Karate_Kid "wikilink")* movie series.
    *[Yoda](Wikipedia:Yoda "wikilink")* is the Jedi master in the *[Star
    Wars](Wikipedia:Star_Wars "wikilink")* movie series. *[Fozzie
    Bear](Wikipedia:Fozzie_Bear "wikilink")* is a character from *[The
    Muppet Show](Wikipedia:The_Mupper_Show "wikilink")*. [Frank
    Oz](Wikipedia:Frank_Oz "wikilink") voiced both Yoda and Fozzie bear,
    thus the two characters voices similar enough that Ryan confused the
    two.
-   *[Batman](Wikipedia:Batman "wikilink")*/*[Robin](Wikipedia:Robin_(comics) "wikilink")*
    and *[The Lone
    Ranger](Wikipedia:The_Lone_Ranger "wikilink")*/*[Tonto](Wikipedia:Tonto "wikilink")*
    are two fictional crime-fighting duos. *Bonto* is a made-up name.
-   *Stat* (short for *statim* =immediately) is medical jargon. Appended
    to a command, it means that the action should be performed
    immediately.
-   *Image is everything* is the tag line to an ad campaign for
    [Canon](Wikipedia:Canon_(company) "wikilink") starring tennis player
    *[Andre Agassi](Wikipedia:Andre_Agassi "wikilink")*.
-   Dwight asks, \"Did my shoes come off in the plane crash?\" Dwight
    assumes that he is stranded on the island due to a plane crash, like
    the characters of the television program
    *[Lost](Wikipedia:Lost_(TV_series) "wikilink")*.
-   Michael confuses *[Stanley
    Kaplan](Wikipedia:Kaplan,_Inc. "wikilink")* with [Sue
    Grafton](Wikipedia:Sue_Grafton "wikilink"). Kaplan is the founder of
    a company that produces test preparation materials. Grafton is known
    for her [Kinsey Millhone](Wikipedia:Kinsey_Millhone "wikilink")
    series of mystery novels, whose titles follow the pattern *[\"A\" Is
    for Alibi](Wikipedia:"A"_Is_for_Alibi "wikilink")*, *[\"B\" Is for
    Burglar](Wikipedia:"B"_Is_for_Burglar "wikilink")*, etc. *\"M\" Is
    for Murder* (possibly a confused reference to the
    [Hitchcock](Wikipedia:Alfred_Hitchcock "wikilink") classic *[Dial M
    for Murder](Wikipedia:Dial_M_for_Murder "wikilink")*) and *\"P\" Is
    for Phone* are not among her titles. Sue Grafton is again referenced
    in the future episode *[Local Ad](Local_Ad "wikilink")*.
-   *[Lebron James](Wikipedia:Lebron_James "wikilink")*, *[Tracy
    McGrady](Wikipedia:Tracy_McGrady "wikilink")*, and *[Kobe
    Bryant](Wikipedia:Kobe_Bryant "wikilink")* are professional
    basketball players noted for beginning their NBA careers directly
    after high school.
-   *Bad Idea Jeans* is a fake brand of blue jeans that was the subject
    of multiple parody advertisements on the sketch comedy program
    \"[Saturday Night Live](Wikipedia:Saturday_Night_Live "wikilink")\".
-   During the *Desert Island* game various prominent books and films
    are mentioned:
    -   Books: *[The Bible](https://en.wikipedia.org/wiki/Bible)* and
        *[The Purpose Driven
        Life](https://en.wikipedia.org/wiki/The_Purpose_Driven_Life)*
        (Angela); *[The Da Vinci
        Code](https://en.wikipedia.org/wiki/The_Da_Vinci_Code)*
        (Phyllis); *[Physicians\' Desk
        Reference](https://en.wikipedia.org/wiki/Physicians'_Desk_Reference),
        [Harry Potter and the Sorcerer\'s
        Stone](https://en.wikipedia.org/wiki/Harry_Potter_and_the_Philosopher's_Stone)*
        and *[Harry Potter and the Prisoner of
        Azkaban](https://en.wikipedia.org/wiki/Harry_Potter_and_the_Prisoner_of_Azkaban)*
        (Dwight)
    -   Films: *[Legends of the
        Fall](https://en.wikipedia.org/wiki/Legends_of_the_Fall)*, *[My
        Big Fat Greek
        Wedding](https://en.wikipedia.org/wiki/My_Big_Fat_Greek_Wedding),*
        *[Legally
        Blonde](https://en.wikipedia.org/wiki/Legally_Blonde),* \'\'[The
        Bridges of Madison
        County](https://en.wikipedia.org/wiki/The_Bridges_of_Madison_County_(film))
        \'\'and
        *[Ghost](https://en.wikipedia.org/wiki/Ghost_(1990_film))*
        (Meredith);
        \'\'[Fargo](https://en.wikipedia.org/wiki/Fargo_(film)), [Edward
        Scissorhands](https://en.wikipedia.org/wiki/Edward_Scissorhands),
        [Dazed and
        Confused](https://en.wikipedia.org/wiki/Dazed_and_Confused_(film)),
        [The Breakfast
        Club](https://en.wikipedia.org/wiki/The_Breakfast_Club) \'\'and
        *[The Princess
        Bride](https://en.wikipedia.org/wiki/The_Princess_Bride_(film))*
        (Pam); *[The
        Crow](https://en.wikipedia.org/wiki/The_Crow_(1994_film))*
        (Dwight).

Quotes
------

:   see *[The Fire Quotes](The_Fire_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring Cast

-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Amy Adams](Amy_Adams "wikilink") as [Katy](Katy "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited)
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink") (Uncredited)

### Guest Cast

-   Shawn C. Sims as Security Guard (Uncredited)

\
