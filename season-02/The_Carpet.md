# The Carpet

**\"The Carpet\"** is the fourteenth episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 20th overall.
It was written by [Paul Lieberstein](Paul_Lieberstein "wikilink") and
directed by [Victor Nelli Jr.](Victor_Nelli_Jr. "wikilink") It first
aired on January 26, 2006. It was viewed by 8.6 million people.

Synopsis
--------

When someone deposits a malodorous substance on the carpet in
[Michael](Michael_Scott "wikilink")\'s office, he spends the day at
[Jim](Jim_Halpert "wikilink")\'s desk, relegating Jim to a back room
with [Kelly](Kelly_Kapoor "wikilink"). Michael institutes a sales
contest, promising \$100 (which later turns out to be \$83) to the
person who racks up the most sales that day.

At his temporary new desk, Jim suffers Kelly\'s constant chattering. She
asks Jim to hook her up with [Ryan](Ryan_Howard "wikilink"). Jim
continues to yearn for [Pam](Pam_Beesly "wikilink"), but her fiancé,
[Roy](Roy_Anderson "wikilink"), is in the office replacing the carpet
along with [Darryl](Darryl_Philbin "wikilink"), and Jim is unable to
speak with her. Interestingly, while exiled in the back of the room, Jim
makes an awkward (but later revealed to be a successful) attempt to ask
Brenda, who joined the office on the [Booze
Cruise](Booze_Cruise "wikilink"), out on a date.

As the day wears on, Michael becomes convinced that what happened to his
office is a [hate crime](Wikipedia:hate_crime "wikilink") and an act of
[terrorism](Wikipedia:terrorism "wikilink"). Believing it to be done by
someone in the office, he begins to lose his faith in his employees, who
he considers his \"friends\". But his mood changes drastically when he
finds out the object in his office was actually left there by his \"Best
Friend Forever,\" [Todd Packer](Todd_Packer "wikilink"). Michael
instantly finds the joke hilarious, and his faith in his friends is
restored. It is never revealed what the substance Todd left on the
carpet is, but it can be seen in one shot, and it appears to be
[feces](Wikipedia:feces "wikilink") of some sort. The smelly substance
highly believed to be feces.

![](The_carpet.jpg "fig:The smelly substance highly believed to be feces.")

At the end of the day, Jim is seen looking rather unhappy as he notices
some unheard voicemails. He looks at the blinking light for a few
seconds and decides to listen to all seven unheard voicemails, all left
by Pam throughout the day. As we listen to the voicemails, Jim leaves
work with a noticeable smile on his face.

Trivia
------

-   This episode is the only one which features an appearance by [Ed
    Truck](Ed_Truck "wikilink"), the manager who preceded Michael. Ed is
    mentioned several times throughout the series.

<!-- -->

-   This episode shares some characteristics with [Episode
    Two](Wikipedia:Episode_Two_(The_Office,_Series_One) "wikilink") of
    [the UK version of the show](Wikipedia:The_Office_(UK) "wikilink"),
    in which [David Brent](Wikipedia:David_Brent "wikilink") is offended
    that someone superimposed his face on a pornographic image until he
    finds out that it was his good friend, [Chris
    Finch](Wikipedia:Chris_Finch "wikilink"). Notably Finch and Packer
    do not physically appear in each episode, and in both episodes
    Michael/David only discovers Packer/Finch as the culprit over the
    phone near the end.

<!-- -->

-   Rock 107, the radio station Dwight keeps calling, is an actual radio
    station in the Scranton area, [WEZX](Wikipedia:WEZX "wikilink").

<!-- -->

-   This is the first episode in which Kelly is a chatterbox. In earlier
    episodes, she did not exhibit this personality trait.

<!-- -->

-   This episode was originally intended to air before \"[The
    Secret](The_Secret "wikilink")\".[1](http://boards.nbc.com/nbc/lofiversion/index.php/t750975.html)
    This explains why this episode does not credit the non-starring cast
    as series regulars even though \"[The
    Secret](The_Secret "wikilink")\" does.
-   Michael says,\"I am Pam.\" to Pam and cites it as a \"Spicoli\",
    Sean Penn in \"Fast Times at Ridgemont High\", reference but it is
    actually from Sean Penn in \"I am Sam\".
-   It is thought that the majority of this episode occurred on January
    23, 2006

Behind the scenes
-----------------

-   Unknown to the rest of the cast, [Steve
    Carell](Steve_Carell "wikilink") had thrown a stinkbomb in
    Michael\'s office moments before shooting began on the scene in
    which employees smell the odor in his office.

Interesting details
-------------------

-   Michael cuts short his first talking-head interview because the
    smell in his office is overpowering. When Michael leaves the office,
    notice that the cameraman was filming the interview from *outside*
    Michael\'s office. The smell was *that* bad.
-   According to the scoreboard in the office, Stanley appeared to be
    winning the sales competition and Michael was in last place.
-   Above Kelly\'s desk hangs a whiteboard with quarterly sales numbers.
    Dwight had the most sales for the quarter
-   When Packer calls Michael to reveal that he left him a \"package\",
    it is thought by some that he can be heard saying \"Shit on the
    floor, Michael\" as Michael is talking, but he is actually saying
    \"Sit on the throne\".
-   Michael uses Jim\'s clients even though in future episodes Michael
    has his own clients as Regional Manager.
-   Michael told Ed there was a problem with his pension to get him to
    come.
-   It is implied that Todd Packer did the same thing he did to Michael
    to Ed Truck many years ago.
-   Creed is the only one that finds what Packer did amusing.
-   The article \"Top Salesman Award\" in the Dunder Mifflin Staff
    Newsletter reads as follows:

\"Welcome to yet another exciting edition of the Dunder Mifflin Employee
Newsletter. Thanks to all of the staff and new contributing writers for
putting this together for all of you, and also many thanks to the folks
at Designtown for printing this up for us. Hopefully you will find alot
(sic) of useless information contained herein that will help you do your
job better, faster and quicker and cheaper and happier.

As anybody can easily tell, this newsletter doesn\'t really have alot
(sic) to say. It\'s really just a prop to fill some space and sort of
look like a newsletter without really being much of a newsletter at all.
By typing alot (sic) of words in two columns on the front of this page,
we can achieve the look of a newsletter without really reporting much
news or provide any real information to the reader at all. In fact, at
times we can probably get away with not using real english (sic) words,
such *\*random letters\**, *\*random letters\**, or the much beloved
*\*random letters\**. These words can also be strung together to form a
sentence, paragraph or even a whole prop book, magazine or newspaper.\"

This text then repeats.

Deleted scenes
--------------

The Season Two DVD will most likely contain a number of deleted scenes
from this episode. Notable cut scenes include:

-   In a talking head interview, Dwight believes that a higher power
    brought Michael to the desk next to him.
-   In separate talking head interviews, Angela and Kevin speculate on
    who may have soiled Michael\'s carpet.
-   A picture on Jim\'s computer prompts Kelly to begin chattering. Jim
    excuses himself to get some food and commiserates with Toby. In a
    talking-head interview, Toby admits, \"I don\'t even hear her any
    more.\" Jim eats his lunch in his car.
-   Michael has computer problems and can\'t complete the sale. He walks
    the office and finds the others busy selling paper. The contest
    scoreboard shows Michael in last place.
-   When Dwight loses a sale, Michael insists that he call the customer
    back and tell a joke. Dwight\'s joke falls flat, and Michael takes
    the phone. \"Sorry about that. Dwight is an idiot.\" Michael steals
    the sale.
-   Roy and Darryl play a drinking game in Michael\'s office.
-   Dwight and Angela have a secret conversation in the kitchen. Angela
    lends her support.
-   In a talking head interview, Michael remembers his best friend ever.
    \"I wonder where he lives.\"

Cultural references
-------------------

-   Michael attributes \"*I Am Pam*\" to \"*Spicoli Guy*\". The
    character of Sam in the movie *I Am Sam* is played by Sean Penn, who
    also played Jeff Spicoli in the movie *Fast Times at Ridgemont
    High*.
-   *Joe Rogan* is the host of the reality game show *Fear Factor*,
    wherein contestants must do something dangerous or disgusting to
    advance.
-   *I will gladly pay you Tuesday for a hamburger today* is a catch
    phrase of the character Wimpy from the Popeye cartoons.
-   *Presidents Day* is a national holiday (formally known as
    Washington\'s Birthday) that falls on the third Monday of February.
-   *Working for the Weekend* is a 1981 song by Loverboy.
-   Michael\'s exclamation *Out of sight, out of the contest* is a
    modification of the adage \"Out of sight, out of mind,\" which means
    \"You don\'t think about things which you can\'t see.\"
-   Michael misidentifies the home remodeling program *Extreme Makeover:
    Home Edition* as *Extreme Home Makeover*. It is actually known as
    this in some foreign markets.
-   Ryan catches himself before he finishes the phrase *Junk in the
    trunk*, slang for large buttocks.
-   *Sudoku* is a number puzzle that became wildly popular in 2005.

Quotes
------

:   *See [The Carpet Quotes](The_Carpet_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring cast

-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [David Koechner](David_Koechner "wikilink") as [Todd
    Packer](Todd_Packer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")

### Guest cast

-   [Ken Howard](Ken_Howard "wikilink") as [Ed
    Truck](Ed_Truck "wikilink")

\
