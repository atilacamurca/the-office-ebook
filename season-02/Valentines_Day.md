# Valentine's Day

**\"Valentine\'s Day\"** is the sixteenth episode of the second season
of \'\'[The Office](The_Office_(US) "wikilink") \'\'and the 22nd
overall. It was written by [Michael Schur](Michael_Schur "wikilink") and
directed by [Greg Daniels](Greg_Daniels "wikilink"). It first aired on
February 9, 2006. It was viewed by 8.95 million people.

Synopsis
--------

[Michael](Michael_Scott "wikilink") spends Valentine\'s Day at
\"[Corporate](Dunder_Mifflin_Corporate_Office "wikilink")\" in New York
City, which he describes as Scranton \"on steroids.\" Before the meeting
with the new [CFO](David_Wallace "wikilink") begins, Michael spills the
beans to the other branch managers that he and Jan \"hooked up.\" The
CFO and Jan arrive, and the presentation of Josh Porter
([Stamford](Dunder_Mifflin_Stamford "wikilink")) goes well. Michael
shows a video titled \"The Faces of Scranton\" before reluctantly
providing the financial information of the Scranton branch. Craig
(\"Craiggers\") from the [Albany](Dunder_Mifflin_Albany "wikilink")
branch is completely unprepared and attempts to cover for it by
suggesting that \"maybe I should\'ve slept with \[Jan\], too.\"

Meanwhile, Angela gives [Dwight](Dwight_Schrute "wikilink") a
[bobblehead doll](Wikipedia:bobblehead_doll "wikilink") of himself for
Valentine's Day, which he proclaims is the best gift he's ever received.
After consulting with [Pam](Pam_Beesly "wikilink") for advice, he gives
Angela a key, presumably to his beet ranch home; she is visibly touched
by this. Phyllis is inundated with gifts from her boyfriend Bob Vance,
Vance Refrigeration, while Pam is irritated with Roy when the only thing
he gives her for Valentine\'s Day is the promise of the \"best sex of
\[her\] life.\" [Jim](Jim_Halpert "wikilink") is forced to witness
[Ryan](Ryan_Howard "wikilink") turning Kelly down for a date after they
hooked up the previous night.

In a private conversation with Michael, Jan is convinced that her career
is over, but Michael assures her that he will \"fix it.\" In an unusual
moment of tact, Michael defuses the situation by explaining to the CFO
that the whole thing was a bad joke (which Craig stupidly misinterpreted
as the truth) and accepts responsibility for the situation. Jan plays
along by accepting Michael\'s apology and agreeing to drop the matter.
Before Michael leaves, Jan catches Michael by the elevator and kisses
him, but groans when she realizes they were caught on camera.

### The Faces of Scranton

-   Pull-back from Dunder Mifflin delivery truck.
-   Michael Scott\'s parking space.
-   Title card: *\"The Faces of Scranton\" by Michael Scott*
-   Pam sits meekly at her desk. (Wearing clothes from \"[Boys and
    Girls](Boys_and_Girls "wikilink")\".)
-   Jim waves off the camera. (Wearing clothes from \"[Boys and
    Girls](Boys_and_Girls "wikilink")\".)
-   Dwight gives two enthusiastic thumbs up. (Wearing clothes from
    \"[The Carpet](The_Carpet "wikilink")\".)
-   *Life moves a little slower in Scranton, Pennsylvania.*
-   Phyllis comes out of the kitchen with a cup of coffee \-- slow
    motion. (Wearing clothes from \"[The
    Carpet](The_Carpet "wikilink")\".)
-   *And that\'s the way we like it, because at Dunder Mifflin,
    Scranton\...*
-   Michael, talking head in his office. (Wearing clothes from \"[Boys
    and Girls](Boys_and_Girls "wikilink")\".)
-   *\... we\'re not just in the paper business\...*
-   Meredith smiles nervously for the camera. (Wearing clothes from
    \"[Boys and Girls](Boys_and_Girls "wikilink")\".)
-   *\... we\'re in the people business.*
-   Oscar and Kevin wave reluctantly for the camera. Ryan hides in the
    kitchen. (Wearing clothes from \"[The
    Injury](The_Injury "wikilink")\".)
-   *Let\'s meet some of the folks\...*
-   Angela smiles unconvincingly. (Wearing clothes from \"[Boys and
    Girls](Boys_and_Girls "wikilink")\".)
-   *\... who make the Scranton branch so darn special.*
-   Roy, driving the forklift, waves Michael out of the way. (\"[Boys
    and Girls](Boys_and_Girls "wikilink")\")
-   *This is Stanley Hudson, one of our talented salesmen.*
-   Close-up of Stanley working at his desk.
-   *An African-American father of two\...*
-   Stanley glares at the camera.
-   *Stanley has worked here for eleven years.*
-   A different shot of Stanley at his desk. He continues to eye the
    camera suspiciously.
-   Michael shoots a live interview with Stanley.\

Michael: \"Just\... I\'m doing a thing for the CFO, okay? A little
movie.\"\
Stanley: \"Did they ask for a movie?\"\
Michael: \"Just act like you\'re working, okay?\"\
Stanley: \"I *am* working!\"

-   *Stanley\'s dedication is no doubt one of the hallmarks of the
    foundation of the business we\'re hoping to build our basis on.*
-   Michael, talking head.

-   Dwight holds up a Dunder Mifflin catalog. (Wearing clothes from
    \"[The Carpet](The_Carpet "wikilink")\".)
-   *And finally, Pam Beesly.*
-   Pull back from receptionist nameplate to Pam\'s desk. (Wearing
    clothes from \"[Boys and Girls](Boys_and_Girls "wikilink")\".)
-   *Look at her. Look how cute.*
-   A different shot of Pam slightly frustrated with Michael. (Wearing
    clothes from \"[Boys and Girls](Boys_and_Girls "wikilink")\".)
-   *Not bad at all.*
-   Michael nuzzles up next to Pam as she talks on the phone. (Wearing
    clothes from \"[Boys and Girls](Boys_and_Girls "wikilink")\".)
-   *As the receptionist\...*
-   Pam works on the phone.
-   *Pam is truly the gateway to our world.*
-   Pam and Jim sit and chat in the break room. Pam gives the camera a
    thumbs-up. Jim joins her. (Wearing clothes from \"[Boys and
    Girls](Boys_and_Girls "wikilink")\".)
-   *Well, I hope this gave you a little taste of what life is like here
    in Dunder Mifflin, Scranton.*
-   Michael, in the kitchen, dips a tea bag into an empty cup. (Wearing
    clothes from \"[Boys and Girls](Boys_and_Girls "wikilink")\".)
-   *What it\'s like to walk a mile in Oscar\'s shoes.*
-   Oscar walks to his desk. (Wearing clothes from \"[The
    Injury](The_Injury "wikilink")\".)
-   *Or try on Phyllis\'s pants.*
-   Phyllis sits at her desk. (Wearing clothes from \"[The
    Carpet](The_Carpet "wikilink")\".)
-   *Next time you\'re in town, give us a call.*
-   Warehouse crew wave for the camera.
-   *Stop on by.*
-   Kelly (at Ryan\'s desk) and Ryan wave for the camera. (Wearing
    clothes from \"[Boys and Girls](Boys_and_Girls "wikilink")\".)
-   *I\'m sure you\'ll be greeted by a big smile\...*
-   Kevin gives a quick smile for the camera.
-   *\... and a \"How\'re you doin\', pal?\"*
-   Dwight gives an enthusiastic thumbs up. Creed walks past. (Wearing
    clothes from \"[The Carpet](The_Carpet "wikilink")\".)
-   *Maybe even one of Angela\'s famous brownies.*
-   Angela works at her desk with a plate of brownies. (Wearing clothes
    from \"[Boys and Girls](Boys_and_Girls "wikilink")\".)
-   *And you\'ll know that you\'re home.*
-   Michael, in the kitchen, dips a tea bag into an empty cup. (Wearing
    clothes from \"[Boys and Girls](Boys_and_Girls "wikilink")\".)
-   Title card: *A Michael Scott Joint*
-   *Great Scott!*
-   Title card: *Great Scott Films International* with lightning bolt,
    and decorated with pictures of Steve Martin and Robin Williams.

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Michael delivers cheap plastic roses to the women of the office. In
    a talking head interview, Michael explains that the most attractive
    part of a women is her brain, because it is the brain that comes up
    with \"nasty ideas for bedroom stuff.\"
-   Kevin explains that his fiancée is out of town, but he doesn\'t know
    where.
-   Creed calls everybody \"Ace\". In a talking head interview, he
    admits, \"I\'m not good with names.\"
-   Many improvised scenes of Michael on the streets of New York.
-   After delivering Phyllis\'s present, the Vance Refrigeration guys
    get into a fistfight.
-   In a talking head interview, Michael can\'t decide whether there is
    any significance that his meeting with Jan is scheduled for
    Valentine\'s Day.
-   Dwight asks Jim for spelling help but won\'t reveal what he\'s
    writing. (In a blooper, we learn that he\'s writing a love poem to
    Angela.)
-   In a talking head interview, Jim says that Dwight having a
    girlfriend proves that \"there really is someone for everybody.\"
-   Kevin gets a phone call from Stacy and learns that she\'s back in
    town.
-   Michael is chased down the street by Devon, who is now homeless in
    New York City.

Trivia- Caution, here be spoilers
---------------------------------

-   David Wallace has a mug on his desk that says \"World\'s Best Boss\"
-   The [Tina Fey](Wikipedia:Tina_Fey "wikilink") joke was based on
    Michael Schur\'s observation that nearly every woman that wore
    glasses walking around Rockefeller Center was mistaken for Tina Fey.
    Ironically, when he went outside with the real Tina Fey, nobody
    recognized her.[^1]
-   The location Michael is sitting in one of the New York scenes is
    actually about two blocks away from where the building is actually
    located as shown in Season 3 during \"The Job\". He is sitting two
    blocks on the opposite side of the Hilton hotel. In \"[The
    Job](The_Job "wikilink")\", you can clearly see the building they
    focus on is on the right side of the Hilton.
-   Michael says, \"New York is like Scranton \[\...\] on steroids\". On
    his September 26, 2007 appearance on *[The Tonight Show with Jay
    Leno](Wikipedia:The_Tonight_Show_with_Jay_Leno "wikilink")*, while
    speaking about expressions that annoy him, Steve Carell said,
    \"people refer to things being on steroids\... that\'s pretty
    stupid.\"
-   When the Dwight bobblehead dolls went on sale on NBC.com, they sold
    out their initial order of 5000 units.
-   First appearance of [David Wallace](David_Wallace "wikilink").
-   Jim and Pam do not speak to each other until the end of episode when
    Jim leaves for the day.
-   During all of Jim\'s talking heads Pam will wander by delivering
    Phyllis gifts and in Pam's talking heads Jim is sitting at the back
    talking to someone.
-   First time Pam wears her hair in a headband instead of half-up
    half-down.
-   First time Kelly and Ryan hook up.
-   Michael says Rockefeller Center was founded by Theodore Rockefeller,
    confusing that with Theodore Roosevelt.
-   Both Dwight and Angela seek relationship advice about the other
    person from Pam.
-   Craig had to fire four people in September, however Michael only had
    to fire one person in October.

Analysis
--------

-   When Oscar receives a delivery of flowers, he quickly dismisses them
    as coming his mother. Oscar was revealed to the documentary crew as
    gay in the earlier episode \"[The Secret](The_Secret "wikilink")\"
    but had not revealed it to the office. It is likely that the flowers
    were sent by his partner Gil.

Amusing details
---------------

-   In the background of Jim\'s talking head at the beginning of the
    episode, Pam delivers Phyllis yet another gift.
-   While Michael walks around Times Square, he claims that while other
    people go straight to the Empire State Building when they come to
    New York, that is too "touristy" for him. Ironically, Times Square
    is one of the most popular tourist spots in the city.
-   In the \"Faces of Scranton\" video, Michael repeatedly dips his tea
    bag into an empty cup.
-   In the \"Faces of Scranton\" video, when Michael first shows Pam,
    the camera darts to Craig, who shows much more interest in the
    video.

Cultural references
-------------------

-   *[Valentine\'s Day](Wikipedia:Valentine's_Day "wikilink")* is a
    holiday that celebrates romantic love.
-   Michael calls New York the *City of Love*. That nickname more
    traditionally belongs to [Paris](Wikipedia:Paris "wikilink").
-   Michael\'s interjection *Fuggedaboutit* is the phrase \"Forget about
    it\" in an exaggerated Italian New York accent. The phrase was
    popularized by the movie *[Donnie
    Basco](Wikipedia:Donnie_Brasco_(film) "wikilink")*. Michael\'s use
    of the phrase is somewhat nonsensical.
-   *The city so nice they named it twice* is a saying applied to New
    York City because its mailing address is often written \"New York,
    New York\" (New York City in the state of New York). Michael
    doesn\'t understand this and says that Manhattan (the central
    borough) is its other name. (Etymologist Barry Popik traced the
    phrase back to
    1975.[1](http://www.barrypopik.com/index.php/new_york_city/entry/new_york_new_york_so_nice_they_named_it_twice/))
-   *[Sbarro](Wikipedia:Sbarro "wikilink")* is a national chain of pizza
    restaurants. It is not a \"local New York pizza joint\" as Michael
    believes. Similarly, Michael identifies *[Bubba Gump
    Shrimp](Wikipedia:Bubba_Gump_Shrimp_Company "wikilink")* and *[Red
    Lobster](Wikipedia:Red_Lobster "wikilink")*, both national seafood
    restaurant chains, as local New York restaurants.
-   *[Rockefeller Center](Wikipedia:Rockefeller_Center "wikilink")* is
    named after philanthropist [John. D. Rockefeller,
    Jr](Wikipedia:John._D._Rockefeller,_Jr "wikilink"), not \"Theodore
    Rockefeller\". Michael was probably thinking of [Theodore
    Roosevelt](wikipedia:Theodore_Roosevelt "wikilink"). The
    *[Rangers](Wikipedia:New_York_Rangers "wikilink")* are a
    professional hockey team; they do not practice in the Rockefeller
    Center ice rink, which is far too small to play hockey.
-   *Tina Fey* was at the time the head writer for the sketch comedy
    program *Saturday Night Live*. The man who walks past is Conan
    O\'Brien, host of a popular nighttime talk show.
-   Michael identifies a river as \"either the Hudson or the East\",
    rivers which are located on opposite sides of Manhattan.
-   *Michael Jordan* is a well-known basketball player. *Stormin\'
    Norman Schwarzkopf* was the general who commanded Coalition forces
    in the 1991 Gulf War.
-   Michael\'s *Faces of Scranton* video concludes with the phrase *A
    Michael Scott Joint*. Director Spike Lee identifies his films as \"A
    Spike Lee Joint\". it ends with \"*Great Scott!*\" (an exclamation
    of surprise and a pun on Michael\'s name) and the images of Robin
    Williams and Steve Martin, two of Michael\'s comedy idols. The song
    that plays in the video is \"With or Without You\" by U2.
-   Michael says *And don\'t call me Shirley*, repeating a joke from the
    movie *[Airplane!](Wikipedia:Airplane! "wikilink")*. The joke is a
    pun, relying on the homophones \"surely\" and \"Shirley\", so when
    Jan says "Surely, you cannot be serious" it sounds like "Shirley,
    you cannot be serious".
-   Michael\'s exclamation \'\'Oy vey" is a shortening of the Yiddish
    phrase *Oy vey\'s mir* (oh woe is me) The word *schmear* which is a
    term for cream cheese spread on a bagel (a stereotypically Jewish
    food).

Quotes
------

:   see *[Valentine\'s Day Quotes](Valentine's_Day_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring cast

-   [Andy Buckley](Andy_Buckley "wikilink") as [David
    Wallace](David_Wallace "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Charles Esten](Charles_Eston "wikilink") as [Josh
    Porter](Josh_Porter "wikilink")
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink") (Deleted Scenes)

### Guest cast

-   [Dan Cole](Dan_Cole "wikilink") as [Dan Gore](Dan_Gore "wikilink")
-   [Craig Anton](Craig_Anton "wikilink") as [Craig](Craig "wikilink")
-   [Lee Eisenberg](Lee_Eisenberg "wikilink") as [Gino](Gino "wikilink")
    (Delivery Man \#1)
-   [Jason Kessler](Jason_Kessler "wikilink") as Delivery Man \#2
-   Jeremy Radin as Delivery Man \#3
-   [Gene Stupnitsky](Gene_Stupnitsky "wikilink") as
    [Leo](Leo "wikilink") (Delivery Man \#4)
-   [Vivianne Collins](Vivianne_Collins "wikilink") as
    [Grace](Grace "wikilink") (Deleted Scenes)
-   Miriam Tolan as Fake Tina Fey (Uncredited)
-   [Conan O\'Brien](Conan_O'Brien "wikilink") as Himself (Uncredited)

References
----------

<references/>

[^1]: Schur, Michael (Writer). 2006. \"The Client\" \[Commentary
    track\], *The Office* Season Two (US/NBC Version) \[DVD\], Los
    Angeles, CA: [Universal](Universal_Studios "wikilink").
