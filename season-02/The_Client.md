# The Client

**\"The Client\"** is the seventh episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 13th overall.
It was written by Paul Lieberstein and directed by [Greg
Daniels](Greg_Daniels "wikilink"). It first aired on November 8, 2005.
It was viewed by 7.5 million people.

Synopsis
--------

[Michael](Michael_Scott "wikilink") and [Jan](Jan_Levinson "wikilink")
take a client to [Chili\'s](Chili's "wikilink") for an important
meeting. Jan is disgusted with Michael\'s antics and general refusal to
talk business but discovers at the end of the day that there is a method
to his madness when the client bonds with Michael, allowing him to close
the deal. Afterwards in the parking lot, Michael and recently-divorced
Jan share a kiss.

Meanwhile, [Pam](Pam_Beesly "wikilink") finds a screenplay written by
Michael, and [Jim](Jim_Halpert "wikilink") leads the office staff in a
table read of the script. An editing error in the script reveals that
Michael based the incompetent sidekick on [Dwight](Dwight "wikilink"),
who quickly shuts down the exercise to invite everyone to set off
fireworks outside. Jim and Pam break off their respective evening plans
to enjoy an impromptu dinner on the roof and watch the firework show,
followed by swaying to music on his iPod. The next day, Jim makes a joke
about their \"first date\", and Pam is too quick to tell him bluntly it
was not a date, and that swaying is not dancing. Jim makes a snide
comment about Roy and Pam\'s first date, at which he left her at a
hockey game. Pam is offended by his comment and walks away.

The following morning, Dwight (who has slept in the office) and Angela
independently observe Jan returning to her car (still parked at Dunder
Mifflin), and word quickly travels around the office. Michael initially
tells the documentary crew that \"nothing happened,\" that they talked
for several hours before falling asleep, but his story gradually changes
to imply that they did more than just talk. Jan calls, clearly regretful
for what she did the previous evening, but Michael refuses to accept her
change of heart.

Deleted scenes
--------------

-   Michael tells an uninterested Pam about his new egg yolk diet.
    (Jenna Fischer breaks character and starts laughing.)
-   Michael empties the kitchen refrigerator of all foods containing
    carbs.
-   Michael jokes around with an unamused Jan regarding plans for the
    client meeting. In a talking head interview, Michael explains that
    he\'s a closer.
-   Jim visits the reception desk and asks Pam what she\'s doing. She
    admits she was just staring at her desk.
-   Phyllis tries gossip with Stanley, but Stanley ignores her.
-   The table reading reaches the last page of *Threat Level: Midnight*,
    which ends abruptly with Agent Michael Scarn jumping out of an
    airplane without a parachute.
-   Jim describes to the camera his worst first date. He\'s being
    evasive and vague about details, but gradually it becomes clear that
    he\'s describing a date he went on with Pam before he learned she
    was engaged to Roy. He says he found out later that she didn\'t even
    think of it as a real date and she was in love with another guy. Jim
    concludes, \"My best first date was also my worst first date.\"

Trivia- Caution, here be spoilers
---------------------------------

-   The illustrations which accompany Michael\'s screenplay were drawn
    by producer [Greg Daniels](Greg_Daniels "wikilink"). The crew
    initially used a professional artist, but the artist couldn\'t make
    the drawing bad enough.[^1]
-   Michael refers to the name of his screenplay, *Threat Level:
    Midnight*, in the episodes [Product
    Recall](Product_Recall "wikilink") and [Money](Money "wikilink").
-   All of the lines that the characters read from the \'\'Threat Level:
    Midnight \'\'script were completely improvised by the actors, and
    Brian Baumgartner ([Kevin Malone](Kevin_Malone "wikilink")) remarked
    that it was difficult to improvise the lines and give the impression
    that the character was actually reading the script.
-   [Devon](Devon "wikilink") appears in the background of some deleted
    scenes even though he was fired in the episode
    *[Halloween](Halloween "wikilink")*. The original plan was to air
    this episode before *[Halloween](Halloween "wikilink")*, but NBC
    changed the airing
    order.[1](http://boards.nbc.com/nbc/lofiversion/index.php/t750975.html)
-   Dwight is the only person to follow Michael\'s instructions. Michael
    tells the office to \"wish us luck\" and to \"sit tight until I
    return\", both of which only Dwight does.
-   Michael\'s lucky tie was sold at auction by NBC as a charity
    fundraiser.
    [2](http://www.officetally.com/nbcs-the-office-auction-starts-jan-21)
-   Near the start of the episode, the group does \"worst first dates\",
    Oscar tells his story about a date with the woman with a checklist.
    At this point in the series, the writers had not chosen him to be
    gay yet. He could be making it all up. It is possible he really did
    go on this date, however, since in season 9 he says, \"When this
    thing (documentary) started I was still having sex with women. As
    was Kevin, I believe.\"
-   Chili\'s makes its second appearance in the series however it is not
    the same Chili\'s from the Dundies

Amusing details
---------------

-   In his pitch to Christian, Michael casually notes that he knows how
    many schools and hospitals there are in the county. Jan included
    this information in her briefing at the start of the episode.
-   Watch Jan\'s face change as she slowly realizes that Michael is
    closing the deal.
-   Even though Jan refused to agree on a secret signal, Michael uses an
    improvised signal to get her to keep quiet as he closes the sale.
-   Michael drives through a stop sign on the way to Chili\'s.
-   In the script reading Michael\'s character's love interest,
    Catherine Zeta-Jones, is reading back messages for his character.
    This could allude that Michael used his real life receptionist, Pam,
    as a model for his fictional love interest
-   The Chili\'s in this episode is not the same one from the Dundies.

Cultural references
-------------------

-   *[Casual Fridays](Wikipedia:Casual_Friday "wikilink")* is the
    practice of dressing less formally for work on Fridays. It became
    popular in the late 1990s.
-   *[USA Today](Wikipedia:USA_Today "wikilink")* is a national
    newspaper known for its colorful charts.
-   *[Radisson](Wikipedia:Radisson_Hotels "wikilink")* is a mid-range
    hotel chain. In reality, there is a Radisson in downtown Scranton.
-   *[Chili\'s](Wikipedia:Chili's "wikilink")* is a chain of casual
    dining restaurants. In this episode, Michael says that there\'s a
    Chili\'s just a few blocks away. In reality, the nearest Chili\'s to
    Scranton is about 30 minutes\' drive away in Wilkes-Barre.
-   To *burn the midnight oil* is to work late into the evening. Michael
    modifies the phrase in order to sound clever.
-   An *Awesome Blossom®* is a battered and deep-fried onion. A single
    blossom contains 203 grams of fat.
-   The story about the lighthouse and the naval vessel is a well-known
    joke. [Read it here](http://www.snopes.com/military/lighthouse.asp).
-   *[Saturn](Wikipedia:Saturn_Corporation "wikilink")*,
    *[Escort](Wikipedia:Ford_Escort_(North_America) "wikilink")* and
    *[Probe](Wikipedia:Ford_Probe "wikilink")* are models of automobile.
    The joke is based on puns: *Saturn* is the sixth planet from the
    sun. An *escort* is a euphemism for a prostitute. A *probe* is a
    device that is inserted into something to obtain information.
-   Abercrombie & Fitch is an American clothing store attracted toward
    teenagers and college students. Pam asks Michael if his jeans are
    from Abercrombie.
-   *[Catherine Zeta-Jones](Wikipedia:Catherine_Zeta-Jones "wikilink")*
    is an Academy Award-winning actress.
-   *[Baby back ribs](Wikipedia:Pork_ribs "wikilink")* are pork ribs,
    typically prepared barbecued. Chili\'s has a special song to
    advertise its baby back ribs, which Michael and Christian perform
    together.
-   *[Oklahoma!](Wikipedia:Oklahoma! "wikilink")* is a prize-winning
    musical.
-   *Samuel L. Chan* is a combination of the names of actors *[Samuel L.
    Jackson](Wikipedia:Samuel_L._Jackson "wikilink")* and *[Jackie
    Chan](Wikipedia:Jackie_Chan "wikilink")*. The character speaks in a
    bizarre mixture of Asian and black caricatures. The grammatical
    errors (incorrect verb forms) are associated with Asian speech, and
    *word* and *brotha* are associated with black urban speech. The term
    *word* is used incorrectly: It is a term of agreement.
-   Kevin gives Michael\'s screenplay *two thumbs down*. Thumbs
    up/thumbs down is the movie rating system used and trademarked by
    Roger Ebert and Gene Siskel in their movie review program *[At the
    Movies](Wikipedia:At_the_Movies_(U.S._TV_series) "wikilink")*.
-   *Goldenface* may be derived from the James Bond movie
    *[Goldfinger](Wikipedia:Goldfinger_(film) "wikilink")*.
-   *[Truth or Dare?](Wikipedia:Truth_or_Dare? "wikilink")* is an
    adolescent party game in which players challenge each other with
    embarrassing questions and embarrassing tasks.
-   To *kiss and tell* is to reveal information about a romantic
    encounter.
-   Jan accuses Michael of *slipping her something*. The full phrase is
    to *slip someone a
    [mickey](Wikipedia:Mickey_Finn_(drugs) "wikilink")*, to
    surreptitiously place an incapacitating drug into someone\'s drink
    in order to take advantage of the person.
-   *[Rocky](Wikipedia:Rocky "wikilink")* is a movie about the boxer
    [Rocky Balboa](Wikipedia:Rocky_Balboa "wikilink"). In the movie, the
    title character [drinks raw eggs for
    breakfast](http://www.youtube.com/watch?v=3EYZCZvV7WM).
-   Michael throws away bread from the refrigerator. The [Atkins
    Diet](Wikipedia:Atkins_Nutritional_Approach "wikilink"), which
    emphasizes avoidance of carbohydrates, became wildly popular
    in 2003.
-   The movie *[Glengarry Glenn
    Ross](Wikipedia:Glengarry_Glenn_Ross_(film) "wikilink")* is about a
    highly competitive group of salesmen. Michael quotes the catch
    phrase *always be closing* and refers to the *Glengarry leads*
    scene.
-   *[Cugino\'s](Miscellaneous_Scranton_locations#Cugino's "wikilink")*
    is an Italian restaurant in nearby Dunmore.

Quotes
------

:   See *[The Client Quotes](The_Client_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring Cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited)

### Guest Cast

-   [Tim Meadows](wikipedia:Tim_Meadows "wikilink") as
    [Christian](Clients_of_Dunder_Mifflin#Christian "wikilink")
-   Allyson Everitt

References
----------

<references/>
\

[^1]: Season 2 DVD commentary, *The Client*.
