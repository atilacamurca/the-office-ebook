# The Secret

**\"The Secret\"** is the thirteenth episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 19th overall.
It was written by [Lee Eisenberg](Lee_Eisenberg "wikilink") and [Gene
Stupnitsky](Gene_Stupnitsky "wikilink") and directed by [Dennie
Gordon](Dennie_Gordon "wikilink"). It first aired on January 19, 2006.
It was viewed by 8.7 million people.

Synopsis
--------

The show starts of with Jim telling a joke to Pam, which Michael then
goes to tell everyone else that he can, only he keeps messing it up.
Meanwhile, Dwight is assigning everyone their spring cleaning tasks, but
soon discovers that Oscar isn\'t at work. He tells Michael and they
proceed to call his house. Oscar says that he has the flu, and Dwight
and Michael interrogate him about his symptoms to see if he is lying.
Dwight is given permission to investigate Oscar\'s sickness.

Afterwards, Pam is talking to Kelly about her wedding stuff, like her
hair and veil. Pam\'s hair is down at the time and Michael notices,
calling it sexy. Pam puts it back up. On his way to the breakroom he
tells Jim that it must be torture for him, knowing what he knows of
Jim\'s feelings for Pam. Jim tells Michael that this subject needs to
remain a secret.

Since Dwight is investigating Oscar, he gives Ryan the task of spring
cleaning. In the meantime, Jim and Michael get a soda, and Michael asks
about Pam in code, i.e., he spells her name, while Stanley is in the
room, causing Jim to leave. Dwight continues to call Oscar. Dwight then
tells the camera that there are ways to tell if someone is lying but you
must speak face-to-face, and since Dwight talked to Oscar over the
phone, he cannot tell. Next, Michael continues to talk to Jim, but
Dwight interrupts and asks what the secret is. Michael then lies to
Dwight, doing everything on Dwight\'s list of ways to determine if
someone is lying. Michael takes Jim to Hooters, and wants to talk about
Pam. Dwight talks to Pam about Oscar\'s sick phone call. He asks about
Oscar\'s sniffling, and rummages through all of the jelly beans, taking
the remaining black ones, forcing Pam to throw the remainder out. Later
on, Michael keeps asking about Pam, he also tells the staff that it was
his birthday. They return to the office, and Dwight updates Michael on
the Oscar situation.

Michael eventually reveals Jim\'s secret and Jim is forced to admit his
crush, wanting to tell Pam himself rather than let her find out from
Michael or someone else who knows. However, he plays it down and tells
her that while he used to have a crush on her, he is over it.
Unbeknownst to Jim, Michael later adds to the ambiguity by implying to
Pam that Jim\'s crush on her had not really ended. Pam and Jim leave
together, however, and she does not seem to feel too awkward.

Trivia
------

-   There are many secrets in this episode: Jim\'s crush on Pam,
    Oscar\'s gay relationship, and Dwight and Angela\'s relationship.
-   As of this episode, [Leslie David
    Baker](Leslie_David_Baker "wikilink") ([Stanley
    Hudson](Stanley_Hudson "wikilink")), [Brian
    Baumgartner](Brian_Baumgartner "wikilink") ([Kevin
    Malone](Kevin_Malone "wikilink")), [Kate
    Flannery](Kate_Flannery "wikilink") ([Meredith
    Palmer](Meredith_Palmer "wikilink")), [Angela
    Kinsey](Angela_Kinsey "wikilink") ([Angela
    Martin](Angela_Martin "wikilink")), [Oscar
    Nunez](Oscar_Nunez "wikilink") ([Oscar
    Martinez](Oscar_Martinez "wikilink")), and [Phyllis
    Smith](Phyllis_Smith "wikilink") ([Phyllis
    Lapin](Phyllis_Lapin "wikilink")) are listed as cast regulars.
-   The shooting location for Oscar\'s house is 5232 Longridge Ave, Los
    Angeles.
-   Jim orders a ham and cheese sandwich. This is the first time we see
    Jim\'s lunch routine. It is pointed out by Ryan in \"Dwight\'s
    Speech.\"
-   In this episode Jim tells Pam that \"I told Michael I had a crush on
    you when you first started here.\" This contradicts [Launch
    Party](Launch_Party "wikilink"), when Jim tells Pam the moment he
    knew he liked her was on Jim\'s first day, Pam walked Jim over to
    his desk and told Jim \"Enjoy this moment because you\'re never
    going to go back to this time before you met your desk mate
    Dwight.\"

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Michael plays with his new putting toy. On his second attempt, the
    ball goes under the bookshelf. While Dwight retrieves the ball,
    Michael accidentally knocks his \"World\'s Best Boss\" mug off his
    desk, destroying it. He retrieves a new mug from the drawer.
-   In a talking head interview, Dwight expounds on the hazards posed by
    dust bunnies.
-   Michael surveys his worker bees and decides that he is the King Bee,
    nay, the Ace Bee.
-   Ryan shows the camera an unfinished [TV
    Guide](Wikipedia:TV_Guide "wikilink") crossword puzzle from
    Michael\'s office. In answer to \"[Mary-Kate and Ashley
    \_\_\_\_\_](Wikipedia:Mary-Kate_and_Ashley_Olsen "wikilink") (5
    letters)\" Michael wrote
    \"[Judd?](Wikipedia:Ashley_Judd "wikilink")\"
-   In a talking head interview, Michael explains that he rushed a few
    fraternities but decided not to accept any offers, which turned out
    well since no offers were made.
-   Dwight tries to extract information about Oscar from Ryan.
-   Michael buys Jim a Hooters t-shirt from the gift shop.

Amusing details
---------------

-   When Dwight asks Ryan if he can handle running spring cleaning, he
    flashes a page from his notebook. Dwight takes his investigation so
    seriously, he even made himself a fake badge.
-   Immediately after Dwight brags in a talking head interview that he
    can spot a liar, he fails to detect that Michael is lying to him.
-   Michael copies Jim by drinking grape soda and later styles his hair
    like him in attempt to bond with him.
-   Jim orders a ham and cheese sandwich from Hooters, which is what he
    said that he was going to eat in the break room for lunch earlier
    that day anyway.
-   The birthday apron Jim wears has balloons under it to give the
    impression that the person wearing it has boobs.
-   During Michael\'s talking head interview defending his magic shop
    spending spree, he is wearing a false thumb, which magicians use to
    conceal objects.

Connections to previous episodes
--------------------------------

-   In [Sexual Harassment](Sexual_Harassment "wikilink") Michael says
    that he and [Todd Packer](Todd_Packer "wikilink") use the \"we\'re
    brothers\" line to get women.

Cultural references
-------------------

-   *Spring cleaning* is the tradition of cleaning one\'s house at the
    beginning of the spring season.
-   Michael sings *Our lips are sealed* by the Go-Go\'s but
    misidentifies the artist as The Bangles.
-   Michael announces, \"*It\'s Grrrrrrape!*\", mimicking cereal mascot
    *Tony the Tiger*\'s exclamation \"They\'re grrreat!\"
-   *Bogey at 3 o\'clock* is military jargon for \"unidentified object
    on the right.\"
-   Michael\'s proclamation \"*We are not worthy!*\" and accompanying
    mock prostration come from the recurring *Wayne\'s World* sketch on
    *Saturday Night Live*.
-   There is no such movie as *More Secrets of a Call Girl*.
-   The *Hokey Pokey* is a dancing song. It is performed pretty much as
    shown in the episode, except that \"front side\" is more typically
    mundane body parts such as \"left hand\". Coincidentally, the
    original copyright holders are from Scranton.

Quotes
------

:   see *[The Secret Quotes](The_Secret_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring cast

-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Tom W. Chick](Tom_W._Chick "wikilink") as [Gil](Gil "wikilink")

### Guest cast

-   Lindsay Stoddart as Dana (Hooters waitress)

\
