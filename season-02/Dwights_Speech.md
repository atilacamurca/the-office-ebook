# Dwight\'s Speech

**\"Dwight\'s Speech\"** is the seventeenth episode of the second season
of \'\'[The Office](The_Office_(US) "wikilink") \'\'and the 23rd
overall. It was written by [Paul
Lieberstein](Paul_Lieberstein "wikilink") and directed by [Charles
McDougall](Charles_McDougall "wikilink"). It first aired on March 2,
2006. It was viewed by 8.4 million people.

Synopsis
--------

Dwight, the company\'s top salesman of the year, worries over a speech
he must give at a ceremony honoring him. Jim \"helps\" him by giving him
suggestions and speech notes from speeches of famous dictators.

Kelly hints to Ryan she\'d like to get married, but Ryan hurts her
feelings by saying he doesn\'t ever plan to get married. When Pam tries
to tell Ryan how Kelly feels about him, he coldly says, \"I know what I
said.\"

Jim, upset at having to hear wedding planning at Pam\'s desk, calls a
travel agent and arranges a trip to Australia. When he tells Pam about
it later, she is initially excited, until he tells her that he\'s
leaving June 8th, meaning he will not be able to attend her wedding.

Dwight\'s Speech: \[bangs fists multiple times\] BLOOD ALONE MOVES THE
WHEELS OF HISTORY! \[pause\] Have you ever asked yourselves in an hour
of meditation - which everyone finds during the day - how long we have
been striving for greatness? \[bangs fist\] Not only the years we\'ve
been at war - the war of work - but from the moment as a child, when we
realized that the world could be conquered. It has been a lifetime
struggle \[bangs fists again\] a never-ending fight, I say to you
\[bangs again\] and you will understand that it is a privilege to fight.
\[light applause\] WE ARE WARRIORS! \[applause\] Salesmen of
north-eastern Pennsylvania, I ask you once more: Rise and be worthy of
this historical hour. \[even bigger applause as Dwight laughs
maniacally.\] No revolution is worth anything unless it can defend
itself! \[applause\] Some people will tell you \"salesman\" is a bad
word. They\'ll conjure up images of used car dealers, and door to door
charlatans. This is our duty to change their perception. I say,
salesmen- and women- of the world\... unite. We must never acquiesce,
for it is together\... TOGETHER THAT WE PREVAIL. WE MUST NEVER CEDE
CONTROL OF THE MOTHERLAND\... FOR IT IS\...! Audience: \...TOGETHER THAT
WE PREVAIL! \[thunderous applause\] \[bangs fists\].

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Dwight comes to work wearing sunglasses and tries to get Jim to
    complete the sentence, \"My future is so bright\...\"
-   Dwight practices his speech, and Michael criticizes his delivery.
-   After Dwight\'s failed announcement, Michael tries to find a joke
    \"that not even you \[Dwight\] can ruin.\" Dwight begins telling the
    staff a sailor joke, but Meredith stops him because her nephew is
    serving in Iraq.
-   In the speaking exercise, Phyllis toasts to her good fortune for
    having found a man she is in love with. Ryan describes how he hopes
    to land a \"challenging full-time or part-time position somewhere
    else.\" This is met with applause from the other staff, but this
    troubles Michael. In a talking-head interview, Michael explains that
    the fact that Ryan (as a temporary employee) can leave at any time
    scares him. This is the first time Michael has shown anything but
    admiration for Ryan.
-   Ryan brings Pam the wrong type of stamps for her wedding
    invitations.
-   Michael and Dwight look for the meeting hall. Their first stop is a
    numismatic meeting, then a science fiction convention, featuring the
    cast of *[Battlestar
    Galactica](Wikipedia:Battlestar_Galactica_(2004_TV_series) "wikilink")*.
    Dwight gushes in adoration for the character of
    [Starbuck](Wikipedia:Kara_Thrace "wikilink"), and Michael asks him,
    \"Are you a twelve-year-old girl?\"
-   An extended scene of Michael\'s unfunny and extremely awkward
    speech.

Trivia
------

:   *For a list of songs featured in this episode, see [List of songs
    that appear in
    episodes](List_of_songs_that_appear_in_episodes#Dwight.27s_Speech "wikilink").*

-   In Creed\'s talking head, he says, \"我的中華的朋友你好\" (\"Hello,
    my Chinese friends\") in an accent so thick it is practically
    unintelligible. What\'s more, his shout-out is in Mandarin, even
    though the primary language in Hong Kong is Cantonese.
-   Global Television Network in Canada aired this episode the day
    before it aired in the United States due to a scheduling conflict
    with *[Skating with
    Celebrities](Wikipedia:Skating_with_Celebrities "wikilink")*.
-   Portions of Dwight\'s speech are drawn from the speeches of
    [Lenin](Wikipedia:Vladimir_Lenin "wikilink") (\"No revolution is
    worth anything unless it can defend itself\") and [Benito
    Mussolini](Wikipedia:Benito_Mussolini "wikilink") (\"Blood alone
    moves the wheels of history\"; \"It is a privilege to fight \[with
    them\]!\"). He also draws on [Karl
    Marx](Wikipedia:Karl_Marx "wikilink") , though it is from the book
    The Communist Manifesto and not a speech (\"Workers \[/salesmen and
    women\] of the world, Unite\").
-   Dwight\'s award may have come at the expense of Jim. In [Diversity
    Day](Diversity_Day "wikilink"), Jim loses a sale that normally makes
    up 25% of his yearly commission to Dwight. More than likely this had
    a significant impact on making Dwight the number one salesperson and
    Jim number nine.
-   In this episode, Ryan tells us that Jim eats a ham and cheese
    sandwich everyday. However, in a deleted scene from [The
    Alliance](The_Alliance "wikilink"), Jim brings a tuna sandwich for
    lunch.
-   According to writer [Paul Lieberstein](Paul_Lieberstein "wikilink"),
    Jim never did go on his Australian
    vacation.[1](http://www.givememyremote.com/remote/jim-halpertdid-he-or-didnt-he/)
-   About halfway through Michael\'s Improvised speech, a shot can be
    seen mirrored, evidenced by the backwards nametag of the closest guy
    named Luke

Amusing details
---------------

-   On the inside door of Michael\'s cabinet is a framed clipping from
    the Dunder Mifflin staff newsletter announcing the promotion of
    \"Jan Gould\". Michael\'s crush on Jan goes back very far.
-   Michael\'s North East Sales Association Top Salesman plaque
    incorrectly spells the word \"association\" with an \"s\" instead of
    a \"c.\"
-   When Stanley tells his wife, \"Get the wallpaper,\" Creed sits at
    his desk pretending to play the drums.
-   In the conference room scene, after Dwight pulls out his index
    cards, Angela is the only one looking forward to his speech.
-   During the cold open, when Michael is throwing the football to
    Dwight he hits Jim\'s desk knocking a bunch of frames and pens onto
    the boxes in front of Jim\'s and Dwight\'s desk. Then right before
    Jim intercepts a subsequent throw you can see that there are no
    boxes anymore.
-   It is implied that Michael has never been to a wedding in his adult
    life despite being in his 40s.
-   Even though Oscar is the one who set the thermostat to a colder
    temprature, he can be seen wearing his jacket when Dwight and
    Michael are leaving.
-   Dwight says at one point that he lost a spelling bee to Raj Patel
    when he misspelled the word \"failure\". This line was completely
    improvised by Rainn Wilson.

Cultural references
-------------------

-   *Heisman* is the name of a football trophy. Michael strikes the pose
    depicted on the award.
-   When Pam teases Jim in the kitchen for losing to Dwight, Jim drinks
    from a mug that says \"Together building a better Scranton.
    Restoring the Pride.\" This was the motto of the Scranton downtown
    revitalization project.
-   *Club Med* is a chain of resorts.
-   *Hedonism Resorts* is a pair of resorts in Jamaica, known for its
    naturism/nudism.
-   *V.A.* is short for *Veterans Administration*. VA buildings are
    often rented for events.
-   *Good Morning, Vietnam* is the title and catch phrase from a 1987
    movie.
-   *The black guy from the \"Police Academy\" movies* is actor *Michael
    Winslow* who has a talent for making sound effects using his voice.

Quotes
------

-   **[Dwight](Dwight_Schrute "wikilink")**: *\"Blood alone moves the
    wheels of history! Have you ever asked yourselves in an hour of
    meditation, which everyone finds during the day,how long we have
    been striving for greatness? Not only the years we\'ve been at war,
    the war of work, but from the moment as a child when we realized
    that the world could be conquered. It has been a lifetime struggle.
    A never-ending fight. I say to you, and you will understand that it
    is a privilege to fight! We are warriors! Salesmen of north-eastern
    Pennsylvania, I ask you once more: Rise and be worthy of this
    historical hour! No revolution is worth anything if it cannot defend
    itself! Some people will tell you salesman is a bad word. They\'ll
    conjure up images of used car dealers and door to door charlatans.
    This is our duty: to change their perception. I say salesmen\... and
    women of the world unite! We must never acquiesce for it is
    together, TOGETHER, THAT WE PREVAIL! We must never cede control of
    the motherland!\"*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting Cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring Cast

-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")

### Guest Cast

-   Ben Carroll as Announcer
-   John Kelly as Salesman
-   Sean Lake as Sci-Fi Attendee (Uncredited)
-   Randy Vinneau as Conference-goer (Uncredited)

External Links
--------------

-   [Jenna Fischer\'s TV Guide
    blog](http://www.tvguide.com/News-Views/Interviews-Features/Article/default.aspx?posting=%7BA4297FDC-0F5B-4E71-AD55-2E2C62CE8EB3%7D)
