# The Dundies

**\"The Dundies\"** is the first episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 7th overall. It
was written by Mindy Kaling and directed Greg Daniels . It first aired
on September 20, 2005, It was viewed by 9 million people.

Synopsis
--------

It\'s time for \"The [Dundies](Dundie "wikilink")\",
[Michael](Michael_Scott "wikilink")\'s annual embarrassing awards show
for the Dunder-Mifflin Scranton employees, which takes place at the
local [Chili\'s](Chili's "wikilink") restaurant. He thinks they love the
awards and his comedy act, but in fact, they\'re dreaded.

Phyllis, Pam, and Kelly chat and giggle by the water cooler. Dwight asks
them what the joke is, and learns that somebody wrote something on the
ladies\' room wall about Michael. He demands to know what it is, but the
women won\'t tell. Dwight\'s attempts to determine what is written on
the wall are embarrassingly unsuccessful.

[Jan](Jan_Levinson "wikilink") tells Michael that corporate will not
fund the party, which breaks his hopes of a drunken show. Michael
invites friends and family to the show to quiet rumors.

Michael asks [Pam](Pam_Beesly "wikilink") to go through the old Dundies
footage for highlights. From an earlier Dundies, we see Michael award
Pam the \"World\'s Longest Engagement\" award. Jim tries to convince
Michael to give Pam a different award this year.

The awards begin with Michael attempting to perform a rap, but he can\'t
keep up with the lyrics. Oscars sums up the Dundies \"as like a kid\'s
birthday party, where there\'s nothing for you to do, but the kid\'s
having a good time.\" As everyone prepares to order, Michael tells them
they have to pay for their meals and drinks. Michael\'s jokes fall flat;
Dwight\'s contributions don\'t help matters any.
[Darryl](Darryl_Philbin "wikilink") and [Roy](Roy_Anderson "wikilink"),
concluding that the awards are lame, leave early, taking Pam with them.
In the parking lot, Pam and Roy have an argument, and Pam returns to the
awards show, just in time for Michael\'s caricature of a Chinese man
named Ping.

While Michael awards Dundies to various members of the office staff, Pam
consumes a beer and two margaritas and shows signs of being drunk. As he
sings another song, Michael gets heckled by some other customers.
Humiliated, Michael decides to wrap it up, but an intoxicated Pam (along
with [Jim](Jim_Halpert "wikilink")) encourages him to finish the show,
which he does. Pam braces herself for her annual \"World\'s Longest
Engagement\" award, but is relieved to get the \"Whitest Sneakers\"
award. Pam thanks Michael and, after an emotional acceptance speech,
drunkenly kisses Jim on the lips.

As Jim recaps the Dundies for the documentary camera, Pam drunkenly
falls off her stool. Dwight springs into action, but his assistance is
unwelcome. In a talking-head interview, the manager of the Chili\'s
restaurant explains that Pam is no longer welcome at the restaurant
chain due to her behavior.

While waiting for designated driver [Angela](Angela_Martin "wikilink")
to arrive, Pam admits to Jim that it was she who wrote the graffiti on
the ladies\' room wall. She also begins to ask Jim a question, but stops
herself when she notices the cameras. Jim helps Pam into Angela\'s car
and smiles as he watches them leave.

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Extension of Michael\'s \"TMI\" talking-head interview.
-   In talking-head interviews:
    -   Toby is glad that he hasn\'t won any Dundies.
    -   Kelly describes having dinner with Toby at the Dundies.
    -   Angela is annoyed at always being the designated driver.
    -   Jim explains that Michael puts more effort into the Dundies than
        anything else.
    -   Dwight describes being a whistleblower.
    -   Kevin finds Ping funny.
    -   Michael tries to guess what the letters
        [O.P.P.](Wikipedia:O.P.P._(song) "wikilink") stand for.
-   Dwight mocks the women as they chat by the water cooler.
-   Michael asks Jim for ideas for Kevin\'s Dundie. When Dwight informs
    Michael of the graffiti in the ladies\' room, Michael instructs him
    to find out what it says.
-   Dwight tries to peek into the ladies\' room without going in. (An
    alternate take of the scene in which Phyllis catches him in the
    ladies\' room.)
-   Dwight interrogates Angela about the graffiti.
-   Kelly and Oscar ask Michael not to do his Ping impression. Michael
    is unrepentant.
-   Extension of the scene in which Phyllis catches Dwight in the
    ladies\' room. Phyllis goes to Michael and demands that Dwight be
    disciplined.
-   Dwight abruptly shuts off the accounting department computers to get
    them to leave for the Dundies.
-   Michael prepares backstage at the Dundies.
-   Extension of the opening Dundies rap.
-   Kevin talks with Pam and Roy about the \"Longest Engagement\" award.
-   Michael hands out the \"Most creative writing on the ladies\' room
    wall\" Dundie.
-   Toby tells Michael that Ryan\'s award was inappropriate..
-   Extension of Dwight tending to Pam\'s fall off a stool.
-   The Chili\'s manager orders Michael to leave, threatened to have a
    black 6 foot, 200 pound bouncer pound his ass.

Trivia
------

> *For a list of Dundie winners, see [Dundie](Dundie "wikilink").*

-   The episode was filmed at the Chili\'s restaurant set in
    Burbank.[^1]
-   One of two scenes with the Chili\'s manager that made the final cut
    almost didn\'t get shot at all. The crew started packing up at the
    end of the day, and Christopher T. Wood had to remind the director
    that they hadn\'t shot the talking head yet.
    [1](http://keithp23.blogspot.com/2007/08/two-cents-five-questions-with_15.html)
    The manager is also seen telling Dwight to put his clothes back on,
    after Dwight takes his shirt off to put under Pam\'s head following
    her fall at the bar.
-   Devon, the employee fired in the Halloween episode, is first
    mentioned here. He previously appeared (without being mentioned by
    name) in several first-season episodes.
-   This episode introduces the recurring joke of Michael\'s unusual
    affection for Ryan: He awards Ryan the \"Hottest in the office\"
    award and slaps him on the buttocks. The writers intended the
    episode *[The Fire](The_Fire "wikilink")* to be the origin of
    Michael\'s weird affection for him, but NBC requested that the
    season begin with *The Dundies*.
-   In the spat between Roy and Pam, the actors were under the
    impression that the scene would be played silently, so they
    improvised an argument.[^2]
-   In the original script, Pam vomits and Dwight responds, \"A woman
    has vomited!\" The completed script was not available to Chili\'s
    until shooting had already started, and they objected to a customer
    vomiting in their restaurant. Chili\'s withdrew its permission to
    shoot, but, after a few hours, Steve Carell developed a compromise:
    Pam falls off her bar stool and Dwight responds, \"A woman has had a
    seizure!\"[^3][^4]
-   In the original script, Pam was to be overserved alcohol by the
    Chili\'s staff. Chili\'s didn\'t want this in the episode either, so
    the writers had Pam steal drinks off other people\'s tables
    instead.[^5]
-   The extras playing the waitstaff at Chili\'s are all actual Chili\'s
    workers.[^6]
-   Since Jenna Fischer doesn\'t drink much in real life, B.J. Novak
    took her out so she could get drunk while Novak described to her how
    she was behaving and how it didn\'t match her own perception. She
    drew upon this experience for her performance.[^7]
-   The episode ends with Elton John\'s *Tiny Dancer* playing in the
    background, the second time in the series that background music is
    added. (The *[Pilot](Pilot "wikilink")* also included background
    music.)
-   The movie *[Almost Famous](Wikipedia:Almost_Famous "wikilink")* uses
    the song *Tiny Dancer* in a notable scene. [Rainn
    Wilson](Rainn_Wilson "wikilink") appeared in the film as one of the
    editors of *[Rolling
    Stone](Wikipedia:Rolling_Stone_magazine "wikilink")* magazine.
-   This was one of six episodes that was submitted to Emmy voters for
    Best Comedy consideration, which they went on to win. The other five
    were *[Booze Cruise](Booze_Cruise "wikilink")*, *[Christmas
    Party](Christmas_Party "wikilink")*, *[The
    Injury](The_Injury "wikilink")*, *[The
    Secret](The_Secret "wikilink")*, and *[Valentine\'s
    Day](Valentine's_Day "wikilink")*.
-   Originally, the characters were planned to go to the [Outback
    Steakhouse](Wikipedia:Outback_Steakhouse "wikilink") instead of
    Chili\'s. Director and Co Creator Ricky Gervais noted a
    then-developing deal between *The Office* and the Outback chain in a
    2005 audio commentary for *[The
    Simpsons](Wikipedia:The_Simpsons "wikilink")* episode \"[Bart Sells
    His Soul](Wikipedia:Bart_Sells_His_Soul "wikilink")\", which
    contains a subplot about a similar family restaurant and which
    Daniels wrote.[^8]
-   Strangely, Michael\'s hair is much thicker and fuller compared to
    his receding hairline and balding that is seen in season one.
-   This episode marks the first physical appearance of Stanley\'s wife
    Teri Hudson.

Connections to previous episodes
--------------------------------

-   This episode reprises Dwight\'s status as a volunteer sheriff\'s
    deputy, initially introduced in a talking head interview in the
    *[Pilot](Pilot "wikilink")*.
-   Michael adjusts a Dundie in the opening credits of every episode.
    The Dundies were first mentioned by name in *[The
    Alliance](The_Alliance "wikilink")* when Dwight tries to help
    Michael write a message in Meredith\'s birthday card.

Cultural references
-------------------

-   *Fat Albert* is a character created by Bill Cosby. Michael does an
    impression of the character when he calls Jim *Fat Halpert*.
-   *T.M.I* is an outdated slang phrase. Short for \"Too much
    information\", it is used when someone reveals too much personal
    information about himself. *Don\'t go there* is another outdated
    slang phrase, used to indicate the speaker\'s discomfort with at a
    new topic of conversation.
-   *Show me the money!* is a catch phrase from the movie *Jerry
    Maguire*.
-   *Dave Barry* is a humorist who used to write for the *Miami Herald*.
-   *Carnac* is a comic character of late night talk show host Johnny
    Carson. Wearing a turban, Carnac would use his psychic powers to
    answer a question written inside a sealed envelope.
-   The *PLO* is the [Palestine Liberation
    Organization](Wikipedia:Palestine_Liberation_Organization "wikilink")
    and the *IRA* is the [Irish Republican
    Army](Wikipedia:Irish_Republican_Army "wikilink"). Neither are
    businesses, but nationalist movements that are sometimes seen as
    terrorist groups.
-   *Chili\'s* is a chain of casual dining restaurants.
-   *Wrap it up music* is Dwight\'s way of describing music played at an
    awards ceremony to indicate that the award recipient has talked too
    long and should leave the stage.
-   *Poor Richard\'s Pub* is a pub in Scranton.
-   A *busy beaver* is a hardworking person. Michael\'s error is an
    inadvertent pun on a vulgar sense of the word *beaver* for female
    genitalia.
-   *Tight ass* is a rude term for a person who is uptight. *Caboose* is
    slang for the buttocks.
-   *Keds* is a brand of casual athletic shoes.
-   *Xerox* is informally used as a verb meaning \"to use a copy
    machine.\"
-   *[Goldschläger](Wikipedia:Goldschläger "wikilink")* is a brand of
    schnapps notable for the tiny gold flakes floating in the bottle.
-   *[Foster\'s](Wikipedia:Foster's_Lager "wikilink")* is an Australian
    brand of beer. For a time, it advertised itself in the United States
    with the catch phrase \"in the big can.\"
-   In Michaels Dundie rap, he remakes the song OPP as performed by
    Naughty by Nature

Quotes
------

:   see *[The Dundies Quotes](The_Dundies_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring cast

-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink") (Voice Only)
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited)
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink") (Uncredited)
-   [Joanne Carlsen](Joanne_Carlsen "wikilink") as [Terri
    Hudson](Dunder_Mifflin_Family_Members_and_Loved_Ones#Terri_Hudson "wikilink")
    (Uncredited)
-   [Trish Gates](Trish_Gates "wikilink") as
    [Stacy](Dunder_Mifflin_Family_Members_and_Loved_Ones#Stacy "wikilink")
    (Uncredited)

### Guest cast

-   Christopher T. Wood as Chili\'s Manager
-   Matthew McKane as Frat Guy
-   Beau Wirick as Frat Guy

References
----------

<references/>
\

[^1]: Novak, B.J. (September 20, 2005). [\"*Office* Gossip\'s First
    Exclusive
    Blog!\"](http://www.tvguide.com/News-Views/Interviews-Features/Article/default.aspx?posting=%7B834BD48E-7594-451E-9659-590D486E5F40%7D),
    *TVGuide.com*

[^2]: Commentary, \"The Dundies\", Season 2 DVD

[^3]:

[^4]:

[^5]:

[^6]:

[^7]: Fischer, J. (April 12, 2006). [\"Return of *The
    Dundies*\"](http://community.tvguide.com/thread.jspa?threadID=700000676),
    *TVGuide.com*

[^8]: Daniels Greg. (2005) 20th Century Fox, \"The Simpsons\". The
    Complete Seventh Season DVD commentary for the episode \"Bart Sells
    His Soul\" DVD.
