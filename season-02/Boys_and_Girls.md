# Boys and Girls

**\"Boys and Girls\"** is the fifteenth episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 21st overall.
It was written by [B.J. Novak](B.J._Novak "wikilink") and directed by
[Dennie Gordon](Dennie_Gordon "wikilink"). It first aired on February 2,
2006. It was viewed by 5.42 million people.

Synopsis
--------

[Jan](Jan_Levinson "wikilink") arrives at the Scranton branch to hold a
Women in the Workplace seminar for the female employees. Although she
has assured the women that Michael isn\'t allowed, he won\'t be
dissuaded. As she begins to talk about the disparity in the perception
of assertive men versus assertive women, Michael jumps into the
conference room to give his own rambling cliché-ridden take on the
subject. Jan forces him out, then has to snap at him for lingering
around the door.

Michael resents having his conference room taken away from him so the
women can talk about clothes (or him). Dwight\'s more concerned that
their menstrual cycles will align and wreck havoc on the plumbing. When
Michael brings all the guys into a circle next to the conference room
and makes a lot of noise, Jan tells them to go somewhere else. Dwight
suggests the warehouse and Jan heartily approves.

Jan continues by asking the women to state something they are good at.
Meredith starts to say her name and that she\'s an alcoholic, but
catches herself and says she\'s good at supplier relations. Phyllis
believes she\'s good with computers until Angela criticizes her work.
They move on to goals. Meredith wants to be five years sober in five
years. Jan starts to praise Kelly who doesn\'t want to be lugging kids
around in mini-van. Then Kelly says she wants an SUV. Pam admits she
wants a house with a terrace on it and to be in a loving marriage with
Roy. But she has always wanted to do something with art. After Phyllis
compliments her work, Jan mentions a graphic design class offered by
Corporate. She\'d have to spend a few weeks in New York, but really only
a few weekends after that. Pam starts to defer, but Jan reminds her that
there are always a million reasons not to do something. Jan explains to
the camera that her intent here is to find out if there are any women
here who would make a good addition to Corporate.

Downstairs, Michael starts to talk about how hard the warehouse staff
works. This is awkward for two reasons. First of all,
[Darryl](Darryl_Philbin "wikilink") and his crew are not thrilled to
have Michael there as he refuses to stay off the machinery. His
recklessness behind the controls causes several supply shelves to come
crashing down. Secondly, [Jim](Jim_Halpert "wikilink") isn\'t keen on
being around Roy who has probably heard the rumor about him liking
[Pam](Pam_Beesly "wikilink"). [Kevin](Kevin_Malone "wikilink") offers to
back Jim up if it comes to blows. It turns out that Roy is fine with it.
He believes it was a long time ago and that Jim and Pam are just
friends. Besides, it gives Pam someone to talk to so she can get it all
out before she comes home.

Jan addresses how one should dress for the job one wants and not the job
one has. Angela doesn\'t think she\'s getting anything out of this.
Besides, Jan\'s clothes clearly indicate that she wants to be a whore.

Michael sits down the guys for a gripe session about women. It quickly
derails into a comparison between the office and warehouse workers.
Darryl thinks that\'s the biggest issue. Michael may think they\'re all
the same, but they\'re not. If they had a union, they could compete
economically with the office. Michael is unable to tone down the union
rhetoric.

While Jan attempts to discuss how sports analogies often leave women
out, Kelly uses the opportunity to ask about \"getting to second base\"
in the context of Jan\'s relationship with Michael. Michael interrupts
to explain about the union problem. Jan tells him he has to handle it
himself. When he tries to patronize her with pros and cons, she tells
him that the con is that the entire branch will be shut down, period. He
attempts to insert their history into the conversation and she tells him
in no uncertain terms they are not discussing it and makes him leave.

The women continue to grill Jan about her relationship with Michael
until she calls for a break.

During the break, Pam tells Jim about the opportunity in New York. She
seems excited about it; Jim believes she should go for it.

The other men are trying to load up a delivery truck.
[Ryan](Ryan_Howard "wikilink") comes up with an idea of speeding up the
process to which [Stanley](Stanley_Hudson "wikilink") immediately
replies, \"This is a run out the clock situation.\" Michael blows
packing peanuts all over the warehouse, pretending it\'s snow.

At that point, Jan arrives, asks Michael if he\'s handled the problem,
then immediately calls for everyone\'s attention. She explains clearly
that any hint of unionizing will result in the closure of the branch,
just like what happened in Pittsfield. They can direct any further
questions to Michael\...who is hiding.

Pam and Roy have an argument about the design class. Jim confirms that
she\'s not going to go after all. She says Roy doesn\'t believe there\'s
any guarantee it will go anywhere. Jim tells her she has to take a
chance on something, unless she wants to be a receptionist for the rest
of her life. Pam snaps that she\'s fine with her choices.

To the camera, she explains that she read about the house with a terrace
when she was a kid and thought it was neat. It was just a story that
stuck with her. It doesn\'t have any bearing on her life now. They
don\'t even make houses like that in Scranton. She stops, finally
vocalizing that she will never get to live her dream and bursts into
tears.

Michael treats the men to pizza as some form of compensation. As the day
comes to a close, the warehouse is in complete disarray (furthered by
Michael trying to create snow by spilling out the foam popcorn in front
of a fan), and as Michael and the office employees depart, Darryl
shouts: \"Hey Michael, this ain\'t over!\" as he stands in the middle of
a complete mess. Michael shuts the door without responding.

Amusing Details
---------------

-   When Pam is saying how she doesn\'t fit it with the other ladies,
    she says she fits in more with-but then it cuts off to Roy saying
    Jim\'s name.
-   Stanley talks about how they \"run the clock\" in the office, which
    is a sporting metaphor. The next scene shows Jan talking to them
    about sports metaphors and how women are left out of it.

Deleted scenes
--------------

The Season Two DVD contained a number of deleted scenes from this
episode. Notable cut scenes include:

-   Michael asks Pam and Jim for help choosing a ringtone for his
    expensive new phone. He\'s looking for the one that sounds like a
    jackhammer. Jim asks, \"Do you mean *vibrate*?\"
-   Roy and Dwight talk about guns. Roy prefers the
    [Bushmaster](Wikipedia:Bushmaster_Firearms_International,_LLC "wikilink");
    Dwight likes his [spud gun](Wikipedia:spud_gun "wikilink").
-   Dwight demonstrates his spud gun and destroys the window to
    Darryl\'s office.
-   In a talking head interview, Dwight talks about a film about a girl
    who went on an incredible adventure but eventually realized that
    \"there\'s no place like home\". But he can\'t remember [the
    movie\'s title](Wikipedia:The_Wizard_of_Oz_(1939_film) "wikilink").

Trivia- Caution here be spoilers
--------------------------------

-   As of this episode, [David Denman](David_Denman "wikilink") ([Roy
    Anderson](Roy_Anderson "wikilink")) and [Melora
    Hardin](Melora_Hardin "wikilink") ([Jan
    Levenson](Jan_Levenson "wikilink")) have joined the main cast.
-   [Oscar Nunez](Oscar_Nunez "wikilink") ([Oscar
    Martinez](Oscar_Martinez "wikilink")) is credited but does not
    appear in this episode.
-   While Darryl says that the warehouse and office staff receive \"very
    different\" compensation, the difference was far less than he
    assumed - a year later, \"[The
    Negotiation](The_Negotiation "wikilink")\" revealed that the
    reasonable raise Darryl asked for would give him a higher salary
    than Michael.
-   A scene was shot in which Roy physically confronts Jim for pushing
    Pam to take the internship. The scene was very intense, but the
    producers opted not to include it in the episode because there was
    no comedy in it.[^1]
-   There\'s a \"do not erase\" warning in the blackboard in the
    warehouse, but Michael erases it anyway.

References to previous episodes
-------------------------------

-   The blow-up doll in the warehouse is the one Michael brought to work
    (and later threw away) in the episode \"[Sexual
    Harassment](Sexual_Harassment "wikilink")\".

Cultural references
-------------------

-   *Ally McBeal* (1998-2002) was a television dramedy about a young
    lawyer and her personal life. During its run, the title character
    became a focal point for the changing face of feminism.
-   *You\'ve come a long way, baby* was the motto of \"Virginia Slims\",
    a cigarette marketed for women. Its advertisements typically
    depicted a woman from the early 1900s being treated unfairly,
    combined with a carefree, stylish modern woman.
-   *[Lost](http://lost.wikia.com/wiki/Main_Page)* is a television drama
    that takes place on an island. *The Others* is a mysterious group of
    island inhabitants.
-   *Brangelina* is the nickname created by the media to refer to the
    relationship between *[Brad Pitt](Wikipedia:Brad_Pitt "wikilink")*
    and *[Angelina Jolie](Wikipedia:Angelina_Jolie "wikilink")*.
-   The Term Second base means having sexual touches;

1st- making out, snogging, french kissing 2nd- fingering/handjob 3rd-
licking out/blowjob home run- full sex

-   *Good Will Hunting* is a movie about a mathematical prodigy.
-   *[Alcoholics Anonymous](Wikipedia:Alcoholics_Anonymous "wikilink")*
    is a support group to assist people recovering from alcoholism.
    Members introduce themselves to the group by saying *I\'m (name) and
    I\'m an alcoholic*. Meredith almost says the same thing during the
    seminar.
-   Lonny mockingly calls Michael *Hasselhoff*, referring to actor and
    singer David Hasselhoff.

Quotes
------

:   see *[Boys and Girls Quotes](Boys_and_Girls_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring cast

-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Patrice O\'Neal](Patrice_O'Neal "wikilink") as [Lonnie
    Collins](Lonnie_Collins "wikilink")
-   [Karly Rothenberg](Karly_Rothenberg "wikilink") as
    [Madge](Madge "wikilink")
-   [Calvin Tenner](Calvin_Tenner "wikilink") as
    [Calvin](Calvin "wikilink") (Uncredited)
-   [Philip Pickard](Philip_Pickard "wikilink") as
    [Philip](Philip "wikilink") (Uncredited)

References
----------

<references/>

[^1]: [GMMR Exclusive Interview with David
    Denman](http://www.givememyremote.com/remote/?p=1163)
