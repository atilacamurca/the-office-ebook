# Drug Testing

**\"Drug Testing\"** is the twentieth episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 26th overall.
It was written by [Jennifer Celotta](Jennifer_Celotta "wikilink") and
directed by [Greg Daniels](Greg_Daniels "wikilink"). It first aired on
April 27, 2006. It was viewed by 7.8 million people.

Synopsis
--------

[Dwight](Dwight_Schrute "wikilink") finds half of a
[joint](Wikipedia:Joint_(cannabis) "wikilink") in the parking lot of
Dunder Mifflin and takes advantage of his
[volunteer](volunteer "wikilink") [sheriff
deputy](Wikipedia:Sheriff "wikilink") status by interviewing the staff
to get to the bottom of it. Meanwhile, [Jim](Jim_Halpert "wikilink")
does an impression of Stanley and gets caught. When he and
[Pam](Pam_Beesly "wikilink") speak at the same time (impersonating
Stanley\'s response), Pam calls
\"[jinx](Wikipedia:Jinx_(children's_game) "wikilink")\" so Jim can\'t
talk until he buys her a [Coke](Wikipedia:Coca-Cola "wikilink"), which
isn\'t an issue until he discovers the vending machine is sold out. When
Dwight sets up urine testing, Michael worries that a \"[clove
cigarette](Wikipedia:kretek "wikilink")\" he smoked at an [Alicia
Keys](Wikipedia:Alicia_Keys "wikilink") concert will show up.

Michael holds an anti-drug meeting for the entire office, where he
over-expresses his hatred of drugs. He tries to get out of the testing,
but Dwight continues to insist that he must take part. Michael pressures
Dwight for a cup of his \"clean\" urine, but Angela is against the idea.
Jim continues to hold up his end of the jinx. Pam continues to tease
him, until she gets to the point where she says, \"you can tell me
anything\", which creates an awkward silence. Michael passes his drug
test (with Dwight\'s urine); Dwight turns in his volunteer badge over
his actions. Pam buys Jim his Coke, which he buys off of her so she can
find out what\'s been happening with Dwight the whole day. Michael feels
guilty, so he makes Dwight an \"Honorary Security Advisor\" for Dunder
Mifflin Scranton. Jim wonders aloud what Dwight gets out of his
relationship with Michael, while the camera focuses on him and Pam,
suggesting that the two have a similarly confusing relationship.

Deleted scenes
--------------

-   Jim does impressions of Kevin and Angela for Pam.
-   In a talking head interview, Dwight is so determined to find the
    culprit that he is prepared to pray \"to Thor himself.\"
-   In a talking head interview, Toby doesn\'t think Michael is taking
    drugs, but maybe he should.
-   Jim secretly does an impression of Dwight right in front of him.
-   In a talking head interview, Dwight talks about Jim and concludes,
    \"If that were my life, I\'d do drugs.\"
-   Phyllis tells Stanley that she told Dwight he wasn\'t acting
    withdrawn. Stanley barely acknowledges her.
-   Stanley ignores Dwight\'s call to be interviewed.
-   Pam toys with Dwight by admitting that she \"was a teensy bit
    high\... in the parking lot at the Quick and Easy.\"
-   Meredith comes to Jim because Pam told her he had something to tell
    her. Jim is under the jinx and says nothing. Meredith assumes the
    worst.
-   Kelly puts a customer on hold when she is called by Dwight.
-   Dwight takes notes during the conference: \"Creed - Shifty eyes.
    Ryan - Dilated pupils. Kelly - Hyperactive.\" Michael makes up more
    absurd drug \"facts\". Jan (via speakerphone) excuses herself from
    the meeting, which is the first time the others were aware she was
    even listening.
-   Pam shows the camera the flyer that Michael made up for the
    conference. \"Drugs: Let\'s not and say we did.\"; she suspects
    Michael was himself on drugs when he made them.
-   Kelly returns to her desk after the conference and realizes that her
    customer has been on hold the whole time.
-   In talking head interviews, Angela and Meredith talk about how they
    aren\'t worried about drug testing.
-   Pam forwards a call from Brenda to Jim\'s phone. Jim is under the
    jinx and doesn\'t answer. Jim holds up a sign for the camera:
    \"It\'s ok\... She\'ll call back!\"
-   Dwight turns in his volunteer sheriff\'s uniform and badge, neither
    of which he was authorized to have in the first place. The sheriff
    discovers other things Dwight was doing without authorization.
-   Leo and Gino, the workers at Vance Refrigeration, discard a joint in
    the parking lot.

Trivia
------

-   Michael\'s list of drugs reads \"crack, cocaine, pot, blow, acid,
    hookah, heroin, speed\". There are a number of errors on this list.
    Firstly, \"blow\" is a common street name for cocaine, which is
    already listed. \"Crack\" is also a form of cocaine. Hookah is not a
    drug, but rather a method of smoking tobacco called \"shisha\" that
    is often mixed with fruit mixtures, as Toby explains in the episode.
    Additionally, the list features many slang terms from drugs, not the
    actual term. In addition to \"blow\", \"speed\" is slang for
    amphetamines, \"acid\" is slang for LSD, and \"pot\" is slang for
    marijuana. Michael also writes the word \"heroine\" instead of
    \"heroin\".
-   The cast and crew joke that this episode must have been the most
    times any television show has said the word \"urine\". The word
    urine is used 16 times.
-   The kind of marijuana Creed identifies during his interrogation is
    an actual variety of cannabis.
-   Steve Carell injured his finger, and it swelled up so much that he
    couldn\'t get his wedding ring off. The finger was bandaged to
    disguise the ring.[^1]
-   Despite Dwight\'s hardline aversion to the illegal substance, Season
    9\'s *[The Farm](The_Farm "wikilink")* would reveal that Dwight\'s
    brother, Jeb, actually grows marijuana for a living, though the fact
    that he lives in California, where the drug can, under license, be
    grown and sold for medicinal purposes, along with (as stated in a
    talking head) being in the \"pain management\" business, implies
    that he does so legally.
-   When Dwight tells the drug testing lady he had \"green urine\"
    tested back in the day, there\'s a mysterious lady with grey hair in
    the background. Michael is eavesdropping just next to her.

Interesting Notes
-----------------

-   It is not clear how Creed was able to [pass drug
    testing](http://kitdetox.com/), as he evidently still uses cannabis.
-   Dwight states that he "likes the people he works with, generally,
    with four exceptions." It is likely those four exceptions are the
    first four people he questions about the joint: Kevin, Ryan, Kelley,
    and Oscar.

Amusing details
---------------

-   When Michael asks Dwight for his urine, the camera reveals a picture
    of everyone in the office that was taken and Photoshopped (by
    Michael) in \"Conflict Resolution\". However, \"Conflict
    Resolution\" was aired after \"Drug Testing\".
-   Dwight assumes that Kevin is taking Rogaine because he is bald.
-   Dwight pesters Angela about the prescription drugs she may be on,
    presumably to try and figure out whether she is on birth control or
    not.
-   Kelly doesn\'t seem to notice that Jim isn\'t responding to her
    while she talks. This contributes to her trait of mindless babbling.
-   Michael\'s statistics are incredibly flawed, as usual.
-   Dwight\'s urine being green implies that he had an infection of some
    sort.
-   Dwight goes into the stairwell to moan/cry, as he does in other
    episodes.

Cultural references
-------------------

-   *Dude where\'s my office?* is a play on the title of the 2000 movie
    *[Dude, Where\'s My
    Car?](Wikipedia:Dude,_Where's_My_Car? "wikilink")* about two dimwits
    who have lost their car.
-   A *doobie* is a slang for a marijuana cigarette. *[The Doobie
    Brothers](Wikipedia:The_Doobie_Brothers "wikilink")* is an American
    rock group from the 1970s and early 1980s. Michael combines them in
    a strange way.
-   Seacrest out is a catchphrase sign-off of television personality
    *[Ryan Seacrest](Wikipedia:Ryan_Seacrest "wikilink")*. *Peace out*
    is a hippie good-bye. Michael combines the two in a somewhat
    nonsensical way.
-   *[Jinx](Wikipedia:Jinx_(children's_game) "wikilink")* is a
    children\'s \"game\". When two people say the same thing at the same
    time, the person who doesn\'t say \"jinx\" first must remain silent
    until the jinx is released. Traditional conditions including buying
    the winner a [Coke](Wikipedia:Coca-Cola "wikilink") (as occurs in
    this episode) or having one\'s name said three times.
-   [Cheech Marin](Wikipedia:Cheech_Marin "wikilink") and [Tommy
    Chong](Wikipedia:Tommy_Chong "wikilink") form the comedy team
    *[Cheech & Chong](Wikipedia:Cheech_&_Chong "wikilink")* which relied
    heavily on drug humor.
-   *[Eeny, meeny, miny,
    moe](Wikipedia:Eeny,_meeny,_miny,_moe "wikilink")* is a children\'s
    counting rhyme. It has a disputed racist history and is considered
    offensive by some.
-   To *bring out the big guns* is a common expression meaning to bring
    a more powerful technique to some type of conflict or competition.
-   *[Alicia Keys](Wikipedia:Alicia_Keys "wikilink")* is an R&B singer.
-   *Let\'s not and say we did* is an idiom used to decline a
    suggestion. It literally means that one should claim to have done
    something without having done it.
-   Kevin asks for a magazine, which are usually provided when semen is
    being collected. This goes with other references of Kevin being
    extremely innapropriate, and storing innapropriate material on his
    computer.

Quotes
------

:   see *[Drug Testing Quotes](Drug_Testing_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting Cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring Cast

-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Creed Bratton](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Hugh Dane](Hugh_Dane "wikilink") as [Hank
    Tate](Hank_Tate "wikilink")
-   [Lee Eisenberg](Lee_Eisenberg "wikilink") as [Gino](Gino "wikilink")
    (Deleted Scenes)
-   [Gene Stupnitsky](Gene_Stupnitsky "wikilink") as
    [Leo](Leo "wikilink") (Deleted Scenes)

### Guest Cast

-   Marilyn Brett as [Linda](Linda "wikilink") the Urinalysis Technician

References
----------

<references />
\

[^1]: [Supersize Office Finale, Supersize
    Blog!](http://community.tvguide.com/blog/Celebrity-Blogs/Jennas-Blog/700000656?month=200605),
    Jenna Fischer\'s blog for TV Guide, May, 11 2006.
