# Conflict Resolution

**Conflict Resolution** is the 21st episode of [Season
2](Season_2 "wikilink") of \'\'[The Office](The_Office_(US) "wikilink")
\'\'and the 27th overall. It was written by Greg Daniels and directed by
[Charles McDougall](Charles_McDougall "wikilink"). It first aired on May
4, 2006. It was viewed by 7.4 million people.

Synopsis
--------

[Angela](Angela_Martin "wikilink") discovers she has not been invited to
[Pam](Pam_Beesly "wikilink")\'s wedding. I.D. pictures are also being
taken in the office, following [Dwight](Dwight_Schrute "wikilink")\'s
appointment as Honorary Security Advisor of the office park in [Drug
Testing](Drug_Testing "wikilink"). [Oscar](Oscar_Martinez "wikilink")
complains to Human Resources Director [Toby](Toby_Flenderson "wikilink")
about Angela\'s baby poster. When [Michael](Michael_Scott "wikilink")
overhears the discussion, he tells Toby he wants to try to resolve the
conflict himself. Using a manual, he decides that Oscar will wear a
T-shirt of the poster so that Angela can look at it, but Oscar won\'t
have to.

Michael takes every other unresolved office complaint from Toby (by
force) to try and resolve them himself with disastrous results ahead.
Michael notes to his pleasure that there are no complaints filed against
him, but Toby shows the camera a separate file in a locked drawer
specifically for complaints against Michael.

Pam hand delivers her \"[save the
date](Wikipedia:save_the_date "wikilink")\" card to Angela. Michael
starts reading the (confidential) complaints out loud in front of the
office and the employees start feeling both angry and awkward at the
complaints made by and against them. Pam is angry about a complaint
against her for planning her wedding during office hours, which she
blames Angela for making. Due to a prank by
[Jim](Jim_Halpert "wikilink"), Dwight\'s security I.D. badge labels him
as a security threat and his middle name is typed as \"Fart\" instead of
\"Kurt\".

Dwight storms off and makes another complaint to Toby, but he discovers
his complaints about Jim were never filed by Toby and just sat in a box
under his desk. Dwight demands Michael take action against Jim. Shortly
after, Dwight begins looking up potential job openings in other
[Dunder-Mifflin](Dunder-Mifflin "wikilink") branches to verbally taunt
Jim (which seems to work by Jim\'s irritated facial expression).

Michael locks Dwight and Jim together in a room for a \"[cage
match](Wikipedia:cage_match "wikilink")\", where they\'re not allowed to
leave until they come to an understanding. As Jim explains each prank he
pulled on Dwight, he starts to realize how unfunny they are when
they\'re listed back to back and feels regret at how he spends his time
in the office. At this point, tensions are high in the office and no one
speaks to one another. Michael tries to resolve them by getting the I.D.
photographer to get a group shot of the staff.

As Michael attempts to get one good group shot (at \$20 a piece), Jim
confesses to Pam that it was he who registered the complaint about
Pam\'s wedding planning. The following day, he leaves a message on
Pam\'s voicemail stating he won\'t be in because he has a doctor\'s
appointment in the city, but we see that he is actually seeing
[Jan](Jan_Levinson "wikilink") for a sales position at the [Dunder
Mifflin Stamford](Dunder_Mifflin_Stamford "wikilink") branch. Toby is
seen storing the papers full of the conflicts in the warehouse.

Trivia
------

-   The poster that Oscar is disturbed by was given to Angela by Toby in
    [Christmas Party](Christmas_Party "wikilink").
-   Not only does Dwight\'s nametag indeed say \"Dwight Fart Schrute,\"
    it also says \"ASST. to the REGIONAL MANAGER\" - despite Dwight
    Having been promoted to \"assistant regional manager\" in the
    episode [The Fight](The_Fight "wikilink") from earlier in the
    season. At the bottom, Dwight also appears to have signed his name
    as \"Dwight Fart Schrute,\" even though Dwight did not notice any of
    the errors until **after** Jim had laminated it. This would imply
    that Jim forged Dwight\'s signature.
-   Creed thinks his ID badge photo is a mugshot.
-   Kevin filed a complaint against Stanley, saying that he takes his
    Miracle Whip without asking. In the previous episode, Jim\'s
    impression of Stanley is, \"I like the tangy zip of Miracle Whip.\"
-   Meredith complains that every morning the lights are too bright and
    everyone is too loud, implying that she is often hungover.
-   Pam refers to Dwight as \"Bobblehead Joe\", reffering to the
    bobblehead of himself that Angela gave him on Valentine\'s Day.
-   Dwight immediately knows which box contains his complaints, although
    he has never seen the box.
-   There are many full-time jobs available at Dunder Mifflin, which are
    shown in two shots of Dwight\'s computer. In Stamford, there is a
    position for Plant Manager and Sales Manager (which Dwight has
    highlighted, showing it requires 3--5 years of experience and a
    Bachelor\'s Degree). In New York, there is a Maintenance Engineer
    position. The second time Dwight\'s computer is shown, there appear
    to be many more jobs, which are not shown very clearly. Albany has
    three positions open, one of which appears to be for Comptroller and
    the other two being repeats for Regional Manager. Stamford has an
    additional position (IT Manager), Scranton has two (Warehouse
    Manager and Warehouse Staff 1), and Buffalo has one (which cannot be
    seen on the screen).
-   When Michael starts reopening grievances, he mentions a Phyllis and
    Angela dispute. Angela responds, \"You already did me.\" The camera
    pans to Jim, who mouths \"That\'s what she said\" in time with
    Michael\'s quip.

List of Dwight\'s grievances mentioned against Jim:

-   Replaced all of Dwight\'s pens and pencils with crayons
-   Jim paid everyone five dollars so they would call Dwight \"Dwayne\"
-   Placed a bloody glove in Dwight\'s desk drawer and tried to convince
    him he committed murder
-   Told Dwight there was an abandoned infant in the women\'s bathroom,
    thus tricking him into going to the bathroom and \"\[seeing\]
    Meredith on the can\"
-   Dwight hit himself in the head with his phone (Jim kept putting
    nickels into the handset until Dwight got used to the weight; Jim
    then abruptly removed all of the nickels; Dwight went to pick up the
    phone, and believing the phone was heavy, pulled it very hard)
-   Every time Dwight typed his name it came out as \"diapers\"
-   By the end of the day, Dwight\'s desk was moved two feet closer to
    the copier
-   Also in the episode, Jim said \"Dwight tried to kiss me. I didn\'t
    say anything because I\'m not really sure how I feel about it.\"

Jim admits that his many pranks don\'t seem nearly as funny when listed
in succession, but claims that Dwight does deserve it. The following
season, in [The Convention](The_Convention "wikilink"), when they meet
for the first time since his transfer to Stamford, Jim says that, upon
seeing Dwight again, he recognized their stupid and petty nature\...
\"then he spoke.\"

The camera the photographer uses is a Canon 20D with an EF 20mm f/2.8
USM. During the group shot, he used an EF 24-105mm f/4L USM IS. Also,
during the shoot for the ID cards, he used the built-in flash, but with
the group picture, he had an external flash. The photographer is Scott
Adsit who we have more recently seen as Pete Hornberger in another NBC
show 30 Rock.

Near the end of the episode, when the photographer is taking group
pictures of the office staff, Michael says in voice-over that \"\[i\]t
was really hard getting a good picture of fifteen people.\" There were
only fourteen people being photographed, however.

-   When asked to smile by the photographer while having his I.D. photo
    taken, Dwight mentions that he never smiles if he can help it. He
    says that showing one\'s teeth is a sign of submission amongst
    primates. In spite of this, he\'s shown baring his teeth and
    grimacing for the camera when the group photo is taken near the end
    of the episode.
-   A humorous conversation transpires \"behind the scenes\" between
    Michael and Stanley, and possibly foreshadows Stanley\'s outburst in
    [Did I Stutter?](Did_I_Stutter? "wikilink") when Pam accuses Angela
    of complaining about her in-office wedding planning (Approximately
    10 minutes and 50 seconds into the episode). If you listen closely,
    you can hear that the dialogue is:

:   

    :   Michael: \"Stanley! You gotta lot of anger under there buddy.
        Come on, start us off, unleash it.\"
    :   Stanley: \"You do not want that Michael.\"
    :   Michael: \"Yes I do\...\"
    :   Stanley: \"Are you serious?\"
    :   Michael: \"\...Nope.\"
    :   Stanley: \"Because I will.\"

When Angela holds up Pam\'s Save the Date, the address for the wedding
is listed as \"TBA.\" Considering [Michael\'s
birthday](Michael's_Birthday "wikilink") is on March 15th and they sent
out the Save the Dates to everyone else sometime after that, Pam and Roy
have less than 2-3 months to find a venue. Furthermore, because this
episode originally aired on May 4th, it could mean that they technically
only have one month, further showing how their wedding isn\'t a top
priority for either of them and foreshadows their eventual breakup.

The 5 styles of conflict resolution are:

1.  Lose-lose
2.  Win-lose
3.  Compromise
4.  Win-Win
5.  Win-Win-Win (\"The important difference here is with win-win-win we
    *all* win, me too. I win for having successfuly mediated a conflict
    at work.\")

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Dwight finds his desk encircled in police tape. Jim secretly calls
    Dwight\'s phone, but Dwight cannot answer because it would require
    him to cross the police tape.
-   Michael explains that in real life, unlike television, some days are
    boring and uneventful.
-   Dwight annoys the photographer in various ways.
-   In talking head interviews, Michael explains that he hates it when
    people don\'t tell each other why they\'re angry. He gives as an
    example how he would repeatedly ask, \"What\'s wrong, Dad?\" without
    getting a response. Later, he tells a story about [two women who
    claim to be the mother of the same
    baby](Wikipedia:Books_of_Kings "wikilink") but messes up the ending.
    In the conference room, he dedicates himself to resolving all the
    old cases \"before Toby can kill or rape another person\". Michael
    later expresses surprise that Dwight \"went postal\". He also claims
    credit for coming up with the \"cage match\" idea.
-   Pam asks the photographer if he does weddings.
-   Dwight gives Pam \"Level Red\" security clearance.
-   Meredith and Kevin get their pictures taken.
-   Phyllis, sore at Stanley, declines to forward a misdirected call to
    him.
-   [Hank](Hank_Tate "wikilink") hassles Dwight before letting him into
    the building, while meanwhile waving the other employees through. By
    the time Dwight clears security, he misses the elevator.

Cultural references
-------------------

-   *Save the date* is a colloquial term indicating that one will not
    schedule other activities for a particular day. It has become
    customary in the United States to send \"Save the Date\" cards to
    wedding guests in advance of the formal invitation.
-   *Fantastic Sam\'s* is a chain of hair salons. The store nearest to
    Scranton is in Wilkes-Barre, approximately a half hour\'s drive
    away.
-   A *spud gun* fires a potato from a plastic pipe via compressed air.
    People build them as a hobby.
-   Creed\'s turn to the side suggests that he thinks he has been
    arrested. In the United States, people who have been arrested are
    photographed twice, one front view and one side view.
-   A *zip code* is a postal code used for addressing mail.
-   *Miracle Whip* is a brand of sandwich spread, similar to mayonnaise.
-   A *cage match* is a type of fight staged in a cage by professional
    wrestlers.
-   A *bloody glove* was a controversial piece of evidence in the murder
    trial of O. J. Simpson.
-   A *price gouger* is a person who charges exorbitant prices.
-   *Photoshop* is a computer program used for manipulating digital
    images.
-   *The Apprentice* and *Big Brother* are two reality-based television
    programs.
-   Michael\'s story about the two women and a baby is a botched
    retelling of Solomon\'s Baby, a story from the Bible (1 Kings).
-   *Cold Case* is a television drama about a Philadelphia police
    division that investigates unsolved crimes. However, the title theme
    does not begin with a gong-like sound; that is from *Law and Order*.
-   At the end of the episode, Toby stores the complaints box in the
    warehouse with hundreds of similar boxes. This pays homage to the
    endings of *Raiders of the Lost Ark* and *Citizen Kane*. This plot
    device is known as the [Government
    Warehouse](Wikipedia:Government_Warehouse "wikilink").

Quotes
------

:   see *[Conflict Resolution
    Quotes](Conflict_Resolution_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting Cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring Cast

-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")

### Guest Cast

-   [Scott Adsit](Scott_Adsit "wikilink") as The Photographer
-   [Vivianne Collins](Vivianne_Collins "wikilink") as [Headquarters
    Receptionist](Grace "wikilink")

\
