{{Office episode
|Title      =Christmas Party
|Image      =[[Image:Christmasparty.jpg|250px]]
|Season     =[[Season 2|2]]
|Episode    =10
|Code       =2010
|Original   =December 6, 2005
|Writer(s)  =Michael Schur
|Director   =[[Charles McDougall]]
|Prev       =[[Email Surveillance]]
|Next       =[[Booze Cruise]]}}
'''"Christmas Party"''' is the tenth episode of the second season of ''[[The Office]] ''and the 16th overall. It was written by Michael Schur and directed by [[Charles McDougall]]. It first aired on December 6, 2005. It was viewed by 9.7 million people.

==Synopsis==

The Scranton branch celebrates Christmas. [[Michael]] and [[Dwight]] bring in a huge Christmas tree in preparation for the office party. It's too tall to fit, so Michael recommends they cut off the top and sell it to charity.

[[Ryan]] is drafted onto the [[Party Planning Committee]]. Michael interrupts to make sure they double everything and gives them extra money. He informs the documentary crew that he received a Christmas bonus of "3,000 g's" ($3,000) for coming under budget due to firing Devon. He wants to serve liquor at the party, but [[Pam]] reminds him that [[Corporate]] does not allow alcohol at company parties.

Dwight informs the employees to put their [[Secret Santa]] presents under the tree. [[Toby]] got a poster of babies dressed as jazz musicians paying instruments for [[Angela]], [[Oscar]] knows nothing about [[Creed]] but thinks he's Irish so got him a shamrock keychain, and [[Kevin]], who drew his own name for Secret Santa and didn't tell anybody, reveals he got something for himself. [[Jim]] has gotten Pam for the first time, bought her a teapot and filled it with little mementos as inside jokes. He also has a card for her, since Christmas is the time to tell people how you really feel.

Michael tries to get people to tell him who they got. Jim reminds him it's supposed to be secret. Michael ignores him and reveals that he got Ryan.

The gift exchange starts off reasonably well. Oscar gets a shower radio from Kelly. Jim gets an old shirt from Creed (Creed forgot about the exchange and pulled the shirt out of his closet at the last minute). When Ryan opens his gift from Michael, he finds a $400 video iPod. He knows how much it cost because Michael left the tag on it. Michael brushes it off as just being a generous giver, but the other employees are concerned at the huge disparity between the iPod and the other gifts. Pam opens Jim's gift. He starts to open the lid to show her the contents and explain them when Michael demands his own gift.

Michael's magnanimity only lasts as long as it takes for him to open Phyllis' gift to him: a home-made oven mitt. He storms out of the party circle, visibly upset. For the cameras, he expresses his belief that the cost of the gift reflects the level of one's affection for the person receiving the gift. This oven mitt isn't worth anything. And, after all, he got Ryan an iPod.

When he returns, he decides the exchange is going to turn into a Yankee Swap where people can steal other gifts. This doesn't work well at all. The gifts were bought for specific people based on the tastes of those people. For example, a pretty decorated name plate that says Kelly on it. (Stanley: "It's for Kelly")

Angela frets to the camera how much work goes into planning a party like this and how unfair it is for Michael to come in and make sudden changes. She bursts into tears.

Kelly gets Angela's baby poster and hates it. Angela steals it back. Kevin makes sure he gets his foot bath. The iPod's expense makes it the most popular of the gifts which starts to cause resentment among the employees. Jim's disturbed when Pam takes the iPod over stealing back the teapot. Worse, Michael is obnoxious when it comes to the oven mitt, loudly hoping no one steals it from him. Seeing how distressed Phyllis is, Meredith steals it from Michael out of pity. Michael nearly jumps for joy and brags about how effective reverse psychology is. Phyllis abruptly leaves.

Michael asks what she's so upset about. Pam points out that it was pretty obvious he didn't like his present. The gifts were not chosen with a Yankee Swap in mind. Stanley grouses about how Michael should have just bought a $20 gift like everybody else.

Michael blurts out that he was just trying to spread some Christmas joy since he got a big bonus for Christmas. They ask him how much it was. He tries to be cagey, but isn't smart enough to pull it off. In disgust, the employees break off into their own small groups and the exchange is over.

Michael says "Happy Birthday, Jesus. Sorry your party's so lame!"

Michael goes out to buy 15 bottles of vodka for $166.41, the clerk assures him will cause 20 people to get plastered, Jim tries to get the teapot back from Dwight who plans to use it to clear out his nostrils, because he gets sinus infections.

Pam shows [[Roy]] her iPod. He claims he was going to get her one, but now doesn't have to. She asks him what he will get her now. He mumbles, "I don't know, like a sweater or something."

Michael returns with the vodka and says, "Uh oh, looks like Santa's been a little naughty". [[Todd Packer]] shows up already drunk and eventually passes out in a chair. Phyllis introduces everyone to her new beau, [[Bob Vance]], who greets everyone with "Bob Vance, Vance Refrigeration". Ryan mirthfully asks him what line of work he's in.

Jim approaches Pam's desk to find out that she traded the iPod for the teapot. He tells her that the gift has "bonus gifts" and she pulls the little items out, such as his yearbook photo, a condiment packet, a mini-golf pencil she'd used once. While Pam's distracted, Jim secretly pulls out the card and slides it into his back pocket.

Kevin puts photo copies of his own butt around the office.

A drunk [[Kelly]] kisses Dwight, who tells her it was inappropriate and that it's a "man's job" to make the first move. Angela, already having had a trying evening, takes her wrath out on the Christmas ornaments by smashing them outside by the dumpster and screaming.

The others leave to meet at [[Poor Richard's]]. Roy and Ryan help Packer out of the office because he is so wasted. While Michael gathers his things, a completely inebriated [[Meredith]] enters his office and takes off her top. He sort of gasps, stares for a moment, quickly takes a picture of her, and leaves.

==Trivia- Caution, here be spoilers==
* The special contents of the teapot:
** A cassette - no explanation given, possibly a mix tape. Jim and Pam shared an iPod outside the office after work, and Jim had put music on during [[Email Surveillance|Jim's party]]
** High school yearbook photo - Pam saw it at [[Email Surveillance|Jim's party]]
** "Boggle" timer - no explanation
** Two hot sauce packets - Pam mistook it for ketchup
** Miniature golf pencil - no explanation. In an interview, John Krasinski explains that Pam threw it at Jim in jest, presumably after losing.[http://www.givememyremote.com/remote/?p=104] In the [[season 9]] episode "[[A.A.R.M.]]", when Pam watches the video Jim made for her he is seen explaining the pencil.
** Card with special message - Jim snuck the card back into his pocket before she could see it
* Writer Michael Schur noted in the DVD commentary that one theme of the episode is that Phyllis's day gets progressively better, while Angela's gets progressively worse.
* Todd Packer hangs mistletoe from his pants. In Episode Five of Series Two of the British version of the series, Chris Finch places a red clown nose on his crotch and invites the receptionist to kiss him "on the nose."
* Ironically, Meredith is the only one in the conference room to not be labelled as a "Ho" or "Pimp" by Michael.

==Deleted scenes==
Notable cut scenes include:
* Oscar and Creed move a desk, but Creed moves it too far and pins Oscar to the wall.
* Michael talks to the camera about the role each of the employees plays at Christmas time. Dwight is the monster who puts the star on the tree. He also claims that Dwight looks like Spock (a reference to the large pointed ears worn as part of his elf costume), which annoys Dwight to no end, since, if he wants to look like Spock, he has a pair of Vulcan-style pointed ears at home.
* The Party Planning Committee decorates the tree while Dwight asks people whom they're bringing to the party. Phyllis confides to the camera that her new boyfriend is Bob Vance.
* Dwight compares humans to bears opening presents.
* More of the Yankee Swap, including Kelly getting mad at Oscar for not liking her gift, Oscar telling the camera that she got him the same gift last year, Oscar opening Pam's present, and Phyllis storming out of the room.
* Kelly and Toby trade presents. Toby had the "Kelly" nametag, and Kelly had the book of short stories.
* Kevin sings a whole verse of "Christmas in Hollis".
* Dwight explains to the camera that he must remain sober at the party because he is an officer of the law. In the background, Meredith's dancing becomes increasingly animated.
* Ryan tells Toby that he got the book for him. Ryan ended up with Creed's jacket, which he intends to give to a homeless person. Creed tells Oscar that he once had a radio show back in the 1970s.
* Angela says to Kelly, "You behaved very badly tonight."
* Michael explains to the camera that he loves it when people envy the gift you gave someone else.

==Cultural references==
* Michael barges into the Party Planning Committee meeting shouting ''Merry Christmas!'' with a lisp, imitating [[Wikipedia:James Stewart (actor)|Jimmy Stewart]]'s character from the movie ''[[Wikipedia:It's a Wonderful Life|It's a Wonderful Life]]''.
* Michael points at Pam, Angela, Phyllis, and Ryan, saying ''Ho, ho, ho, pimp!'' "Ho-ho-ho" is the traditional laugh of [[Wikipedia:Santa Claus|Santa Claus]]; Michael makes a pun on ''ho'' (the laugh syllable) and ''<nowiki>ho'</nowiki>'' (slang for ''whore'').
* Michael misstates his bonus as ''three thousand G's'', or $3 million. In the episode [[Email Surveillance]], Michael said, "There are certain things a boss does not share with his employees. His salary." But in this episode, he shares the fact that he got a $3000 bonus.
* ''Secret Santa'' is a gift exchange in which each participant is randomly assigned another participant for whom to buy a gift. The identity of the giver is typically not revealed.
* ''Rockefeller Center'' is a plaza in New York City. The lighting of the center's Christmas tree is a major local event.
* ''Yankee Swap'', also known as ''Nasty Christmas'' or ''White Elephant Exchange'', is pretty much as described in the episode.
* The liquor store clerk is wearing a necktie because Michael is shopping at a state-run liquor store, and the dress code requires the clerk to wear a necktie.
* Roy and Darryl's conversation in the kitchen is about ''Fantasy Football'', a game in which participants build a team of actual football players and earn points based on the performance of those players.
* Michael calls Jim ''Ebenezer''. Charles Dickens' story ''[[Wikipedia:A Christmas Carol|A Christmas Carol]]'' is a tale of the curmudgeonly ''[[Wikipedia:Ebenezer Scrooge|Ebenezer Scrooge]]''.
* ''Boggle'' is a word game by Parker Brothers.
* Dwight plans to use the teapot as a [[Wikipedia:Nasal irrigation|neti-pot]].
* ''Poor Richard's Pub'' is a real pub in Scranton.
* The animated special Michael refers to in a deleted scene is ''Rudoph the Red-Nosed Reindeer''. The abominable snowman Bumble loses his teeth but reforms and puts the star atop the tree.
* [[MemoryAlpha:Spock|Spock]] is a character from the ''[[Wikipedia:Star Trek|Star Trek]]'' franchise; his people, the [[MemoryAlpha:Vulcan|Vulcans]], are distinguishable from Humans by their ears, which form a point at the top.

==Quotes==
:see ''[[Christmas Party Quotes]]''

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Recurring Cast===
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Phyllis Smith]] as [[Phyllis Lapin]]
*[[David Denman]] as [[Roy Anderson]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[David Koechner]] as [[Todd Packer]]
*[[Bobby Ray Shafer]] as [[Bob Vance]]

===Guest Cast===
*Ryan Martin as Liquor Store Cashier
<br />



{{Season2}}
