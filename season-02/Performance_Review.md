# Performance Review

**\"Performance Review\"** is the eighth episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 14th overall.
It was written by Larry Wilmore and directed by Paul Feig. It first
aired on November 15, 2005. It was viewed by 8 million people.

Synopsis
--------

Michael gets a call from Jan who will be coming down to Scranton to do
his performance review. She explains that there will be no other topics
of discussion. Michael tells the camera that he and Jan mutually agreed
that their relationship will remain professional.

In the meantime, it\'s everyone else\'s performance review time, as
well. Dwight tries to convince Jim to tell Michael that they should sell
more of a product they don\'t sell at all. Jim won\'t fall for it and
irritates Dwight by telling him he\'s going to ask for a pay decrease
instead of a raise. Then, after hearing Dwight inadvertently mess up the
day, Jim decides to spend the rest of the day convincing him it\'s
Friday instead of Thursday.

Michael plays back Jan\'s message during Pam\'s review, asking her
opinion. Pam starts to explain that she believes Jan means she didn\'t
want to talk about anything else. Michael feels bummed and decides to
proceed with her review. Not wanting his bad mood to affect her salary,
Pam quickly backpedals to state that she believes Jan is conflicted
because they work together and doesn\'t want her relationship with
Michael to be awkward.

Stanley recommends that Michael focus on Jan\'s pauses as many women say
more in their pauses. He even agrees that he learned all this wisdom
\"on the ghetto\". He tells the camera this is all about his bonus.

Jan calls back in response to Michael\'s many calls, interrupting
Angela\'s highly-anticipated review. He tells her he needs closure on
their relationship. She is adamant that they are not going to discuss
anything else at their meeting. Michael realizes he needs more help so
he asks Pam to get the suggestion box and get everyone in the conference
room for a suggestion box meeting.

Upon Jan\'s arrival, Michael continues to try to steer their
conversation to personal issues. He can\'t seem to understand that her
duties as his supervisor do not constitute a personal interest in him.
Pam tells him that the employees are gathered for the suggestion box
meeting. Jan decides to sit in on it.

The suggestion box has clearly not been emptied in years. One question
is about preparation for Y2K. Another appeal for mental health support
is nixed when it\'s revealed that it was submitted by Tom, a former
employee who left about a year ago. Phyllis mimes shooting oneself.

Michael is particularly dismissive of the suggestions, especially when
they become personal. He gets into rude arguments with some of the
employees when they try to explain. He lectures them about how they
don\'t want to have to do this tomorrow. Dwight agrees. After all, they
don\'t want to come in on Saturday. Michael starts to wonder at that,
then pulls another suggestion: Michael shouldn\'t sleep with his boss.

Jan pulls Michael to his office where she can\'t understand what she has
to do to convince him that they have no personal relationship. Dwight
bursts in for his performance review, revealing that he takes no sick
time and shows up on weekends and holidays using a copy of the building
key. Jan expresses her concern that this is a serious offense. When
Dwight insists on pitching himself, Jan takes a few minutes out in the
hallway, refuses to answer the camera crew\'s questions, but does ask
for a light for her cigarette.

Finally, Jan emerges to tell Michael she\'s leaving. She and Alan,
Dunder-Mifflin\'s CFO, will do Michael\'s performance review tomorrow by
phone. Michael races to the elevator to try to get an understanding of
what\'s happening. Jan explains in no uncertain terms that they are not
in a relationship. Michael was very nice that evening after the Chili\'s
meeting, letting her cry and encouraging her during this vulnerable time
after her divorce, but that\'s all it was. She has no romantic feelings
towards him. It\'s got nothing to do with his looks, but his personality
is selfish and rude and annoying.

Now that Michael knows it\'s not his looks, he\'s much happier. He tells
the camera that Jan isn\'t ready for a relationship right now\...still
completely missing the point.

The next day, Michael wanders into the sales department and asks where
Dwight is. Jim hasn\'t seen him. Michael wanders off muttering about
Dwight\'s claim to never miss work. Outside, Dwight, having realized
it\'s not Saturday, after all, races inside, yelling his apologies.

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode.

Trivia
------

-   This episode contains some of the first hints of the secret romance
    between Dwight and Angela: Dwight smirks when Jim believes he\'s not
    having sex, and Angela tells Oscar and Kevin that \"office romances
    are nobody\'s business but the people involved.\"
-   The popped fitness orb is actually a blooper. The script called for
    the orb to deflate slowly, but [John
    Krasinski](John_Krasinski "wikilink") hit a seam, and the orb
    popped. You can even see Krasinski jump out of the camera\'s view
    because he was laughing so much.
-   When rehearsing the welcome scene, [Steve
    Carell](Steve_Carell "wikilink") accidentally touched [Melora
    Hardin](Melora_Hardin "wikilink")\'s breast. She thought it was so
    funny that she pushed for it be added to the script.[^1]
-   Right before Pam talks to Jim about \"The Apprentice\", Meredith is
    seen placing a piece of lined yellow paper into the suggestion box.
    Later the note reading \"Don\'t sleep with your boss\" is shown as
    being on lined yellow paper, implying Meredith wrote it.
-   During the suggestion box meeting, one of the messages, regarding
    employee outreach for depression, comes from \"Tom\", a co-worker
    who had recently committed suicide. During the [Writer\'s
    Block](Writer's_Block "wikilink") Q&A session at [The Office
    Convention](The_Office_Convention "wikilink") in 2007 (shown on the
    [Season 4 DVD](Season_4_DVD "wikilink")), it is implied that,
    possibly\...

:\* Ryan was hired as a temp to replace Tom

:\* The documentary crew\'s original purpose was to observe how
DM-Scranton coped with the death of an employee, but changed their focus
when they realized that the day-to-day ennui of a Michael-run office was
a far more interesting subject.

-   Jim states that the Saturday of the week that the episode takes
    place within is the 15th. It is likely that the Saturday will be
    November 15, meaning that this episode takes place on November 13,
    2005

Goofs
-----

-   In the episode [The Fire](The_Fire "wikilink"), Dwight says,
    \"Michael is in there right now evaluating the temp. He hasn\'t
    evaluated me in years.\" However, Pam says that performance reviews
    are annual and company wide, and goes on to describe her review from
    the previous year.

<!-- -->

-   The members of the office are able to clearly hear the entire
    conversation between Michael and Jan outside the elevator despite
    the fact that there are multiple doors separating them.

<!-- -->

-   In the beginning scene when Jim pops the fitness orb he was so
    surprised he jumps off screen laughing.

<!-- -->

-   Dwight mentions having walking pneumonia, although in the episode
    Health Care, he says he has never been sick.

Cultural references
-------------------

-   *I missed you* can be interpreted two ways. Jan intended it to mean
    \"I failed to get in contact with you,\" but Michael interprets it
    to mean \"I am saddened by your absence.\"
-   *The Apprentice* is a television reality game show hosted by Donald
    Trump. Every week one contestant is eliminated (\"fired\").
-   Dwight repeats every suggestion after Michael reads it, in the style
    of Ed McMahon, sidekick to former late night talk show host Johnny
    Carson.
-   *Y2K* is shorthand for the year 2000. Computer failures caused by
    the change of century were a major concern in the 1990s.
-   *B.O.* is slang for *body odor*.
-   *Lex Luthor* is the villain from the television series *Smallville*,
    which is about the teenage superhero Superman.

Quotes
------

:   see *[Performance Review
    Quotes](Performance_Review_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

References
----------

<references/>
\

[^1]: *Performance Review* DVD commentary
