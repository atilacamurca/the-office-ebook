# Email Surveillance

**\"Email Surveillance\"** is the ninth episode of the second season of
*[The Office](The_Office_(US) "wikilink")* and the\'\' \'\'15th overall.
It was written by Jennifer Celotta and directed by [Paul
Feig](Paul_Feig "wikilink"). It first aired on November 22, 2005. It was
viewed by 8.1 million people.

Synopsis
--------

Michael spies the IT guy walking across the parking lot and quickly runs
into the office, (assuming that he\'s a terrorist) telling everyone to
get down and be quiet. He turns out the light and their cover is blown
by Kevin, who asks Michael questions. Michael explains that he and the
IT guy, Sadiq, did not get off to a good start.

Sadiq is setting up an email surveillance program so that employee
emails can be monitored. Michael goes through a few and discovers that
all the employees except for him have been invited to a BBQ at Jim\'s
place.

The employees learn that their emails will be monitored. After Pam
overhears Dwight and Angela discussing deleting sensitive emails, she
wonders if they are in a relationship.

No one will admit to Michael that they are going to a party. Jim
doesn\'t want Michael there because he wants the employees to feel free
to have fun. Besides, his roommate doesn\'t believe Dwight actually
exists. To get Dwight to stay quiet, he claimed the party was actually a
surprise party for Michael.

Finally, Michael decides he is just going to go to his Improv class.
Michael\'s not very good at Improv. He repeatedly ruins his classmates\'
scenes by resorting to his sole character: a gun-wielding action hero.
The teacher is unable to get him to cooperate. When the class goes to
dinner together after their session, they do not include Michael.
Instead, he crashes Jim\'s party.

At the party, Jim conducts a tour of his house, and finds a distracted
Pam in his bedroom. She looks at his yearbook, calling him dorky. Kelly
doesn\'t want to talk about work. Angela is disgusted because Jim has
offered no vegetarian food. Pam asks Phyllis if she knows of any office
romances. Phyllis immediately asks if she\'s talking about herself and
Jim. Stunned, Pam backs off.

She tells the camera that, sometimes, people can just be friends and it
doesn\'t mean there\'s anything else there. It\'s best not to make
assumptions. She doesn\'t see Dwight\'s sandals intertwined with
Angela\'s shoes in the background where they\'re clearly making out.

When Michael arrives, Jim joins him in Karaoke.

Trivia
------

-   When Jim and Pam are speaking in Jim\'s room at the BBQ, the video
    game *Call of Duty: United Offensive*, an expansion pack to *Call of
    Duty*, lies on Jim\'s desk. (It is best visible when Pam is looking
    at Jim\'s yearbook.) Toby plays the [Xbox 360](Xbox_360 "wikilink")
    version of \'\'Call of Duty 2, \'\'at 20:08 there is a clear shot of
    a WWII-era sniper rifle on each split section of the screen. *Call
    of Duty* plays a more prominent role in the third season episode
    \"[The Coup](The_Coup "wikilink")\".
-   When Michael is reading Jim\'s invitation, the date on the e-mail
    for the party is November 5, 2005, which was a Saturday, even though
    this episode takes place on a Friday. It was likely an error by the
    production staff or a liberty taken by the writers. This places this
    episode on November 4 or 5, 2005, depending on which of those
    scenarios is correct.
-   One of Michael\'s improv partners is Ken Jeong who we have more
    recently seen in Community and The Hangover.
-   When a cameraman helps Pam figure out if Dwight and Angela are
    secretly dating, it becomes the first time, and the only time until
    season 9, that the documentary crew bears a direct impact on the
    plot.

Deleted scenes
--------------

Notable cut scenes include:

-   Michael tries to eliminate wasted e-mail space on computers,
    specifically by suggesting that Jim cancel a charge from Amazon
    rather than continue to contact them regarding an undelivered CD.
-   Michael and Mary Beth argue following their improv scene
-   Michael makes suggestions for an improv scene despite being asked
    not to.
-   Kevin\'s mocking of Ryan by calling him the \"Fire Guy\" during
    grilling is taken to an obnoxious level.

Analysis
--------

-   Michael explains that he and [Sadiq](Sadiq "wikilink"), the IT guy,
    \"did not get off to a great start,\" indicating that the two had
    never met before. Nevertheless, Sadiq attends Jim\'s barbecue,
    either an indication that Jim is quick to make friends or that
    Michael does not pay attention to the IT employees, as is brought up
    in later episodes.

Amusing details
---------------

-   Michael arrives just as Kevin sings, \"Just turn around now, \'cause
    you\'re not welcome any more.\"

<!-- -->

-   Tacked to the cork board above Jim\'s desk at home is what appears
    to be a tiny doll dressed as a Dunder Mifflin warehouse employee
    hanging from a noose. Maybe Roy?
-   The shirt that Creed is wearing in the background at Jim\'s party
    when Pam asks Phyllis about any possible office romances, is the
    same shirt he gives to Jim as Jim\'s Secret Santa in the following
    episode \'Christmas Party\'.
-   When Jim and Michael are singing *Islands in the Stream*, Jim winks
    at Pam during the part, \"Makin\' love with each other.\"

<!-- -->

-   The Directors make a point to show Dwight and Angela\'s shoes at
    Jim\'s BBQ, He shows off his Birkenstock, Angela says she got sap on
    her shoes and the camera pans down and shows her shoes. later in the
    episode the camera shows both of there feet together implying they
    are kissing or have sex. And we know its them because we have seen
    there shoes earlier in the episode.
-   \"Michael Scarn\" is Michael\'s ficticious Secret Agent character
    who appears in his movie, Threat Level Midnight.
-   Even the IT Tech guy was invited to the party.

Goofs
-----

-   In Meredith\'s e-vite that Michael opens, the \"Yes\" list says 14,
    but only 10 names are listed. When Michael reads off the names of
    attendees, he does not say (and the screen does not show) Joe, Mike
    and Matt - who all appeared the first time the e-vite was shown.
    This further reduces the \"Yes\" confirmations to 7.
-   When Michael asks Dwight about his evening plans, Dwight\'s coat
    collar changes from flipped up to folded down.
-   When Michael leaves the improv class, he does not wear a necktie,
    but he has one on when he arrives at Jim\'s party. It seems unlikely
    that he would put on a necktie to go to Jim\'s party, so this is
    probably a continuity error.
-   The set decorators used the same poster in Jim\'s room and along the
    staircase.
-   Michael mentions eating ramen noodles in college, but he never went
    to college. This may just be Michael lying, but in other episodes he
    seems more open about his lack of post-secondary education.
-   A copy of *Call of Duty* is seen on Jim\'s desk at home. However, in
    \"[The Coup](The_Coup "wikilink"),\" Jim indicates that he has never
    played the game before.
-   We see a Guitar in Jim\'s room, and later in local ad (season 4
    ep 5) Jim makes a Second Life account where he has a guitar slung
    across his back. When Pam said \"I did not know you played guitar\",
    he quickly changes the subject. Does Jim want to play guitar and
    never got around to it, or is he just embarrassed to let anyone know
    he plays guitar because he makes himself out to be a sports kind of
    guy?

Cultural references
-------------------

-   Michael says *Oil can* in an impression of the Tin Man character
    from the movie *The Wonderful Wizard of Oz*.
-   *Big Brother* is the personification of the surveillance state in
    George Orwell\'s dystopia *1984*. Also from *1984*, the direct quote
    *\"I love Big Brother\"*
-   *Battlestar Galactica* is a science fiction television program.
-   Michael shouts, *Oooh, ooh, Mr. Kotter!* in imitation of the
    character Horshack from the 1970s television comedy *Welcome Back,
    Kotter*.
-   *Bernie\'s Tavern* is a pub in Scranton.
-   *Dale Earnhardt* was an American race car driver.
-   *Amazon.com* is a large Internet retailer.
-   *Cup Noodles* or *Cup of Noodles* is a brand of instant,
    microwaveable ramen noodles.
-   An *e-vite* is a form of invitation distributed by email.
-   Michael says \"You too, Dwight?\" which could be a reference to
    Shakespeare\'s \'\'Julius Caesar \'\'and the famous quote, \"Et tu,
    Brute?\" as Michael\'s most loyal companion, Dwight, betrays him
    just as Brutus betrayed Caesar. Another \'\'Julius Caesar
    *reference* *is made in \"[Special
    Project](Special_Project "wikilink").\"* \'\'

Quotes
------

:   see *[Email Surveillance
    Quotes](Email_Surveillance_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited)
-   [Omi Vaidya](Omi_Vaidya "wikilink") as [Sadiq](Sadiq "wikilink")
-   [Trish Gates](Trish_Gates "wikilink") as [Stacy](Stacy "wikilink")

### Recurring cast

-   [Jeffrey Muller](Jeffrey_Muller "wikilink") as
    [Mark](Mark "wikilink")
-   [Michael Naughton](Michael_Naughton "wikilink") as
    [Chris](Chris "wikilink")
-   Jonathan Browning as Steve
-   Ken Jeong as Bill
-   Colleen Smith as Stephanie
-   Anderson Reid as Dave
-   Nikki McCauley
-   Dee Ryan

\
