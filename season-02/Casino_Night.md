# Casino Night

**\"Casino Night\"** is the twenty-second episode of the second season
of \'\'[The Office](The_Office_(US) "wikilink") \'\'and the 28th
overall. It was written by Steve Carell and directed by Ken Kwapis. It
first aired on May 11, 2006. It was viewed by 7.7 million people. It is
the first \"supersized\" episode of the series, being 28 minutes in
length instead of the standard 22 minutes.

Synopsis
--------

The Dunder Mifflin warehouse is converted into a full-blown gambling
hall for the [Scranton Business
Park](Scranton_Business_Park "wikilink")\'s Casino Night.
[Jim](Jim_Halpert "wikilink") \"proves\" to
[Dwight](Dwight_Schrute "wikilink") that he has telekinesis (with help
from [Pam](Pam_Beesly "wikilink")). [Michael](Michael_Scott "wikilink")
invites [Jan](Jan_Levinson "wikilink") to the Casino Night, but she
declines. Afterwards, he announces to the office that the employee with
the highest chip count will get a mini-fridge and \$500 to donate to the
charity of their choice. Jim and Pam set out to watch horrible band demo
tapes for Pam\'s wedding. Jim explains to the documentary crew that he
met with Jan about the sales position in Stamford because he has \"no
future here.\"

Among the tapes, the two spot a Police cover band called
[Scrantonicity](Scrantonicity "wikilink"), in which
[Kevin](Kevin_Malone "wikilink") is the lead singer and the drummer. Jim
is all for this choice but Pam is embarrassed about it and prevents him
from asking Kevin. Michael\'s real-estate agent
[Carol](Carol_Stills "wikilink") calls to set up an appointment to sign
his insurance papers for his mortgage and he asks her to come to the
festivities. In the middle of the call, Michael takes a call from Jan,
who tells him that she has decided to come to the casino night after
all. When he returns to Carol\'s call, she also accepts his invitation.
Michael states that he has \"two queens\" for the Casino Night and that
he is going to \"drop a deuce on everyone.\"

Carol arrives at the warehouse. Dwight states that he is in charge of
keeping Jan away from Carol. [Creed](Creed_Bratton "wikilink") reveals
that he is a kleptomaniac, stealing chips, money, and various items from
work and at Casino Night. Michael loses all of his money to
[Toby](Toby_Flenderson "wikilink") early in Texas hold \'em. Jan arrives
and tells Michael she is okay with Carol. Dwight wins at craps and
kisses [Angela](Angela_Martin "wikilink") on the cheek. She slaps him
and walks away with a grin on her face (Dwight has a smirk, too). Jim
confronts Ryan about his relationship with
[Kelly](Kelly_Kapoor "wikilink"), to which Ryan responds with a sheepish
grin in the affirmative. Jim tries to check to see if Pam is bluffing in
poker, which she isn\'t.

At the bar, Jan and Carol share an awkward conversation. Jan asks Carol
how long she and Michael have been in a relationship. Carol replies that
it is their first date, to which Jan says condescendingly that Carol is
a \"good sport\".

After Kevin---who claims to be a professional card player---loses to the
amateur [Phyllis](Phyllis_Lapin "wikilink") in poker (\"Look,\" she
exclaims happily, \"I got all the clovers!\"),
[Roy](Roy_Anderson "wikilink") approaches him and approves his band for
the wedding (without talking to Pam about it), and when Kevin asks
whether or not Pam had given her okay on the matter, Roy simply states
\"Whatever, I\'m in charge of the music.\". Jan and Jim share a scene
outside, where she laments ever hooking up with Michael. She asks him if
he has thought about the transfer, which he says he has. Jan suggests
that he talk to someone in the office about it. Creed ends up winning
the mini-fridge due to all the chips he has stolen, saying that it will
be the first refrigerator he has owned.

Roy leaves the party early despite Pam\'s requests that he stay.

Jim approaches Pam because he needs to talk to her, presumably to tell
her about the possibility of him transferring to Stamford. Instead, he
blurts out that he is in love with her. She asks why he is telling her,
to which he replies that he wanted her to know. After a stunned pause,
she states she cannot be with him. He tells her he wants to be more than
friends, but she is sorry he \"misinterpreted things.\" With tears in
his eyes, he states he is sorry he misinterpreted their friendship and
discreetly wipes a tear from his cheek as he walks away.

Jan leaves the party bitterly and when she gets to her car, she tosses
an overnight bag angrily into the backseat. Michael is pleased \"the
hero got the girl.\"

Upstairs in the dark office area, Pam calls her mother to talk about
what just happened with Jim, explaining that she does not know what to
do. Jim enters the office, and Pam abruptly ends the phone conversation.
He approaches her, and she starts to say something but Jim kisses her
deeply on the lips, and she returns the kiss. The episode ends with them
staring at each other in awkward confusion.

Trivia
------

-   The playing cards used during the poker games are \"Bicycle Rider
    Back\" brand, produced by the United States Playing Card Company.
    This is strange because the Rider back is uncommonly used in large
    or multiple games such as these, because of their expensive price
    when purchased in bulk. \"Bee\" brand playing cards, also produced
    by the United States Playing Card Company, are much more common
    (because of their cheap price) to a point where they are almost
    synonymous with public poker games, or poker games in film.
    1.  Kevin, Dwight, and Jim all claim that they have a leg up in the
        casino night. Kevin says he won the 2002 \$2,500 No-Limit
        Deuce-to-Seven-Draw Tournament at the World Series of Poker in
        Vegas. Dwight says he can \"read people\" (he says whenever Jim
        coughs, Jim has a good hand). And Jim claims he has telekinesis,
        which he can use in roulette.
    2.  Coincidentally, all of these seem to be wrong, as Kevin sucks at
        Poker, Jim just coughs a lot (most likely to mess with Dwight),
        and Jim and Pam cooperated to show Dwight and the others that he
        has telekinetic powers.

<!-- -->

-   When Pam calls her mother she is using Jim\'s phone.

Amusing details
---------------

-   In the opening talking head interview, Michael refers to himself as
    a \"great philanderer\" in reference to hosting the casino night for
    charity. The word he is looking for is \"philanthropist,\" which is
    a person dedicated to contributing to charitable efforts to promote
    human welfare. A \"philanderer\" is somebody who is known to have
    many sexual relationships with many different women over short
    periods of time.

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Michael calls a conference and asks for parts of speech for a
    homemade version of Dunder Mifflin [Mad
    Libs](Wikipedia:Mad_Libs "wikilink").
-   In separate talking head interviews, Dwight and Jim reveal the
    charities they\'ve chosen.
-   In a talking head interview, Dwight cautions that Jim should use his
    telekinetic powers wisely.
-   Michael reassures Darryl that the company can reimburse for any
    damages to the warehouse. In a talking head interview, Michael
    admits that \"our numbers are down\" but that Jan is on his side.
-   In a talking head interview, Pam admits that planning a wedding by
    herself is stressful.
-   The casino dealer recognizes Meredith, who, embarrassed, admits that
    she doesn\'t remember people she\'s had sex with. The dealer
    explains, \"I\'m your vet.\" He then adds, \"We had sex in the
    parking lot.\"

Cultural References
-------------------

-   *[David and Goliath](Wikipedia:Goliath "wikilink")* is the Biblical
    tale of a young boy who defeats a giant.
-   *[Eva Perón](Wikipedia:Eva_Perón "wikilink")* was the first lady of
    Argentina in the 1940s and 50\'s. *[César
    Chávez](Wikipedia:César_Chávez "wikilink")* was an American labor
    activist in the 1950s through the 70\'s. There is no connection
    between them.
-   *[Friends with
    privileges](Wikipedia:Casual_relationship "wikilink")* is most
    likely a mishearing of the phrase "friends with benefits," which is
    colloquially considered slang for friends who also share a sexual
    relationship.
-   The *[Boy Scouts of
    America](Wikipedia:Boy_Scouts_of_America "wikilink")* is an
    organization for boys, building character via outdoor activities.
    Homosexuals are not allowed to hold positions within the
    organization, which may be the source of Oscar\'s objection. The
    *[Girl Scouts](Wikipedia:Girl_Scout_of_the_USA "wikilink")* is a
    corresponding organization for girls. Girl Scouts are well known for
    [selling cookies](Wikipedia:Girl_Scout_cookie "wikilink") to raise
    money.
-   The American *[Comic
    Relief](Wikipedia:Comic_Relief#In_the_United_States "wikilink")*
    charity organization raised money to provide health care for the
    homeless. It was revived in 2006 (after this episode aired) to raise
    money for the victims of [Hurricane
    Katrina](Wikipedia:Hurricane_Katrina "wikilink"). (Coincidentally,
    an episode of the British *The Office* included a fundraiser for the
    British *[Comic Relief](Wikipedia:Comic_Relief "wikilink")*
    organization.)
-   *[Kobe Bryant](Wikipedia:Kobe_Bryant "wikilink")* is a professional
    basketball player who in 2003 was charged with sexual assault. Upon
    admitting to the incident (which Bryant claimed was consensual), he
    bought his wife a \$4 million diamond ring as an apology. The
    charges were ultimately dropped. His charitable foundation, devoted
    to benefiting children and preventing child abuse, was dissolved
    shortly before the alleged sexual assault took place.
-   *Hooters* is a chain of restaurants featuring buxom waitresses.
-   The fake band name *Till Death Do Us Rock* is a play on the vow
    *Till death do us part*, part of the traditional Christian wedding
    ceremony.
-   *KISS* was a rock band from the 1970s whose members wore
    characteristic makeup and costumes.
-   *Negroes* is a term for people from sub-Saharan Africa. After the
    Civil Rights movement in the United States, the term fell out of
    favor and is considered offensive by some. Darryl uses it
    ironically.
-   *Lollapalooza* is an American music festival featuring three stages.
    (Later versions featured even more.)
-   *Scrantonicity* is a play on the album *Synchronicity*, produced by
    the band *The Police*, whose songs Scrantonicity performs.
-   *Jan Levinson I presume?* is a play on *Dr. Livingstone I presume?*,
    words uttered by explorer H. M. Stanley upon successfully finding
    David Livingstone in Africa.
-   *Dropping a deuce* is slang for defecation. Michael means to say
    that he will \"drop a bomb\" on everyone, which means to reveal a
    big surprise.
-   *RE/MAX* is the largest real estate agency in the United States.
-   *Chili\'s* is a chain of casual dining restaurants.
-   In his opening speech at casino night, Michael says \"Willkommen,
    Bienvenue, and welcome to Monte Carlo!\", similar to the opening
    number from the [musical](Wikipedia:Cabaret_(musical) "wikilink")
    and [film](Wikipedia:Cabaret_(film) "wikilink") *Cabaret* which
    begins \"Wilkommen, Bienvenue, welcome, im cabaret, au cabaret, to
    cabaret!\" Michael adds, \"Leave all your preconceived notions about
    casinos at the door,\" echoing the famous line \"Leave your troubles
    outside!\"
-   *Let\'s Get It Started* is a song by musical group Black Eyed Peas,
    which Michael mis-identifies as Black Eyed Crows, confusing it with
    the musical group The Black Crowes.
-   To go *all in* is to bet all one\'s money. To *take someone all in*
    is to make a bet that forces an opponent to decide whether or not to
    go all in.
-   A *tell* is an unconscious signal that betrays whether the player
    has a strong hand.
-   Michael\'s explanation *If luck weren\'t involved, I would always be
    winning* is a paraphrase of professional poker player Phil Hellmuth
    who said, \"If there weren\'t luck involved, I would win every
    time.\"
-   *Craps* is a dice-rolling game.
-   To *let it ride* is to take the winnings of a bet and bet it again
    on the same thing.
-   A *cosmopolitan* is a cocktail made with vodka, triple sec, and
    cranberry juice. Suiting Jan, it is more of a \"big city\" drink.
-   A *Seven and Seven* is a cocktail made with Seagram\'s 7 whiskeys
    and 7-Up.
-   In craps, the *point* is the number the person throwing the dice
    (the *shooter*) is trying to roll. The honor of *blowing on dice for
    luck* is traditionally given to the shooter\'s girlfriend (or any
    pretty girl).
-   Dwight lists several hotel and motel chains. The *Hyatt* is a
    high-end hotel, *Radisson* is an upper-mid-range hotel, *Best
    Western*, and *Holiday Inn* are mid-range hotels, and *Super 8* and
    *Motel 6* are motel chains. There is a Radisson in downtown Scranton
    and a Super 8 and a Holiday Inn in nearby Dunmore, all reasonable
    choices for Jan. The nearest Best Western is in Wilkes-Barre, a less
    plausible choice. The nearest Motel 6 is in Binghamton, an hour\'s
    drive away, and nearest Hyatt is in Morristown, NJ, nearly two
    hours\' drive away; both are unreasonable.
-   *Wilkes-Barre* is a city approximately 30 minutes\' drive from
    Scranton.
-   Phyllis uses the term *clovers*, a children\'s term for the suit
    more formally known as *clubs*. A *flush* is a hand that consists of
    five cards all the same suit.
-   *Mad Libs* is a children\'s game consisting of an incomplete story.
    Players fill in the words without knowing the story; the result is
    intended to be humorous. *Office Depot* is a large chain of office
    supply stores, and one of Dunder Mifflin\'s main competitors. The
    planet *Caprica* is the human home planet in the science fiction
    television series *Battlestar Galactica*. *Cylons* are the robotic
    enemy.
-   Dwight\'s line \"With great power comes great responsibility\" is
    taken from the Spider-Man comics.
-   Jim\'s story about his telekinetic powers and their use to him in
    gambling is a reference to *The Prime Mover*; episode 57 of the
    1950s and \'60s American sci-fi anthology series\'\' The Twilight
    Zone\'\', where a man discovers that his business partner has had
    telekinetic powers from a young age, and manipulates him so that he
    may use his powers to succeed in gambling.

Quotes
------

:   see *[Casino Night Quotes](Casino_Night_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Supporting Cast

-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")

### Recurring Cast

-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Nancy Walls](Nancy_Walls "wikilink") as [Carol
    Stills](Carol_Stills "wikilink")
-   [Bobby Ray Shafer](Bobby_Ray_Shafer "wikilink") as [Bob
    Vance](Bob_Vance "wikilink")
-   [Marcus A. York](Marcus_A._York "wikilink") as [Billy
    Merchant](Billy_Merchant "wikilink")
-   [Phillip Pickard](Philip_Pickard "wikilink") as
    [Philip](Philip "wikilink") (Uncredited)

### Guest Cast

-   Britain Spellings as Craps Dealer
-   Bob Harrell
-   Mike Kruzel
-   Amanda Warren
-   Bryan Spinelli
-   Angelo Middione as Bass Player - Kevin\'s Garage Band (Uncredited)

\
