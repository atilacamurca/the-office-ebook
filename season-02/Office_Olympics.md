# Office Olympics

**\"Office Olympics\"** is the third episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 9th overall. It
was written by [Michael Schur](Michael_Schur "wikilink") and directed by
[Paul Feig](Paul_Feig "wikilink"). It first aired on October 4, 2005. It
was viewed by 8.3 million people.

Synopsis
--------

[Ryan](Ryan_Howard "wikilink") comes into work early at
[Michael](Michael_Scott "wikilink")\'s request, merely to bring him his
breakfast. When Michael tells him the office is his for the next few
hours, Ryan opts to spend those hours sleeping in his car until work
starts.

Michael announces that he will be becoming a homeowner.
[Jim](Jim_Halpert "wikilink") \"dies\" of boredom, and to revive him, he
and [Pam](Pam_Beesly "wikilink") try to throw things in
[Dwight](Dwight_Schrute "wikilink")\'s coffee mug. Michael prepares to
leave to sign the final papers for his new condo and Dwight insists that
he join him as a second voice, to which he reluctantly agrees. Michael
assigns Pam to have the office work on the expense reports for the
afternoon. Jim asks [Oscar](Oscar_Martinez "wikilink") about the expense
reports and notices material on his desk that reads \"2005 SEASON\" as
well as a small scoreboard. Oscar explains that he and
[Kevin](Kevin_Malone "wikilink") play a paper football game when
Michael\'s out (or when they\'re bored). Jim joins in and Kevin explains
the game is called \"[Hate Ball](Hate_Ball "wikilink")\", since
[Angela](Angela_Martin "wikilink") hates when they play it. Other games
they play are \"Who Can Put The Most M&Ms In Their Mouth?\" and
\"[Dunderball](Dunderball "wikilink").\" Meanwhile, Michael admires his
condo from the street, until he notices he\'s looking at the wrong
house.

At the condo, Michael\'s agent [Carol](Carol_Stills "wikilink")
introduces him to Bill, the head of the condo association and makes a
Mr. Bill reference. Dwight talks about his 60-acre beet farm that he
runs with his cousin [Mose](Mose_Schrute "wikilink"). He begins asking
questions about the house, and Carol and Bill start to think that he and
Michael are homosexual. Back at the office, Jim tries to get
[Stanley](Stanley_Hudson "wikilink") to join in the fun, but he outright
refuses (although he is seen enjoying the games later). Michael shows
the camera crew the master bedroom at the condo and starts bragging.
Dwight starts to rain on his parade, bringing up the condo\'s
weaknesses, which starts to worry Michael. In the office, Jim and Pam
begin the opening ceremonies of the \"Games of the 1st Dunder Mifflin
Olympiad\" in the lunch room. Jim explains they will be competing for
medals made of gold, silver and bronze yogurt lids\" that Pam made
herself. Angela is annoyed by the games.

At the condo, Michael starts to regret his purchase when he realizes he
got a 30-year mortgage and Dwight makes the situation sound even worse.
Michael takes a breather before he signs the contract. Meanwhile, Jim
and Pam explain the [Flonkerton](Flonkerton "wikilink") race (where full
paper boxes are strapped to your feet) and
[Phyllis](Phyllis_Lapin "wikilink") challenges Kevin. Michael starts
complaining about the condo to Carol and demands a price reduction, but
she explains he will lose \$7,000 if he walks away, so he signs. Dwight
and Michael laugh about the \"saps stuck at the office\" as we see
Phyllis narrowly defeating Kevin in [Flonkerton](Flonkerton "wikilink").

At the condo, Michael tells Dwight he is going to \"let\" him move into
his third bedroom and pay rent. Dwight starts asking Michael a series of
questions, which become increasingly strange. In the office, Angela
tells Pam about a game she plays called \"Pam Pong\", where she counts
how many times Jim goes to Pam\'s desk. The questions continue at the
condo, which pushes Michael to the point of revoking his offer, much to
Dwight\'s relief. At the office, the employees gather around the
elevator to bet who will arrive next and Ryan wins. Pam shows Jim a box
of items for the closing ceremonies and Angela marks off another tally
for \"Pam Pong\", to Pam\'s annoyance. Toby and Oscar race with full
coffee mugs around the office, but the games immediately stop when
Michael and Dwight return, much to Jim\'s disappointment.

Things get quiet again. Jim fills out the expense reports, which only
take five minutes. Ryan throws away Pam\'s medals which she sadly
notices. Pam has 59 voicemails, but attends to Jim\'s insistence that
the closing ceremonies will occur at 5pm. Jim brings out Michael to join
in the ceremonies and gives him the gold medal in front of the entire
staff for closing on his condo. Michael is moved to tears. A perplexed
Dwight gets the silver medal. Jim gets the bronze. Pam plays the
national anthem and releases the origami paper doves she made for the
ceremonies. The employees smile and Michael wells up with tears.

Trivia
------

-   Michael\'s real estate agent, Carol Stills, is portrayed by Steve
    Carell\'s wife Nancy Walls. Like Carell, Walls was also a
    correspondent for *The Daily Show*.
-   This is the first episode to feature a cold open, a device which has
    been used in all episodes since.
-   Dwight tells Michael that he may date a 55 year old woman, a
    foreshadow to when he dated Pam\'s mother,
    [Helene](Helene "wikilink").
-   Michael points out the cable jack then moments later says he will
    place his T.V. on the opposite wall.
-   It can be inferred that Ryan came in at 7:00 A.M. to give Michael
    his sandwich because of Michael\'s line to Ryan, \"Take a **couple
    of hours**, office is yours\...\". The usual workday starts at 9:00
    A.M., and the phrase \"couple of hours\" would normally mean two
    hours, so two hours before nine would be seven o\'clock. In an
    interview with TV Guide, however, B.J. Novak confirmed that the
    scene took place around eight o\'clock.
-   During the closing ceremonies, the workers set the podium with the
    2nd place to the left of the 1st when, traditionally, it is located
    to the right.
-   During the scene in which Pam shows the box of paper doves to Jim,
    and the others are coming back into the office, Stanley walks past
    the camera two times. He walks by once behind Meredith, then after
    Pam looks at Angela, he passes again while speaking with Kelly.
-   The man shouting at his daughter to practice the cello sounds a lot
    like Brian Baumgartner in real life when he isn\'t doing the Kevin
    voice. It is entirely possible that they just had Baumgartner do
    this since his real life voice is different than Kevin\'s voice.

Deleted scenes
--------------

The Season Two DVD will most likely contain a number of deleted scenes
from this episode. Notable cut scenes include:

-   Jim found Dwight\'s wallet in the parking lot, and decides, on
    Pam\'s suggestion, to give the wallet back to Dwight intact and
    unchanged. Dwight suspiciously cancels all his credit cards.
-   Oscar catches Kevin trying to expense a movie rental.
-   Extension of the opening scene with Dwight in Michael\'s office.
    Michael shows Dwight the brochure for his condo.
-   In a talking head interview, Michael explains that real estate
    ownership is the last thing that goes through a man\'s mind on his
    deathbed.
-   Extension of Jim and Pam throwing things into Dwight\'s mug, which
    Pam calls Skeet Schruting.
-   Michael waxes poetic before leaving with Dwight.
-   Dwight has crime statistics for Michael\'s neighborhood. Michael
    talks with a neighbor (played by creator Greg Daniels), who is
    disturbed by Michael\'s chatter.
-   Toby teaches Jim how to play Dunderball.
-   Michael shows off the two microwave ovens in his kitchen before
    meeting Carol and Bill.
-   Dwight inspects the condo.
-   Phyllis denies playing any office games.
-   Michael shows off his condo, and talks about his future plans to get
    married and have children. Dwight continues to find problems.
-   In a talking head interview outside Michael\'s condo, Dwight
    demonstrates his keen sense of smell.
-   Michael threatens to back out of the deal.
-   Dwight finds his mug full of junk.

References to previous episodes
-------------------------------

-   Michael once again reveals that he reads *American Way* (\"[Hot
    Girl](Hot_Girl "wikilink")\").

References in later episodes
----------------------------

-   In \"[The Job](The_Job "wikilink")\", Pam puts one of the yogurt-lid
    medals, along with note asking \"Don\'t forget us when you\'re
    famous!\", in a folder Jim needs when he interviews for a position
    at the DM corporate offices in New York; seeing this, Jim, realizing
    that his heart is still in Scranton, withdraws from the potential
    promotion, and returns home to be with his true love, Pam - who is
    in the middle of a talking head accepting that Jim is most likely
    gone, and will never be returning, when he walks in on the camera
    crew to ask her on their first official date.
-   In \"[Finale](Finale "wikilink")\", Phyllis mentions Flonkerton and
    the Office Olympics when they\'re gathered in the office after the
    panel, and [Creed](Creed "wikilink") mentions that he still has his
    yogurt-lid medal.

Cultural references
-------------------

-   Michael takes his bird analogy too far and says that he \"has
    worms\", unintentionally suggesting that he suffers from an
    intestinal parasite.
-   *Home Alone* and *Risky Business* are movies notable for scenes in
    which the young lead character frolics in an empty house.
-   *Butch Cassidy* was a train and bank robber from the 19th century.
-   *Maxim* is a men\'s magazine; *American Way* is the in-flight
    magazine of American Airlines; *Cracked* was a satire magazine known
    for juvenile humor.
-   *The Terminator* is a science fiction movie from 1984. The lead
    character notably wore sunglasses.
-   *Mr. Bill* was a recurring sketch on *Saturday Night Live* from 1976
    to 1980. Each episode ended with the title character screaming,
    \"Ohhhh, nooooooo!\"
-   *Pam Pong* is a pun on the game *ping pong*.
-   Jim assigns scores to Stanley and Phyllis from the first few letters
    of the word \"horse\". They are apparently playing a variation on
    the basketball game of the same name.
-   The screeching sound Michael makes when he rescinds his offer to
    Dwight is an attempt to mimic the final buzzer of a basketball game.

Quotes
------

:   see *[Office Olympics Quotes](Office_Olympics_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring cast

-   [Nancy Walls](Nancy_Walls "wikilink") as [Carol
    Stills](Carol_Stills "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited)
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink") (Uncredited)
-   [Michael Schur](Michael_Schur "wikilink") as [Mose
    Schrute](Mose_Schrute "wikilink") (Uncredited)

### Guest cast

-   Nancy Walls as Carol Stills
-   John Harrington Bland as Bill

\
