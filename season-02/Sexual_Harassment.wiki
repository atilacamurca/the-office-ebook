{{Office episode
|Title      =Sexual Harassment
|Image      =[[Image:Sexualharassment.jpg|250px]]
|Season     =[[Season 2|2]]
|Episode    =2
|Code       =2002
|Original   =September 27, 2005
|Writer(s)  =[[B.J. Novak]]
|Director   =[[Ken Kwapis]]
|Prev       =[[The Dundies]]
|Next       =[[Office Olympics]]
}}
'''"Sexual Harassment"''' is the second episode of the second season of ''[[The Office (US)|The Office]] ''and the 8th overall. It was written by [[B.J. Novak]] and directed by [[Ken Kwapis]]. It first aired on September 27, 2005. It was viewed by 7.13 million people.

==Synopsis==
[[Michael Scott|Michael]] declares himself "King of Forwards" because he is a master of forwarding what he considers to be funny email to the rest of the office. Michael's "best friend forever" [[Todd Packer]] arrives and shares crude and sexist gossip about an upper management scandal, which continues to offend the staff. [[Ryan Howard|Ryan]] is called upon to drive Packer around town because of Packer's "[[Wikipedia:Driving under the influence|DUI]] situation". Meanwhile, [[Pam Beesly|Pam]] waits with anticipation for her mother to arrive from out of town.

[[Toby Flenderson|Toby]] privately informs Michael that their CFO resigned because of sexual harassment. Consequently, he has been asked by Corporate to hold a five-minute review of the company's sexual harassment policies. Michael interprets this as Corporate cracking down on his inappropriate jokes and is upset that this will put a damper on his easy-going office environment by forcing everybody to "scrutinize every little thing we say and do all day." His indignation rises to outrage when he learns that Corporate is sending down a lawyer to talk to Michael.

Michael goes down to the warehouse to find some dirty jokes that he can use to remind the staff how great jokes can be, but he ends up being sexually harassed himself and runs away in embarrassment. Upstairs, Toby's presentation goes well, and Pam asks everyone to keep it clean when her mother arrives. Just as Toby is wrapping up, Michael arrives with a blow-up sex doll. He makes a speech on not losing the sexual humor around the office, but fails to get the support of the employees.

For laughs, Michael and the warehouse staff watch the sexual harassment video in the conference room. Meanwhile, [[Dwight Schrute|Dwight]] asks Toby questions about the female anatomy. Just as Michael shares a crude remark about the video with the rest of the staff, [[Jan Levinson|Jan]] and the lawyer arrive from Corporate.

Todd Packer returns to the office as Michael meets with Jan and the lawyer, upset that their crackdown on his inappropriate e-mail forwarding means that he basically can't say anything in the office at all. In a talking head interview, Michael defensively explains that as the King of Forwards, he's just forwarding the jokes; he didn't write them and therefore cannot be held responsible for their content. He elaborates, "You wouldn't arrest a guy who's just delivering drugs from one guy to another." Michael emerges from the meeting to announce that he can no longer be friends with his staff and that he will talk only about work. As a result, he will never tell another joke again. [[Jim Halpert|Jim]] goads Michael into breaking his vow immediately.

Jan calls Michael back into his office for another meeting as Michael's "free speech" lawyer arrives. Jan explains that as the corporate lawyer, Mr. O'Malley's job is protect the company and upper-level management, including Michael. Michael is instantly relieved that he's not in trouble.

Just as Michael throws away the sex doll, Pam's mother arrives and after [[Roy Anderson|Roy]] leaves, she asks in whispers (and is shushed by an embarrassed Pam) which one is Jim. Todd Packer shares some more inappropriate jokes with the employees and Michael tries to draw the line, but points the blame at Kevin. He gets teased by Packer and ends up saying how beautiful Phyllis is and saying the only thing he's worried about "is gettin' a [[Wikipedia:erection|boner]]."

==Deleted scenes==
The Season Two DVD contains a number of deleted scenes from this episode.

*Michael is baffled by a casual compliment from Pam.
*Dwight warns Jim not to drink directly from his soda can due to the risk of cross-contamination.
*Kevin receives a doctored celebrity photo via e-mail.
*Dwight carefully washes a soda can.
*Extension of Michael taking over Toby's sexual harassment training.
*In a talking head interview, Michael bad-mouths Toby.
*Pam teases Jim by apologizing for sending instant messages with the winking-face emoticon, concerned that it might be sexual harassment. Jim responds that he has retained the angry-face icon as his attorney.

==Trivia==
*Packer's car occupies two spaces when parked, commonly recognised as a jerk move.
* Jan plays with her cell phone a lot in this episode. In the DVD commentary, [[Melora Hardin]] explains that the prop team gave her a RAZR phone (a novel gadget at the time), and she couldn't stop playing with it.
* When Michael tells Jim to check his email, Michael's email is in the spam folder. Apparently, Jim registers emails from Michael as spam.
* The instructional video was filmed in the writers' offices.
* Writer [[B.J. Novak]] says that the lines "That's what she said" and "What has two thumbs and...? This guy" are things he heard in college.
* During Toby's talking head regarding Dwight's questions about female anatomy, all the candy bars in the vending machine behind him have been turned backwards, so that the names are not visible (though they appear to include M&Ms and Hershey bars).
* When the camera shows a close up of Kevin's computer screen, his list of e-mails shows Pam's last name spelled "Beasly", not "Beesly" like later established. It also shows Dwight's last name as "Shrute" instead of "Schrute."
* It's also interesting to note that Kevin's computer contains two different e-mails with the subject line "Copier isn't working" from Ryan Howard. Since the more recent e-mail doesn't have the prefix "Re:" (as do other e-mails in the inbox), it suggests the copier was either broken twice, or Ryan felt like complaining a second time. Kevin also has an e-mail from "Dwight Shrute" titled "FWD: I found this on the internet;" at this point, Michael had not yet asked Dwight for the link to the "monkey sex video" - meaning Dwight would have forwarded it on his own already.

==Continuity==
* When the camera shows a close up of Kevin's computer screen, his list of e-mails shows Pam's last name spelled "Beasly", not "Beesly" like later established, unless that they were trying to make Kevin look like he wasn't good at spelling like he did with Dwight's surname (putting Shrute instead of Schrute).

==Cultural references==
* ''[[Wikipedia:Michael Jackson|Michael Jackson]]'' is a pop singer who has multiple times been accused of child molestation. In the 1990s, a series of [[Wikipedia:Roman Catholic sex abuse cases|sex abuse scandals involving Roman Catholic priests]] became prominent in the news. Michael combines the two in his joke.
* ''[[Wikipedia:Friends|Friends]]'' is a television comedy that ran from 1994 to 2004.
** [[Wikipedia:Chandler Bing|Chandler]] is sarcastic.
** [[Wikipedia:Joey Tribbiani|Joey]] is dim-witted but has enormous success with women.
** [[Wikipedia:Rachel Green|Rachel]] is attractive but emotionally immature.
** [[Wikipedia:Cosmo Kramer|Kramer]] is the "wacky neighbor" from the show ''[[Wikipedia:Seinfeld|Seinfeld]]''.
* This episode contains, not surprisingly, several slang terms for sex, most of them crude. Packer jokes that he ''boned'' Michael's mother, Darryl claims to have ''banged'' the woman in the video, Michael tells a story of how Packer ''did'' a set of twins, and Randall the CFO was ''nailing'' his secretary.
* Michael says ''Buckle up. It's going to be a bumpy one!'', a botched version of the line "Fasten your seat belts. It's going to be a bumpy night." from the movie ''[[Wikipedia:All About Eve|All About Eve]]'' (ranked #9 on the [[Wikipedia:AFI's 100 Years... 100 Movie Quotes|American Film Institute's 100 best movie quotes]]).
* Michael says ''Forward it like it's hot'' in the style of the [[Wikipedia:Snoop Dogg|Snoop Dogg]] song ''[[Wikipedia:Drop It Like It's Hot|Drop It Like It's Hot]]'', but he mistakenly identifies the song as "old school" hip-hop, despite it being released a mere year before the episode aired.
* ''[[Wikipedia:Driving under the influence|DUI]]'' stands for ''Driving while Under the Influence'', known as ''drink-driving'' in British English.
* Michael tells a joke about a ''lady of the evening'' (a [[Wikipedia:Prostitution|prostitute]]) who gives a customer ''crabs'' (the [[Wikipedia:Crab louse|crab louse]] parasite). The joke is a pun on crabs the parasite and crabs the animal.
* ''Queers R Us'' and ''Boys R Us'' are puns on toy store chain ''[[Wikipedia:Toys "R" Us|Toys "R" Us]]''. ''Business'' and ''package'' are slang for the male groin.
* ''[[Wiktionary:MILF|MILF]]'' is crude slang for ''Mother I'd like to F---;''.
* To ''make out'' is to kiss for an extended period of time.
*The hairstyles and fashion in the instructional video is from the 1980s.
* ''[[Wikipedia:Mo Money Mo Problems|Mo Money Mo Problems]]'' is a hip hop song by [[Wikipedia:Notorious B.I.G.|Notorious B.I.G.]]
* A ''perfect 10'' is someone who is so attractive they would rate 10 points on a scale from 1 to 10. The term entered common usage as the result of the movie ''[[Wikipedia:10 (film)|10]]''.
* The academic year in the United States runs from September to June. In Pennsylvania, a child must have reached age 4 years 7 months prior to the first day of school. Children who begin school under the age of 5 are informally referred to as having a ''late birthday'', and they are the youngest students in class.

==Quotes==
:see ''[[Sexual Harassment Quotes]]''

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Recurring Cast===
*[[David Koechner]] as [[Todd Packer]]
*[[Melora Hardin]] as [[Jan Levenson]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Phyllis Smith]] as [[Phyllis Lapin]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[David Denman]] as [[Roy Anderson]]
*[[Karly Rothenberg]] as [[Madge]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]] (Uncredited)
*[[Devon Abner]] as [[Devon White]] (Uncredited)
*[[Calvin Tenner]] as [[Calvin]] (Uncredited)

===Guest Cast===
*[[Shannon Cochran]] as [[Dunder Mifflin Family Members and Loved Ones#Pam's Mom|Mrs. Beesly]]
*[[R.F. Daley]] as [[Mr. O'Malley]]
*[[Dennis Garber]] as [[James P. Albini]]
*Melinda Chilton as The Natural Redhead
*Andrew Donnelly as Mike
*Keith Valcourt as Ted

{{Season2}}
