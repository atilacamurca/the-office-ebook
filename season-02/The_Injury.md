# The Injury

**\"The Injury\"** is the twelfth episode of the second season of
\'\'[The Office](The_Office_(US) "wikilink") \'\'and the 18th overall.
It was written by Mindy Kaling and directed by [Bryan
Gordon](Bryan_Gordon "wikilink"). It first aired on January 12, 2006. It
was viewed by 10.3 million people.

Synopsis
--------

Michael burns his foot stepping on to a George Foreman grill, and then
tries to convince his staff that he has a serious injury. To do this, he
irritates the office (Pam and Ryan, notably) to help him with his
\"injury\". For example: He asks Pam to spread butter on his foot, and
when Michael falls off the toilet, he yells for Ryan to help him. When
Ryan refuses to do this, Michael calls a meeting in the conference room.
During all this,Dwight, who tries to pick Michael up and bring him in to
work upon hearing of his injury, crashes his car exiting Dunder
Mifflin\'s parking lot, giving himself a concussion. Because of this
Dwight is more helpful and friendly to Pam, which Jim notes. In the
conference room meeting, Michael shows pictures of handicapped people
(Forrest Gump, Franklin D. Roosevelt, Tom Hanks as Josh Baskin in Big
which he mistakes for Philadelphia.) Then the handicapped business park
manager enters and is told he was invited by Michael to come. When he
asks if anyone has any questions about the building rules, Dwight raises
his hand and says nothing. When Michael tells the building\'s manager
that they are similar because they are \"disabled\" this causes him to
wheel out. When Jim sees him enter the elevator he asks Jim \"What\'s
wrong with that guy?\" meaning Dwight. He then states that he looks like
he has a concussion. Ryan, sick of Michael, puts medicine in his pudding
to help him recover. While Michael is boasting about his recovery,
Dwight collapses, and Jim and Michael (Michael being less than
willingly) take him to the hospital. While Jim waits in the waiting
room, he calls Pam at the office, and tells her that Michael is taking
Dwight to get a CATscan. Angela tries to listen into this. The episode
ends with Michael trying to stick his foot into the CATscan machine.

Deleted scenes
--------------

The Season Two DVD contains a number of deleted scenes from this
episode. Notable cut scenes include:

-   Jim popping some of Michael\'s foot packaging bubbles. Jim then says
    he has had a grill for six years, and has never burnt himself on it
    because he does not use it as a pillow.
-   Michael explains that sometimes when he is zoned out, he will pop
    some bubble wrap at home.
-   Dwight explaining that the pioneers never wasted anything, and Jim
    says Michael should go to the hospital.
-   Ryan is assigned to draw a handicapped sign for Michael so he can
    park closer to the building.
-   Michael rubbing butter on his foot, and Angela passes and asks,
    \"Who made popcorn?\"
-   Michael talking to someone on the phone, but he hangs up on him when
    Ryan walks in with Michael\'s movies. He rented some for Michael to
    watch, and then gives Michael some dark meat chicken. Michael then
    tells the camera no matter what you have, if you eat dark meat
    chicken, you can walk on the moon.
-   Dwight using the [copier](copier "wikilink") to copy nothing. In a
    talking head interview, Dwight describes the signs of a concussion,
    but his explanation becomes disjointed and incoherent.
-   Dwight getting scared that he needs to go into a machine. He then
    asks Michael if he will come into the room with him and stand next
    to him.
-   Michael and Jim watch a movie in Spanish in the hospital waiting
    room.

Trivia
------

-   [Brian Baumgartner](Brian_Baumgartner "wikilink") and [Angela
    Kinsey](Angela_Kinsey "wikilink") are caught out of character in the
    opening seconds of the episode. Kinsey admits that she didn\'t
    realize the cameras were rolling.
-   Dwight refers to Pam\'s MP3 player as a Prism Durosport. In reality,
    it looks much more like a [Creative Zen
    Micro](http://uk.gizmodo.com/creative%20zen%20micro.jpg). The
    show\'s writers developed [a fake Web
    site](http://www.prismdurosport.com/) for the Prism Durosport.
-   In [Christmas Party](Christmas_Party "wikilink"), Roy says he was
    planning to buy Pam an iPod, and since Pam traded with Dwight for
    the teapot, it makes sense for her to have an MP3 player from Roy.

Amusing details
---------------

-   When Michael says (in the conference room meeting), \"When I clamped
    my foot into a non-stick grill,\" Pam turns to Dwight and laughs.
-   Michael\'s exasperated, \"I\'m sick of Chuck E. Cheese,\" suggests
    that he goes to the children\'s restaurant often.
-   Despite Michael\'s anger about Pam not having any messages, he did
    have messages when he showed up. Pam says he missed two calls from
    \"corporate.\"
-   The talking heads are conducted outside of the break room, instead
    of the conference room, because of Michael resting there (in the
    conference room).
-   After the exchange in which Dwight gives Pam the cover for her mp3
    player he taps on the counter the same way Jim usually does as he
    walks away.
-   When Jim, Michael, and Dwight are driving to the hospital, an
    allusion to a three person family, Jim being the Mother, Michael
    being the Father, and Dwight being the child, can possibly be seen.
-   Pam has a standard MP3 Player instead of the video iPod that Roy was
    going to get her, implying that he instead took the cheaper route.
-   Michael calls his mom to complain about coworkers several other
    times in the series.
-   Creed says he was in an iron lung as a teenager, suggesting he had
    polio as a child.
-   Michael is completely dry when he leaves the bathroom, even though
    when he falls you can hear several splashes.
-   The property manager complains that someone parked in the handicap
    spot, and it is suggested that Michael was the one who did it. This
    goes back to him calling himself a disabled person.
-   Michael says Meredith has \"one kid and no husband\", but several
    other times in the series it is referenced that she has more than
    one kid.

Cultural references
-------------------

-   Oscar mentions the *Lord of the Rings* trilogy, a series of movies
    released between 2001 and 2003.
-   Michael interjects *Sue me!* into opening his talking head. The
    litigious nature of United States culture is reflected in its slang.
    \"Sue me!\" means roughly \"So what are you going to do about it?\",
    suggesting that if the listener doesn\'t like it, they can sue the
    speaker.
-   A *George Foreman Grill* is a small cooking appliance sold under the
    name of the former heavyweight boxer. It has a hinged cover.
-   *Protruberance* is a mispronunciation of *protuberance*.
-   *Stroudsburg* is a town about an hour\'s drive from Scranton.
-   Dwight mentions *this Russian website where you can download songs
    for two cents apiece.* At the time this episode aired, the Russian
    web site [AllofMP3.com](Wikipedia:AllofMP3 "wikilink") made songs
    available for pennies per song.
-   *Country Crock* is a brand of oil-based butter substitute.
-   *[Mail Boxes Etc.](Wikipedia:Mail_Boxes_Etc. "wikilink")* (a
    [UPS](Wikipedia:United_Parcel_Service "wikilink") company) is a
    chain of mailing supply stores.
-   *Stevie Wonder* is a singer who is blind.
-   *Helen Keller* is a woman who was deaf and blind. *Larry Flynt* is a
    publisher of pornography, paralyzed from a murder attempt.
    *Franklin D. Roosevelt* was the 32nd President of the United States
    and hid his paralysis from the public. *Tom Hanks* portrayed Forrest
    Gump, a man with low intelligence; He also played the adult Josh
    Baskin in *Big*, where the main character is a boy in a man\'s body.
-   *Born on the Fourth of July* is a movie about a paralyzed Vietnam
    War veteran.
-   *Carbondale* is a town about a half-hour\'s drive from Scranton.
-   *Shotgun* is a protocol for determining who sits in the front
    passenger seat.
-   *Chuck E. Cheese* is a chain of restaurants targeting children.

Quotes
------

:   see *[The Injury Quotes](The_Injury_Quotes "wikilink")*

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Marcus A. York](Marcus_A._York "wikilink") as [Billy
    Merchant](Billy_Merchant "wikilink")

### Guest cast

-   [David Doty](David_Doty "wikilink") as Doctor
-   [Julia Prud\'homme](Julia_Prud'homme "wikilink") as Nurse
-   [Stephen Pisani](Stephen_Pisani "wikilink") as Hospital Visitor
    (Uncredited)

\
