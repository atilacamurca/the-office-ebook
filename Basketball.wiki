{{Office episode
|Title      =Basketball
|Image      =[[Image:Basketball.jpg|250px]]
|Season     =[[Season 1|1]]
|Episode    =5
|Code       =R1104
|Original   =April 19, 2005
|Writer(s)  =[[Greg Daniels]]
|Director   =[[Greg Daniels]]
|Prev       =[[The Alliance]]
|Next       =[[Hot Girl]]
}}'''"Basketball"''' is the fifth episode of the [[Season 1|first season]] of ''[[The Office (US)|The Office]] ''and the 5th overall . It was written and directed by Greg Daniels. It first aired on April 19, 2005. It was viewed by 5 million people.

==Synopsis==
[[Michael Scott|Michael]] comes in to work prepared to pit the office staff against the warehouse in a game of basketball, with the losers having to work on Saturday. Michael picks [[Jim Halpert|Jim]], [[Ryan Howard|Ryan]], [[Stanley Hudson|Stanley]] (Michael's "secret weapon"), and, reluctantly, [[Dwight Schrute|Dwight]] and [[Phyllis Lapin|Phyllis]] (as an alternate).

The game begins and Stanley, despite being black (which is the main reason that Michael chose him), is a horrible player. Furthermore, Michael cannot make a shot, nor is he a fan of passing or defending. During the game, Roy and Jim gradually become aggressive toward each other, with [[Pam Beesly|Pam]] looking on. At a crucial point in the game, Michael is accidentally hit in the face. Michael knew his team was ahead so he claims it is a "flagrant personal intentional foul". He pettily stops the game and declares the office winners since they were winning when the foul occurred. The warehouse finds the call unfair and Michael caves under pressure, conceding the victory to the warehouse staff. As everybody returns to work, [[Kevin Malone|Kevin]] demonstrates his excellent shooting skills.

Afterward, Michael, in a rare moment of heart, tells the office that they don't have to come in on Saturday either. However, his justification does little to calm them: "Like coming in an extra day is going to prevent us from being downsized."

==Trivia==
* At least two of the scenes in the show's opening sequence are from this episode: Ryan holding up his bag and Dwight flipping his tie over his shoulder.
* Michael thinks Stanley is his secret weapon due to the fact that he believes all African Americans are good at Basketball.
* Actor [[Brian Baumgartner]] actually made 14 shots in a row, as revealed on the DVD commentary.
* Much of the basketball game was improvised. [[Greg Daniels]] simply told the cast to play basketball, and they kept the best parts.
* Although it was in the script for Roy to hit Jim in the mouth during the basketball game (and they rehearsed it), the scene in the episode was entirely accidental.<ref name="Denman">[http://www.givememyremote.com/remote/?p=1163 GMMR Exclusive Interview with David Denman]</ref>
* Michael says he will use Oscar during baseball season, in an off-color joke about his Latino heritage. Actually, in the series' second episode ("Diversity Day"), there is a shot of Kevin using an oversize pencil where a magazine titled "Baseball" is seen on Oscar's neighboring desk.
* In the beginning of the episode when Dwight is wondering who will work on Saturday, Pam says that Roy wanted to do something with her on Saturday, but later Pam states that Roy wanted to take the Wave Runners to the lake that day.

==Deleted scenes==
The Season One DVD contains a number of deleted scenes from this episode.
* Michael and Dwight discuss the differences between Assistant Regional Manager and Assistant to the Regional Manager.
* Michael, Darryl and Roy talk about the Philadelphia 76ers.
* Michael reveals his nickname which he uses while playing basketball.
* Todd Packer tells Michael a racist joke over the phone, and then says he can't play the game.
* Dwight takes Tootsie Rolls from Angela.
* Kevin tells Dwight he can't come in on Saturday because he plays in a band.
* Pam says how Roy get her a WaveRunner, but her brother races it with Roy.
* Dwight shows Jim a new schedule he's made.
* Angela and Dwight argue over who has more authority in safety.
* Michael talks about basketball movies and compares it to jazz but can't remember the name of a musician.
* Michael almost scores a basket.
* Michael remembers that Kenny G is the musician he was thinking of.
* Stanley hurts his ankle but Michael says he still has to play, but then brings Phyllis on for him.
* Kevin scores seven consecutive baskets.
* Michael finally scores and celebrates.

==Amusing details==
* During the early part of the game, after Roy makes an easy lay-up, Michael angrily shouts, "Who's got Roy?" presumably having forgotten that ''he'' is responsible for covering Roy. This could also be attributed to the fact that Michael doesn't want to be held accountable for Roy's scoring.

==Cultural references==
* Michael greets Pam by saying, "''Pam, Pam, thank you ma'am''." The phrase ''Wham bam thank you ma'am'' is crude slang referring to a man having sex with a woman very quickly and impersonally.
* A ''pick-up game'' is a game formed spontaneously in a public place (such as a park) among people who do not all know each other.
* ''[[Wikipedia:Regis Philbin|Regis Philbin]]'' is a television personality. ''[[Wikipedia:Fred Rogers|Mr. Rogers]]'' was the host of the children's television program ''[[Wikipedia:Mister Rogers' Neighborhood|Mister Rogers' Neighborhood]]''.
* To ''chicken out'' is to decline an option due to cowardice, usually applied to backing out of a previous agreement.
* ''[[Wikipedia:Gimli (Middle-earth)|Gimli]]'' is a fictional character from the fantasy book series ''[[Wikipedia:The Lord of the Ring|The Lord of the Ring]]''.
* ''[[Wikipedia:WaveRunner|WaveRunner]]'' is a brand of jet-ski [[Wikipedia:personal water craft|personal water craft]].
* Michael refers to [[Madge|a muscular woman]] as ''the East German gal''. East Germany (which reunited with West Germany in 1990) [[Wikipedia:East Germany#Sports|dominated international sports competitions]] in part due to its athletes' use of steroids.
* ''[[Wikipedia:Larry Bird|Larry Bird]]'' is a former professional basketball player, considered by many to be one of the best players ever.
* In a deleted scene, Michael describes the basketball movie ''[[Wikipedia:Like Mike|Like Mike]]'' but cannot remember its name.
* Dwight wears a mask that is similar to Richard Hamilton, a retired professional basketball player who won a championship with the Detroit Pistons.
* Michael kicks a ball like Nick Nolte in Blue Chips movie from 1990
* Michael and Dwight shouts "defence" it's a quote from Remember the Titans movie from 2000.

==Quotes==
:see ''[[Basketball Quotes]]''

==Cast==
===Main Cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly]]
*[[B.J. Novak]] as [[Ryan Howard]]

===Recurring Cast===
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[David Denman]] as [[Roy Anderson]]
*[[Patrice O'Neal]] as [[Lonnie]]
*[[Matt DeCaro]] as [[Jerry]]
*[[Karly Rothenberg]] as [[Madge]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Phyllis Smith]] as [[Phyllis Lapin]]
*[[Melora Hardin]] as [[Jan Levenson]]
*[[Philip Pickard]] as [[Philip]] (Uncredited)
*[[Creed Bratton (actor)]] as [[Creed Bratton]] (Uncredited)
*[[Devon Abner]] as [[Devon White]] (Uncredited)

==References==
<references/><br />

{{Season1}}
