# The Cover-Up

![](Normal_oth624_278.jpg "Normal_oth624_278.jpg")

**\"The Cover-Up\"** is the twenty-fourth episode of the [sixth
season](Season_6 "wikilink") and the 124th episode of *[The
Office](The_Office "wikilink")* overall. It was written by [Lee
Eisenberg](Lee_Eisenberg "wikilink") and [Gene
Stupnitsky](Gene_Stupnitsky "wikilink") who wrote \"[Scott\'s
Tots](Scott's_Tots "wikilink")\" and \"[The
Lover](The_Lover "wikilink")\" and directed by [Rainn
Wilson](Rainn_Wilson "wikilink"), who marked his directorial debut with
this episode. It aired on May 6, 2010. It was viewed by 6.84 million
people.

Synopsis
--------

Jim and Pam prank Dwight by discussing him using Morse code in front of
him. Dwight, who knows Morse code, asks Michael to intervene but cannot
prove that the couple are doing anything more than clicking their office
equipment. Michael, in a good mood due to his wildly successful
relationship with [Donna](Donna "wikilink"), is unsympathetic to
Dwight\'s plight.

During a meeting, Michael asks the office for recommendation for an
upcoming trip with Donna. In the meeting, the other members of the
office, particularly [Ryan](Ryan "wikilink") and
[Kelly](Kelly "wikilink"), convince him that she might be cheating on
him. Worried sick, Michael hires [Dwight](Dwight "wikilink") to tail her
to see if she is spending time with anyone else. Dwight follows Donna to
her gym and attempts to seduce her. Donna rebukes him and calls security
on him, whereby he openly admits he was sent there by Michael to watch
her. Dwight comes back to the office and says to Michael that Donna
isn\'t cheating, and that Michael has to pay for his gym membership
(since he must pay for any of the fees Dwight had during the
investigation). An enraged Donna comes to the office to talk to Michael
about the whole situation, but the two forgive each other and reconcile
by planning a private vacation together.

[Andy](Andy "wikilink") receives a call from a concerned customer that a
[Sabre](Sabre "wikilink") printer he bought caught fire during a routine
print-job. Andy becomes frustrated when [Gabe](Gabe "wikilink") fails to
take his customer\'s complaint seriously. Capitalizing on his fears,
[Darryl](Darryl "wikilink") pranks Andy into believing he has uncovered
a major conspiracy. Darryl is pranking Andy in retaliation for his
blaming one of his mistakes on the warehouse a few years earlier, an act
that put Darryl\'s career at Dunder Mifflin in jeopardy. After
pretending to help Andy to not get fired for knowing that the printers
may be dangerous, Darryl agrees to film Andy testing a printer in a
typical fashion to test his burning printer theory. During the shooting
of the video, while Darryl is pranking Andy into speaking with a high
pitch voice, the printer indeed catches fire and explodes. This confirms
Andy\'s suspicions, and terrifies Darryl who vows to never prank again.

Both Pam and Kelly become suspicious when Donna, wearing earrings that
no woman would buy for herself, cannot remember where she got them.
While Donna and Michael are planning their trip, [Pam](Pam "wikilink")
investigates Donna\'s Facebook account and finds pictures of her hugging
and kissing another man. At first she and [Jim](Jim "wikilink") had been
skeptical of Donna cheating, and even berated Michael for having
destructive tendencies with his personal life, as well as his eating a
bowl of mayonnaise and olives due to stress. But now they realize his
fears had been correct and try to warn him as best they can. Pam shows
Michael the pictures she found and he confronts Donna with the evidence.
At the end of the episode, Donna reveals that she is indeed cheating,
but not on Michael - with Michael. She is actually married to someone.

In the last scene, Dwight is taking a spin class at Donna\'s gym. While
Dwight is instructing the class (with an imaginary scenario involving
going up a hill and cycling off a cliff), the instructor tries to regain
control. Everyone in the class ends up listening to Dwight.

Trivia
------

-   The episode is dedicated to the memory of Larry Einhorn, father of
    [Randall Einhorn](Randall_Einhorn "wikilink"), the show\'s director
    of photography and occasional director.
-   The episode title continues in the tradition of titles with multiple
    meanings. In this episode, there are two cover-ups: Donna hides her
    married status, and Sabre hides the fact that their printers catch
    fire.
-   Andy and Darryl test the printer in the storage room previously home
    to [The Michael Scott Paper
    Company](The_Michael_Scott_Paper_Company "wikilink").
-   The printer Andy and Darryl are testing is the [Sabre 9900
    All-in-One Color Ink-jet](http://www.sabre-corp.com/products.shtml)
-   After the episode aired, the producers of the show created the
    [Dwight Schrute, P.I.](http://www.schrutepi.com/) Web site in which
    Dwight offers his services as a private investigator.
-   This is the first episode that [Rainn
    Wilson](Rainn_Wilson "wikilink") directed. This season also featured
    [John Krasinski](John_Krasinski "wikilink") and [Mindy
    Kaling](Mindy_Kaling "wikilink") directing for the first time.
-   The pictures Pam holds up are from the album \"profile pictures\" by
    \"Donna.\" The album has eleven photos, but both photos Pam shows
    are \"1 of 11.\" Everything else on the page is the same as well,
    including an advertisement for \"Sid Dexter\'s,\" titled \"Donna\'s
    Networks\" and saying \"find out who has been stalking your online
    profiles and searching your name.\" This suggests the prop team
    created an image and merely swapped the photos, as opposed to
    creating a fake profile or album for Donna or it could simply mean
    that the ads on Facebook didn\'t change, which frequently happens.
    It also appears that Pam has one unseen message and four unseen
    notifications.
-   Angela is disgusted by Michael and Donna kissing, while Hank watches
    intently while eating cereal.
-   Michael hangs up a fake phone call before he finishes talking. He
    does this several times while on pretend phone calls in the series.
-   The scene where Michael is in the kitchen with Jim and Pam eating
    his "comfort food" (mayonnaise with black olives), you can see that
    his bowl, in between different camera angles, has moved further away
    from him, and then closer to him, and then back to being far away
    from him.

Cultural references
-------------------

-   *[P.F. Chang\'s](Wikipedia:P._F._Chang's_China_Bistro "wikilink")*
    is an American [casual dining](Wikipedia:Casual_dining "wikilink")
    restaurant chain which serves American-Chinese cuisine.
-   *[Facebook](Wikipedia:Facebook "wikilink")* is a widely popular
    social networking site that was launched on February 4, 2004.
-   Andy\'s voice during the test prank sounds like Pinocchio from the
    *[Shrek](Wikipedia:Shrek "wikilink")* movie.
-   Meredith is seen reading the magazine *[Men\'s
    Health](Wikipedia:Men's_Health "wikilink")*.
-   Michael mentions a *[Weird
    Al](Wikipedia:"Weird_Al"_Yankovic "wikilink")* and *[Spice
    Girls](Wikipedia:Spice_Girls "wikilink")* concert.
-   The Great Wall, mentioned by Kelly, is a Chinese restaurant located
    in the San Fernando Valley in Los Angeles, CA.

Cast
----

### Main cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Halpert](Pam_Halpert "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")

### Supporting cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Recurring cast

-   [Zach Woods](Zach_Woods "wikilink") as [Gabe
    Lewis](Gabe_Lewis "wikilink")
-   [Amy Pietz](Amy_Pietz "wikilink") as [Donna](Donna "wikilink")
-   [Hugh Dane](Hugh_Dane "wikilink") as [Hank
    Tate](Hank_Tate "wikilink")
-   [Calvin Tenner](Calvin_Tenner "wikilink") as
    [Calvin](Calvin "wikilink")

### Guest cast

-   David Mate as Flex
-   Fay DeWitt as Lady at the Gym
