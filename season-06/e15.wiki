''For the company see: [[Sabre (company)]]''

{{Office episode
|Title       =Sabre
|Image       =[[File:Sabreep.jpg|250px]]
|Season      =[[Season 6|6]]
|Episode     =15
|Code        =6015
|Original    =February 4, 2010
|Writer(s)   =[[Jennifer Celotta]]
|Director    =[[John Krasinski]]
|Prev        =[[The Banker]]
|Next        =[[Manager and Salesman‎]]
}}

'''"Sabre"''' is the fifteenth episode of the sixth season and the 115th episode overall. It was written by [[Jennifer Celotta]], who previously wrote [[The Promotion]], [[Company Picnic]], and [[The Duel]]. It was directed by [[John Krasinski]] who marked his directorial debut with the episode. It aired on February 4, 2010. [[Kathy Bates]] and [[Zach Woods]] guest star. It was viewed by 7.36 million people.

==Synopsis==
The office gets a box of printer supplies from [[Sabre (company)|Sabre]] (pronounced 'Say-ber'). [[Michael]] thinks it is for him and distributes the items around the office. However, it is for [[Gabe Lewis]], new director of sales ([[Jan Levinson|Jan Levinson, Ryan Howard]] and [[Charles Miner]]'s former jobs). When Michael realizes this, he tries to put everything back in the box. Everyone works hard and they manage to pack it all away, however Michael accidentally leaves his cell phone in the box and they have to unpack it all again.

[[Erin]] and [[Andy]], assuming that Sabre is pronounced 'Saab-ray', sing a song for Gabe titled "Dunder Mifflin Is A Part Of Sabre" to the tune of Miley Cyrus's "Party In The USA". Gabe takes a video of the song, however, when 'Sabre' is repeatedly mispronounced, he shuts off his cell phone. Meanwhile, Andy and Erin continue to drop hints that they like each other with both oblivious to the other's advances.

In the conference room, Gabe shows everyone a promotional video for Sabre, featuring Christian Slater. Michael and the rest of the office learn about Sabre's new policies. He is outraged that sites like YouTube and Twitter are blocked and that he will now be asked to drink out of a metal container rather than waste a huge amount of plastic cups. Gabe also pushes the idea that Sabre is primarily a printer company and that Dunder Mifflin will be a printer company which also sells paper.

Michael asks Gabe to talk to someone higher up in the company. Gabe talks to CEO [[Jo Bennett]], a stubborn no-nonsense boss who tells Michael to think hard about his future at Dunder Mifflin. It is clear she has no problem telling him off.

Meanwhile, [[Pam]] and [[Jim]] try to get into a highly rated daycare center for their unborn baby. They arrive early for their interview and take the time to admire the facility. However, Jim walks into a children's bathroom to find a man sitting on the toilet and becomes embarrassed, before telling Pam about it. Jim and Pam have the meeting with Jerry, the daycare guy (who, it emerges, Jim had earlier walked in on the toilet). He doesn't seem to like Jim and Pam and tells them that they may not get in.

Pam, finally, asks the guy if his problem is about Jim walking in on him in the bathroom, and Jerry awkwardly tries to defend himself. The meeting does not appear to end well.

Michael visits a newly unemployed [[David Wallace]], who is at home with his wife Rachel and their son. Their son plays drums loudly while David makes Michael a marshmallow fluff sandwich. David seems to have crashed, much like Jan Levinson after she was fired. David and Michael sit in the outdoor hot tub and Michael asks David how to deal with his problems with Sabre, however David is unable to come to Michael's rescue. They talk about David's future and David asks Michael if he would like to become involved with an idea called [[Suck It]], a vacuum that sucks toys up.

Michael shares with the documentary crew that he dislikes the new David and leaves.

Back at Dunder Mifflin, Michael brings in some orange juice for the office to drink out of their containers. He then makes a speech saying he thinks the relationship with Sabre will be a successful one.

[[File:Sabredivision.png|thumb|110px]]

==Amusing Details==
* The pizza Andy takes out of the refrigerator while looking for strawberries is Alfredo's Pizza, a pizza place mentioned in the episode [[Launch Party]].
* When Michael brings in some orange juice to make a toast, his Sabre metal container is damaged. This is because he threw it out of his car window whilst driving away earlier in the episode.
* Stanley has a bunch of Hot Wheels on his desk.
* After contemplating for about 15 seconds, [[Dwight]] finally decides on putting the hotdogs on the ground.
* As of this episode, [[Ed Helms]] is considered part of the main cast and is in the opening credits of the show along with [[B.J. Novak]], [[Jenna Fischer]], [[John Krasinski]], [[Rainn Wilson]], and [[Steve Carell]].
* [http://www.sabre-corp.com/ Sabre] has its own website. It is full of misspellings and odd quips.
* Sabre has its own cafeteria.
* The daycare center is the fictional "Sandbox Daycare."
* [[Gabe Lewis]] uses a [[wikipedia:MacBook|MacBook Pro]], a computer seen many times on the show as a product placement. Macs are seen at least three times in this episode: at the daycare, at David's house and with Gabe.
* Michael throws the Sabre thermos and hits [[Stanley]]'s car, last seen in season opener [[Gossip]]. This is the second time that Stanley's car has been hit by an object - the first instance was when [[Dwight]] threw a watermelon from the rooftop in [[Safety Training]].
* David Wallace stocks his refrigerator with beer.
* The beer that Michael and David drink in the hot tub is Yuengling beer, America's oldest brewery based in Pottsville, PA--- especially cool, as it wasn't distributed west of Ohio. Now it is also available in Tennessee.
* When David Wallace and Michael are in the hot tub, Rachel watches them closely from the window.
* Michael has always wanted everybody to be his friend. In this episode, he turns to David Wallace as a boss, but Wallace just wants to be his friend. Ironically, the one time he wants a boss, he gets a friend instead, and he doesn't like it.
* David Wallace offers Michael a stake in his "[[Suck It]]" concept, which Michael is non-committal about. In season 8's "[[Fundraiser]]", it's revealed Wallace made $20 million selling the "Suck It" patent to the US Military, meaning Michael missed out on what would likely have been a multi-million dollar opportunity.
* Despite Gabe saying that paper cups are terrible for the environment when giving out the metal bottles he can be seen drinking tea from a paper cup when Michael asks to speak with Jo.

*The building depicted as Sabre's headquarters in the orientation video shown to Dunder Mifflin employees explaining "Who is Sabre" is commonly known as "The Baxter Building" (named after Baxter Healthcare, not affiliated with any comic universe) in Westlake Village, CA. This building was also featured in the movie "Demolition Man".

==Quotes==
''For quotes see: [[Sabre Quotes]]''

==Flashbacks==
* A scene from [[The Deposition]] was seen when Michael was explaining his relationship with David Wallace.
* When Erin talks about waiting for Andy to ask her out, a scene from [[Secret Santa]] was seen, showing Andy getting twelve drummers as part of his gift to Erin. Andy then mentions the scene.

==Connections to previous episodes==
* [[Nick]], a graphic design guy who now works for Dunder Mifflin as an IT guy met Pam in [[Season 4]]'s [[Job Fair]].
* [[Teddy Wallace]], [[David Wallace]]'s son was last seen in the [[Season 3]] episode [[Cocktails]], along with Rachel Wallace. The roles were not re-cast.
* YouTube may have been banned after [[Michael]] told [[Holly Flax]] that he spent five days watching [http://www.youtube.com/watch?v=zJsJ5dW7jHo ''Cookie Monster Sings Chocolate Rain''] on the popular website. This was revealed in [[Business Ethics]].

==Trivia==
* Dwight's presence in this episode was limited. Actor [[Rainn Wilson]] was available for only one day of shooting due to his participation in the movie ''Si''.
* Michael's hair was so short because [[Steve Carell]] was shooting ''Dinner for Schmucks'' during the winter hiatus.
* Lyrics to the "Sabre" song:
<blockquote>Hopped off the train in Scranton, PA<br />Another cloudy gray afternoon<br />Jumped in the cab here you are for the first time<br />Look to the right and you see the Electric City sign<br />This is gonna be a good day<br />For Dunder Mifflin and Sab-ray<br />So yeah, yeah, yay, yay<br />Dunder Mifflin is a part of Sab-ray (repeated)</blockquote>
* The scissors in the scene where [[Michael]] and [[Erin]] pass the sharp utensils were made out of rubber.<ref>[http://www.officetally.com/the-office-sabre-qa http://www.officetally.co][[Category:Season 6]][[Category:Episodes]][[Category:Season 6 Episodes]][http://www.officetally.com/the-office-sabre-qa m/the-office-sabre-qa]</ref>
* When Michael says, "Suck B---," it was cut off because the writers thought that either the documentary or [[NBC]] would edit it out.<ref>http://www.officetally.com/the-office-sabre-qa/3</ref>

==Cultural references==
* Erin joins Michael's chant, "I got a big box," unaware that box is crude slang for [[Wikipedia:vagina|vagina]].
* Andy and Erin's "Sabre" song was a parody of Miley Cyrus' song "Party in the USA".
* [http://www.youtube.com/ ''YouTube''] is a hugely popular website where videos can be uploaded and watched by people.
* [http://twitter.com/ ''Twitter''] is a social networking site in which people can post things on walls in under 140 characters. It is used by many people including several crew members of the show.
* [[Wikipedia:Global Warming|Climate change]] is a change in the weather and temperature on Earth and has scientists worried.
* David Wallace makes a ''[http://www.marshmallowfluff.com/pages/fluffernutter.html Fluffernutter sandwich]'', a sandwich consisting of bread, peanut butter and marshmallow fluff.
* Jim asks the daycare administrator about [[Wikipedia:Easter|Easter]] (a religious holiday) and [[Wikipedia:Memorial Day|Memorial Day]] (an American holiday).
* Michael calls orange juice the ''sweet nectar of Gabe's homeland.'' The Florida orange juice industry actively promotes orange juice made in their state.

==Cast==
===Main cast===
* [[Steve Carell]] as [[Michael Scott]]
* [[Rainn Wilson]] as [[Dwight Schrute]]
* [[John Krasinski]] as [[Jim Halpert]]
* [[Jenna Fischer]] as [[Pam Halpert]]
* [[B.J. Novak]] as [[Ryan Howard]]
* [[Ed Helms]] as [[Andy Bernard]]

===Supporting cast===
* [[Leslie David Baker]] as [[Stanley Hudson]]
* [[Brian Baumgartner]] as [[Kevin Malone]]
* [[Creed Bratton (actor)]] as [[Creed Bratton]]
* [[Kate Flannery]] as [[Meredith Palmer]]
* [[Mindy Kaling]] as [[Kelly Kapoor]]
* [[Ellie Kemper]] as [[Erin Hannon]]
* [[Angela Kinsey]] as [[Angela Martin]]
* [[Paul Lieberstein]] as [[Toby Flenderson]]
* [[Oscar Nunez]] as [[Oscar Martinez]]
* [[Phyllis Smith]] as [[Phyllis Vance]]

===Special guest star===
* [[Kathy Bates]] as [[Jo Bennett]]

===Recurring cast===
* [[Andy Buckley]] as [[David Wallace]]
* [[Zach Woods]] as [[Gabe Louis|Gabe Lewis]]
* [[Nelson Franklin]] as [[Nick]]

===Guest cast===
*Owen Daniels as [[Teddy Wallace]]
*Jean Villepique as [[Rachel Wallace]]
*Joey Slotnick as Jerry the Daycare Center Director
*[[Christian Slater]] as himself (Uncredited)
