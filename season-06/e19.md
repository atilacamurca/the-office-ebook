# St. Patrick\'s Day

![](St.patrick'sday.jpg "St.patrick'sday.jpg")

**\"St. Patrick\'s Day\"** is the nineteenth episode of the [sixth
season](Season_6 "wikilink") and the 119th episode overall. It was
written by [Jonathan Hughes](Jonathan_Hughes "wikilink"), who wrote
\"[The Office: Subtle Sexuality](The_Office:\_Subtle_Sexuality "wikilink"),\"
and directed by [Randall Einhorn](Randall_Einhorn "wikilink") who directed
\"[Golden Ticket](Golden_Ticket "wikilink")\" and \"[Blood
Drive](Blood_Drive "wikilink")\". It aired on March 11, 2010. [Kathy
Bates](Kathy_Bates "wikilink") guest stars. It was viewed by 7.51
million people.

Summary
-------

It\'s St. Patrick\'s Day in [Scranton](Scranton "wikilink") and everyone
in the office is thrilled, since it is one of the biggest holidays in
Scranton. [Meredith](Meredith "wikilink") left her kids somewhere, while
everyone looks forward to go to the parade. Everyone dresses up in
green, and many things across the office are green as well.

[Jim](Jim "wikilink") is welcomed back by the rest of the office from
paternity leave, but comes back to see [Dwight](Dwight "wikilink") using
three desks to make up a huge \"megadesk,\" which Jim takes apart.
Dwight is extremely angry that his megadesk is getting destroyed. Later,
Dwight sets up megadesk again, to the dismay of Jim, who takes it apart
on seeing it. Jim and [Pam](Pam "wikilink") talk about
[Cecilia](Cecilia_Halpert "wikilink"), and Jim is sad that he is missing
it. Dwight tries to get him to go back home so that he can have megadesk
and tries to convince him that Cecilia is finding the world without him
and that [Mose](Mose_Schrute "wikilink") was scarred without his father.
Although Jim realizes what Dwight is doing, he realizes that he is
missing her and Pam.

![Kevin is outraged.](Kevinst.patrick'sday.jpg "fig:Kevin is outraged.")

Dwight starts to play \"[Cat\'s in the
Cradle](Wikipedia:Cat's_in_the_Cradle "wikilink")\" loudly to show Jim
that he is missing fatherhood. Jim calls Pam and decides to keep working
for the meantime, but he is surprised that Dwight\'s plan is working.
Dwight continues to try to get Jim to leave.

![Michael starts to bother Jo.](Joandmichael.jpg "fig:Michael starts to bother Jo.")

[Jo Bennett](Jo_Bennett "wikilink") comes back to the office and is thrilled
to see everyone in team spirit for St. Patrick\'s Day. It is her last
day at [Dunder Mifflin Scranton](Dunder_Mifflin_Scranton "wikilink") for
awhile, and she says that the people at the branch are lovely.
[Michael](Michael "wikilink") comes to talk to Jo and gives her a gift.
She thanks him and offers him a room at her house in Tallahassee, which
he takes very seriously. Jo holds an office meeting and asks if anyone
has any ideas relating to the business. Michael bothers Jo by telling
her about everyone who asks her a question and tries to get her to like
him. [Darryl](Darryl "wikilink") tells Jo about how
[Sabre](Sabre_(company) "wikilink")\'s delivery system isn\'t efficient,
and he catches her eye. Jo accepts Darryl\'s plan and offers Darryl
Jim\'s old office. [Gabe](Gabe_Lewis "wikilink") tells her that he is
working there, but scared of Jo, tells her that it\'s okay and he will
go clean it out immediately.

[Michael](Michael_Scott "wikilink") hugs Jo and tells her he bought
tickets and planned a trip. Jo is surprised, and says that he should
check with her office. When Michael continues to bother her, she snaps
and tells Michael, \"enough,\" leaving the office surprised. Michael is
sad that Jo still doesn\'t like him despite his attempts. Michael asks
Jo out to lunch, and she tells him to do some work. Michael asks
[Darryl](Darryl "wikilink") why Jo likes him and asks him if her family
owes him something. Michael says that he is going home for the day, and
Jo says that he can go if he believes that he did a great day of work,
but she decides to stay to work. Michael tells everyone not to leave
because Jo wants them to stay until they did superb work, and Michael is
trying to get Jo to like him. [Kevin](Kevin "wikilink") is outraged.

Dwight tries to bail out early by claiming to have a late meeting with a
client, but Jim gets in ahead of him. Erin lucks out and sneezes,
causing a sympathetic Jo to urge her to go home and get some rest. When
Andy, who\'s wearing his sister\'s field hockey skirt as a kilt,
pretends to be sick so he can visit Erin, Jo sends him home, too,
telling him to put some pants on. Michael calls an inebriated [Todd
Packer](Todd_Packer "wikilink") who\'s been drinking since three and
complains that the girls are fat and ugly. [Calvin](Calvin "wikilink")
and the rest of the [warehouse](warehouse "wikilink") workers come to
ask [Darryl](Darryl "wikilink") to come back, but he refuses. The
cleaning crew comes to the office, but can\'t do anything since Jo and
everyone else is still there. While [Angela](Angela "wikilink") wants to
leave so she can protest St. Patrick\'s Day, Gabe helps get Jo\'s bags
and explains that he has no time for a personal life because Jo is very
demanding. Michael enters the conference room, explains to Jo that it\'s
a holiday and he is dismissing his hard-working employees so they can
enjoy their evening. He also lets her know that he\'s cancelled his
Florida trip, but is looking forward to their working relationship. She
is looking forward to it, as well.

Todd Packer and [Michael](Michael "wikilink") enjoy the St. Patrick\'s
Day party at Cooper\'s. Michael says that he rather be with people he
likes than Jo. Todd Packer is humped by a drunken
[Meredith](Meredith "wikilink").

[Erin](Erin "wikilink") and [Andy](Andy "wikilink") are going on their
first date, despite Erin being sick. [Angela](Angela "wikilink") is
disgusted by her being sick, while Jo sends her home, because she
doesn\'t want her dogs to catch a bug. Andy decides to pretend to be
sick in front of Jo, who sends him home. Andy comes to Erin\'s house,
even though she is sick. Andy and Erin hold hands and watch a movie. It
is revealed that Erin was a foster child and lives with her foster
brother, who seems to have an interest in her. Andy realizes this and
becomes threatened by the thought that someone else likes Erin.

![Andy has an \"interesting\" first date.](Erinandandyandreedst.patrick'sday.jpg "fig:Andy has an "interesting" first date.")

Andy leaves the date, and Erin kisses him on the cheek.
[Jim](Jim "wikilink") sets up a
[\"Quad-Desk\"](Stacked_Pam's_and_Dwight's_desks_to_form_"Quad-desk" "wikilink")
with three desks, and Dwight is angry.

Deleted Scenes
--------------

-   Michael explains to [Oscar](Oscar "wikilink") that he is leaving for
    Tallahassee, and Michael tells Oscar that they are close. Oscar says
    that Jo is very friendly, but doesn\'t always mean what she says. Jo
    then comes up and tells them that they are both skinny and
    good-looking.

Trivia
------

-   This episode was supposed to air on March 18th, however, it moved up
    because \"[The Delivery](The_Delivery "wikilink")\" became an
    hour-long rather than a two-parter and \"[New
    Leads](New_Leads "wikilink")\" was put as the March 18th episode.
    The producers wanted the Scranton St. Patrick\'s Day parade to be
    filmed, however, since the date was brought up, this was impossible.
    This fact is more supplemented as [Jo](Jo "wikilink") wasn\'t seen
    in the two parter. [^1]
-   The music that the office cleaners are listening to when they enter
    the office is Yarilo, by the Pagan Metal band Arkona (Аркона in
    Russian).

Errors
------

-   Andy says that his kilt is his sister\'s old field hockey skirt;
    however, Andy does not have a sister. He could\'ve lied about it
    being his sister\'s to avoid admitting he owned a skirt himself.

<!-- -->

-   At the end of the episode, when Dwight sees Jim using \"Quad-desk\",
    a tiny chair appears. In the first shot of the encounter, there is
    no chair in front of Dwight\'s desk setup underneath Quad-desk.
    However when Dwight\'s phone rings, Dwight crouches down to answer
    his phone and places his briefcase on a tiny chair that was not
    originally in the frame.

Amusing details
---------------

-   The water tank near Stanley\'s desk is green, not blue.
-   Even the exit sign (normally red) is green.
-   [Michael](Michael "wikilink"), [Jim](Jim "wikilink"), and (possibly)
    [Andy](Andy "wikilink") all have Scottish roots. Michael explained
    that he was Scottish and \"2/15\" Native American. . Jim\'s dad was
    seen wearing a kilt at his wedding. . Andy was seen wearing a kilt,
    possibly for St. Patrick\'s Day or for Scottish roots.
-   Angela\'s mask has her name on it in red.
-   Michael likes to tell new bosses about [Oscar](Oscar "wikilink")\'s
    homosexuality. Michael tells [Charles
    Miner](Charles_Miner "wikilink") that Oscar broke up with a man and
    never brought it into work , while he tells
    [Jo](Jo_Bennett "wikilink") that Oscar is a \"homosexual
    accountant\".
-   Michael\'s desk flag is Italian.
-   Gabe suggests that he must go to Amsterdam with Jo seven times a
    year. It is unknown why, but it contributes to Jo\'s interesting but
    confusing life outside of Sabre.
-   Oscar is sitting next to Matt at the bar.

Cultural References
-------------------

-   [*St. Patrick\'s Day*](Wikipedia:_St._Patrick's_Day "wikilink") is
    an annual holiday on March 17th that celebrates Saint Patrick. Over
    time, it has turned from a religious holiday to a holiday to
    celebrate Irish culture. Symbols of the holiday include shamrocks,
    [corned beef and cabbage](wikipedia:Corned_beef "wikilink"), and the
    color green. (Even the beer is colored green.) Ireland is
    predominantly Catholic, and they do celebrate Christmas, despite
    Michael\'s remarks to the contrary.
-   Andy wears a *[kilt](Wikipedia:kilt "wikilink")*, an skirt-like
    article of clothing for men. While more closely associated with
    Scottish culture, the kilt also has a place in Irish culture.
    (However, Irish kilts are solid colors.)
-   Michael calls green [M&M\'s candies](Wikipedia:M&M's "wikilink")
    \"nature\'s Viagra.\" [Viagra](Wikipedia:Viagra "wikilink") is the
    brand name for a drug to treat erectile dysfunction, and according
    to folklore, green M&Ms are an aphrodisiac.
-   Dwight calls Jim and Pam *Tweedle Dee and Tweedle Dumbass*,
    referring to the characters *[Tweedledum and
    Tweedledee](Wikipedia:Tweedledum_and_Tweedledee "wikilink")* from
    the children\'s story *[Through the
    Looking-Glass](Wikipedia:Through_the_Looking-Glass "wikilink")*.
-   Andy greets Erin by saying \"[Erin Go
    Bragh](Wikipedia:Erin_Go_Bragh "wikilink")\", an Irish phrase which
    means \"Ireland Forever\". The phrase is associated with St.
    Patrick\'s Day. Erin doesn\'t understand this and responds, \"Andy
    go Bragh.\"
-   Dwight disputes the statement that *No man is an island*, a phrase
    famously used by writer [John
    Donne](Wikipedia:John_Donne "wikilink") in *[Devotions upon Emergent
    Occasions](Wikipedia:Devotions_upon_Emergent_Occasions "wikilink")*.
-   Michael reveals information in the spirit of \"fool disclosure\",
    misstating the phrase \"full disclosure.\"
-   Dwight taunts Jim by listening to (and singing along to) the song
    *[Cat\'s in the Cradle](Wikipedia:Cat's_in_the_Cradle "wikilink")*,
    a song about a father who is too busy to spend time with his son.
-   [*How I Met Your
    Mother*](Wikipedia:How_I_Met_Your_Mother "wikilink") is a television
    show told in flashback form in which the main character describes to
    his children the events leading up to meeting his future wife.
-   Todd Packer tells Michael that he *booted* (vomited) and *rallied*
    (resumed drinking) twice.
-   *[Bobby McFerrin](Wikipedia:Bobby_McFerrin "wikilink")* is an
    American musician (not an Irish poet) best known for his song
    *[Don\'t Worry, Be
    Happy](Wikipedia:Don't_Worry,_Be_Happy "wikilink")*.
-   Jo misunderstands Michael\'s gift to mean that she\'s been naughty.
    According to American culture, good kids get presents from Santa
    Claus while naughty kids get a lump of coal.
-   Jo is wearing Dolce & Gabbana glasses.

Connections to Previous Episodes
--------------------------------

-   [Dwight](Dwight "wikilink") also enjoys other games. He used [Second
    Life](Wikipedia:Second_Life "wikilink") to replicate his real life.
-   [Jo Bennett](Jo_Bennett "wikilink")\'s great danes were last seen in
    \"[Manager and Salesman](Manager_and_Salesman "wikilink").\"
-   The cleaning crew has changed since \"[Chair
    Model](Chair_Model "wikilink"),\" when the cleaning crew was a group
    of only Spanish speaking ladies, who [Oscar](Oscar "wikilink")
    talked to.
-   Meredith and Packer also had a bit of a relationship in other
    parties, especially when they are drunk.

Quotes
------

*For quotes see: [St. Patrick\'s Day
Quotes](St._Patrick's_Day_Quotes "wikilink").*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Halpert](Pam_Halpert "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")

### Supporting Cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Special Guest Star

-   [Kathy Bates](Kathy_Bates "wikilink") as [Jo
    Bennett](Jo_Bennett "wikilink")

### Recurring Cast

-   [Zach Woods](Zach_Woods "wikilink") as [Gabe
    Lewis](Gabe_Lewis "wikilink")
-   [David Koechner](David_Koechner "wikilink") as [Todd
    Packer](Todd_Packer "wikilink")
-   [Calvin Tenner](Calvin_Tenner "wikilink") as
    [Calvin](Calvin "wikilink")
-   [Sam Daly](Sam_Daly "wikilink") as [Matt](Matt "wikilink")

### Guest Cast

-   [Sean Davis](Sean_Davis "wikilink") as [Reed](Reed "wikilink")
-   [Boris Kievsky](Boris_Kievsky "wikilink") as the Cleaner

[^1]: <http://thetimes-tribune.com/arts-living/tonight-s-the-office-takes-place-on-st-patrick-s-day-1.669326>
