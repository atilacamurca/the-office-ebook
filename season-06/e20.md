# New Leads

![](Newleads.jpg "Newleads.jpg")

**\"New Leads\"** is the twentieth episode of the [sixth
season](Season_6 "wikilink") and the 120th episode overall. It was
written and directed by [Brent Forrester](Brent_Forrester "wikilink")
who wrote, \"[Mafia](Mafia "wikilink"),\" \"[Blood
Drive](Blood_Drive "wikilink"),\" and \"[Business
School](Business_School "wikilink").\" It aired on March 18, 2010. It
was viewed by 7.63 million people.

Synopsis
--------

In the cold open [Michael](Michael "wikilink") calls
[Jim](Jim_Halpert "wikilink") into his office and shows him photos of
allegedly [Johnny Depp](Wikipedia:Johnny_Depp "wikilink") at his condo.
Jim messes with Michael before bluntly telling him that there is no way
it is Johnny Depp. After a short argument Michael relents after stating
that the name on the mail box was \"M. Shulman.\" Jim, who is on his way
out, pauses and asks, \"M. Night Shulman?\", to which a shocked Michael
has no reply.

Michael holds a meeting but [Stanley](Stanley "wikilink") and
[Dwight](Dwight "wikilink") interrupt Michael demanding if there are any
sales topics or any news on the \$50,000 leads that
[Sabre](Sabre_(company) "wikilink") bought them. Michael lets them know
they aren\'t here yet. The salespeople are especially anxious to get the
leads which is causing them to snap at Michael and their co-workers.
They abruptly leave the conference room. Michael tells Dwight that he
would like his undivided attention. Dwight responds coldly, \"You
couldn\'t handle my undivided attention.\" Michael explains that
Sabre\'s policy of \"Sales is King\" means that the company emphasizes
the importance of the sales staff which reduces the rest of the office
to support them. This has gone to the salespeople\'s heads and is
creating tension in the office.

![Jim tries to find the new leads.](Jimnewleads.jpg "fig:Jim tries to find the new leads.")

[Dwight](Dwight "wikilink") comes into Michael\'s office to ask him
patronizingly about news on the leads. Jim sends a text, ignores when
Michael tries to speak to him and has to literally force his hand to
sign Jim\'s commission check. [Phyllis](Phyllis "wikilink") refuses to
answer [Angela\'s](Angela "wikilink") repeated emails.
[Andy](Andy "wikilink") demands a pencil from
[Darryl](Darryl "wikilink") during a sale and wrestles him for it when
Darryl refuses. Darryl tells Michael he needs to put the salespeople in
their place.

![Dwight tries to find the leads.](Dwightnewleads.jpg "fig:Dwight tries to find the leads.")

Thus, when the leads arrive, Michael refuses to hand them out,
especially when Phyllis demands, \"hand them over numbnuts\". While this
earns him the appreciation of the rest of the branch,
[Gabe](Gabe_Lewis "wikilink") explains that it is Michael\'s job to hand
those leads over. So Michael hands them to the non-sales staff. Jim
tries to convince Michael that this plan isn\'t going to work. Michael
responds by giving Jim a list of clues to find his share of the leads
which he has hidden all over the industrial park.

Phyllis asks Angela for her share of the leads. Angela agrees to do so
as soon as Phyllis fills out a stack of paperwork that Angela plans to
shred in front of her as soon as she\'s done. Stanley wins leads from
[Ryan](Ryan "wikilink") and [Kelly](Kelly "wikilink") by agreeing to
both their arguments in a relationship debate. Jim decides to call
[Pam](Pam "wikilink"), tells her about the situation at [Dunder
Mifflin](Dunder_Mifflin "wikilink") and Pam helps him solve some of the
clues.

[Erin](Erin "wikilink") hides her share of the leads, and plays the
hot-cold game with Andy which awkwardly leads his hands toward her chest
before she reveals them underneath her keyboard.

When Dwight returns from his sales call and demands his share of the
leads, Michael advises him, \"If you want to find your leads, go to the
man who never breeds.\" Dwight immediately identifies
[Kevin](Kevin "wikilink"), who attempts to humiliate Dwight but only
results in getting strangled. After a few seconds of being choked by
Dwight, Kevin tells him that the leads are in the trash, but Erin
already threw it away. [Dwight](Dwight "wikilink") heads to the
dumpster, to find nothing there. Michael realizes that it\'s Friday and
runs out into the street where a garbage truck can be seen driving away.

Michael asks everyone to go to the dump with him to find the leads, but
only [Dwight](Dwight "wikilink") will, fearing that Michael won\'t be
able to find them on his own. While at the dump, Michael confronts
Dwight about how Dwight no longer shows him respect. Dwight admits that
he resents how being under Michael\'s tutelage hasn\'t gone as far as he
thought it would. He believes he would have been in a higher position at
another company had he accepted one of the other job offers he\'d been
given. As a result, he\'s still just a salesman at a company that failed
under a boss that isn\'t going anywhere. \"I hitched my wagon to a horse
with no legs.\" This results in a short trash fight, which ends when
Michael accidentally dumps a sink of garbage water onto himself. The
two, exhausted, rest on a bathtub and resume their friendship after a
short conversation, acknowledging they\'ll never find the leads. Jim
meanwhile convinces the sales-staff to acknowledge their behavior and to
give up 2% of their commissions to appease the rest of the office.

![Andy and Erin kiss.](Andyerinnewleads.jpg "fig:Andy and Erin kiss.")

Jim, Andy, Phyllis, and [Stanley](Stanley "wikilink") present the non-sales
staff with a platter of pastries. The other employees are so delighted
that Jim realizes that\'s all it takes to get their forgiveness. The 2%
commission offer is never made. Michael and Dwight return with a dirty
bean bag chair that Dwight exclaims is, \"For me and Michael only!\",
showing they have become friends again.

At the end of the episode, [Erin](Erin "wikilink") and Andy are at the
dump, and Erin gives Andy her coat after he states he is cold. Andy
tells her that she is the nicest person he has ever met, and they have
their first kiss in the landfill.

Cultural references
-------------------

-   A *[lead](Wikipedia:Sales_lead "wikilink")* is a person who has
    expressed an interest in purchasing a product or service.
-   *[Johnny Depp](Wikipedia:Johnny_Depp "wikilink")* is an actor known
    for roles such as [Jack Sparrow](Wikipedia:Jack_Sparrow "wikilink")
    in the *[Pirates of the
    Caribbean](Wikipedia:Pirates_of_the_Caribbean_(film_series) "wikilink")*
    film series.
-   *[Cap\'n Crunch](Wikipedia:Cap'n_Crunch "wikilink")* is a sweetened
    cereal for children.
-   Jim teases Michael by suggesting that his neighbor\'s name is *M.
    Night Shulman*. [M. Night
    Shyamalan](Wikipedia:M._Night_Shyamalan "wikilink") is a filmmaker
    and screenwriter who grew up and lives in Pennsylvania and films
    nearly all his movies in that state.
-   Michael says that Jim thought he saw [Roger
    Clemens](Wikipedia:Roger_Clemens "wikilink"), a former Major League
    baseball pitcher for the New York Yankees.
-   [*Sexting*](Wikipedia:Sexting "wikilink") is a form of texting in
    which people send sexually explicit photos, messages, etc., using
    electronics.
-   A [Conspiracy](Wikipedia:_Conspiracy "wikilink") is a usually an
    attempt to defraud people of their legal rights, or the overthrowing
    of a government, in some cases.
-   Michael wants to call Stanley *uppity* but reconsiders and applies
    the term to Jim instead. *Uppity* is a racially-tinged term used
    during the Civil Rights era to refer disparagingly to blacks who
    tried to improve their social and economic status.
-   [Kelly](Kelly "wikilink") refers to the *Kardashians,* from the hit
    reality television series, [*Keeping Up with the
    Kardashians*](Wikipedia:Keeping_Up_with_the_Kardashians "wikilink"),
    about a family, which consists of an ex-wife and her daughters.
    Kelly is a devoted fan. Khloe and Kim Kardashian are known for
    dating/marrying African-American men, which is likely why Kelly
    mentioned the show to Darryl.
-   [Baba Ghanoush](Wikipedia:Baba_ghanoush "wikilink") is an Arab dish
    made from eggplants, which are mixed and mashed with several spices.
    It is eaten throughout western Asia, and can be dipped with pita
    bread.
-   A [onesie](Wikipedia:Infant_bodysuit "wikilink") is a garment
    designed to be worn by infants, much like pajamas, and have snaps.
-   [Kevin](Kevin "wikilink") refers to [*Ghost
    Whisperer*](Wikipedia:Ghost_Whisperer "wikilink"), a show about a
    woman who can see dead people and helps to bring closure to families
    who have recently seen death in their families.
-   A [Falafel](Wikipedia:Falafel "wikilink") is a fried ball or patty
    derived from chickpeas, and usually served within a pita. It is from
    Egypt, but is eaten throughout the world.
-   Erin plays the hotter/colder game with Andy. In this children\'s
    game, warmer temperatures indicate that the player is closer to the
    target. (The game was last seen in the episode *[The
    Fight](The_Fight "wikilink")*.)

Goofs
-----

-   On the board in the conference room, it said that
    [Phyllis](Phyllis "wikilink") made the most sales. However,
    [Dwight](Dwight "wikilink") says that he made the most sales, and
    that he is \"King of Kings.\"

Deleted Scenes
--------------

-   [Creed](Creed "wikilink") asks Andy for \$10,000, but Andy declines,
    and offers something else. [Creed](Creed "wikilink") asks for
    Andy\'s hairpiece and \$500, which Andy declines.
-   [Phyllis](Phyllis "wikilink") and [Stanley](Stanley "wikilink")
    agree that in over the 31 years that they worked at [Dunder
    Mifflin](Dunder_Mifflin "wikilink"), they are happy that salesmen
    are being taken seriously and that
    [Sabre](Sabre_(company) "wikilink") recognizes that Salesman is
    King.
-   Andy talks to [Erin](Erin "wikilink") and asks her for leads. Erin
    says she doesn\'t have any.
-   An extended scene/alternate scene of when Andy, Phyllis, Stanley and
    Jim discuss solutions to the issue. Stanley questions Jim\'s
    actions.
-   [Dwight](Dwight "wikilink") and [Darryl](Darryl "wikilink") fight
    over Sabre\'s demands about alerting client on freezing rain, which
    will affect shipping time.

Quotes
------

*For quotes, see: [New Leads Quotes](New_Leads_Quotes "wikilink").*

Amusing Details
---------------

-   When Michael is in the conference room, it shows how many sales each
    salesman made. While [Phyllis](Phyllis "wikilink") has the most
    sales, [Jim](Jim "wikilink"), [Dwight](Dwight "wikilink"),
    [Andy](Andy "wikilink"), and [Stanley](Stanley "wikilink") are also
    doing well in sales. However, [Pam](Pam "wikilink"), is once again
    doing bad in sales, possibly due to her low sales (she had to make
    cold calls) or also because she is on maternity leave, and only had
    three days to make sales.
-   In the meeting in the beginning Michael explains the Lost and Found
    is itself lost and Creed can be seen nervously adjusting glasses
    clearly made for a woman, indicating he in fact stole the Lost and
    Found.
-   As receptionist, [Pam](Pam "wikilink") was always forced to take
    care of Michael, explaining why she knows everything he means,
    including his \"Mopey Place.\"
-   Even Kevin seems repulsed by the idea of seeing Meredith naked.
-   Michael refers to Johnny Depp\'s character as Captain Jack Sparrow
    which is a running joke throughout the movies. Captain Jack Sparrow
    and his friends always insist on including his title when he is
    named.
-   The cold open for this episode appears to have been filmed while Jim
    was still a co-manager.
-   Michael says \"Make friends first, make sales second, make love
    third, in no particular order,\" even though he just put the three
    items in order.
-   When Michael is signing Jim\'s seemingly handsome commission check,
    and Michael comments on how much money it is, Jim says, \"It\'s
    good, you know, with the kid.\" And Michael responds, \"Don\'t
    gloat,\" referring to his longing for children that is never able to
    be fulfilled.

Connections to Previous Episodes
--------------------------------

-   Pam also had bad sales numbers in \"[Koi
    Pond](Koi_Pond "wikilink"),\" when she was forced to make cold calls
    with [Andy](Andy "wikilink"), as the people with the two lowest
    sales in the office had to do them. She also only doubled her sales
    from 2 to 4 in \"[Scott\'s Tots](Scott's_Tots "wikilink").\"
-   Phyllis wears purple so much, she was called \"Mother Goose,\" by
    [Sasha Flenderson](Sasha_Flenderson "wikilink") in \"[Take Your
    Daughter to Work Day](Take_Your_Daughter_to_Work_Day "wikilink").\"
-   Kelly also told [Charles Miner](Charles_Miner "wikilink") that her
    family was much like the Kardashians, showing that she is a huge fan
    of their show. She later tries to persuade [Ryan](Ryan "wikilink")
    to watch it.
-   Phyllis and Andy have apparently stepped up their sales ability as
    in [Employee Transfer](Employee_Transfer "wikilink") Dwight scoffs
    at Andy for \"barely being able to outsell Phyllis\".

Trivia
------

-   The scenes where [Michael](Michael_Scott "wikilink"),
    [Dwight](Dwight "wikilink"), [Andy](Andy "wikilink"), and
    [Erin](Erin "wikilink") are at the dump were actually taken using a
    green screen. Many fans noticed this.
-   Jim says Michael\'s idea for the fourth *Pirates of the Caribbean*
    movie is \"that they should do one.\" *[Pirates of the Caribbean: On
    Stranger
    Tides](wikipedia:Pirates_of_the_Caribbean:_On_Stranger_Tides "wikilink")*
    was the fourth movie in the series.

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Halpert](Pam_Halpert "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")

### Supporting Cast

-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Mindy Kaling](Mindy_Kaling "wikilink") as [Kelly
    Kapoor](Kelly_Kapoor "wikilink")
-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Recurring Cast

-   [Zach Woods](Zach_Woods "wikilink") as [Gabe
    Lewis](Gabe_Lewis "wikilink")
