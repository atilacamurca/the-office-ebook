{{Office episode
|Title       =Body Language
|Image       =Bodylanguage.jpg
|Season      =[[Season 6|6]]
|Episode     =23
|Code        =6023
|Original    =April 29, 2010
|Writer(s)   =[[Justin Spitzer]]
|Director    =[[Mindy Kaling]]
|Prev        =[[Secretary's Day]]
|Next        =[[The Cover-Up]]
}}'''"Body Language"''' is the twenty-third episode of the [[Season 6|sixth season]] and the 123rd episode of ''[[The Office]]'' overall. It was written by [[Justin Spitzer]], who also wrote "[[Shareholder Meeting]]" and "[[Michael Scott Paper Company]]" and directed by [[Mindy Kaling]] who directed "[[The Office: Subtle Sexuality]]". It originally aired on April 29, 2010. It was viewed by 7.01 million people.

==Synopsis==
[[Michael]] is trying to speak in Spanish, because he believes that everyone in the country should speak the language. Michael reveals that he is also going to Cancun. Jim speaks Spanish to Michael, however, Michael can't respond. While [[Dwight]] believes that it is a waste since everyone will be speaking German-Chinese, [[Oscar]] tries to teach Michael the words. Michael uses genitalia to represent whether the word is feminine or masculine.

[[File:Bodylanguage1.jpg|thumb|160px|Michael uses genitalia to represent words.]]
[[Jim]] and [[Pam]] plan their sales pitch, while [[Dwight]] is angry that he isn't getting the client. Pam makes lame jokes. [[Donna]], Jim and Pam's client, and the manager of the bar from "[[Happy Hour]]," comes into the office, much happier than she was before. Michael asks her if she is a hooker, however, she takes it jokingly. Donna believes that Jim and Pam are twins at first, but realizes they are married. As Jim tries to explain to Donna the prices, Michael barges in and offers Donna a Victoria's Secret catalogue.

Meanwhile, [[Gabe Lewis|Gabe]] talks to [[Darryl]] about a minority program. Dwight wants to join the program ("Print in All Colors"), however, only minorities can join. Dwight is angry, because Darryl is competent, so he has a chance at succeeding. Knowing that [[Kelly]] has no chance at succeeding, he goes up to her and asks her to consider the program. Dwight tells Kelly that she could be the CEO. While she'd rather be a superstar, she accepts the offer to be in the minority program.

[[Michael]], continuing to ruin Jim and Pam's sale, shows Donna a slide show. Michael uses pictures of himself every other slide, and the other slides are pictures that "turn her [Donna] on," so that she will associate Michael with being turned on. The last slide reads, "SEX." Donna is acting nice, but Michael takes it too far by trying to kiss her, however, she declines, leaving both of them embarrassed. Jim and Pam, watching, are shocked.

[[Jim]] tells Michael that he shouldn't kiss people at work, while Pam says that if she wasn't interested she would have left by now out of anger. Michael agrees with Pam and tells Jim that he and Pam did a lot more than kiss. Donna says that it wasn't professional, but was flattering. Michael apologizes for ruining their sale, Jim is shocked when Pam lets Michael finish the sale with Jim.

[[File:Bodylanguage2.jpg|thumb|left|180px|Kelly tries to act more "Indian."]]
[[Dwight]] continues to talk to [[Kelly]] about being a minority, as Jim and Michael talk to Donna. Michael tries to get Jim to offer a lower price, despite Jim and [[Oscar]] telling him that the company will lose money. Jim says that Donna could be flirting to get a lower price. [[Pam]] says that Donna likes Michael, while Jim doesn't believe she does. Kelly says that she is going to go in the program and be an executive, and Ryan will become a manager, which will cause them to fire most of the employees. Dwight is shocked and angry.

Michael continues to flirt with Donna, by giving her a mint. However, things turn awkward, when Donna offers Michael a mint, and Michael picks it up with a tongue in her hand. This time, Donna is angry. Michael asks everyone what to do, and everyone says that Michael should end it. Pam, however, says that it is not impossible for her to still like him.

Dwight tries to convince Gabe that Kelly is not applicable, and Darryl is far better at the job, however, Darryl dropped out. Kelly is acting more ethnic so she can prove that she is a true minority. Kelly, however, continues to be unaware about her culture. Donna and Michael say goodbye to each other, and Donna has no hard feelings towards Michael, but Michael continues to awkwardly follow her. Donna tries to be nice, however, Michael keeps following her, but eventually stops. Michael is saddened that Donna left.

[[File:Bodylanguage3.jpg|thumb|192px|Michael confronts Donna.]]
Dwight talks to [[Stanley]] and Oscar about the minority program, and no one wants to do it. [[Erin]] drops in, but Dwight tells her to go away. Kelly says that she likes many of the things that Indian people like. Dwight introduces [[Hide]] to Gabe, and Hide tells him about his life in Japan as a surgeon.

Michael, sad because Donna isn't interested in him, sits on the couch. [[Pam]] tells Michael that she thought Donna was interested. [[Phyllis]] and [[Andy]] try to console Michael, while Erin tells him that Donna forgot her hair clip. Michael wants to return it to her, and everyone tells him not to, but he runs down to see her anyway. [[Donna]] is still in the parking lot, and Michael gives her the hair clip. Michael tells Donna that he thought she was interested in him. Donna says that he was actually right, and they kiss.

Kelly is accepted into the minority training program, and Gabe is happy because "the program is too black," which he immediately regrets saying. Michael tells everyone that Donna and he kissed, however, no one believes him because he didn't give her the hair clip. Michael says that he doesn't care, but tries to convince people that he did, anyway.

==Deleted scenes==
* Attempting to bolster Kelly's chances for the executive training program, Dwight challenges Gabe to name an Indian CEO. Gabe names several, including [[Wikipedia:Indra Nooyi|Indra Nooyi]]. Dwight then challenges him to name one female Indian CEO. Gabe points out that Indra Nooyi is a woman. Dwight then asks him to name a female Indian CEO of a paper company, after which "I'm out of your hair." Gabe names one. Dwight then challenges Gabe to name ''two''.
* In Michael's office, Donna sits at his desk and plays with his toys while mentioning that in her business, she offers free onion rings to her friends. Michael continues to have trouble reading Donna's behavior.
* [[Talking head]] interviews with Michael, Andy, and Stanley on how to tell if someone is interested in you.
* Kelly visits Ryan in his workspace (the closet) and is disgusted that Yale's mascot is a bulldog. (Recall that the executive training program includes two weeks study at Yale Business School.) Ryan reassures her that nobody actually wears the clothes from their school. Andy proves them wrong by showing his undershirt, his socks, and pulls up his underpants, accidentally giving himself a "front wedgie."

==Cultural references==
* ''Sid & Dexter's'' is a fictitious bar (the location of the episode ''[[Happy Hour]]'') based on the real bar [[Wikipedia:Dave & Buster's|Dave &amp; Buster's]].
* ''[[Wikipedia:Victoria's Secret|Victoria's Secret]]'' is a chain of stores selling women's clothing and lingerie. It is known for its provocative catalogs.
* The ''[[Wikipedia:Yale School of Management|Yale School of Management]]'' is the graduate business school of ''[[Wikipedia:Yale University|Yale University]]'', a member of the prestigious [[Wikipedia:Ivy League|Ivy League]].
* ''[[Wikipedia:Cinderella|Cinderella]]'' is a folk tale in which a mistreated girl triumphs over her situation. In common use, it is used to describe a situation in which an unappreciated person meets with sudden success.
* ''Texting'', short for [[Wikipedia:Text messaging|text messaging]], is a form of short-message communication over mobile devices. Although laws prohibiting [[Wikipedia:Texting while driving|texting while driving]] became popular in the United States starting in 2008, there was no law specifically banning texting while driving in Pennsylvania at the time the episode aired.
* ''[[Wikipedia:Bill Gates|Bill Gates]]'' and ''[[Wikipedia:Ted Turner|Ted Turner]]'' are well-known business magnates. ''[[Wikipedia:Julia Roberts|Julia Roberts]]'' is an award-winning actress.
* Michael shows a picture of his ''[[Wikipedia:Shrek (character)|Shrek]]-green eyes''. Shrek is a green ogre, the lead character in a series of animated movies.
* Michael inserts a [[Wikipedia:Subliminal stimuli|subliminal message]] into his presentation by showing very briefly a slide consisting of the word SEX.
* ''[[Wikipedia:M&M's|M&amp;M's candies]]'' are candy-covered chocolates. They are Kevin's favorite candy.
* Pam wishes Jim luck as Michael's ''[[Wikipedia:Wingman (social)|wingman]]'', an informal term for somebody who helps a friend approach potential sexual partners.
* Kelly creates a fake ''IM'' account, short for [[Wikipedia:Instant messaging|instant messaging]].
* ''[[Wikipedia:Altoid|Altoid]]'' is a brand of breath mint, previously seen in ''[[Phyllis' Wedding]]''.
* ''[[Wikipedia:Rudy (film)|Rudy]]'' is a 1993 sports film about Daniel "Rudy" Ruettiger who overcomes a series of setbacks (including being rejected three times) to enroll in the [[Wikipedia:University of Notre Dame|University of Notre Dame]] and play on the football team. The movie is not a comedy.
* Michael asks Donna if she needs ''validation'', short for ''parking validation''. Some parking lots intended for use by customers of a particular business or collection of businesses charge for parking but will waive the fee if the driver demonstrates that they made a qualifying purchase at a participating business.
* Dwight calls Erin ''paleface'', an insulting term for "white people" originally used by Native Americans to describe European settlers.
* Dwight says, "speaking of rainbows" when addressing Oscar. The gay movement adopted the [[Wikipedia:Rainbow flag (LGBT movement)|rainbow flag]] as its symbol.
* Andy mentions tearing his ''scrote'' (short for ''scrotum'') and that the urologist was performing examinations to charge his ''HMO'', short for [[Wikipedia:Health maintenance organization|health maintenance organization]], an organization which manages health insurance.
* The red dot on Kelly's forehead is a [[Wikipedia:Bindi|bindi]], a traditional decoration for women in South Asia. Although it carried meaning in the past, in modern times it is used merely as a fashion accessory.
* The Indian CEOs named by Gabe are [[Wikipedia:Vikram Pandit|Vikram Pandit]] (Citigroup), Surya Mohapatra (Quest Diagnostics), [[Wikipedia:Dinesh Paliwal|Dinesh Paliwal]] (Harman International Industries), and [[Wikipedia:Indra Nooyi|Indra Nooyi]] (PepsiCo). Paper company CEO Nisha Das appears to be fictitious.
* Michael says that 50 years ago, dating was much simpler, citing ''[[Wikipedia:Mad Men|Mad Men]]'', a dramatic television program set at an advertising agency in the 1960s.
* Kelly wishes she were going to ''[[Wikipedia:Harvard University|Harvard]]'' instead of ''[[Wikipedia:Yale University|Yale]]'' so it would be just like ''[[Wikipedia:Legally Blonde|Legally Blonde]]'', a 2001 comedy movie about a sorority girl who attends Harvard Law School. Harvard is also a member of the [[Wikipedia:Ivy League|Ivy League]].

==Amusing details==
* Jim says "Soy fantástico" (I'm fantastic), implying he is not fluent in Spanish, as the correct way to phrase that would be "Estoy fantástico" (I'm feeling/currently fantastic).
* Kelly is rude to the customer service representative on the phone, even though her own job is customer service.
* Dwight wears his cell phone on his belt, a trait Kelly mentioned her dislike for earlier in the episode.
* Dwight mentions that he is a Cholera survivor, a disease that has long not affected developed society.
* Kelly says she cannot name "any CEO, any race", even though the CEO of Sabre [[Jo Bennet]] visited the Scranton branch episodes earlier.

==Connections to previous episodes==
* Dwight pretends to call 911 to report the [[Scranton Strangler]].
* Andy mentions his torn scrotum, an injury he suffered in the episode ''[[Niagara]]''.
* Jim jokes about Pam being a golddigger in ''[[Customer Survey]]''.

==Flashbacks==
* [[Donna]] tells Michael to get off the table. Michael tells Donna that she can not talk to him like that in front of his friends. {{6x19}}

==Cast==
===Main cast===
*[[Steve Carell]] as [[Michael Scott]]
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Halpert]]
*[[B.J. Novak]] as [[Ryan Howard]]
*[[Ed Helms]] as [[Andy Bernard]]

===Supporting cast===
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Mindy Kaling]] as [[Kelly Kapoor]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]

===Recurring cast===
* [[Zach Woods]] as [[Gabe Lewis]]
* [[Amy Pietz]] as [[Donna]]
* [[Hidetoshi Imura]] as [[Hidetoshi Hasagawa]]
