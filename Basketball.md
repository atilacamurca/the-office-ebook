# Basketball

**Basketball** is the fifth episode of the [first
season](Season_1 "wikilink") of \'\'[The
Office](The_Office_(US) "wikilink") \'\'and the 5th overall . It was
written and directed by Greg Daniels. It first aired on April 19, 2005.
It was viewed by 5 million people.

Synopsis
--------

[Michael](Michael_Scott "wikilink") comes in to work prepared to pit the
office staff against the warehouse in a game of basketball, with the
losers having to work on Saturday. Michael picks
[Jim](Jim_Halpert "wikilink"), [Ryan](Ryan_Howard "wikilink"),
[Stanley](Stanley_Hudson "wikilink") (Michael\'s \"secret weapon\"),
and, reluctantly, [Dwight](Dwight_Schrute "wikilink") and
[Phyllis](Phyllis_Lapin "wikilink") (as an alternate).

The game begins and Stanley, despite being black (which is the main
reason that Michael chose him), is a horrible player. Furthermore,
Michael cannot make a shot, nor is he a fan of passing or defending.
During the game, Roy and Jim gradually become aggressive toward each
other, with [Pam](Pam_Beesly "wikilink") looking on. At a crucial point
in the game, Michael is accidentally hit in the face. Michael knew his
team was ahead so he claims it is a \"flagrant personal intentional
foul\". He pettily stops the game and declares the office winners since
they were winning when the foul occurred. The warehouse finds the call
unfair and Michael caves under pressure, conceding the victory to the
warehouse staff. As everybody returns to work,
[Kevin](Kevin_Malone "wikilink") demonstrates his excellent shooting
skills.

Afterward, Michael, in a rare moment of heart, tells the office that
they don\'t have to come in on Saturday either. However, his
justification does little to calm them: \"Like coming in an extra day is
going to prevent us from being downsized.\"

Trivia
------

-   At least two of the scenes in the show\'s opening sequence are from
    this episode: Ryan holding up his bag and Dwight flipping his tie
    over his shoulder.
-   Michael thinks Stanley is his secret weapon due to the fact that he
    believes all African Americans are good at Basketball.
-   Actor [Brian Baumgartner](Brian_Baumgartner "wikilink") actually
    made 14 shots in a row, as revealed on the DVD commentary.
-   Much of the basketball game was improvised. [Greg
    Daniels](Greg_Daniels "wikilink") simply told the cast to play
    basketball, and they kept the best parts.
-   Although it was in the script for Roy to hit Jim in the mouth during
    the basketball game (and they rehearsed it), the scene in the
    episode was entirely accidental.[^1]
-   Michael says he will use Oscar during baseball season, in an
    off-color joke about his Latino heritage. Actually, in the series\'
    second episode (\"Diversity Day\"), there is a shot of Kevin using
    an oversize pencil where a magazine titled \"Baseball\" is seen on
    Oscar\'s neighboring desk.
-   In the beginning of the episode when Dwight is wondering who will
    work on Saturday, Pam says that Roy wanted to do something with her
    on Saturday, but later Pam states that Roy wanted to take the Wave
    Runners to the lake that day.

Deleted scenes
--------------

The Season One DVD contains a number of deleted scenes from this
episode.

-   Michael and Dwight discuss the differences between Assistant
    Regional Manager and Assistant to the Regional Manager.
-   Michael, Darryl and Roy talk about the Philadelphia 76ers.
-   Michael reveals his nickname which he uses while playing basketball.
-   Todd Packer tells Michael a racist joke over the phone, and then
    says he can\'t play the game.
-   Dwight takes Tootsie Rolls from Angela.
-   Kevin tells Dwight he can\'t come in on Saturday because he plays in
    a band.
-   Pam says how Roy get her a WaveRunner, but her brother races it with
    Roy.
-   Dwight shows Jim a new schedule he\'s made.
-   Angela and Dwight argue over who has more authority in safety.
-   Michael talks about basketball movies and compares it to jazz but
    can\'t remember the name of a musician.
-   Michael almost scores a basket.
-   Michael remembers that Kenny G is the musician he was thinking of.
-   Stanley hurts his ankle but Michael says he still has to play, but
    then brings Phyllis on for him.
-   Kevin scores seven consecutive baskets.
-   Michael finally scores and celebrates.

Amusing details
---------------

-   During the early part of the game, after Roy makes an easy lay-up,
    Michael angrily shouts, \"Who\'s got Roy?\" presumably having
    forgotten that *he* is responsible for covering Roy. This could also
    be attributed to the fact that Michael doesn\'t want to be held
    accountable for Roy\'s scoring.

Cultural references
-------------------

-   Michael greets Pam by saying, \"*Pam, Pam, thank you ma\'am*.\" The
    phrase *Wham bam thank you ma\'am* is crude slang referring to a man
    having sex with a woman very quickly and impersonally.
-   A *pick-up game* is a game formed spontaneously in a public place
    (such as a park) among people who do not all know each other.
-   *[Regis Philbin](Wikipedia:Regis_Philbin "wikilink")* is a
    television personality. *[Mr.
    Rogers](Wikipedia:Fred_Rogers "wikilink")* was the host of the
    children\'s television program *[Mister Rogers\'
    Neighborhood](Wikipedia:Mister_Rogers'_Neighborhood "wikilink")*.
-   To *chicken out* is to decline an option due to cowardice, usually
    applied to backing out of a previous agreement.
-   *[Gimli](Wikipedia:Gimli_(Middle-earth) "wikilink")* is a fictional
    character from the fantasy book series *[The Lord of the
    Ring](Wikipedia:The_Lord_of_the_Ring "wikilink")*.
-   *[WaveRunner](Wikipedia:WaveRunner "wikilink")* is a brand of
    jet-ski [personal water
    craft](Wikipedia:personal_water_craft "wikilink").
-   Michael refers to [a muscular woman](Madge "wikilink") as *the East
    German gal*. East Germany (which reunited with West Germany in 1990)
    [dominated international sports
    competitions](Wikipedia:East_Germany#Sports "wikilink") in part due
    to its athletes\' use of steroids.
-   *[Larry Bird](Wikipedia:Larry_Bird "wikilink")* is a former
    professional basketball player, considered by many to be one of the
    best players ever.
-   In a deleted scene, Michael describes the basketball movie *[Like
    Mike](Wikipedia:Like_Mike "wikilink")* but cannot remember its name.
-   Dwight wears a mask that is similar to Richard Hamilton, a retired
    professional basketball player who won a championship with the
    Detroit Pistons.
-   Michael kicks a ball like Nick Nolte in Blue Chips movie from 1990
-   Michael and Dwight shouts \"defence\" it\'s a quote from Remember
    the Titans movie from 2000.

Quotes
------

:   see *[Basketball Quotes](Basketball_Quotes "wikilink")*

Cast
----

### Main Cast

-   [Steve Carell](Steve_Carell "wikilink") as [Michael
    Scott](Michael_Scott "wikilink")
-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Beesly](Pam_Beesly "wikilink")
-   [B.J. Novak](B.J._Novak "wikilink") as [Ryan
    Howard](Ryan_Howard "wikilink")

### Recurring Cast

-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [David Denman](David_Denman "wikilink") as [Roy
    Anderson](Roy_Anderson "wikilink")
-   [Patrice O\'Neal](Patrice_O'Neal "wikilink") as
    [Lonnie](Lonnie "wikilink")
-   [Matt DeCaro](Matt_DeCaro "wikilink") as [Jerry](Jerry "wikilink")
-   [Karly Rothenberg](Karly_Rothenberg "wikilink") as
    [Madge](Madge "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Lapin](Phyllis_Lapin "wikilink")
-   [Melora Hardin](Melora_Hardin "wikilink") as [Jan
    Levenson](Jan_Levenson "wikilink")
-   [Philip Pickard](Philip_Pickard "wikilink") as
    [Philip](Philip "wikilink") (Uncredited)
-   [Creed Bratton (actor)](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink") (Uncredited)
-   [Devon Abner](Devon_Abner "wikilink") as [Devon
    White](Devon_White "wikilink") (Uncredited)

References
----------

<references/>
\

[^1]: [GMMR Exclusive Interview with David
    Denman](http://www.givememyremote.com/remote/?p=1163)
