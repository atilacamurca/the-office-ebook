# Season 01

![](Cast.jpg "fig:"){width="400"}

**Season 1** of *[The
Office](The_Office "wikilink")* began on March 24, 2005 on
[NBC](NBC "wikilink"). It is based on the BBC series created by [Ricky
Gervais](Ricky_Gervais "wikilink") and [Stephen
Merchant](Stephen_Merchant "wikilink"), and developed for American
television by [Greg Daniels](Greg_Daniels "wikilink").

*The Office* on NBC . There are only 6 episodes in Season 1. It aired
from March 24, 2005, to April 26, 2005.

Season 1 was released on [DVD](DVD "wikilink") in North America on
August 16, 2005.

Season overview
---------------

The first season was a mid-season replacement and thus did not include a
full set of episodes. The season had six episodes that began airing from
March 24, 2005 to April 26, 2005. The premiere episode was written near
word for word from the first episode of the British series, though names
and cultural references were changed and some small extra scenes were
added.

The season begins with a documentary crew that arrives at the offices of
Dunder Mifflin to observe the employees and learn about modern
management. The main characters are introduced as well as some minor
characters. Rumors of downsizing begin to spread in the office and the
possible closure of the branch. [Michael
Scott](Michael_Scott "wikilink"), Dunder Mifflin Scranton\'s regional
manager, denies the rumors to boost morale.

In the season finale, [Jim Halpert](Jim_Halpert "wikilink"), a sales
representative, begins dating [Katy](Katy "wikilink"), a purse
saleswoman who visited and set up shop briefly in the office. Jim also
has an interest in the office receptionist [Pam
Beesly](Pam_Beesly "wikilink"), though she is engaged to Roy Anderson,
which expands later in [season 2](season_2 "wikilink") of the show.

Episodes
--------

  Picture                                                                                                                                                                                                                                                                                                                     Episode Title                               Season Episode Number   Production Code   Original Airdate
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------- ----------------------- ----------------- ------------------

  ![](pilot.jpg "pilot.jpg"){width="150"}                                                                                                                                                                                                                                                                                     [Pilot](Pilot "wikilink")                   1                       RP006             March 24, 2005
  An introduction to the staff of the [Scranton, Pennsylvania branch](Dunder_Mifflin_Scranton "wikilink") of the [Dunder Mifflin Paper Company](Dunder_Mifflin_Paper_Company "wikilink").                                                                                                                                                                                                                           

  ![](diversityday.jpg "diversityday.jpg"){width="150"}                                                                                                                                                                                                                                                                       [Diversity Day](Diversity_Day "wikilink")   2                       R1101             March 29, 2005
  After the corporate office discovers that [Michael](Michael "wikilink") has been making insensitive remarks to his co-workers, they send a [sensitivity trainer](Mr._Brown "wikilink") to the Scranton branch, but when Michael doesn\'t agree with everything the trainer says, he sets up his own sensitivity workshop.                                                                                         

  ![](healthcare.jpg "healthcare.jpg"){width="150"}                                                                                                                                                                                                                                                                           [Health Care](Health_Care "wikilink")       3                       R1105             April 5, 2005
  Michael passes off the responsibility of picking a new health care plan to [Dwight](Dwight "wikilink").                                                                                                                                                                                                                                                                                                           

  ![](thealliance.jpg "thealliance.jpg"){width="150"}                                                                                                                                                                                                                                                                         [The Alliance](The_Alliance "wikilink")     4                       R1103             April 12, 2005
  [Jim](Jim "wikilink") jokingly forms an \"alliance\" with Dwight when rumors of downsizing come about.                                                                                                                                                                                                                                                                                                            

  ![](basketball.jpg "basketball.jpg"){width="150"}                                                                                                                                                                                                                                                                           [Basketball](Basketball "wikilink")         5                       R1104             April 19, 2005
  It\'s the office versus the [warehouse](Warehouse "wikilink") in a game of basketball.                                                                                                                                                                                                                                                                                                                            

  ![](hotgirl.jpg "hotgirl.jpg"){width="150"}                                                                                                                                                                                                                                                                                 [Hot Girl](Hot_Girl "wikilink")             6                       R1102             April 26, 2005
  When an attractive handbag [salesgirl](Katy "wikilink") comes to the office, Michael and Dwight compete for her attention.                                                                                                                                                                                                                                                                                        
