'''"A.A.R.M."''' is the twenty-second/twenty-third episode of the ninth season of the American television comedy series ''The Office'' and is the show's 198th/199th episode overall. It is the second to last episode of the series. It aired on May 9, 2013.

As the office readies for the premiere of the documentary that night, Jim convinces [[Dwight Schrute|Dwight]], who is planning to propose to Esther, that he needs an Assistant to the Assistant to the Regional Manager, and holds tryouts to find the best candidate. [[Angela Martin|Angela]] brings her baby to work after her daycare turns her away. Meanwhile, [[Andy Bernard|Andy]] attends auditions for ''America's Next A Cappella Sensation''.

==Synopsis==
[[Jim Halpert|Jim]] convinces [[Dwight Schrute|Dwight]] that he needs to choose someone to act as an Assistant to the Assistant to the Regional Manager (A.A.R.M.); the two subsequently hold tryouts for the position. The tryouts consist of several office-oriented challenges and it is found, through Jim's scheming, that Dwight is the most qualified for the newly founded A.A.R.M. position. [[Angela Martin|Angela]] is forced to bring her son, [[Philip Lipton|Phillip]], to work, after her daycare turns her child away. Dwight, who initially wanted to marry his current girlfriend Esther, begins to think that Angela's son is actually his. He confronts her, but she tells him that this is not the case. Dwight then confers with Jim, who tells Dwight that if he truly loves Angela then that's who he should pursue. Dwight makes up his mind and proposes to Angela, who says yes. Angela finally reveals to Dwight that Phillip is indeed his son.

Meanwhile, [[Darryl Philbin|Darryl]] returns to [[Scranton]], after quietly quitting a week ago to work full-time at [[Athlead]], the sports marketing company that Jim founded. When the others spot him, they are upset that he left without saying goodbye and demand to spend some time with him one way or another before he leaves for good. The office ultimately decides on one final dance together.

When Darryl runs into [[Pam Beesly|Pam]], he tells her about Athlead's success and how Jim is missing out by staying in Scranton. Pam begins to worry that she is making Jim do something that he does not want to do. She later confronts her husband and admits that she thinks she might not be enough for him; he tries to tell her that she is the most important thing in his life. Later, he decides to get the documentary crew's help; the crew makes a DVD of many of the highlights of Jim and Pam's relationship throughout the series. The final scene is from the [[Season 2|second season]] Christmas episode "[[Christmas Party]]" in which Jim gives Pam a teapot as a gift, but takes back his very emotional note that expresses his feelings for Pam before she notices it. After the DVD plays, Jim presents Pam with the actual, unopened note. She reads it, and the two happily embrace.

Meanwhile, [[Andy Bernard|Andy]] auditions for ''America's Next A Cappella Sensation''. Andy meets an exuberant and like-minded lady, Casey Dean from Cincinnati, Ohio. However, before he is able to tryout in front of the judges&mdash;who include [[Wikipedia:Aaron Rodgers|Aaron Rodgers]], [[Wikipedia:Clay Aiken|Clay Aiken]], and [[Wikipedia:Santigold|Santigold]]&mdash;the auditions close, and Andy makes a fool of himself. Dejected, he journeys to [[Poor Richard's]] Pub, where he meets up with the rest of his former office workers. Altogether, the office watches the official airing of the documentary on [[Wikipedia:PBS|PBS]]' Scranton affiliate station, [[Wikipedia:WVIA-TV|WVIA-TV]]. As the office workers stare at the screen, the the opening dialogue from the series' first episode, "[[Pilot]]", begins.

==Deleted scenes==
* In a talking head interview, Angela explains that she drops off Philip at her church daycare. She loves St. Julian's because they accept all people regardless of social class. Even the custodians get to attend some services.
* Angela is turned away by the church daycare because they learned that she is living with a gay man. As she walks away, Angela mutters, "What a b&mdash;!"
* In a talking head interview, Nellie is worried that the documentary may be viewed even by people as far away as China.
* In a talking head interview, Angela blames the documentary for ruining her fairy-tale marriage by exposing her husband as secretly gay.
* The employees discuss how their lives may change as a result of the documentary. Dwight shuts it down because it's chit-chat, which can lead to nonsense.
* Jim messes up Dwight's attempt to announce the final agenda item under the guise of trying to help.
* Extended subplot involving Toby and Nellie.
** Toby awkwardly asks Nellie on a date to watch the documentary at Poor Richard's with everyone else. She turns him down.
** Toby tries a second time. Nellie, frustrated, agrees to go to Poor Richard's with everyone else.
** Toby comes back and breaks up with her before their first non-date. He just wanted to be the one to break up with someone rather than being the victim.
** In a talking head interview, Nellie convinces herself that Toby is using reverse psychology and concludes that he is her last chance at love. "My God, he's good."
** Nellie interrupts Toby (who was looking at an online dating site) and accepts his offer of a date. Toby turns her down because he doesn't want to be anybody's second choice. "I want the fairy tale."
** Nellie returns, holding her phone over her head in the style of the movie ''Say Anything'' and dramatically asks Toby out again. Toby accepts. "That's all I wanted." Nellie gradually realizes what just happens and recoils. "It's remarkable how close we've come to making life-destroying choices." She does suggest, "Buffalo wings on me tonight." Toby accepts it.
==Connections to previous episodes==
* In the last few moments of the episode, you can hear the opening lines from [[Pilot]].
* Dwight is his own assistant during the brief hours when he is Regional Manager in [[The Job]].
* Jim finally decides to give Pam the note he wrote for her in [[Christmas Party]], when he gives her the teapot letter he originally took back from her in the same episode.
* Dwight hangs paintings of himself around the office when he becomes acting manager in [[Dwight K. Schrute, (Acting) Manager]].
* While Philip is crying, Dwight offers to try to calm him down, even saying, "Pam can attest, there's no one better at getting brats to shut their yaps", referencing the episode [[Viewing Party]], when he was able to quiet down [[Cecelia Halpert|Cece]], Jim and Pam's daughter, just by holding her in his arms. 

==Goofs==
*Phillip is seen as a baby two episodes ago yet was acted by a toddler in the episode.
*Angela admits she lied about Phillip not being Dwight's baby. However, Dwight was informed of the opposite by a geneticist in the episode [[New Guys]]. (Greg Daniels explained that they shot but cut a scene in which it is explained that Dwight grabbed the wrong diaper.)
*When Andy is auditioning for the singing competition, he claims he is singing the Cornell fight song ("Give my Regards to Davy"). He is actually singing the Cornell Alma Mater ("Far Above Cayuga's Waters").
*Jim's video misspells Pam's last name as "Beesley" instead of "Beesly".

==Amusing details==
* In the painting hung in the conference room, Dwight is portrayed in the same manner as [[Wikipedia:North Korea|DPRK]] president [[Wikipedia:Kim Il-sung|Kim Il-sung]] in his statue at [[Wikipedia:Mansu Hill Grand Monument|Mansu Hill Grand Monument]] in [[Wikipedia:Pyongyang|Pyongyang]].
* In this episode, Kevin acts like many former only children do when the parents bring home a new baby. Oscar and Angela perform the common practice of giving the child (Kevin) a gift and saying that it's from the baby to make them feel better.
* There is no implication that Dwight leaves work to get a beet, meaning he already had one in the office.

==Behind the scenes==
*The card from the teapot contains a personal message from John Krasinski to Jenna Fischer about how much their time together meant to him.<ref name="officeladies-christmas-party">Kinsey, Angela and Jenna Fischer, [https://officeladies.com/episodes/2020/02/19/episode-16-christmas-party Office Ladies: Christmas Party] podcast.</ref> She was not aware of this beforehand and reads the letter for the first time on camera.
* The church daycare scene (from the deleted scenes) was filmed at the [https://www.fccnh.org/ First Christian Church of North Hollywood] in Studio City.<ref name="officeladies-05:50">Kinsey, Angela and Jenna Fischer. [https://officeladies.com/episodes/2020/09/09/episode-44-phyllis-wedding Episode 44: Phyllis' Wedding with Ken Whittingham]. "Office Ladies" podcast, September 9, 2020. Time code 05:50.</ref>

==Cultural references==
*Dwight's test to see if Phillip is his son—in which Dwight offered the child either a check for "one million dollars", or a [[Wikipedia:Beet|beet]], and saw which one he chose—has been compared to a similar scene in the 1997 film ''[[Wikipedia:Kundun|Kundun]]'', in which Tibetan monks practice a similar ritual.
*The song that plays over the montage of Pam and Jim's DVD is "[[Wikipedia:Open Your Eyes (Snow Patrol song)|Open Your Eyes]]" by [[Wikipedia:Snow Patrol|Snow Patrol]].
*Darryl and the rest of the office dance to the song "[[Wikipedia:Boogie Wonderland|Boogie Wonderland]]" by [[Wikipedia:Earth, Wind, and Fire|Earth, Wind, and Fire]].
*Oscar, during the office dance portion of the episode, seems to do an admittedly shoddy version of "voguing". This is a form of dance derived from the gay ball culture of New York from the 1920s onwards, and more recently, a hit Madonna song.
*The show Andy auditions for ''America's Next A Cappella Sensation'', is a parody of popular singing shows like ''[[Wikipedia:American Idol|American Idol]]'', ''[[Wikipedia:The Voice (U.S. TV series)|The Voice]]'', and ''[[Wikipedia:The Sing-Off|The Sing-Off]]''.
**The fictional show is hosted by [[Wikipedia:Mark McGrath|Mark McGrath]], the lead singer from the rock band [[Wikipedia:Sugar Ray|Sugar Ray]]. The judges are
**[[Wikipedia:Aaron Rodgers|Aaron Rodgers]], an [[Wikipedia:American football|American football]] [[Wikipedia:Quarterback|quarterback]] for the [[Wikipedia:Green Bay Packers|Green Bay Packers]].
**[[Wikipedia:Clay Aiken|Clay Aiken]], a singer who gained prominence following the [[Wikipedia:American Idol (season_2)|second season]] of ''American Idol'' in 2003.
**[[Wikipedia:Santigold|Santigold]], an American singer, songwriter, and producer.
*In the movie ''[[Wikipedia:Say Anything|Say Anything]]'', character Lloyd Dobler plays music from a boom box held over his head in an attempt to win back his former girlfriend Diane Court. Nellie does the same with a phone in a deleted scene.

==Cast==
===Main Cast===
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly|Pam Halpert]]
*[[Ed Helms]] as [[Andy Bernard]]

===Supporting Cast===
*[[Catherine Tate]] as [[Nellie Bertram]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)|Creed Bratton]] as [[Creed Bratton]]
*[[Clark Duke]] as [[Clark Green]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Jake Lacy]] as [[Pete Miller]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]

===Guest Cast===
*[http://en.wikipedia.org/wiki/Clay_Aiken Clay Aiken] as Himself
*[[Hugh Dane]] as [[Hank Tate|Hank]]
*Nora Kirkpatrick as [[Esther]]
*[http://en.wikipedia.org/wiki/Mark_McGrath Mark McGrath] as Himself
*Mark Proksch as [[Nate Nickerson|Nate]]
*[http://en.wikipedia.org/wiki/Aaron_Rodgers Aaron Rodgers] as Himself
*[http://en.wikipedia.org/wiki/Santigold Santigold] as Herself
*[http://en.wikipedia.org/wiki/Jessica_St._Clair Jessica St. Clair] as Casey Dean
*Nico Evers-Swindell as Cpl. Miller
*Ryan Kyler Bailey as Producer
*Rachel Crow as Gabriella
*Scot Robinson as The Bartender
*[[Calvin Tenner]] as [[Glenn]]
*[[Steve Carell]] as [[Michael Scott]] (Stock Footage Voice Only) (uncredited)

==Gallery==
<gallery type="slideshow" widths="350" position="center">
The-Office-AARM 09.jpg|Looking for the "Assistant to the Assistant to the Regional Manager"
The-Office-AARM 01.jpg
The-Office-AARM 02.jpg
The-Office-AARM 03.jpg
The-Office-AARM 04.jpg
The-Office-AARM_05.png
The-Office-AARM 06.jpg
The-Office-AARM 07.jpg
The-Office-AARM 08.jpg|The office watching "The Office"
The-Office-AARM 10.jpg|"Not enough for me? You are everything" - Jim
The-Office-AARM 11.jpg|Dwight proposing to Angela
dwight's hierarchy mobile.png|dwight's hierarchy mobile
dwight's hierarchy mobile2.png|dwight's hierarchy mobile - readable
</gallery>

==References==
<references/>

{{Season9}}
[[Category:Season 9 episodes]]
