'''"Livin' the Dream"''' is the twenty-first episode of the [[Season 9|ninth season]] of ''[[The Office]]'' and is the show's 197th episode overall. It aired on May 2, 2013.

==Synopsis==
Andy attempts to get enough strength to quit his job and pursue his dream as an actor. Dwight receives his black belt in karate from his new sensei (Michael Imperioli). Jim returns to Scranton and spends time with Pam.

==Summary==
Chief Executive Officer [[David Wallace]] talks to the camera crew about him informing [[Andy Bernard|Andy]] that he was on very thin ice after Andy lied to him about being at Dunder Mifflin, but was really on his boat for three months. After Andy buys a top-of-the-line printer "that is good for head shots" and asks David to pay for him to have cheek implants, David informs the camera crew that, "At a certain point, the ice gets too thin, and you fall through. And that is when you get fired." Andy attempts to get enough strength to quit his job and pursue his dream as an actor and talks to David about it in his office before he even has the chance to fire Andy. He changes his mind twice, but eventually he makes up his mind and leaves the office. As a goodbye, Andy serenades the rest of the office employees, who initially dismissed Andy's dream as a giant mistake, with a beautiful and moving rendition of [[wikipedia:Sarah McLachlan|Sarah McLachlan's]] song "[[wikipedia:I Will Remember You (Sarah McLachlan song)|I Will Remember You]]". He also defecates on the hood of David Wallace's new car.

Meanwhile, [[Dwight Schrute|Dwight]] receives his [[wikipedia:Black belt (martial arts)|black belt]] in karate from his new sensei. Seeing Dwight's tenacity and devotion, David is inspired to promote Dwight to the regional manager. [[Jim Halpert|Jim]] returns to Scranton and spends time with [[Pam Beesly|Pam]]. In a talking head Jim says they had nice days together then Pam says, “nice mornin, too.“ Implying  they had sex that morning. Dwight then appoints Jim the new assistant to the regional manager. [[Darryl Philbin|Darryl]] informs Jim that [[Athlead]] has found a buyer and they are set to travel around the country for three months. Jim tells Darryl he cannot leave Pam for an extended period of time. Pam is seen overhearing their conversation in the bathroom.

[[Angela Martin|Angela]] is evicted from her studio apartment after her cats have been taken away by Animal Control. She considers living in a tent in the woods, prompting [[Oscar Martinez|Oscar]] to offer for her to stay with him. At the end of the episode, Angela breaks down into tears informing Oscar that she still loves Dwight.

==Cultural references==
*Stanley compares Andy to [[wikipedia:Romeo Miller|Lil' Romeo]].

==Connections to previous episodes==
*During their joint talking head, Pam reveals unnecessary information about their sex life. Jim does the same thing in "[[PDA]]."
*The ant farm seen in Dwight's new office is the ant farm seen in "[[Todd Packer (episode)]]."
*When David Wallace takes a phone call right as he's about to instate Dwight as manager, David can be heard exclaiming, "Then we'll get him a new set of drums!" while presumably on the phone with his wife. In "[[Sabre]]," David's son plays the drums loudly in the background while Michael is at the Wallace's house.

==Amusing details/Trivia==
*The official ''Office'' Darryl Philbin "GetGlue" sticker was released alongside this episode.
*As of this episode, all five of the new employees transferred from the Stamford branch have left the Scranton branch for good:
**[[Tony Gardner]] announced he was quitting, prompting [[Michael Scott|Michael]] to fire him instead in "[[The Merger]]."
**[[Martin Nash]] quit in the episode "[[The Convict]]" after his colleagues learned of his past and [[Michael Scott|Michael]] made a big deal out of it.
**[[Hannah Smoterich-Barr]] filed a complaint against the company and left (off screen) between the episodes "[[A Benihana Christmas]]" and "[[Back From Vacation]]."
**[[Karen Filippelli]] transferred to the Utica branch between the episodes "[[The Job]]" and "[[Fun Run]]" (as seen in a flashback in the Fun Run episode) after breaking up with [[Jim Halpert|Jim]].
**And finally, after having been sent to anger management between "[[The Return]]" and "[[The Negotiation]]," having been fired in "[[Angry Andy]]" and hired back in "[[Free Family Portrait Studio]]," and being AWOL between "[[The Boat]]" and "[[Couples Discount]]," [[Andy Bernard]] quits in this episode to pursue an acting career.
*When Jim is trying to get his old desk back and Clark replies, "Well, I'm here to sell paper," Dwight can be heard offscreen quietly saying, "Burn."

==Cast==
===Main Cast===
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly|Pam Halpert]]
*[[Ed Helms]] as [[Andy Bernard]]

===Supporting Cast===
*[[Catherine Tate]] as [[Nellie Bertram]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)|Creed Bratton]] as [[Creed Bratton]]
*[[Clark Duke]] as [[Clark Green]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin]]
*[[Jake Lacy]] as [[Pete Miller]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]

===Guest Cast===
*[[Andy Buckley]] as [[David Wallace]]
*[[wikipedia:Michael Imperioli|Michael Imperioli]] as Sensei Billy
*Nora Kirkpatrick as Esther


==Gallery==
<gallery type="slideshow" position="center" widths="350">
Livin'_the_Dream_01.jpg
Livin'_the_Dream_02.jpg|Dwight got his black belt
Livin'_the_Dream_03.jpg
Livin'_the_Dream_04.jpg
Livin'_the_Dream_05.jpg
Livin'_the_Dream_06.jpg|Angela is passing through a rough patch
Livin'_the_Dream_07.jpg|Pam and Jim reconnecting
Livin'_the_Dream_08.jpg|Dwight Schrute is the new office manager!
Livin'_the_Dream_09.jpg|Andy makes a choice about his career
Livin'_the_Dream_10.jpg|Andy sings his farewell to the office
Livin'_the_Dream_11.jpg
Livin'_the_Dream_12.jpg|Andy takes a dump on David's car
Livin'_the_Dream_13.jpg
Livin'_the_Dream_14.jpg|Darryl has an offer for Jim
</gallery>

{{Season9}}
[[Category:Season 9 episodes]]
