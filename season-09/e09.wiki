'''"Dwight Christmas"''' is the ninth episode of the ninth season of the American television comedy series ''The Office'' and the 185th overall. It was written by Robert Padnick and directed by Charles McDougall. It aired on December 6, 2012. It was viewed by 4.16 million people.

When the party planning committee drops the ball on the annual Christmas party, Dwight gets everyone to celebrate with a traditional Schrute Pennsylvania Dutch Christmas. Darryl fears Jim has forgotten his promise to take him along to the new job in Philadelphia. Meanwhile, Pete teaches Erin about his favorite movie ''Die Hard''.

==Plot==
The party planning committee drops the ball on the annual Christmas party, and at the behest of [[Jim Halpert|Jim]], [[Dwight Schrute|Dwight]] gets everyone to celebrate with a traditional Schrute Pennsylvania Dutch Christmas. He dresses up as the traditional winter Christmas gift-bringer figure Belsnickel, cooks Dutch food, and plays a game called "Impish or Admirable". Jim, however, has to leave the party early to arrive in Philadelphia for his sports marketing job; this causes Dwight to be visibly saddened. Dwight ends the festivities, and a "normal" Christmas party is started. Although [[Pam Beesly|Pam]] tries to cheer him up, only Jim's sudden return makes him feel better. During the party, [[Darryl Philbin|Darryl]] fears that Jim has forgotten to include him in the new job in Philadelphia. As such, he gets extremely drunk. When he goes to confront Jim, Jim—unknowing that Darryl was upset—excitedly tells him that he has arranged an interview for him. Darryl, appeased, turns around but passes out and crashes down on the catering table.

Meanwhile, [[Pete Miller|Pete]] teaches [[Erin Hannon|Erin]] about his favorite movie, ''Die Hard''. Pete displays serious devotion to the film by quoting a big majority of it, much to Erin's delight. [[Andy Bernard|Andy]], who is still away in the Caribbean after sailing his family's boat, emails Erin and says that he thinks he is going to stay for a few weeks. Hurt, Erin decides to watch ''Die Hard'' with Pete rather than just hear him quote it. While watching the movie, Erin begins to cry and Pete tries to comfort her by placing his arm around her. Although Erin notes that she is still dating Andy, she tells Pete that he can continue putting his arm around her. In the meantime, [[Toby Flenderson|Toby]] tells [[Nellie Bertram|Nellie]] about the [[Scranton Strangler]] case he was on. After boring her for what is implied to be several hours, she hushes him, before gently kissing him. Toby then whips off his glasses and kisses her back.

==Cultural references==
*Dwight, who dresses up as [[wikipedia:Belsnickel|Belsnickel]], has one of the warehouse workers, Nate, dress up as Belsnickel's assistant, [[wikipedia:Zwarte Piet|Zwarte Piet]], before the office vetoes the idea, feeling that it is racist.
*Dwight presents the camera with a picture of his brother and him celebrating Christmas, and a version of the same photo decorated in the style of the 1999 science fiction film ''[[wikipedia:The Matrix|The Matrix]]''.
*Jim intentionally mispronounces one of the German character's names, calling him "[[wikipedia:Hogwarts#Hufflepuff|Hufflepuff]]", one of the four houses of the magical school [[wikipedia:Hogwarts|Hogwarts]] from the ''[[wikipedia:Harry Potter|Harry Potter]]'' book series.
*Creed mentions that, for this year's party, he wants a "Tapas Swiss Miss"-themed party, which is a combination of Spanish [[wikipedia:Tapas|tapas]] and [[wikipedia:Swiss Miss|Swiss Miss]] hot cocoa.
*Erin confuses the main character [[wikipedia:John McClane|John McClane]] from the movie ''[[wikipedia:Die Hard|Die Hard]]'' for former US presidential candidate and Arizona Senator [[wikipedia:John McCain|John McCain]].
*Andy, via email, tells Erin that he saw the 2012 movie ''[[wikipedia:Life of Pi (film)|Life of Pi]]'', an adaptation of the book of the same name. He also notes that he wants to see the first part of Peter Jackson's 2012 film ''[[wikipedia:The Hobbit: An Unexpected Journey|The Hobbit: An Unexpected Journey]]''.
*Phyllis compares the party planning situation with the classic Christmas movie ''[[wikipedia:It's a Wonderful Life|It's a Wonderful Life]]''.
*A drunken Darryl starts seeing the resemblance between Meredith and actress [[wikipedia:Emma Stone|Emma Stone]], who's known for her red hair.

==Connections to previous episodes==
*Jim offered Darryl a job in his sports marketing company in [[Andy's Ancestry]].
*The taxi that takes Jim to Philadelphia is the same one that took [[Michael Scott]] to the airport in [[Goodbye, Michael]].
*It is revealed that Dwight now takes Dumatril, the anxiety pill that Nellie takes in [[Here Comes Treble (Episode)|Here Comes Treble]].

==Amusing details/Trivia==
*This is the second episode to not feature Andy or Clark. Andy, portrayed by Ed Helms, left the show temporarily in the season's sixth episode, [[The Boat]], in order to film ''The Hangover Part III'', whereas Clark, portrayed by Clark Duke, left for a few episodes to film ''Kick-Ass 2''.
*The [[Toby Flenderson]] "Get Glue" sticker was released alongside the episode.
*This is the only episode in the series to not include an Opening Sequence.
*Dwight hangs his bugle on the spot where Nellie's portrait used to hang.
*On June 26, 2020 Netflix removed the scene where Nate appears in black face.

==Cast==
===Main Cast===
*[[Rainn Wilson]] as [[Dwight Schrute]]
*[[John Krasinski]] as [[Jim Halpert]]
*[[Jenna Fischer]] as [[Pam Beesly|Pam Halpert]]
*[[Ed Helms]] as [[Andy Bernard]]

===Supporting Cast===
*[[Catherine Tate]] as [[Nellie Bertram]]
*[[Leslie David Baker]] as [[Stanley Hudson]]
*[[Brian Baumgartner]] as [[Kevin Malone]]
*[[Creed Bratton (actor)|Creed Bratton]] as [[Creed Bratton]]
*[[Kate Flannery]] as [[Meredith Palmer]]
*[[Ellie Kemper]] as [[Erin Hannon]]
*[[Angela Kinsey]] as [[Angela Martin|Angela Lipton]]
*[[Jake Lacy]] as [[Pete Miller]]
*[[Paul Lieberstein]] as [[Toby Flenderson]]
*[[Oscar Nunez]] as [[Oscar Martinez]]
*[[Craig Robinson]] as [[Darryl Philbin]]
*[[Phyllis Smith]] as [[Phyllis Vance]]

===Recurring Cast===
*Mark Proksch as [[Nate Nickerson]]
*[[Bobby Ray Shafer]] as [[Bob Vance]]

==Gallery==
<gallery type="slideshow" widths="350" position="center">
The-office-396.jpg
The-office-397.jpg
The-office-398.jpg
The-office-399.jpg
The-office-400.jpg
The-office-401.jpg
The-office-402.jpg
The-office-403.jpg
</gallery>

{{Season9}}


[[Category:Christmas Specials]]
[[Category:Season 9 episodes]]
