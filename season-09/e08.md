# The Target

![](The-office-the-target_0757.jpg "The-office-the-target_0757.jpg")

**\"The Target\"** is the eighth episode of the [ninth
season](Season_9 "wikilink") of the *[The
Office](The_Office "wikilink")* and the show\'s 184th overall. It was
written by Graham Wagner and directed by Brent Forrester. It aired on
November 29, 2012 and was viewed by 3.88 million people.

In the episode, Angela discovers her husband\'s affair with Oscar and
goes to Dwight for help. Jim needs a favor from Stanley and Phyllis who
milk it for all it\'s worth. Pam begins painting her mural in the
warehouse but gets distracted by Pete who is building a tower of
complaint cards.

Synopsis
--------

Oscar is extremely nervous as he is afraid that Angela found out that he
is having an affair with the Senator. Angela comes in and asks if she
can ask him a question. Oscar is prepared for her to ask about the
Senator; however, she asks if it\'s cold in here. Oscar then reveals he
is excited about Angela not knowing and that if she couldn\'t figure out
that her husband is gay then she probably couldn\'t figure it out now.
However, Angela is seen in the background watching Oscar with a face of
intense hatred.

Pete is very bored with his assignment of filing customer service
complaint cards as he knows it\'s a busy work assignment since the
complaints are already filed on the computer. He instead spends his day
building a house of cards with them and he draws the interest of Kevin
and Darryl.

Jim is extremely nervous as today is the day that he asks David Wallace
if he can work part-time so he can work on his new startup company in
Philadelphia. Pam meanwhile is finally going to start working on the
mural in the warehouse she has been planning for a while.

Angela tells Dwight in the break room that she wants to meet him in
their old spot and that she \"needs\" him. When Angela goes down to the
warehouse she finds a naked Dwight waiting for her having misinterpreted
her message. Angela tells Dwight, as he puts his clothes back on, that
she needed him for \"investigative\" purposes.

Jim becomes disappointed when David Wallace is not thrilled about the
idea of an employee becoming part-time and tells him he wants someone to
be there in person for the clients. Jim then decides to tell David that
Stanley and Phyllis have agreed to cover for him even though they
haven\'t; however, this idea convinces David and he gives Jim
permission.

Meanwhile, Pete and many other office staff are attempting to build a
house of cards using the customer cards to reach the ceiling. They read
each complaint as they do so. Every single office member has had a
complaint filed against them except for Pam for which they make fun of
her for. Kevin tries to put the next card on the tower and it falls over
causing everyone to yell at him. Pete intervenes and gives Kevin a pep
talk and tells everyone that mistakes are what this tower is all about
and that everyone can fail. This speech causes Erin to give Pete a very
approving look.

Dwight pulls Angela into a van and introduces him to a man inside,
[Trevor](Trevor "wikilink"), a former volunteer sheriff. It is revealed
that Angela is looking for a hitman. She tells Trevor that she wants her
husband\'s lover murdered, causing Dwight to be stunned.

Meanwhile, Pam is having trouble getting started on her mural.

Jim takes Stanley and Phyllis out to lunch in order to butter them up
and tries to persuade them to cover for him, but Stanley and Phyllis
take advantage of him and order a lot of expensive food and get very
drunk.

Dwight tries to talk Angela out of her plan to murder whoever is having
an affair with the Senator, but Angela insists that this is what she
plans to do. Dwight eventually agrees to Trevor\'s idea of a
[kneecapping](Wikipedia:kneecapping "wikilink") the target with a lead
pipe instead of murder. Dwight is even more surprised when he discovers
that the Senator is gay and that Oscar is the one he is having an affair
with.

Dwight arrives at work and his horrified when he sees Trevor\'s van in
the parking lot. Wanting to save Oscar, Dwight runs in and gets a very
confused Oscar away from the office and into the warehouse. Trevor
enters the office posing as a sandwich delivery man and asking for Oscar
Martinez. Kevin pretends to be Oscar, as he wants the sandwich, but
Angela saves him at the last minute. Dwight explains that he and Angela
hired a man to break Oscar\'s knees to which Oscar becomes furious. They
run outside and Trevor is waiting for him with a lead pipe. Dwight tries
to prevent Trevor from attacking, but Trevor admits that for once he is
going to follow through with something. Oscar wrestles away the pipe and
the now defenseless Trevor retreats. Angela appears and she and Oscar
finally have a talk, albeit yelling. Oscar apologizes and offers Angela
the pipe and tells her to hit him if she wishes. Angela doesn\'t and
instead simply kicks Oscar and walks away. Dwight follows her and
comforts her on a bench outside the warehouse.

The staff in the annex discover that they are one card short of reaching
the ceiling for their card house. Pam finally decides she is done being
so nice and sweet and decides to call up a customer and insult them in
order to get that final customer complaint card. She calls one of her
clients and makes a \"Yo Mama So Fat\" joke. The client not only
complains but also decides to drop Dunder Mifflin as their paper
supplier as it turns out the boss\'s obese mother recently died. Pam
however finally came out of her shell and they get the final card needed
for the tower to reach the ceiling. Pam is inspired by all their
teamwork and togetherness and decides to use this as an inspiration in
her mural.

Jim arrives back at the office with a sleeping Stanley and Phyllis.
Phyllis reveals that they will cover for Jim and that she and Stanley
were giving him a rough time on purpose just to mess with him. Jim is
very happy and hugs them both.

Dwight and Angela end up in Toby\'s office where they ask several
disturbing and uneducated questions about how homosexual men have sex
causing Toby to appear bewildered and uncomfortable.

Goofs
-----

-   In the season eight episode [Christmas
    Wishes](Christmas_Wishes "wikilink"), [Jim](Jim_Halpert "wikilink")
    mentions he gave [Stanley](Stanley_Hudson "wikilink") some [Lewis
    Black](wikipedia:Lewis_Black "wikilink") tickets for his birthday.
    In this episode however, upon being asked when Stanley\'s birthday
    is, Jim doesn\'t remember.

Cultural references {#cultural_references}
-------------------

-   Dwight assures Angela that she is not stupid, rather
    \"[Jazz](wikipedia:Jazz "wikilink") is stupid.\"
-   Angela concurs, saying: \"[Just play the right
    notes](wikipedia:Improvisation#Music "wikilink")!\"
-   The episode featured several
    [meta-references](wikipedia:Meta-reference "wikilink"). Dwight tells
    Angela and [Trevor](Trevor "wikilink") that \"this documentary crew
    has been following our every move for the past nine years but I
    don\'t see them so I think we\'re good,\" when they are in Trevor\'s
    van.

Connections to previous episodes {#connections_to_previous_episodes}
--------------------------------

-   Pam wears a Pratt Institute sweater. She attended classes there in
    early season five.
-   Angela and Dwight question Toby about the tendencies of homosexuals
    in a way similar to how Dwight questions Toby about the appearance
    of female genitalia in the second-season episode [Sexual
    Harassment](Sexual_Harassment "wikilink").

```{=html}
<!-- -->
```
-   Dwight tells Angela \"Come on in the water\'s fine\" while naked in
    the storage locker. Ryan had previously used this same line on Erin
    as an invite to join him in bed in Season 8, Episode 5
    \"Tallahasee.\"

Amusing details/Trivia {#amusing_detailstrivia}
----------------------

-   [Ed Helms](Ed_Helms "wikilink") and [Clark
    Duke](Clark_Duke "wikilink") did not appear in the episode. This is
    due to the fact that they were both written out of several episodes
    of the season in order to film *[The Hangover Part
    III](wikipedia:The_Hangover_Part_III "wikilink")* and *[Kick-Ass
    2](wikipedia:Kick-Ass_2_(film) "wikilink")*, respectively.
-   The [Oscar Martinez](Oscar_Martinez "wikilink") \"Get Glue\" sticker
    was released alongside with the episode.
-   When the characters are discussing various offenses on the complaint
    cards they turn to Creed who says \"let\'s see what I did.\" He is
    holding one of the tiny coffee cups that Michael used at Cafe Disco.
-   Creed is also wearing sweatpants with his suit.

Cast
----

### Main Cast {#main_cast}

-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Halpert](Pam_Beesly "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")

### Supporting Cast {#supporting_cast}

-   [Catherine Tate](Catherine_Tate "wikilink") as [Nellie
    Bertram](Nellie_Bertram "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Lipton](Angela_Martin "wikilink")
-   [Jake Lacy](Jake_Lacy "wikilink") as [Pete
    Miller](Pete_Miller "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Guest Cast {#guest_cast}

-   [Chris Gethard](Chris_Gethard "wikilink") as
    [Trevor](Trevor "wikilink")
-   Noah Blake as Waiter
-   [Hidetoshi Imura](Hidetoshi_Imura "wikilink") as
    [Hide](Hide "wikilink")
-   [Andy Buckley](Andy_Buckley "wikilink") as [David
    Wallace](David_Wallace "wikilink") (Voice only) (uncredited)

Gallery
-------

Office_s9e8_jimatreception.jpg The-office-391.jpg The-office-392.jpg
The-office-393.jpg The-office-394.jpg

```{=mediawiki}
{{Season9}}
```
[Category:Season 9 episodes](Category:Season_9_episodes "wikilink")
