# Moving On

![](9x16movingon.jpg "9x16movingon.jpg")

**\"Moving On\"** is the sixteenth episode of the ninth season of the
American television comedy series *The Office* and is the show\'s 192nd
episode overall. It was written by Graham Wagner and directed by Jon
Favreau. It aired on February 14, 2013 and was viewed by 4.06 million
people.

In the episode, Pam interviews for a job in Philadelphia with a manager
who gives her deja vu. Dwight brings Angela with him on a mission to
clean his elderly Aunt Shirley. Andy learns that Pete and Erin are
dating.

Synopsis
--------

[Andy](Andy_Bernard "wikilink") learns that
[Pete](Pete_Miller "wikilink") and [Erin](Erin_Hannon "wikilink") are
dating each other, which they\'ve been holding in secrecy from him.
After attempting to fire Pete, Andy is lectured by both Erin and Pete to
move on. Seeking revenge, Andy hires both Alice, Pete\'s ex-girlfriend
and [Gabe Lewis](Gabe_Lewis "wikilink"), who is still attracted to Erin,
in order to make Pete and Erin feel uncomfortable. The plan inevitably
works as the new couple\'s past relationship issues surface with Gabe
making several uncomfortable remarks and Alice bringing up Pete\'s
immature past, prompting arguments to break out in the conference room.
Andy smugly tells the camera that seeing Erin and Pete unhappy makes him
feel better.

[Pam](Pam_Beesly "wikilink") interviews for a job in Philadelphia, where
she is amused to find her potential manager, Mark, reminiscent of her
former [Dunder Mifflin](Dunder_Mifflin_Scranton "wikilink") regional
manager [Michael Scott](Michael_Scott "wikilink"). When Pam realizes
that she is simply interviewing for a glorified receptionist\'s job, she
turns down the offer. Pam later tells [Jim](Jim_Halpert "wikilink"),
during a romantic dinner, that she does not really want to move to
Philadelphia, despite Jim having started his own business. Pam\'s
admission leaves their conversation in awkward silence.

[Dwight](Dwight_Schrute "wikilink") acquires the help of
[Angela](Angela_Martin "wikilink") in order to give his elderly Aunt
Shirley a bath. Dwight initially wants to merely take his aunt out and
spray her with a hose, but Angela talks him into letting her do it in a
more dignified manner. Dwight agrees, and through the process, the two
begin to remember their feelings for each other. At the end of the day,
Angela and Dwight begin kissing, but Angela reminds Dwight that she is
married. Dwight dejectedly tells her that, while he still loves her, she
should be faithful to her husband.

In the episode\'s other subplots, [Toby](Toby_Flenderson "wikilink")
goes to the local prison to talk to the person accused of being the
notorious local murderer, the [Scranton
Strangler](Scranton_Strangler "wikilink"); Toby had been discussing the
details of the case for some time, forcing
[Nellie](Nellie_Bertram "wikilink") to tell him to do something about
it, or to stop talking. Toby believes he may be innocent, but he is
proven wrong when he is nearly strangled to death in prison. However, he
does have his spirits lifted after Nellie gives him a lift from the
hospital and commends him for his bravery. During the last scene of the
show, [Oscar](Oscar_Martinez "wikilink") queues up a television show on
his computer, and while the ad loads, he tries to do gravity boot
sit-ups. While he is struggling to get down, the camera pans over to
Oscar\'s computer and zooms in on an upcoming television guide on his
screen that reveals that the in-series documentary---called The Office:
An American Workplace---will air in May.

Goofs
-----

-   In her résumé, Pam states that her only experience with higher
    education was at Pratt, but she mentions going to college in [Boys
    and Girls](Boys_and_Girls "wikilink") and [Company
    Picnic](Company_Picnic "wikilink").
-   Pam\'s résumé states that she started at Dunder Mifflin in August
    2003, creating yet more confusion as to whether Jim or Pam was there
    first.
-   In the interview with the new boss he says she has been at Dunder
    Mifflin for 8 years and later she says she has been Michael\'s
    receptionist for 10.
-   In the scene where David Wallace is confronting Andy about being
    gone for three months, the subtitles say, "You pretended to be in
    this office for three months, and you were on a \[bleep\] sailboat,"
    when in actuality, David says, "You pretended to be in this office
    for three months, and you were sailing on your boat!"
-   Dwight offers Angela the 'stink sack' from the skunk he and his aunt
    are eating for dinner. Angela seems pleased asking, "Is it any
    good?" This is odd due to the fact that Angela has stated several
    times throughout the series that she is a vegetarian.
-   When Andy calls Pete into his office to find out if he has been
    dating Erin, he mispronounces "chlamydia." In [Sex
    Ed](Sex_Ed "wikilink"), Andy teaches a sex ed lesson to the office,
    so this is odd as he would presumably know how to pronounce it
    correctly. Also, in one episode he used the word \"lachrymose\",
    which shows that his vocabulary is not weak, unlike Michael.

Connections to previous episodes {#connections_to_previous_episodes}
--------------------------------

-   When Erin and Alice first meet she tells Erin that she loves her
    sweater, similar to when Pam and Karen Filippelli met in Season 3\'s
    \"[The Merger](The_Merger "wikilink")\".
-   Pam\'s boss at her possible new job is reminiscent to her of her
    former boss, [Michael Scott](Michael_Scott "wikilink").

Amusing details/Trivia {#amusing_detailstrivia}
----------------------

-   The [Darryl Philbin](Darryl_Philbin "wikilink") \"Get Glue\" sticker
    was released alongside this episode.
-   Andy states that \"when people say office relationships are a good
    idea they never talk about what might happen if you break up\" even
    though he has been in an office relationship with Angela and broken
    up.
-   Pete tells Andy there was no overlap with Erin, but really there was
    one kiss of overlap.
-   Bob Odenkirk, who plays Mark, was actually considered to play
    Michael Scott because Steve Carell was unavailable at the time.
    Later on, Carell became available and was cast over Odenkirk.
-   Pam appears to have no problem in claiming that she majored in Fine
    Arts at Pratt even though she only took a summer course on graphic
    design and failed, yet she doesn\'t mention her time at the Michael
    Scott Paper Company or her time as a sales representative at Dunder
    Mifflin, despite complaining that her résumé is too small.
-   Mark, played by Bob Odenkirk, asks Pam if she likes the show *Curb
    Your Enthusiasm*, before saying that he \"prefers scripted\". Bob
    Odenkirk appeared as Gil in the season one episode of *Curb Your
    Enthusiasm*, \"Porno Gil\".
-   The only episode of Season 9 to feature Zach Woods as Gabe Lewis.

Cast
----

### Main Cast {#main_cast}

-   [Rainn Wilson](Rainn_Wilson "wikilink") as [Dwight
    Schrute](Dwight_Schrute "wikilink")
-   [John Krasinski](John_Krasinski "wikilink") as [Jim
    Halpert](Jim_Halpert "wikilink")
-   [Jenna Fischer](Jenna_Fischer "wikilink") as [Pam
    Halpert](Pam_Beesly "wikilink")
-   [Ed Helms](Ed_Helms "wikilink") as [Andy
    Bernard](Andy_Bernard "wikilink")

### Supporting Cast {#supporting_cast}

-   [Catherine Tate](Catherine_Tate "wikilink") as [Nellie
    Bertram](Nellie_Bertram "wikilink")
-   [Leslie David Baker](Leslie_David_Baker "wikilink") as [Stanley
    Hudson](Stanley_Hudson "wikilink")
-   [Brian Baumgartner](Brian_Baumgartner "wikilink") as [Kevin
    Malone](Kevin_Malone "wikilink")
-   [Creed Bratton](Creed_Bratton_(actor) "wikilink") as [Creed
    Bratton](Creed_Bratton "wikilink")
-   [Clark Duke](Clark_Duke "wikilink") as [Clark
    Green](Clark_Green "wikilink")
-   [Kate Flannery](Kate_Flannery "wikilink") as [Meredith
    Palmer](Meredith_Palmer "wikilink")
-   [Ellie Kemper](Ellie_Kemper "wikilink") as [Erin
    Hannon](Erin_Hannon "wikilink")
-   [Angela Kinsey](Angela_Kinsey "wikilink") as [Angela
    Martin](Angela_Martin "wikilink")
-   [Jake Lacy](Jake_Lacy "wikilink") as [Pete
    Miller](Pete_Miller "wikilink")
-   [Paul Lieberstein](Paul_Lieberstein "wikilink") as [Toby
    Flenderson](Toby_Flenderson "wikilink")
-   [Oscar Nunez](Oscar_Nunez "wikilink") as [Oscar
    Martinez](Oscar_Martinez "wikilink")
-   [Craig Robinson](Craig_Robinson "wikilink") as [Darryl
    Philbin](Darryl_Philbin "wikilink")
-   [Phyllis Smith](Phyllis_Smith "wikilink") as [Phyllis
    Vance](Phyllis_Vance "wikilink")

### Guest Cast {#guest_cast}

-   [Andy Buckley](Andy_Buckley "wikilink") as [David
    Wallace](David_Wallace "wikilink")
-   Bob Odenkirk as Mark
-   [Zach Woods](Zach_Woods "wikilink") as [Gabe
    Lewis](Gabe_Lewis "wikilink")
-   Mary Gillis as Aunt Shirley
-   Michael Weston as Roger
-   Collette Wolfe as Alice
-   Adam Lustick as Athlead Employee
-   Ben Silverman as Isaac

Gallery
-------

The-office-430.jpg The-office-431.jpg The-office-432.jpg
Office_s9odenkirk.png The-office-us-s09e16-moving-on-1.jpg

```{=mediawiki}
{{Season9}}
```
[Category:Season 9 episodes](Category:Season_9_episodes "wikilink")
